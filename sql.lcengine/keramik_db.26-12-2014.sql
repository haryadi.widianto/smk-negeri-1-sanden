-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 26, 2014 at 02:51 PM
-- Server version: 5.5.38
-- PHP Version: 5.5.15-1+deb.sury.org~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `keramik_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `history_crud`
--

CREATE TABLE IF NOT EXISTS `history_crud` (
  `id` bigint(20) NOT NULL,
  `table` varchar(255) NOT NULL,
  `table_id` bigint(20) NOT NULL,
  `status` enum('create','update','delete') NOT NULL,
  `date` datetime NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `history_update`
--

CREATE TABLE IF NOT EXISTS `history_update` (
  `id` bigint(20) NOT NULL,
  `id_crud_history` bigint(20) NOT NULL,
  `field` varchar(255) NOT NULL,
  `last_value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kr_chart`
--

CREATE TABLE IF NOT EXISTS `kr_chart` (
  `id_product` int(11) NOT NULL,
  `id_session` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kr_chart`
--

INSERT INTO `kr_chart` (`id_product`, `id_session`, `qty`) VALUES
(17, 3, 130),
(12, 3, 3),
(14, 3, 24),
(10, 3, 2),
(15, 3, 13),
(16, 3, 1),
(7, 5, 2),
(5, 3, 4),
(3, 3, 1),
(11, 3, 1),
(13, 3, 1),
(15, 0, 2),
(17, 5, 3),
(13, 5, 2),
(12, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kr_extend_layout`
--

CREATE TABLE IF NOT EXISTS `kr_extend_layout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `type` enum('special-offer','sidebar','featured-product') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `kr_extend_layout`
--

INSERT INTO `kr_extend_layout` (`id`, `id_product`, `type`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, 'special-offer', 15, '2014-11-28 23:05:17', '2014-11-30 16:13:45', '0000-00-00 00:00:00'),
(2, 12, 'special-offer', 15, '2014-11-28 23:09:29', '2014-11-29 08:22:53', '2014-11-29 08:11:26'),
(3, 6, 'special-offer', 15, '2014-11-28 23:10:14', '2014-11-30 16:13:49', '0000-00-00 00:00:00'),
(4, 2, 'sidebar', 15, '2014-11-28 23:10:19', '0000-00-00 00:00:00', '2014-11-30 10:11:17'),
(5, 3, 'special-offer', 15, '2014-11-28 23:10:30', '2014-11-30 16:13:53', '0000-00-00 00:00:00'),
(6, 11, 'special-offer', 15, '2014-11-28 23:26:03', '2014-11-30 16:13:57', '0000-00-00 00:00:00'),
(7, 2, 'special-offer', 15, '2014-11-29 07:27:25', '2014-11-30 16:14:01', '0000-00-00 00:00:00'),
(8, 2, 'special-offer', 15, '2014-11-30 16:44:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 3, 'special-offer', 15, '2014-11-30 16:44:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 5, 'special-offer', 15, '2014-11-30 16:44:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 6, 'special-offer', 15, '2014-11-30 16:44:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 7, 'special-offer', 15, '2014-11-30 16:44:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 11, 'special-offer', 15, '2014-11-30 16:44:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 3, 'featured-product', 15, '2014-11-30 18:22:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 3, 'sidebar', 15, '2014-11-30 18:22:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 5, 'sidebar', 15, '2014-11-30 18:22:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 4, 'featured-product', 15, '2014-11-30 18:22:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 5, 'featured-product', 15, '2014-11-30 18:22:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 10, 'featured-product', 15, '2014-11-30 18:22:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 15, 'sidebar', 15, '2014-11-30 18:23:04', '2014-11-30 18:24:08', '0000-00-00 00:00:00'),
(21, 7, 'special-offer', 15, '2014-11-30 19:54:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `kr_media`
--

CREATE TABLE IF NOT EXISTS `kr_media` (
  `id_media` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `file` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_media`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kr_payment_port`
--

CREATE TABLE IF NOT EXISTS `kr_payment_port` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `no_rekening` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `kr_payment_port`
--

INSERT INTO `kr_payment_port` (`id`, `name`, `no_rekening`, `image`, `created_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'rr', 'ccc', '7672a852a26810d2d54b74595d6733ea.jpg', '15', '2014-11-30 10:49:44', '2014-11-30 15:27:08', '0000-00-00 00:00:00'),
(2, 'eee', 'eee', '', '15', '2014-11-30 10:50:16', '0000-00-00 00:00:00', '2014-11-30 11:11:37'),
(3, 'Testing-eidt', 'halo333', '39c9f77bae7852d49ac48e53d758b77b.jpg', '15', '2014-11-30 10:52:21', '2014-11-30 10:54:18', '2014-11-30 11:11:37'),
(4, 'sdadf', 'adfds', '8c7daf15d4637f0ef4b82c2b65ba3bbb.jpg', '15', '2014-11-30 11:36:57', '0000-00-00 00:00:00', '2014-11-30 11:11:37'),
(5, 'sdadf', 'halo333', 'dcca912d5ae0de079c58a078b73bf510.jpg', '15', '2014-11-30 15:27:24', '0000-00-00 00:00:00', '2014-11-30 15:11:27');

-- --------------------------------------------------------

--
-- Table structure for table `kr_post`
--

CREATE TABLE IF NOT EXISTS `kr_post` (
  `id_post` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `content` text NOT NULL,
  `status` enum('draft','publish') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `id_post_category` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `deleted_at` datetime NOT NULL,
  `image` varchar(200) NOT NULL,
  `tag` text NOT NULL,
  `slug` varchar(250) NOT NULL,
  PRIMARY KEY (`id_post`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kr_post`
--

INSERT INTO `kr_post` (`id_post`, `title`, `content`, `status`, `created_at`, `updated_at`, `id_post_category`, `created_by`, `deleted_at`, `image`, `tag`, `slug`) VALUES
(1, 'Coba', '', 'draft', '2014-10-20 07:24:00', '0000-00-00 00:00:00', 3, 15, '2014-10-20 08:10:22', '', '', ''),
(2, 'TI100-edited', '<p>CO100-ed</p>\n', 'publish', '2014-10-20 07:26:34', '2014-10-20 08:19:39', 11, 15, '2014-10-20 08:10:22', '', 'akdf, kadf, kadsjf, adksf, edited', ''),
(3, 'Coba Berita Terkini', '<p>Under normal circumstances you won&#39;t even notice the Output class since it works transparently without your intervention. For example, when you use the&nbsp;<a href="http://localhost/user_guide/libraries/loader.html">Loader</a>&nbsp;class to load a view file, it&#39;s automatically passed to the Output class, which will be called automatically by CodeIgniter at the end of system execution. It is possible, however, for you to manually intervene with the output if you need to, using either of the two following functions:</p>\n', 'draft', '2014-12-02 09:05:45', '2014-12-02 09:10:42', 3, 15, '0000-00-00 00:00:00', '', '', 'coba-berita-terkini-2014-12-02-091042');

-- --------------------------------------------------------

--
-- Table structure for table `kr_post_category`
--

CREATE TABLE IF NOT EXISTS `kr_post_category` (
  `id_post_category` int(11) NOT NULL AUTO_INCREMENT,
  `post_category` varchar(150) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `slug` varchar(250) NOT NULL,
  PRIMARY KEY (`id_post_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `kr_post_category`
--

INSERT INTO `kr_post_category` (`id_post_category`, `post_category`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(3, 'cccc yyuy eee', 15, '2014-10-13 08:22:37', '2014-10-13 08:49:57', '0000-00-00 00:00:00', ''),
(4, 'ccc', 15, '2014-10-13 08:51:29', '0000-00-00 00:00:00', '2014-10-13 08:10:51', ''),
(5, 'af', 15, '2014-10-13 20:41:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(6, 'dfadsf', 15, '2014-10-13 20:41:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(7, 'eree', 15, '2014-10-13 20:41:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(8, 'afdasfs', 15, '2014-10-13 20:42:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(9, 'rrrr', 15, '2014-10-13 20:42:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(10, 'cccc', 15, '2014-10-13 20:42:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(11, 'fdgfgfdg', 15, '2014-10-13 20:42:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(12, 'bxvbx', 15, '2014-10-13 20:42:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(13, 'Vas Pohon', 15, '2014-12-02 09:11:42', '2014-12-02 09:12:10', '0000-00-00 00:00:00', 'vas-pohon-2014-12-02-091210');

-- --------------------------------------------------------

--
-- Table structure for table `kr_product`
--

CREATE TABLE IF NOT EXISTS `kr_product` (
  `id_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `price` int(11) NOT NULL,
  `id_product_category` int(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` enum('sold','available','empty') NOT NULL,
  `qty_stocked` int(11) NOT NULL,
  `qty_sold` int(11) NOT NULL,
  `size` varchar(25) NOT NULL,
  `unit` varchar(25) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `product` varchar(200) NOT NULL,
  `deleted_at` datetime NOT NULL,
  `image` varchar(200) NOT NULL,
  `tag` text NOT NULL,
  `new_tag` tinyint(1) NOT NULL,
  `status_publish` enum('publish','draft') NOT NULL,
  `slug` varchar(250) NOT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `kr_product`
--

INSERT INTO `kr_product` (`id_product`, `price`, `id_product_category`, `created_at`, `updated_at`, `status`, `qty_stocked`, `qty_sold`, `size`, `unit`, `description`, `created_by`, `product`, `deleted_at`, `image`, `tag`, `new_tag`, `status_publish`, `slug`) VALUES
(1, 111, 6, '2014-10-10 07:10:31', '0000-00-00 00:00:00', 'available', 11, 0, '21', '232', '<p>PD100</p>\n', 15, 'PN100', '2014-10-13 07:10:59', '', '', 0, 'publish', ''),
(2, 323, 8, '2014-10-11 08:10:36', '2014-12-06 15:39:45', 'available', 23, 0, '23', '34', '<p>adfadsfd</p>\n', 15, 'adfafad', '0000-00-00 00:00:00', '77d4b376ef94a9323af5860982714db1.jpg', '', 0, 'publish', 'adfafad-2014-12-06-153945'),
(3, 0, 8, '2014-10-11 18:10:38', '2014-12-06 15:39:50', 'available', 200, 0, 'XL', 'Potong', '<p>PD101</p>\n', 15, 'PN101', '0000-00-00 00:00:00', '', '', 0, 'publish', 'pn101-2014-12-06-153950'),
(4, 0, 8, '2014-10-11 18:10:41', '2014-12-06 15:39:54', 'available', 12, 0, 'L', 'Potong', '<p>PD102</p>\n', 15, 'PN102', '0000-00-00 00:00:00', 'bd6dc42f594ecf3421dcbea7bded237e.jpg', '', 1, 'publish', 'pn102-2014-12-06-153954'),
(5, 0, 6, '2014-10-11 20:10:35', '2014-12-06 15:39:59', 'available', 10, 0, 'M', 'Potong', '<p>PD103</p>\n', 15, 'PN103', '0000-00-00 00:00:00', '84e0045da63a0176f96ce6e155fbb9bf.jpg', '', 0, 'publish', 'pn103-2014-12-06-153959'),
(6, 0, 7, '2014-10-11 20:10:36', '2014-12-06 15:40:04', 'available', 10, 0, 'XL', 'Potong', '<p>PD104</p>\n', 15, 'PN104', '0000-00-00 00:00:00', '34a5efea73fc151d28c4389c0fcb3f57.jpg', '', 0, 'publish', 'pn104-2014-12-06-154004'),
(7, 9999, 7, '2014-10-11 21:10:02', '2014-12-06 15:40:10', 'available', 99, 0, 'XL', 'Buah', '<p>PD110</p>\n', 15, 'PN110', '0000-00-00 00:00:00', '1d69e2094184530013085ce554292a84.jpg', '', 0, 'publish', 'pn110-2014-12-06-154010'),
(8, 323, 6, '2014-10-11 21:10:20', '2014-10-13 07:35:08', 'available', 11, 0, 'XL', 'Potong', '<p>dads</p>\n', 15, 'fdfs', '2014-10-13 07:10:58', '26c1a54746d49803ee158e127a449f2b.jpg', '', 0, 'publish', ''),
(9, 11223, 8, '2014-10-11 21:10:56', '2014-10-13 07:38:40', 'available', 21474, 0, 'XLLLL', 'Kodi', '<p>sfsfsf12-edited-Strika</p>\n', 16, 'Setrika', '2014-10-13 07:10:57', '43951e5eacb97935b8de1f204f324d81.gif', '', 0, 'publish', ''),
(10, 324, 6, '2014-10-13 20:35:27', '2014-12-06 15:40:16', 'available', 23, 0, '32', 'Lusin', '<p>CodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.</p>\n\n<p>CodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.</p>\n\n<p>CodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks, as well as a simple interface and logical structure to access these libraries. CodeIgniter lets you creatively focus on your project by minimizing the amount of code needed for a given task.</p>\n', 15, 'Keramik Menaik', '0000-00-00 00:00:00', '218a53d5ef018c9345873e37c2680fec.png', 'Murah, Keramik, Unik', 0, 'publish', 'keramik-menaik-2014-12-06-154016'),
(11, 5000, 6, '2014-10-19 20:40:58', '2014-12-06 15:40:21', 'available', 10, 0, 'M', 'Kodi', '<p>PD100219</p>\n', 15, 'PN102', '0000-00-00 00:00:00', '8c7dde9d92c7a7ed15701c975741dc1a.jpg', '434,adfkja,adsfaf.xxxx', 1, 'publish', 'pn102-2014-12-06-154021'),
(12, 0, 6, '2014-11-29 08:16:49', '2014-12-06 15:40:26', 'available', 0, 0, '', '', '<p>sdfadsfdf</p>\n', 15, 'Test', '0000-00-00 00:00:00', '088cd1b0a4dafb1d75dc2cae7af7b092.jpg', 'dasf,adf,asdf', 0, 'publish', 'test-2014-12-06-154026'),
(13, 0, 6, '2014-11-29 08:17:36', '2014-12-06 15:40:32', 'available', 0, 0, '', '', '<p>sddsf</p>\n', 15, 'Test123', '0000-00-00 00:00:00', '79077145199537df03a61f7766f3c1d5.jpg', '', 0, 'publish', 'test123-2014-12-06-154032'),
(14, 0, 6, '2014-11-29 08:18:11', '2014-12-06 15:40:39', 'available', 0, 0, '', '', '<p>njhh</p>\n', 15, 'Test', '0000-00-00 00:00:00', '9781d9a678c04e1eaea60fc5a3530830.jpg', '', 0, 'publish', 'test-2014-12-06-154039'),
(15, 0, 7, '2014-11-30 02:55:43', '2014-11-30 09:35:28', 'available', 0, 0, '', '', '<p>adsfafadfsdsf</p>\n', 15, 'PN2000', '0000-00-00 00:00:00', '', 'fdasaf, adsfasdk. adsf', 1, 'publish', ''),
(16, 0, 6, '2014-12-02 09:13:42', '2014-12-02 09:14:22', 'sold', 0, 0, '', '', '', 15, 'Keramik Emas', '0000-00-00 00:00:00', '', '', 0, 'publish', 'keramik-emas-2014-12-02-091422'),
(17, 5000, 11, '2014-12-05 06:15:45', '2014-12-06 15:40:47', 'sold', 10, 0, 'M', 'Potong', '<p>adsfasdfdasadf</p>\n', 15, 'Rumah Busana', '0000-00-00 00:00:00', '1c8b5b4690e578ef4550079eadace9a2.png', 'dasfda, adf, adsf, ', 1, 'publish', 'rumah-busana-2014-12-06-154047');

-- --------------------------------------------------------

--
-- Table structure for table `kr_product_category`
--

CREATE TABLE IF NOT EXISTS `kr_product_category` (
  `id_product_category` int(11) NOT NULL AUTO_INCREMENT,
  `product_category` varchar(150) NOT NULL,
  `user_created` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `id_parent` int(11) NOT NULL,
  `slug` varchar(250) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id_product_category`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `kr_product_category`
--

INSERT INTO `kr_product_category` (`id_product_category`, `product_category`, `user_created`, `created_at`, `updated_at`, `deleted_at`, `id_parent`, `slug`, `description`) VALUES
(5, 'fdsfsdcc', 15, '2014-10-05 06:10:48', '2014-10-05 07:10:05', '2014-10-05 07:10:26', 0, 'fdsfsdcc', ''),
(6, 'Souvenir', 15, '2014-10-05 06:10:55', '2014-12-06 07:40:07', '0000-00-00 00:00:00', 0, 'souvenir-2014-12-06-074007', '<p>Converts ASCII values to character entities, including high ASCII and MS Word characters that can cause problems when used in a web page, so that they can be shown consistently regardless of browser settings or stored reliably in a database. There is some dependence on your server&#39;s supported character sets, so it may not be 100% reliable in all cases, but for the most part it should correctly identify characters outside the normal range (like accented characters). Example:</p>\n\n<p>Converts ASCII values to character entities, including high ASCII and MS Word characters that can cause problems when used in a web page, so that they can be shown consistently regardless of browser settings or stored reliably in a database. There is some dependence on your server&#39;s supported character sets, so it may not be 100% reliable in all cases, but for the most part it should correctly identify characters outside the normal range (like accented characters). Example:</p>\n'),
(7, 'Keramik Lantai', 15, '2014-10-05 06:10:55', '2014-11-29 11:57:07', '0000-00-00 00:00:00', 0, 'keramik-lantai', ''),
(8, 'Guci', 15, '2014-10-11 21:10:41', '2014-12-02 09:20:01', '0000-00-00 00:00:00', 11, 'guci-2014-12-02-092001', '<p>Under normal circumstances you won&#39;t even notice the Output class since it works transparently without your intervention. For example, when you use the&nbsp;<a href="http://localhost/user_guide/libraries/loader.html">Loader</a>&nbsp;class to load a view file, it&#39;s automatically passed to the Output class, which will be called automatically by CodeIgniter at the end of system execution. It is possible, however, for you to manually intervene with the output if you need to, using either of the two following functions:</p>\n'),
(9, 'fksgk', 15, '2014-10-11 21:10:42', '0000-00-00 00:00:00', '2014-10-11 21:10:44', 0, 'fksgk', ''),
(10, 'Pot', 16, '2014-10-11 21:10:54', '2014-11-29 11:57:25', '0000-00-00 00:00:00', 0, 'pot', ''),
(11, 'Vas Bunga', 15, '2014-11-24 07:08:27', '2014-11-29 11:57:33', '0000-00-00 00:00:00', 0, 'vas-bunga', ''),
(12, 'Lain-Lain', 15, '2014-11-24 07:15:06', '2014-11-29 11:57:58', '0000-00-00 00:00:00', 0, 'lain-lain', ''),
(13, 'dsdsf', 15, '2014-11-24 07:16:06', '2014-11-24 07:35:52', '2014-11-29 11:11:58', 11, 'dsdsf', ''),
(14, 'TTTT', 15, '2014-12-01 08:11:32', '2014-12-01 08:12:34', '0000-00-00 00:00:00', 8, '', '<p>fgsdfggsgdfgsd-ediit</p>\n'),
(15, 'Coba Category', 15, '2014-12-02 09:18:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'coba-category-2014-12-02-091842', '');

-- --------------------------------------------------------

--
-- Table structure for table `kr_product_category_relation`
--

CREATE TABLE IF NOT EXISTS `kr_product_category_relation` (
  `id_product_category` int(11) NOT NULL,
  `id_product` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kr_product_media`
--

CREATE TABLE IF NOT EXISTS `kr_product_media` (
  `id_product` int(11) NOT NULL,
  `id_media` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kr_session`
--

CREATE TABLE IF NOT EXISTS `kr_session` (
  `id_session` int(11) NOT NULL AUTO_INCREMENT,
  `session_date` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `submit_date` datetime DEFAULT NULL,
  `no_resensi` varchar(50) NOT NULL,
  PRIMARY KEY (`id_session`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `kr_session`
--

INSERT INTO `kr_session` (`id_session`, `session_date`, `id_user`, `submit_date`, `no_resensi`) VALUES
(2, '2014-12-21 13:00:38', 29, '2014-12-21 13:00:38', ''),
(3, '2014-12-21 13:05:48', 29, NULL, ''),
(5, '2014-12-21 18:00:16', 31, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `kr_site_setting`
--

CREATE TABLE IF NOT EXISTS `kr_site_setting` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `logo` varchar(250) NOT NULL,
  `favicon` varchar(250) NOT NULL,
  `keyword` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kr_site_setting`
--

INSERT INTO `kr_site_setting` (`id`, `title`, `description`, `logo`, `favicon`, `keyword`) VALUES
(1, 'Toko Online Keramik - Home', '<p>CodeIgniter is an Application Development Framework - a toolkit - for people who build web sites using PHP. Its goal is to enable you to develop projects much faster than you could if you were writing code from scratch, by providing a rich set of libraries for commonly needed tasks testing</p>\n', '49d3a746a4264e5317f0c5443c9ef7b3.png', 'c4026bbd09d5a425492a789a0318f313.jpg', 'CodeIgniter,Test, coba');

-- --------------------------------------------------------

--
-- Table structure for table `kr_slider`
--

CREATE TABLE IF NOT EXISTS `kr_slider` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `url_post` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `status` enum('publish','draft') NOT NULL,
  `data` enum('url','product') NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kr_slider`
--

INSERT INTO `kr_slider` (`id`, `title`, `image`, `url_post`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `status`, `data`, `description`) VALUES
(1, 'Promo Murah', '', '3', 15, '2014-11-14 08:11:11', '2014-11-16 14:59:39', '2014-11-16 15:11:24', 'draft', 'product', ''),
(2, 'Testing', '26ea37d914de374b985bae696a75f1e3.png', '', 15, '2014-11-14 08:13:37', '2014-11-22 09:41:38', '0000-00-00 00:00:00', 'publish', 'product', ''),
(3, 'Testing', 'f2885f550fd5b0445a0a4b44a1dbd763.png', '3', 15, '2014-11-14 08:36:55', '2014-11-22 09:41:53', '0000-00-00 00:00:00', 'publish', 'url', ''),
(4, 'Promo Murah', '', '', 15, '2014-11-14 08:37:12', '0000-00-00 00:00:00', '2014-11-16 15:11:24', 'publish', 'url', ''),
(5, 'TI100', '69107ec05439c10606c82624bba49501.png', '6', 15, '2014-11-14 08:37:25', '2014-11-22 09:42:01', '0000-00-00 00:00:00', 'publish', 'url', ''),
(6, 'GDDG', '76cae26301b617700324d956f4533ae0.png', 'https://www.google.co.id/?gws_rd=ssl', 15, '2014-11-14 08:37:40', '2014-11-22 09:42:09', '0000-00-00 00:00:00', 'publish', 'url', ''),
(7, 'TI101', 'ec02b43b72f27d89a204be48b25fb323.png', '6', 15, '2014-11-15 19:33:27', '2014-11-22 09:42:20', '0000-00-00 00:00:00', 'publish', 'url', ''),
(8, 'TI102', '', 'http://google.com', 15, '2014-11-15 19:34:02', '0000-00-00 00:00:00', '2014-11-22 09:11:39', 'publish', 'url', ''),
(9, 'TI103', '', 'https://www.youtube.com/watch?v=2hASOre63Nk', 15, '2014-11-15 19:52:42', '0000-00-00 00:00:00', '2014-11-22 09:11:39', 'publish', 'url', ''),
(10, 'TI104', '', '3', 15, '2014-11-15 19:53:19', '2014-11-16 14:48:20', '2014-11-22 09:11:39', 'publish', 'product', ''),
(11, 'TI105-edited', 'b9918cd8d7d0afae89272632b9cffc41.jpg', '5', 15, '2014-11-16 11:45:00', '2014-11-16 15:21:50', '2014-11-22 09:11:39', 'publish', 'url', ''),
(0, 'Testing', '2aad4ec7f9eec2e6783ce44a5e54d52d.png', '4', 15, '2014-11-23 21:26:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'publish', 'product', '<p>sdfadsfdafsf</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `kr_social_media`
--

CREATE TABLE IF NOT EXISTS `kr_social_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `social_media` varchar(250) NOT NULL,
  `link` varchar(250) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `icon` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `kr_social_media`
--

INSERT INTO `kr_social_media` (`id`, `social_media`, `link`, `created_by`, `created_at`, `updated_at`, `deleted_at`, `icon`) VALUES
(2, 'sm100', 'li100', 15, '2014-11-23 23:35:46', '2014-11-23 23:49:16', '2014-11-23 23:11:49', '28b8fb211cb86d02d90a28a13fe66c38.png'),
(3, 'facebook', 'http://facebook/', 15, '2014-11-23 23:37:07', '2014-11-24 00:33:25', '0000-00-00 00:00:00', 'aad2a11b75cf0a7e578158559f8f5d6a.png'),
(4, 'sm100', 'li100', 15, '2014-11-23 23:36:26', '2014-11-23 23:47:03', '2014-11-24 00:11:02', ''),
(5, 'twiiter', 'http://twitter/', 15, '2014-11-24 00:09:59', '2014-11-24 00:33:50', '0000-00-00 00:00:00', '3bfdf697e9039b5b1390947b594d6892.png'),
(6, 'sm101', 'li101', 15, '2014-11-23 23:36:02', '2014-11-24 00:09:44', '2014-11-24 00:11:30', '44469f0c95d8aef061c8da84a9151790.jpg'),
(7, 'youtube', 'http://youtube', 15, '2014-11-24 00:34:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '3e64b88f7e8f245fef9fb3fb5816112b.png'),
(10, 'Test-edit', 'fadsffd-edit', 15, '2014-11-23 23:13:31', '2014-11-23 23:46:31', '2014-11-23 23:11:49', ''),
(11, 'Test-edit', 'fadsffd-edit', 15, '2014-11-28 22:32:14', '0000-00-00 00:00:00', '2014-11-28 22:11:32', 'efb4ea20a1be478acf9506375de9031b.png');

-- --------------------------------------------------------

--
-- Table structure for table `kr_user`
--

CREATE TABLE IF NOT EXISTS `kr_user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(100) NOT NULL,
  `level` enum('admin','costumer') NOT NULL,
  `status` enum('active','deactive') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `token` text NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `kr_user`
--

INSERT INTO `kr_user` (`id_user`, `name`, `username`, `password`, `email`, `level`, `status`, `created_at`, `updated_at`, `deleted_at`, `token`) VALUES
(6, 'Haryadi Widianto', 'anto', 'GyBS9YLfm2TbbnzoFl78O9yCbwsFtr27DmF9kJQgeFlymwPbCu6sz/46GjWVNCnxgra3oKBQp5DqSB7QaN/g+Q==', 'haryadi.widianto@gmail.com', 'admin', 'active', '2014-10-04 13:10:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(7, 'new user', 'usertest12', 'j9oNS+lB20Govq2Z6bhLrYo4zqLUBj0WGEO6vZrLTHec8lAU+8G1t0yiHOv2sFJxYDt1Z8Th1MrTTDsW9ynXBQ==', 'new.user@gmail.com', 'admin', 'active', '2014-10-04 14:10:25', '2014-10-04 15:10:11', '0000-00-00 00:00:00', ''),
(8, 'Haryadi Widianto', 'us200', 'OS+BuJQgfPyNVbeJtY1Fbv4cih/mjLfwLkaRv1xwKbOrxdKt5UWNosUfI6I9UWikIhlA2WRJScD99ckWbZv0Fw==', 'haryadi.widianto@gmail.com', 'admin', 'active', '2014-10-04 14:10:26', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(15, 'Haryadi Widianto', 'haryadi', 'nP3ugjHSpm5rGzkfvcjsOJVi4YVv2oGQNFNFgx4PzU3IXgjGGj+Hnv56raWjqstTYANpTWPIU1rp+m4jo8YRsA==', 'haryadi.widianto@gmail.com', 'admin', 'active', '2014-10-04 15:10:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(16, 'andi', 'andi', '1bDsKsGMisli/GbW4CFawlY2tF9HJYnAuetmQLDx3N0QI7USgWasaZKVPjuLK8fAyHD/a0631HblmCLcbuvoSw==', 'andi@gmail.com', 'admin', 'active', '2014-10-11 21:10:52', '2014-10-11 21:10:52', '0000-00-00 00:00:00', ''),
(17, 'Haryadi Widianto', 'haryadi', '20021988', 'haryadi.widianto@gmail.com', 'admin', 'deactive', '2014-12-14 14:57:57', '0000-00-00 00:00:00', '2014-12-14 16:12:00', ''),
(18, 'Sri Setianingsih', 'jeng_sri', 'GwNcz00NuwGmzv2GQBg35h02/zNjKft2IDcmq2Znp3/0LnScxy7PI1kyQoI6quZosYlFLYjSg6b1oOfcZj6lRg==', 'sri@gmail.com', 'costumer', 'deactive', '2014-12-14 15:15:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'T6A544Rn7d9WO1p9oem1SubJfCzgSpXhRM5O/eZOCp45go6KNGvL8ABrd52so96bK9nlILC+LekGg5YXur4pbw=='),
(19, 'Sri Setianingsih', 'jeng_sri', 'Z0Wje5f3n/WkEyF35F+4eKCbG/luMizth3AzbPxqnh7q4MaZN56aZGTpzDFWkXYgLln/ChxhCegAZ9DO0XB9Mg==', 'haryadi.widianto@gmail.com', 'costumer', 'deactive', '2014-12-14 15:17:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'ZX64vRakgcZJ5l18TaeQAwBetbXyHAItDTFpTuMvT9oM/oSewZEKh34rVAO68uNGo1QD+mQjnpRH/4fZ4gv7OrahMeIKwzmtstsm1GC9jECtE+OBrZ2YcaGz5buIgJJ5'),
(20, 'Haryadi', 'kafjkajf', 'X1P0OeutvV4+I+yjDSeeVOD990CdTBkIGIHL5kOFsFlwDZdUJsqkGxsqcDxZcnPCnObst07/Gt8UYCILjHhqhQ==', 'haryadi.widianto@gmail.com', 'costumer', 'deactive', '2014-12-14 15:18:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '6MzZ3W22UGAG/9jRzBEaWEEfIhl0J4to6aG3D8JueD5zLOeq59PnUNG+OcinPuzZVwOhVTzVbRcWq7ItEgAKDkWrfndbrIc6IwkvXnT3XQCulvtYOn+lCzv/xBqx//AN'),
(21, 'Dd', 'dedededed', '64Of2CTyNZS1Oe3vpvRaJY866dFh5yfNkP6bfbdKo0Y29ShmIHnx36YOj15NR9k2C09TsuiodycMJ1XdYjH6Dw==', 'haryadi.widianto@gmail.com', 'costumer', 'deactive', '2014-12-14 15:18:19', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'fycT1jzBno/XB5BtXrS2nEvtrFKUddH2cks20/VvbNsc4Q2w7lhjln/vaJtgRDRZgLrnWN4STHarW4i8tMeGwjbhOgxWFDu83xVnidC1LmrJKWQzYsWm9AE5S4WefxFP'),
(22, 'na450', 'us450', 'KxpbvbUPayI0IteX5U9xmED1cWkqFhypgmEPhI4VTqGDp/Y49H5VqlQ9gCLSx0qkyoH6PcvO3h591lwZC//zwA==', 'em450@gmail.com', 'costumer', 'deactive', '2014-12-14 15:19:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'OjbEKAraNHhhgENnV33G7dWj5mqZ82JkJq6wrGWGz0SZYKnSXqgcw9QZlUpz9GRcjZRLLeubt3oG1zJPh9TZgVd+kuJDgfqooFfIB29ePC8bs6hp8/fFZKErJ75vPD4o'),
(23, 'na500', 'us500', 'cuUwjGY737o6elPUral6bojyIRncONa/AsNWOopCNhndWR0+Ns5NVrRk33qAdcWiE+MOk2cCf/aT6SgaxDPZ0w==', 'em500@gmail.com', 'costumer', 'deactive', '2014-12-14 15:21:20', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'RHoDdQZPqqYkzP2UnuF493L65zCo+HjDeqNU6drJxgE8blfICLLS1YQStpW1+udpIcgMhKwaWB0ry0MMlFQSQ9CJJErTLNjJUkNzlQD97ZA95xdDaphr0BCHw77FzjC2'),
(24, 'fadff', 'asfafafas', 'cluP3eEEWLayiY9aLhKEcIs1iyM5ZUuUP6s2d41HVMfiL6nYMfHPx8Pih8t2b/Zr6lFTC0NBwoH4/iQlWHxvVg==', 'haryadi.widianto@gmail.com', 'costumer', 'deactive', '2014-12-14 15:22:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'vGIDc/DZSz8yoxym+/FO0P9ntr0QhJohqpg+RJHLQnw9I17qgYH8tkpINfvkZ9VqEDh1fnLvghl81w3OKIWUdarkPY29QoREZHwqJif69Ho2H/6Uz+7Gv2Py+XzKE7en'),
(25, 'afdakfjk', 'kafkakaf', 'Z8/FCYpK7XlB3vdk39tlWK0QzJCyffQNyp0KpAr8b5EBWXBz8DhENM8cMOSnZruclkgWWURVZWRu/6mDnBshrw==', 'adfafharyadi.widianto@gmail.com', 'costumer', 'deactive', '2014-12-14 15:22:47', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0GyyNgMU1iTF6G52s+20fksEgh+NdLsxIKGlQQ8/a86H5NRwBW4NIahHKtQyey9I2ky9wMEWn7yIV3E3Lw2BhxyBbqYH++40iQ9uOPqhNJybArKunY6MWhz6S+9BjgYU'),
(26, 'Haryadi Widianto', '08018124dfasfdf', 'X2lDRrnc1cLSwe5xEbgT6ok74wPH7FbNWLQynkHRbUzTinU6EKZlJnXo4VLj9MbL4QXlyz9BzmxNRFOfbsFNLg==', 'haryadi.widianto@gmail.com', 'costumer', 'deactive', '2014-12-14 15:23:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'QruNKVxWTELXzKM+BD+Fwmr5GK2l4DHWeKfp2Yds2Ust6dErYYoczBk7BxMnIrJ1g/g1wtuwc6sVld4KaDb1YguObeTs9UlqAMEd9/YvV7C4GgdjdPmDVMyO49972nAf'),
(27, 'na90909090', 'fdkaskfaf', 'q6gysW4tU3Be1HMPf1ACRjWx6XYRSffbDmbh2XhrXC33nxWpx/M78c5KPQUnuC2yxIjd4PqWvb4lwe+QSkz8hg==', 'haryadi.widianto@gmail.com', 'costumer', 'deactive', '2014-12-14 15:23:48', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'z/pB2k7KDsiYPatR6jQTCPLwb9rvwVquDXQfYZ/wsycxDGO8BNJDcYf0xvPZSGUC1AUI3VmkNO4TZl5gZD1OS8EYIPnhF6T/uf3qUUbOx/VQlbtbZBwEsbEeoJtlwkwN'),
(28, 'Haryadi Widiantoww', '08018124eeee', 'ntmA5u/ldD2zGSN9HoHbe/L1ycfqrQ0El0fhXZh42uoQaTatDMVReJRahkZjs6qSY8cOwczMTth1EYsfh9/Jrw==', 'haryadi.widianto@gmail.com', 'costumer', 'deactive', '2014-12-14 15:23:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'vny7/d0T5CJfW+RLsyheZEcI8vhM/5i4dEwTn+8C8PN6lXCva7P99rMgWciBxitM19iGYf0bbrA9Vs1L3dUawwvOXW/smkfTSH4puvqa9YuwRhUbAEQcPa0o4R8Pfhtk'),
(29, 'Costumer Dummy', 'cosdum', 'CHnKrSw4TZNowyX3+2eUICDDxbW3PKD6PQzcv+J2h481Q2jKBYezFgHdjAnjbwJlWIRatBTCcJmWOrIUqSKOPg==', 'customer.dummy@gmail.com', 'costumer', 'active', '2014-12-15 08:12:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(30, 'Andy', 'andi123', 'us40Pw/73hLLpx+Hf2J/p+cKgY19hTHN7RSElouNFK4mdGu0ZFoko+XYFjj0s/im11eM6OMKLU5Jckk9HJo49Q==', 'andi@gmail.com', 'admin', 'active', '2014-12-17 20:12:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00', ''),
(31, 'Dafi Hazel', 'cosdum02', '94XxUOZpGuCk08Vt623NDsuAYcnsPTVI96Y8v5baH9p6w1+z+KQb4RVgB5JH2CmSOjWuYzPiuBYaWsQrdNW9EQ==', 'cosdum02@gmail.com', 'costumer', 'active', '2014-12-21 17:12:46', '2014-12-21 20:12:56', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `kr_user_profile`
--

CREATE TABLE IF NOT EXISTS `kr_user_profile` (
  `id_user` int(11) NOT NULL,
  `address` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `post_code` varchar(7) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `foto` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
