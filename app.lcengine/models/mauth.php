<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Mauth extends CI_Model{
    
    private $table = '';

    public function __construct() {
        parent::__construct();
        $this->table = 'kr_user';
    }
    
    public function get_auth_var($username = '') {
        $array = array('username' => $username,'status' => 'active');
        $result = $this->db->where($array)->where( '(deleted_at = "0000-00-00 00:00:00" OR deleted_at IS NULL)')->get($this->table);
        return (!empty($result->row_array()))?$result->row_array():false;
    }
    
    
}

