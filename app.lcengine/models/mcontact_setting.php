<?php

class Mcontact_setting extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_contact_setting';
        $this->id = 'id';
        
    }
    
    public function get_row($id = 1) {
        
        $this->db->select('*');
        $this->db->where($this->id, 1);
        $result = $this->db->get($this->table);

        return $result->row_array();
        
    }
    
    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }
    
}

?>
