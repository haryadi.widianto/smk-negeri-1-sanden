<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mproduct extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_product';
        $this->id = 'id_product';
        
    }

    public function get_data($limit = 0, $index = 0, $by = '', $keyword = '') {
        $this->db->select('kr_product.*, kr_user.name, kr_product_category.product_category as product_category');
        $this->db->join('kr_user', 'kr_product.created_by = kr_user.id_user');
        $this->db->join('kr_product_category', 'kr_product.id_product_category = kr_product_category.id_product_category');
        $this->db->where('kr_product.deleted_at', '0000-00-00 00:00:00');
        $result = $this->db->get($this->table);
        return $result->result();
    }
    
    public function get_row($id = '') {
        
        $this->db->select('*');
        $this->db->where('id_product', $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
        
    }
    
    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];
    }
    
    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }
    
    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }
    
     public function delete_data($id = '') {
        
        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );
        
        $this->db->where($this->id , $data[$this->id]);
        $this->db->update($this->table, $data);
        
    }
    
    public function get_old_filename($id = '') {
        
        $this->db->select('image');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return $result['image'];
        
    }
    
    /* Front End */
    public function get_lastest($limit = 6) {
        
        $this->db->select('kr_product.*, kr_user.name, kr_product_category.product_category as product_category');
        $this->db->join('kr_user', 'kr_product.created_by = kr_user.id_user');
        $this->db->join('kr_product_category', 'kr_product.id_product_category = kr_product_category.id_product_category');
        $this->db->where('kr_product.deleted_at', '0000-00-00 00:00:00');
        $this->db->where('kr_product.status_publish', 'publish');
        $this->db->order_by("kr_product.created_at", "desc"); 
        $this->db->limit($limit);
        $result = $this->db->get($this->table);
        return $result->result();
        
    }
    
    public function get_by_category($param = '', $limit = 0, $index = 0) {
        
        $this->db->select('kr_product.*, kr_user.name, kr_product_category.product_category as product_category');
        $this->db->join('kr_user', 'kr_product.created_by = kr_user.id_user');
        $this->db->join('kr_product_category', 'kr_product.id_product_category = kr_product_category.id_product_category');
        $this->db->where('kr_product.deleted_at', '0000-00-00 00:00:00');
        $this->db->where('kr_product.status_publish', 'publish');
        $this->db->where('kr_product_category.slug', $param);
        $this->db->order_by("kr_product.created_at", "desc"); 
        $this->db->limit($limit, $index);
        $result = $this->db->get($this->table);
        return $result->result();
        
    }
    
    public function get_bylike($category = '', $keyword = '', $limit = 0, $index = 0) {
        
        $this->db->select('kr_product.*, kr_user.name, kr_product_category.product_category as product_category');
        $this->db->join('kr_user', 'kr_product.created_by = kr_user.id_user');
        $this->db->join('kr_product_category', 'kr_product.id_product_category = kr_product_category.id_product_category');
        $this->db->where('kr_product.deleted_at', '0000-00-00 00:00:00');
        $this->db->where('kr_product.status_publish', 'publish');
        
        if($category != '') $this->db->where('kr_product_category.slug', $category);
        if($keyword != '') $this->db->like('kr_product.product', $keyword);
        
        $this->db->order_by("kr_product.created_at", "desc"); 
        $this->db->limit($limit, $index);
        $result = $this->db->get($this->table);
        return $result->result();
        
    }
    
    public function get_count() {
        
        $this->db->select('kr_product.*, kr_user.name, kr_product_category.product_category as product_category');
        $this->db->from($this->table);
        $this->db->join('kr_user', 'kr_product.created_by = kr_user.id_user');
        $this->db->join('kr_product_category', 'kr_product.id_product_category = kr_product_category.id_product_category');
        $this->db->where('kr_product.deleted_at', '0000-00-00 00:00:00');
        $this->db->where('kr_product.status_publish', 'publish');
        return $this->db->count_all_results();
       
    }
    
    /* Front End */
    
    public function get_count_per_category($category = '') {
        
        $this->db->select('kr_product.*, kr_user.name, kr_product_category.product_category as product_category');
        $this->db->from($this->table);
        $this->db->join('kr_user', 'kr_product.created_by = kr_user.id_user');
        $this->db->join('kr_product_category', 'kr_product.id_product_category = kr_product_category.id_product_category');
        $this->db->where('kr_product.deleted_at', '0000-00-00 00:00:00');
        $this->db->where('kr_product.status_publish', 'publish');
        $this->db->where('kr_product_category.slug', $category);
        return $this->db->count_all_results();
       
    }
    
    public function get_count_bylike($category = '', $keyword = '') {
        
        $this->db->select('kr_product.*, kr_user.name, kr_product_category.product_category as product_category');
        $this->db->from($this->table);
        $this->db->join('kr_user', 'kr_product.created_by = kr_user.id_user');
        $this->db->join('kr_product_category', 'kr_product.id_product_category = kr_product_category.id_product_category');
        $this->db->where('kr_product.deleted_at', '0000-00-00 00:00:00');
        
        if($category != '') $this->db->where('kr_product_category.slug', $category);
        if($keyword != '') $this->db->like('kr_product.product', $keyword);
        
        return $this->db->count_all_results();
       
    }
    
    public function get_related($param = '', $limit = 6) {
        
        $this->db->select('kr_product.*, kr_user.name, kr_product_category.product_category as product_category');
        $this->db->join('kr_user', 'kr_product.created_by = kr_user.id_user');
        $this->db->join('kr_product_category', 'kr_product.id_product_category = kr_product_category.id_product_category');
        $this->db->where('kr_product.deleted_at', '0000-00-00 00:00:00');
        $this->db->where('kr_product.status_publish', 'publish');
        $this->db->where('kr_product_category.id_product_category', $param);
        $this->db->order_by("kr_product.created_at", "desc"); 
        $this->db->limit($limit);
        $result = $this->db->get($this->table);
        return $result->result();
        
    }
    
    public function get_row_slug($slug = '') {
        
        $this->db->select('*');
        $this->db->where('slug', $slug);
        $result = $this->db->get($this->table);

        return $result->row_array();
        
    }


}

?>
