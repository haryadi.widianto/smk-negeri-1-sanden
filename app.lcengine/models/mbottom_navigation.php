<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mbottom_navigation extends CI_Model {

    private $table = '';

    public function __construct() {
        parent::__construct();
        $this->table = 'kr_bottom_navigation';
        $this->id = 'id';
    }
    
    public function get_data() {
        $this->db->select("$this->table.*, kr_user.name as name");
        $this->db->join("kr_user", "$this->table.created_by = kr_user.id_user");
        $this->db->where("$this->table.deleted_at", "0000-00-00 00:00:00");
        $this->db->order_by("$this->table.id", "asc"); 
        $result = $this->db->get($this->table);
        return $result->result();
    }
    
    public function get_row($id = '') {
        
        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
        
    }
    
    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];
    }
    
    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }
    
    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }
    
     public function delete_data($id = '') {
        
        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );
        
        $this->db->where($this->id , $data[$this->id]);
        $this->db->update($this->table, $data);
        
    }
    
    public function get_old_filename($id = '') {
        
        $this->db->select('icon');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return $result['icon'];
        
    }


}

?>
