<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mmenu_title extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_menu_title';
        $this->id = 'id';
        
    }
    
    public function get_row($id = '') {
        
        $this->db->select('*');
        $this->db->where($this->id, 1);
        $result = $this->db->get($this->table);

        return $result->row_array();
        
    }
    
    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }
    
}

?>
