<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mevent extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_event';
        $this->id = 'id';
    }

    public function get_data($limit = 0, $index = 0) {
        $this->db->select('kr_event.*, kr_user.name as name');
        $this->db->join('kr_user', 'kr_event.created_by = kr_user.id_user');
        $this->db->where('kr_event.deleted_at = "0000-00-00 00:00:00" OR kr_event.deleted_at IS NULL');
        if ($limit != 0) {
            $this->db->limit($limit, $index);
        }
        $this->db->order_by("kr_event.created_at", "desc");
        $result = $this->db->get($this->table);
        return $result->result();
    }

    public function get_row($id = '') {

        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
    }

    public function get_byslug($slug = '') {

        $this->db->select('*');
        $this->db->where("slug", $slug);
        $result = $this->db->get($this->table);

        return $result->row_array();
    }

    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];
    }

    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }

    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    public function delete_data($id = '') {

        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );

        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    public function get_old_filename($id = '') {

        $this->db->select('image');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return $result['image'];
    }

    public function get_publish($limit = 0, $index = 0) {

        $this->db->select('kr_event.*, kr_user.name as name');
        $this->db->join('kr_user', 'kr_event.created_by = kr_user.id_user');
        $this->db->where('kr_event.deleted_at = "0000-00-00 00:00:00" OR kr_event.deleted_at IS NULL');
        $this->db->where('kr_event.status', 'publish');
        if ($limit != 0) {
            $this->db->limit($limit, $index);
        }
        $this->db->order_by("kr_event.created_at", "desc");
        $result = $this->db->get($this->table);
        return $result->result();
    }

    public function get_byyear($year = '') {

        $this->db->select('kr_event.*, kr_user.name as name');
        $this->db->join('kr_user', 'kr_event.created_by = kr_user.id_user');
        $this->db->where('kr_event.deleted_at = "0000-00-00 00:00:00" OR kr_event.deleted_at IS NULL');
        $this->db->where('kr_event.status', 'publish');
        if ($year == '') {
            $this->db->where('YEAR(kr_event.day_start)', date('Y'));
        } else {
            $this->db->where('YEAR(kr_event.day_start)', "$year");
        }

        $this->db->order_by("kr_event.created_at", "desc");
        $result = $this->db->get($this->table);
        return $result->result();
    }

    public function get_count($status = '') {
        $this->db->join('kr_user', 'kr_event.created_by = kr_user.id_user');
        $this->db->where('kr_event.deleted_at = "0000-00-00 00:00:00" OR kr_event.deleted_at IS NULL');

        if ($status != '') {
            $this->db->where('kr_event.status', $status);
        }

        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}

?>
