<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Msite_setting extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_site_setting';
        $this->id = 'id';
        
    }
    
    public function get_row($id = 1) {
        
        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
        
    }
    
    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }
    
    public function get_old_filename($id = '', $select = '') {
        
        $this->db->select($select);
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return $result[$select];
        
    }


}

?>
