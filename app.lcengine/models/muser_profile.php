<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Muser_profile extends CI_Model {

    private $table = '';
    private $id = '';

    public function __construct() {
        parent::__construct();
        $this->table = 'kr_user_profile';
        $this->id = 'id_user';
    }

    public function get_row($id = '') {

        $this->db->select('*');
        $this->db->where('id_user', $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
        
    }

    public function insert_data($data = array()) {
        
        $data['created_at'] = date('Y-m-d H:m:i');
        
        $this->db->insert($this->table, $data);
        
    }

    public function update_data($data = array()) {
        
        $data['updated_at'] = date('Y-m-d H:m:i');
        
        $this->db->where('id_user', $data['id_user']);
        $this->db->update($this->table, $data);
    }

    public function delete_data($id = '') {
        
        $data['id_user'] = $id;
        $data['deleted_at'] = date('Y-m-d H:m:i');
        
        $this->db->where('id_user', $data['id_user']);
        $this->db->update($this->table, $data);
        
    }

    public function user_profile_exist($id) {

        $this->db->where('id_user', $id);
        $this->db->from($this->table);
        return $this->db->count_all_results();

    }
    
    public function get_old_filename($id = '') {
        
        $this->db->select('foto');
        $this->db->where('id_user', $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return (isset($result['foto']))?$result['foto']:'0000.png';
        
    }
    
    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return (isset($result["$field"]))?$result["$field"]:'';
        
    }
    

}

?>
