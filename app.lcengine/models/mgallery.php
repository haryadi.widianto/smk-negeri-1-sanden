<?php

class Mgallery extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_gallery';
        $this->id = 'id';
    }

    public function get_data($album = 0) {
        $this->db->select('kr_gallery.*, kr_user.name as name');
        $this->db->join('kr_user', 'kr_gallery.created_by = kr_user.id_user');
        $this->db->where('kr_gallery.id_album', $album);
        $this->db->where('(kr_gallery.deleted_at = "0000-00-00 00:00:00" OR kr_gallery.deleted_at IS NULL)');
        $this->db->order_by("kr_gallery.id", "asc");
        $result = $this->db->get($this->table);
        return $result->result();
    }

    public function get_row($id = '') {
        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        return $result->row_array();
    }

    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];
    }

    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }

    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    public function delete_data($id = '') {

        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );

        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    public function get_old_filename($id = '') {

        $this->db->select('image');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return $result['image'];
    }

    public function get_cover($id_album = '') {
        $this->db->select('*');
        $this->db->where('id_album', $id_album);
        $this->db->limit(1);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return (count($result) == 0)?'':$result;
    }
    

}

?>
