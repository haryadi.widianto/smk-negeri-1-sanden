<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mcontact extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_contact';
        $this->id = 'id';
    }

    public function get_data($limit = 0, $index = 0, $by = '', $keyword = '') {
        $this->db->select('*');
        $this->db->where('kr_contact.deleted_at', '0000-00-00 00:00:00');
        $this->db->order_by("date", "desc");
        $result = $this->db->get($this->table);
        return $result->result();
    }

    public function get_row($id = '') {

        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
    }

    public function get_total() {
        $this->db->select('*');
        $this->db->where('kr_contact.deleted_at', '0000-00-00 00:00:00');
        $this->db->where('kr_contact.status', 'new');
        $this->db->order_by("date", "desc");
        $this->db->limit(7);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];
    }

    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }

    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    public function delete_data($id = '') {

        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );

        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    /* Front End  */

    public function get_row_byid($id = '') {

        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->result();
    }
    
    public function get_data_where($field = '', $value= '') {
        $this->db->select('*');
        $this->db->where('kr_contact.deleted_at', '0000-00-00 00:00:00');
        $this->db->where($field, $value);
        $this->db->order_by("date", "desc");
        $this->db->limit(7);
        $result = $this->db->get($this->table);
        return $result->result();
    }

}

?>
