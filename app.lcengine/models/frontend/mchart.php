<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mchart extends CI_Model {

    private $table = '';
    private $id_session = '';

    public function __construct() {
        parent::__construct();
        $this->table = 'kr_chart';
        $this->load->model('frontend/msession');
        $this->id_session = $this->msession->get_id_session();
        
    }
    
    public function get_chart($id_session = '') {
        
        $str  = 'kr_product.id_product as id, ';
        $str .= 'kr_product.product as product, ';
        $str .= 'kr_product.image as image, ';
        $str .= 'kr_product.description as description, ';
        $str .= 'kr_chart.qty as qty, ';
        $str .= 'kr_product.price as price, ';
        $str .= 'kr_product.price * kr_chart.qty  as total';
        
        $this->db->select($str);
        $this->db->from($this->table);
        $this->db->join('kr_product', 'kr_chart.id_product = kr_product.id_product');
        
        if($id_session == ''){
            $this->db->where('kr_chart.id_session', $this->id_session);
        }else{
            $this->db->where('kr_chart.id_session', $id_session);
        }
        
        $data = $this->db->get();
        $data = $data->result();
                
        return (!empty($data))?$data:'';
        
    }

    public function set_chart($id_product = '', $qty = 0) {
        
        if ($this->check_chart($id_product) > 0) {
            
            if($qty == 0){
                $this->update_qty($id_product);
            }else{
                $this->update_qty($id_product, $qty);
            }
            
        } else {
            if ($qty == 0) {

                $data = array(
                    'id_product' => $id_product,
                    'id_session' => $this->id_session,
                    'qty' => 1
                );
            } else {

                $data = array(
                    'id_product' => $id_product,
                    'id_session' => $this->id_session,
                    'qty' => $qty
                );
            }

            $this->db->insert($this->table, $data);
        }
    }

    public function update_qty($id_product = '', $qty_ = 0) {

        if ($qty_ == 0) {
            $qty = $this->get_qty($id_product) + 1;
        } else {
            $qty = $this->get_qty($id_product) + $qty_;
        }
        
        $this->db->where('id_product', $id_product);
        $this->db->where('id_session', $this->id_session);
        $this->db->update($this->table, array('qty' => $qty));
    }
    
    public function get_qty($id_product = '') {
        
        $qty = $this->db->select('qty')
                    ->where('id_product', $id_product)
                    ->where('id_session', $this->id_session)
                    ->get($this->table)
                    ->row_array();
        
        return $qty['qty'];
        
    }

    public function check_chart($id_product = '') {

        $this->db->where('id_product', $id_product);
        $this->db->where('id_session', $this->id_session);
        $this->db->from($this->table);

        return $this->db->count_all_results();

    }
    
    public function get_total_item($id_session = '') {
        
        $this->db->select('SUM(qty) AS total');
        if($id_session == ''){
            $this->db->where('id_session', $this->id_session);
        }else{
            $this->db->where('id_session', $id_session);
        }
        $data = $this->db->get($this->table);
        $data = $data->row_array();
        
        return (isset($data['total']))?$data['total']:'';
        
    }
    
    public function get_total_price($id_session = '') {
        
        $this->db->select('SUM(kr_chart.qty * kr_product.price) As result');
        $this->db->from($this->table);
        $this->db->join('kr_product', 'kr_chart.id_product = kr_product.id_product');
        if($id_session == ''){
            $this->db->where('kr_chart.id_session', $this->id_session);
        }else{
            $this->db->where('kr_chart.id_session', $id_session);
        }
        $data = $this->db->get();
        $data = $data->row_array();
                
        return (isset($data['result']))?$data['result']:'';
        
    }
    
    public function delete_item($id_product = '') {
        
        $this->db->where('id_product', $id_product);
        $this->db->where('id_session', $this->id_session);
        $this->db->delete($this->table);
        
    }

}

?>
