<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Msession extends CI_Model {

    private $table = '';
    private $id = '';
    private $id_session ='';

    public function __construct() {
        parent::__construct();
        $this->table = 'kr_session';
        $this->id = 'id_session';
        $this->id_session = $this->get_id_session();
    }

    public function set_session() {

        $user = $this->session->userdata('login');

        $count = $this->db->where('submit_date', NULL)
                ->where('id_user', $user['id_user'])
                ->from($this->table)
                ->count_all_results();

        if ($count == 0) {

            $data = array(
                'session_date' => date('Y-m-d H:i:s'),
                'id_user' => $user['id_user']
            );

            $this->db->insert($this->table, $data);
        }
    }

    public function get_id_session() {

        $user = $this->session->userdata('login');

        $id_session = $this->db->select('kr_session.id_session as idSession')
                ->where('id_user', $user['id_user'])
                ->where('submit_date', NULL)
                ->get($this->table)
                ->row_array();

        return (isset($id_session['idSession'])) ? $id_session['idSession'] : 0;
    }
    
    public function get_session_date($id_session = NULL) {
        
        $this->db->select('session_date');
        if($id_session == NULL){
            $this->db->where('kr_session.id_session', $this->id_session);
        }else{
            $this->db->where('kr_session.id_session', $id_session);
        }
        
        $data = $this->db->get($this->table);
        
        return $data->row_array();
        
    }
    
    public function get_submit_date($id_session = NULL) {
        $this->db->select('submit_date');
        if($id_session == NULL){
            $this->db->where('kr_session.id_session', $this->id_session);
        }else{
            $this->db->where('kr_session.id_session', $id_session);
        }
        $data = $this->db->get($this->table);
        return $data->row_array();
    }

    public function submit_session($params = array()) {
        $data = array(
            'submit_date' => date('Y-m-d H:i:s'),
            'no_resensi' => $params['no_resensi']
        );
        $this->db->where('id_session', $this->get_id_session());
        $this->db->update($this->table, $data);
    }
    
    public function get_by_date($start ='', $end = '') {
        
        $str  = 'kr_product.product as product, ';
        $str .= 'kr_chart.qty as qty, ';
        $str .= 'kr_product.description as description, ';
        $str .= 'SUM(qty) as total,';
        $str .= '(kr_product.price * SUM(qty)) as subtotal';
    
        $this->db->select($str);
        $this->db->join('kr_chart', 'kr_session.id_session = kr_chart.id_session');
        $this->db->join('kr_product', 'kr_chart.id_product = kr_product.id_product');
        $this->db->where('deliver_date >=', $start); 
        $this->db->where('deliver_date <=' , $end);
        $this->db->group_by("kr_product.id_product"); 
        $result = $this->db->get($this->table);
        
        return $result->result();
        
    }
    
    
    

    public function get_transaction_history() {

        $this->load->model('frontend/mchart');

        $user = $this->session->userdata('login');
        
        $data = $this->get_session_byid($user['id_user']);
        
        foreach ($data as $key => $item){
            $data[$key]['total_price'] = $this->mchart->get_total_price($item['id_session']);
            $data[$key]['total_item'] = $this->mchart->get_total_item($item['id_session']);
        }
        
        return $data;
        
    }
    

    private function get_session_byid($id_user = '') {

        $this->db->select('*');
        $this->db->where('id_user', $id_user);
        $this->db->where('submit_date <>' , 'NULL');
        $this->db->order_by('session_date', 'desc');
        $data = $this->db->get($this->table);
        
        return $data->result_array();
    }
    
    /* Backend */
    
    public function get_transaction_history_all() {

        $this->load->model('frontend/mchart');
        
        $data = $this->get_session_all();
        
        foreach ($data as $key => $item){
            $data[$key]['total_price'] = $this->mchart->get_total_price($item['id_session']);
            $data[$key]['total_item'] = $this->mchart->get_total_item($item['id_session']);
            
            if($item['submit_date'] == NULL){
                $html = "<small class='badge pull-right bg-yellow'><i class='fa fa-fw fa-refresh'></i> Progress</small>";
                $data[$key]['status'] = $html;
            }
            
            if($item['submit_date'] != NULL && $item['deliver_date'] == NULL){
                $html = "<small class='badge pull-right bg-green'><i class='fa fa-fw fa-shopping-cart'></i> Submited</small>";
                $data[$key]['status'] = $html;
            }
            
            if($item['submit_date'] != NULL && $item['deliver_date'] !== NULL){
                $html = "<small class='badge pull-right bg-blue'><i class='fa fa-fw fa-truck'></i> Delivered</small>";
                $data[$key]['status'] = $html;
            }
            
        }
        
        return $data;
        
    }
    
    private function get_session_all() {

        $this->db->select('kr_session.*, kr_user.name as name');
        $this->db->join('kr_user', 'kr_session.id_user = kr_user.id_user');
        $this->db->where('kr_session.deleted_at', NULL);
        $this->db->order_by('session_date', 'desc');
        $data = $this->db->get($this->table);
        
        return $data->result_array();
    }
    
    public function get_session_row($id_session = '') {

        $this->db->select('kr_session.*, kr_user.name AS name');
        $this->db->join('kr_user', 'kr_session.id_user = kr_user.id_user');
        $this->db->where('kr_session.id_session', $id_session);
        $data = $this->db->get($this->table);
        
        return $data->row_array();
        
    }
    
    public function update($data = array()) {
        
        $this->db->where($this->id, $data['id_session']);
        $this->db->update($this->table, $data);
        
    }
    
    public function delete($id = '') {
        
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
        );
        
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
        
    }
    
    public function get_last_order() {
        
        $this->db->select('*');
        $this->db->where('submit_date !=', 'NULL');
        $this->db->where('deliver_date',NULL);
        $this->db->order_by('submit_date', 'desc');
        $this->db->limit(7);
        
        $data = $this->db->get($this->table);
        return $data->result();
        
    }
    
    public function get_total_order() {
        
        $this->db->select('*');
        $this->db->where('submit_date !=','NULL');
        $this->db->where('deliver_date', NULL);
        $this->db->from($this->table);
        return  $this->db->count_all_results();
        
    }
    
     public function get_total_transaction() {
        
        $this->db->select('*');
        $this->db->where('deleted_at', NULL);
        $this->db->from($this->table);
        return  $this->db->count_all_results();
        
    }
    

}

?>
