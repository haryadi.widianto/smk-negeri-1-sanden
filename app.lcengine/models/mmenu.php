<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mmenu extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_menu';
        $this->id = 'id';
        
    }
    
    public function get_data($postition  = 0) {
        $this->db->select('kr_menu.*, kr_user.name as name');
        $this->db->join('kr_user', 'kr_menu.created_by = kr_user.id_user');
        $this->db->where('kr_menu.position', $postition);
        $this->db->where('( kr_menu.deleted_at = "0000-00-00 00:00:00" OR kr_menu.deleted_at IS NULL )');
        $this->db->order_by("kr_menu.id", "asc"); 
        $result = $this->db->get($this->table);
        return $result->result();
    }
    
    public function get_data_array($postition  = 0) {
        $this->db->select('kr_menu.*, kr_user.name as name');
        $this->db->join('kr_user', 'kr_menu.created_by = kr_user.id_user');
        $this->db->where('kr_menu.position', $postition);
        $this->db->where('(kr_menu.deleted_at = "0000-00-00 00:00:00" OR kr_menu.deleted_at IS NULL)');
        $this->db->order_by("kr_menu.id", "asc"); 
        $result = $this->db->get($this->table);
        return $result->result_array();
    }
    
    public function get_row($id = '') {
        
        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
        
    }
    
    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];
    }
    
    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }
    
    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }
    
     public function delete_data($id = '') {
        
        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );
        
        $this->db->where($this->id , $data[$this->id]);
        $this->db->update($this->table, $data);
        
    }
    
    public function get_old_filename($id = '') {
        
        $this->db->select('image');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return $result['image'];
        
    }
    

}

?>
