<?php

class Prestasi extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_reg_prestasi';
        $this->id = 'id_student';
    }
    
      public function get_by_id($id = '') {
        
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->id, $id);
        $data = $this->db->get();
        
        return $data->result();
        
    }
  
    public function insert_data($data = array()) {
        $this->db->insert_batch($this->table, $data);
    }
    
    

}


?>
