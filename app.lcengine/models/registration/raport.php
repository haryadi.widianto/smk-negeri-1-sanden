<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Raport extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_reg_raport';
        $this->id = 'id_student';
    }

    public function get_byId($id = '') {

        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->id, $id);
        $data = $this->db->get();

        return $data->row_array();
    }

    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }

}

?>
