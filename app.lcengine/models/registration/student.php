<?php

class Student extends CI_Model {

    private $table = '';
    private $id = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_reg_student';
        $this->id = 'id';
    }

    public function get_whole() {

        $parent = ' kr_reg_parents.name as pname, ';
        $parent .= ' kr_reg_parents.address as paddress, ';
        $parent .= ' kr_reg_parents.telp as ptelp, ';
        $parent .= ' kr_reg_parents.religion as preligion, ';
        $parent .= ' kr_reg_parents.occupation as poccupation, ';

        $student = ' kr_reg_student.name as sname, ';
        $student .= ' kr_reg_student.telp as stelp, ';
        $student .= ' kr_reg_student.religion as sreligion, ';

        $str = 'kr_reg_student.*, kr_reg_school_from.*,  kr_reg_raport.*, kr_reg_parents.*, ' . $parent . $student;

        $this->db->select($str);
        $this->db->from('kr_reg_student');
        $this->db->join('kr_reg_school_from', 'kr_reg_student.id = kr_reg_school_from.id_student');
        $this->db->join('kr_reg_parents', 'kr_reg_student.id = kr_reg_parents.id_student');
        $this->db->join('kr_reg_raport', 'kr_reg_student.id = kr_reg_raport.id_student');

        $this->db->where('kr_reg_student.deleted_at', NULL);

        /* Order by "Nilai Bahasa Indoneasia" */
        $this->db->order_by("kr_reg_raport.bi1", "asc");
        $this->db->order_by("kr_reg_raport.bi2", "asc");
        $this->db->order_by("kr_reg_raport.bi3", "asc");
        $this->db->order_by("kr_reg_raport.bi4", "asc");
        $this->db->order_by("kr_reg_raport.bi5", "asc");

        /* Order by "Nilai Bahasa Inggris" */
        $this->db->order_by("kr_reg_raport.be1", "asc");
        $this->db->order_by("kr_reg_raport.be2", "asc");
        $this->db->order_by("kr_reg_raport.be3", "asc");
        $this->db->order_by("kr_reg_raport.be4", "asc");
        $this->db->order_by("kr_reg_raport.be5", "asc");

        /* Order by "Nilai Matematika" */
        $this->db->order_by("kr_reg_raport.mt1", "asc");
        $this->db->order_by("kr_reg_raport.mt2", "asc");
        $this->db->order_by("kr_reg_raport.mt3", "asc");
        $this->db->order_by("kr_reg_raport.mt4", "asc");
        $this->db->order_by("kr_reg_raport.mt5", "asc");

        /* Order by "Nilai IPA" */
        $this->db->order_by("kr_reg_raport.ipa1", "asc");
        $this->db->order_by("kr_reg_raport.ipa2", "asc");
        $this->db->order_by("kr_reg_raport.ipa3", "asc");
        $this->db->order_by("kr_reg_raport.ipa4", "asc");
        $this->db->order_by("kr_reg_raport.ipa5", "asc");

        /* Order by "Nilai NEM" */
        $this->db->order_by("kr_reg_raport.nem", "asc");


        $query = $this->db->get();

        return $query->result();
    }

    public function get_whole_by($id = '') {

        $parent = ' kr_reg_parents.name as pname, ';
        $parent .= ' kr_reg_parents.address as paddress, ';
        $parent .= ' kr_reg_parents.telp as ptelp, ';
        $parent .= ' kr_reg_parents.religion as preligion, ';
        $parent .= ' kr_reg_parents.occupation as poccupation, ';

        $student = ' kr_reg_student.name as sname, ';
        $student .= ' kr_reg_student.telp as stelp, ';
        $student .= ' kr_reg_student.religion as sreligion, ';

        $str = 'kr_reg_student.*, kr_reg_school_from.*, kr_reg_raport.*, kr_reg_parents.*, ' . $parent . $student;

        $this->db->select($str);
        $this->db->from('kr_reg_student');
        $this->db->join('kr_reg_school_from', 'kr_reg_student.id = kr_reg_school_from.id_student');
        $this->db->join('kr_reg_parents', 'kr_reg_student.id = kr_reg_parents.id_student');
        $this->db->join('kr_reg_raport', 'kr_reg_student.id = kr_reg_raport.id_student');

        $this->db->where('kr_reg_student.id', $id);

        $query = $this->db->get();

        return $query->row_array();
    }

    public function get_lasted() {

        $parent = ' kr_reg_parents.name as pname, ';
        $parent .= ' kr_reg_parents.address as paddress, ';
        $parent .= ' kr_reg_parents.telp as ptelp, ';
        $parent .= ' kr_reg_parents.religion as preligion, ';
        $parent .= ' kr_reg_parents.occupation as poccupation, ';

        $student = ' kr_reg_student.name as sname, ';
        $student .= ' kr_reg_student.telp as stelp, ';
        $student .= ' kr_reg_student.religion as sreligion, ';
        $student .= ' kr_reg_student.image as simage, ';
        $student .= ' kr_reg_student.register_number as sreg_number, ';
        $student .= ' kr_reg_student.id as sid, ';

        $str = 'kr_reg_student.*, kr_reg_school_from.*,  kr_reg_raport.*, kr_reg_parents.*, ' . $parent . $student;

        $this->db->select($str);
        $this->db->from('kr_reg_student');
        $this->db->join('kr_reg_school_from', 'kr_reg_student.id = kr_reg_school_from.id_student');
        $this->db->join('kr_reg_parents', 'kr_reg_student.id = kr_reg_parents.id_student');
        $this->db->join('kr_reg_raport', 'kr_reg_student.id = kr_reg_raport.id_student');

        $this->db->where('kr_reg_student.deleted_at', NULL);

        /* Order by created */
        $this->db->order_by("kr_reg_student.created_at", "desc");

        $query = $this->db->get();

        return $query->result();
    }

    public function get_byId($id = '') {

        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where($this->id, $id);
        $data = $this->db->get();

        return $data->row_array();
    }

    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }

    public function update($param = array()) {
        $this->db->where($this->id, $param[$this->id]);
        $this->db->update($this->table, $param);
    }

    public function delete_data($id = '') {

        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );

        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return (isset($result["$field"])) ? $result["$field"] : 0;
    }

    public function get_total() {

        $this->db->from('kr_reg_student');
        $this->db->join('kr_reg_school_from', 'kr_reg_student.id = kr_reg_school_from.id_student');
        $this->db->join('kr_reg_parents', 'kr_reg_student.id = kr_reg_parents.id_student');
        $this->db->join('kr_reg_raport', 'kr_reg_student.id = kr_reg_raport.id_student');
        $this->db->where('kr_reg_student.deleted_at', NULL);

        $query = $this->db->count_all_results();

        return $query;
    }

}

?>
