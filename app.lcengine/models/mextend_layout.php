<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mextend_layout extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_extend_layout';
        $this->id = 'id';
    }

    public function get_data($limit = 0, $index = 0, $by = '', $keyword = '') {
        $this->db->select('kr_extend_layout.*, kr_user.name as name, kr_product.product as product, kr_product.image as image');
        $this->db->join('kr_user', 'kr_extend_layout.created_by = kr_user.id_user');
        $this->db->join('kr_product', 'kr_extend_layout.id_product = kr_product.id_product');
        $this->db->where('kr_extend_layout.deleted_at', '0000-00-00 00:00:00');
        $this->db->order_by("kr_extend_layout.type", "asc");
        $result = $this->db->get($this->table);
        return $result->result();
    }

    public function get_row($id = '') {

        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
    }

    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];
    }

    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }

    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    public function delete_data($id = '') {

        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );

        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }
    
    public function get_by_category($param = '', $limit = 0, $index = 0) {
        $this->db->select('kr_extend_layout.*, kr_product.*, kr_user.name as name');
        $this->db->join('kr_user', 'kr_extend_layout.created_by = kr_user.id_user');
        $this->db->join('kr_product', 'kr_extend_layout.id_product = kr_product.id_product');
        $this->db->where('kr_extend_layout.deleted_at', '0000-00-00 00:00:00');
        $this->db->where("kr_product.status_publish", 'publish');
        $this->db->where("kr_extend_layout.type", $param);
        if($limit != 0 ) $this->db->limit($limit, $index);
        $this->db->order_by("kr_extend_layout.created_at", "desc");
        $result = $this->db->get($this->table);
        return $result->result();
    }
    
    public function get_count($param = '') {
        $this->db->select('kr_extend_layout.*, kr_product.*, kr_user.name as name');
        $this->db->from($this->table);
        $this->db->join('kr_user', 'kr_extend_layout.created_by = kr_user.id_user');
        $this->db->join('kr_product', 'kr_extend_layout.id_product = kr_product.id_product');
        $this->db->where('kr_extend_layout.deleted_at', '0000-00-00 00:00:00');
        $this->db->where("kr_product.status_publish", 'publish');
        $this->db->where("kr_extend_layout.type", $param);
        return $this->db->count_all_results();
    }

}

?>
