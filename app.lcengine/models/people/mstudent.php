<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mstudent extends CI_Model
{

    private $table = '';
    private $id = '';
    private $image_field = '';

    public function __construct()
    {
        parent::__construct();
        $this->table = 'people_student';
        $this->id = 'id';
        $this->image_field = 'photo';

    }

    public function get_data()
    {

        $this->db->select($this->table . '.*');
        $this->db->where('(' . $this->table . '.deleted_at = "0000-00-00 00:00:00" OR ' . $this->table . '.deleted_at IS NULL)');
        $this->db->order_by($this->table . '.id', 'asc');
        $result = $this->db->get($this->table);
        return $result->result();

    }

    public function get_row($id = '')
    {

        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->row_array();

    }

    public function get_per_field($id = '', $field = '')
    {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];

    }

    public function insert_data($data = array())
    {
        $this->db->insert($this->table, $data);

    }

    public function update_data($data = array())
    {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);

    }

    public function delete_data($id = '')
    {

        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );

        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);

    }

    public function get_old_filename($id = '')
    {

        $this->db->select($this->image_field);
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result[$this->image_field];

    }

    public function get_profile($id = '')
    {

        $this->db->select('*');
        $this->db->join('kr_user', 'kr_user.id_user = ' . $this->table . '.user_id');
        $this->db->where('kr_user.id_user', $id);
        $result = $this->db->get($this->table);

        return $result->row_array();

    }

    public function get_list_student()
    {

        $this->db->select('*');
        $this->db->join('kr_user', 'kr_user.id_user = ' . $this->table . '.user_id');
        $this->db->where('kr_user.status', 'active');
        $this->db->where('(people_student.alumni = 0 OR people_student.alumni IS NULL)');
        $this->db->where('(kr_user.deleted_at = "0000-00-00 00:00:00" OR kr_user.deleted_at IS NULL)');
        $result = $this->db->get($this->table);

        return $result->result();

    }

    public function get_list_alumni()
    {

        $this->db->select('*');
        $this->db->join('kr_user', 'kr_user.id_user = ' . $this->table . '.user_id');
        $this->db->where('kr_user.status', 'active');
        $this->db->where('people_student.alumni', 1);
        $this->db->where('(kr_user.deleted_at = "0000-00-00 00:00:00" OR kr_user.deleted_at IS NULL)');
        $result = $this->db->get($this->table);

        return $result->result();

    }

}

?>
