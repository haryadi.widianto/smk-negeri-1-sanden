<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Mslider extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_slider';
        $this->id = 'id';
        
    }

    public function get_data($limit = 0, $index = 0, $by = '', $keyword = '') {
        $this->db->select('kr_slider.*, kr_user.name');
        $this->db->join('kr_user', 'kr_slider.created_by = kr_user.id_user');
        $this->db->where('(kr_slider.deleted_at = "0000-00-00 00:00:00" OR kr_slider.deleted_at IS NULL)');
        $this->db->order_by('updated_at', 'desc');
        $result = $this->db->get($this->table);
        return $result->result();
    }
    
   
    
    public function get_row($id = '') {
        
        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
        
    }
    
    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];
    }
    
    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }
    
    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }
    
     public function delete_data($id = '') {
        
        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );
        
        $this->db->where($this->id , $data[$this->id]);
        $this->db->update($this->table, $data);
        
    }
    
    public function get_old_filename($id = '') {
        
        $this->db->select('image');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return $result['image'];
        
    }
    
    /* To front end */
     public function get_publish($limit = 5) {
        $this->db->select('kr_slider.*, kr_user.name');
        $this->db->join('kr_user', 'kr_slider.created_by = kr_user.id_user');
        $this->db->where('(kr_slider.deleted_at = "0000-00-00 00:00:00" OR kr_slider.deleted_at IS NULL)');
        $this->db->where('kr_slider.status', 'publish');
        $this->db->order_by('updated_at', 'desc');
        $this->db->limit($limit);
        $result = $this->db->get($this->table);
        return $result->result_array();
    }


}

?>
