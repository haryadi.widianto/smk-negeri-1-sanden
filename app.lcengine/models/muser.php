<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Muser extends CI_Model
{

    private $table = '';
    private $sub_table = '';

    public function __construct()
    {
        parent::__construct();
        $this->table = 'kr_user';
        $this->sub_table = 'kr_user_profile';
    }

    public function get_data($limit = 0, $index = 0, $by = '', $keyword = '')
    {
        $this->db->select('*');
        $this->db->where('(deleted_at = "0000-00-00 00:00:00" OR deleted_at IS NULL)');
        $result = $this->db->get($this->table);
        return $result->result();

    }

    public function get_row($id = '')
    {

        $this->db->select('*');
        $this->db->where('id_user', $id);
        $result = $this->db->get($this->table);

        return $result->row_array();

    }

    public function get_per_field($id = '', $field = '')
    {

        $this->db->select("$field");
        $this->db->where('id_user', $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];

    }

    public function insert_data($data = array())
    {
        $this->db->insert($this->table, $data);

    }

    public function update_data($data = array())
    {
        $this->db->where('id_user', $data['id_user']);
        $this->db->update($this->table, $data);

    }

    public function delete_data($id = '')
    {

        $data = array(
            'id_user' => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );

        $this->db->where('id_user', $data['id_user']);
        $this->db->update($this->table, $data);

    }

    /* Addition to user detail */
    public function user_detail_exist($id)
    {

        $this->db->where('id_user', $id);
        $this->db->from($this->sub_table);
        return $this->db->count_all_results();

    }

    public function check_available_user($data = array(/* [username] => '', [password] => '' */), $mode = 'add', $id = '')
    {

        $data = $this->db->where('username', $data['username'])->get($this->table);
        $result = $data->row_array();

        if ($mode === 'add') {
            return (count($data->result()) > 0) ? false : true;
        } else if ($mode === 'edit') {
            if (count($data->result()) == 0)
                return true;
            if (count($data->result()) == 1 && $result['id_user'] == $id)
                return true;
            if (count($data->result()) > 0)
                return false;
        }

    }

    public function get_total_user_registered()
    {

        $this->db->where('deleted_at', '0000-00-00 00:00:00');
        $this->db->from($this->table);

        return $this->db->count_all_results();

    }

    public function get_all($level = '', $limit = '', $index = '')
    {
        $this->db->select('kr_user.id_user as id,  kr_user.name as name, kr_user_profile.foto as image, kr_user_profile.resume as resume, kr_user_profile.position as position');
        $this->db->join('kr_user_profile', 'kr_user.id_user = kr_user_profile.id_user');
        $this->db->where('kr_user.status', 'active');
        $this->db->where('kr_user.level', $level);
        $this->db->where('kr_user.deleted_at', '0000-00-00 00:00:00');
        $this->db->limit($limit, $index);
        $result = $this->db->get($this->table);
        return $result->result();

    }

    public function get_people_list($level = '', $limit = '', $index = '')
    {

        $this->db->select('*');

        if ($level == 'teacher') {
            $this->db->join('kr_user_profile', 'kr_user.id_user = people_teacher.id_user');
        } elseif ($level == 'staf') {
            $this->db->join('kr_user_profile', 'kr_user.id_user = people_teacher.id_user');
        } elseif ($level == 'alumni') {
            $this->db->join('kr_user_profile', 'kr_user.id_user = people_teacher.id_user');
        } elseif ($level == 'siswa') {
            $this->db->join('kr_user_profile', 'kr_user.id_user = kr_user_profile.id_user');
        }

        $this->db->where('kr_user.status', 'active');
        $this->db->where('kr_user.level', $level);
        $this->db->where('kr_user.deleted_at', '0000-00-00 00:00:00');
        $this->db->limit($limit, $index);

        $result = $this->db->get($this->table);
        return $result->result();

    }

    public function get_bylevel($level = '')
    {
        $this->db->select('kr_user.id_user as id,  kr_user.name as name, kr_user_profile.foto as image');
        $this->db->join('kr_user_profile', 'kr_user.id_user = kr_user_profile.id_user');
        $this->db->where('kr_user.status', 'active');
        if ($level != '') {
            $this->db->where('kr_user.level', $level);
        }
        $this->db->where('kr_user.deleted_at', '0000-00-00 00:00:00');
        $result = $this->db->get($this->table);
        return $result->result();

    }

    public function get_bylevel_count($level = '')
    {
        $this->db->select('kr_user.id_user as id,  kr_user.name as name, kr_user_profile.foto as image');
        $this->db->from($this->table);
        $this->db->join('kr_user_profile', 'kr_user.id_user = kr_user_profile.id_user');
        $this->db->where('kr_user.status', 'active');
        if ($level != '') {
            $this->db->where('kr_user.level', $level);
        }
        $this->db->where('kr_user.deleted_at', '0000-00-00 00:00:00');
        return $this->db->count_all_results();

    }

    public function get_row_byid($id = '')
    {
        $str = 'kr_user.id_user as id,  kr_user.name as name, kr_user_profile.foto as image';
        $str .= ', kr_user_profile.resume as resume, kr_user_profile.position, kr_user.email as email';
        $str .= ', kr_user_profile.facebook as facebook, kr_user_profile.twitter as twitter';
        $str .= ', kr_user_profile.gplus as gplus, kr_user_profile.linkedin as linkedin';
        $this->db->select($str);
        $this->db->join('kr_user_profile', 'kr_user.id_user = kr_user_profile.id_user');
        $this->db->where('kr_user.status', 'active');
        $this->db->where('kr_user.id_user', $id);
        $this->db->where('kr_user.deleted_at', '0000-00-00 00:00:00');
        $result = $this->db->get($this->table);
        return $result->row_array();

    }

}

?>
