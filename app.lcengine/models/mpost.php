<?php

class Mpost extends CI_Model {

    private $table = '';

    public function __construct() {

        parent::__construct();
        $this->table = 'kr_post';
        $this->id = 'id_post';
    }

    public function get_data($limit = 0, $index = 0, $by = '', $keyword = '') {
        $this->db->select('kr_post.*, kr_user.name, kr_post_category.post_category As post_category');
        $this->db->join('kr_user', 'kr_post.created_by = kr_user.id_user');
        $this->db->join('kr_post_category', 'kr_post.id_post_category = kr_post_category.id_post_category');
        $this->db->where('(kr_post.deleted_at = "0000-00-00 00:00:00" OR kr_post.deleted_at IS NULL)');
        $result = $this->db->get($this->table);
        return $result->result();
    }

    public function get_row($id = '') {

        $this->db->select('*');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);

        return $result->row_array();
    }

    public function get_per_field($id = '', $field = '') {

        $this->db->select("$field");
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();

        return $result["$field"];
        
    }

    public function insert_data($data = array()) {
        $this->db->insert($this->table, $data);
    }

    public function update_data($data = array()) {
        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    public function delete_data($id = '') {

        $data = array(
            $this->id => $id,
            'deleted_at' => date('Y-m-d H:m:i')
        );

        $this->db->where($this->id, $data[$this->id]);
        $this->db->update($this->table, $data);
    }

    public function get_old_filename($id = '') {

        $this->db->select('image');
        $this->db->where($this->id, $id);
        $result = $this->db->get($this->table);
        $result = $result->row_array();
        return $result['image'];
    }

    /* Front End  */

    public function get_row_by_category($id = '', $limit = '') {

        $this->db->select('*');
        $this->db->where('id_post_category', $id);
        $this->db->where('status', 'publish');
        $this->db->where('(kr_post.deleted_at = "0000-00-00 00:00:00" OR kr_post.deleted_at IS NULL)');
        if ($limit != '') {
            $this->db->limit($limit);
        }
        $this->db->order_by('created_at', 'desc');
        $result = $this->db->get($this->table);

        return $result->result();
    }

    public function get_publish() {

        $this->db->select('*');
        $this->db->where('status', 'publish');
        $this->db->where('(kr_post.deleted_at = "0000-00-00 00:00:00" OR kr_post.deleted_at IS NULL)');
        $this->db->order_by('created_at', 'desc');
        $result = $this->db->get($this->table);

        return $result->result();
    }

    public function get_row_byslug($slug = '') {

        $this->db->select('kr_post.*, kr_user.name as name');
         $this->db->join('kr_user', 'kr_post.created_by = kr_user.id_user');
        $this->db->where('slug', $slug);
        $result = $this->db->get($this->table);

        return $result->row_array();
    }
    
     public function get_byslug_percategory($slug = '', $limit = '', $index = '') {

        $this->db->select('kr_post.*');
        $this->db->join('kr_post_category', 'kr_post.id_post_category = kr_post_category.id_post_category');
        $this->db->where('kr_post_category.slug', $slug);
        $this->db->where('kr_post.status', 'publish');
        $this->db->where('(kr_post.deleted_at = "0000-00-00 00:00:00" OR kr_post.deleted_at IS NULL)');
        if ($limit != '') {
            $this->db->limit($limit, $index);
        }
        $this->db->order_by('created_at', 'desc');
        $result = $this->db->get($this->table);

        return $result->result();
    }
    
    public function get_count($slug = '') {
      
        $this->db->from($this->table);
        $this->db->join('kr_user', 'kr_post.created_by = kr_user.id_user');
        $this->db->join('kr_post_category', 'kr_post.id_post_category = kr_post_category.id_post_category');
        if($slug != ''){
             $this->db->where('kr_post_category.slug', $slug);
        }
        $this->db->where('(kr_post.deleted_at = "0000-00-00 00:00:00" OR kr_post.deleted_at IS NULL)');
        return $this->db->count_all_results();
       
    }

}

?>
