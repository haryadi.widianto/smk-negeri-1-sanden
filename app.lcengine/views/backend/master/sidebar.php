<aside class="left-side sidebar-offcanvas">                
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <?php $user = $this->session->userdata('login'); ?>
            <div class="pull-left image">
                <img src="<?= set_image( get_photo($user['id_user']))  ?>" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Halo, <?= explode(' ', $user['name'])[0] ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li>
                <a href="<?= base_url('index.php/backend/dashboard') ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="<?= base_url('index.php/backend/cuser') ?>">
                    <i class="fa fa-user"></i> <span>User</span>
                </a>
            </li>
             <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil"></i>
                    <span>Post</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?= base_url('index.php/backend/cpost_category') ?>"><i class="fa fa-angle-double-right"></i> Post Category</a></li>
                    <li><a href="<?= base_url('index.php/backend/cpost') ?>"><i class="fa fa-angle-double-right"></i> Post List</a></li>
                </ul>
            </li>

            <li>
                <a href="<?= base_url('index.php/backend/cslider') ?>">
                    <i class="fa fa-picture-o"></i> <span>Image Slider</span>
                </a>
            </li>
            
            <li>
                <a href="<?= base_url('index.php/backend/cregistration') ?>">
                    <i class="fa fa-pencil-square-o"></i> <span>Registration List</span>
                </a>
            </li>

            <li>
                <a href="<?= base_url('index.php/backend/cevent') ?>">
                    <i class="fa fa-calendar-o"></i> <span>Event</span>
                </a>
            </li>

            <li>
                <a href="<?= base_url('index.php/backend/csocial_media') ?>">
                    <i class="fa fa-comments-o"></i> <span>Social Media</span>
                </a>
            </li>
            
            <li >
                <a href="<?= base_url('index.php/backend/cgallery') ?>">
                    <i class="fa fa-film"></i>
                    <span>Media</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-sitemap"></i>
                    <span>Site Map</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= base_url('index.php/backend/cmenu_category') ?>">
                            <i class="fa fa-angle-double-right"></i>
                            Navigation Category
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url('index.php/backend/cmenu') ?>">
                            <i class="fa fa-angle-double-right"></i> 
                            Navigation List
                        </a>
                    </li>
                     <li>
                        <a href="<?= base_url('index.php/backend/cbottom_navigation') ?>">
                            <i class="fa fa-angle-double-right"></i> 
                            Bottom Navigation
                        </a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-phone"></i>
                    <span>Contact</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= base_url('index.php/backend/cmessage') ?>">
                            <i class="fa fa-angle-double-right"></i>
                            Message list
                        </a>
                    </li>
                    <li>
                        <a href="<?= base_url('index.php/backend/ccontact_setting/edit') ?>">
                            <i class="fa fa-angle-double-right"></i> 
                            Setting
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<?= base_url('index.php/backend/csite_setting/edit/1') ?>">
                    <i class="fa fa-desktop"></i> <span>Site Setting</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>