<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<!-- jQuery 2.0.2 -->
<script src="<?= base_url('pub.lcengine/backend/js/jquery-2.1.1.min.js') ?>"></script>
<!-- Bootstrap -->
<script src="<?= base_url('pub.lcengine/backend/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<!-- DATA TABES SCRIPT -->
<script src="<?= base_url('pub.lcengine/backend/js/plugins/datatables/jquery.dataTables.js') ?>" type="text/javascript"></script>
<script src="<?= base_url('pub.lcengine/backend/js/plugins/datatables/dataTables.bootstrap.js') ?>" type="text/javascript"></script>

<!-- date-range-picker -->
<script src="<?= base_url('pub.lcengine/backend/js/plugins/daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>

<!-- bootstrap time picker -->
<script src="<?= base_url('pub.lcengine/backend/js/plugins/timepicker/bootstrap-timepicker.min.js') ?>" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="<?= base_url('pub.lcengine/backend/js/AdminLTE/app.js') ?>" type="text/javascript"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url('pub.lcengine/backend/js/AdminLTE/demo.js') ?>" type="text/javascript"></script>
<!-- page script -->
<script src="<?= base_url('pub.lcengine/backend/js/plugins/ckeditor/ckeditor.js') ?>" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= base_url('pub.lcengine/backend/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') ?>" type="text/javascript"></script>

<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=bre6pyiq8pgq1vn4ywhxqtx4izd11rgqmfiuvb5jam2lmzdf"></script>

<script type="text/javascript" >
    tinymce.init({
        selector: '#tinyMCETextarea',
        plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker',
        toolbar: 'formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat',
        image_advtab: true,
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tiny.cloud/css/codepen.min.css'
        ],
        link_list: [
            {title: 'My page 1', value: 'http://www.tinymce.com'},
            {title: 'My page 2', value: 'http://www.moxiecode.com'}
        ],
        image_list: [
            {title: 'My page 1', value: 'http://www.tinymce.com'},
            {title: 'My page 2', value: 'http://www.moxiecode.com'}
        ],
        image_class_list: [
            {title: 'None', value: ''},
            {title: 'Some class', value: 'class-name'}
        ],
        importcss_append: true,
        height: 400,
        file_picker_callback: function (callback, value, meta) {
            /* Provide file and text for the link dialog */
            if (meta.filetype === 'file') {
                callback('https://www.google.com/logos/google.jpg', {text: 'My text'});
            }

            /* Provide image and alt text for the image dialog */
            if (meta.filetype === 'image') {
                callback('https://www.google.com/logos/google.jpg', {alt: 'My alt text'});
            }

            /* Provide alternative source and posted for the media dialog */
            if (meta.filetype === 'media') {
                callback('movie.mp4', {source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg'});
            }
        },
        templates: [
            {title: 'Some title 1', description: 'Some desc 1', content: 'My content'},
            {title: 'Some title 2', description: 'Some desc 2', content: '<div class="mceTmpl"><span class="cdate">cdate</span><span class="mdate">mdate</span>My content2</div>'}
        ],
        template_cdate_format: '[CDATE: %m/%d/%Y : %H:%M:%S]',
        template_mdate_format: '[MDATE: %m/%d/%Y : %H:%M:%S]',
        image_caption: true,
        spellchecker_dialog: true,
        spellchecker_whitelist: ['Ephox', 'Moxiecode'],
        tinycomments_mode: 'embedded',
        content_style: '.mce-annotation { background: #fff0b7; } .tc-active-annotation {background: #ffe168; color: black; }'
    });

</script>

