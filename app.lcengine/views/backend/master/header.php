<header class="header">
    <a href="<?= base_url('index.php/backend/dashboard') ?>" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        SMK N 1 Sanden
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">

        <!-- Sidebar toggle button-->
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-right">
            <ul class="nav navbar-nav">

                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-book"></i>
                        <span class="label label-danger"><?= get_total_student_registered() ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?= get_total_student_registered() ?> orders</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <?php
                                $list = get_lasted_student_registered();
                                ?>
                            <ul class="menu">
                                
                                <?php if (!empty($list)): ?>
                                    <?php foreach ($list as $key => $value) : ?>
                                        <li><!-- start message -->
                                            <a href="<?= site_url('backend/cregistration/view/'.$value->sid) ?>">
                                                <div class="pull-left">
                                                    <img src="<?= set_image($value->simage) ?>" class="img-circle" alt="User Image"/>
                                                </div>
                                                <h4>
                                                    <?= $value->sname ?>
                                                    <small><i class="fa fa-clock-o"></i> <?= date('d-m-Y',strtotime($value->created_at))  ?></small>
                                                </h4>
                                                <p><small class="badge bg-blue">Reg. No  :  <?= $value->sreg_number ?></small></p>
                                            </a>
                                        </li><!-- end message -->
                                    <?php endforeach; ?>
                                <?php endif; ?> 
                            </ul>
                        </li>
                        <li class="footer"><a href="<?= site_url('backend/cregistration') ?>">See All Orders</a></li>
                    </ul>
                </li>

                <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-envelope"></i>
                        <span class="label label-success"><?= get_total_message() ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?= get_total_message() ?> messages</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <?php $data = get_message(); ?>
                                <?php if (!empty($data)): ?>
                                    <?php foreach ($data as $key => $value) : ?>
                                        <li><!-- start message -->
                                            <a href="<?= base_url('index.php/backend/cmessage/view/' . $value->id) ?>" style="margin-left: -35px;">
                                                <h4>
                                                    <?= $value->name ?>
                                                    <small><i class="fa fa-clock-o"></i> <?= $value->date ?> </small>
                                                </h4>
                                                <p><?= word_limiter($value->content, 5) ?></p>
                                            </a>
                                        </li><!-- end message --> 
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <li class="footer"><a href="<?= site_url('backend/cmessage') ?>">See All Messages</a></li>
                    </ul>
                </li>
                <!-- Notifications: style can be found in dropdown.less -->
              
                <li class="dropdown messages-menu">
                    <a href="<?= base_url('') ?>"  target="blank" >
                        <i class="fa fa-desktop"></i>&nbsp;&nbsp;View Site
                    </a>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">

                    <?php $user = $this->session->userdata('login'); ?>

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>
                        <span><?= $user['name'] ?> <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            <img src="<?= set_image( get_photo($user['id_user']))  ?>" class="img-circle" alt="User Image" />
                            <p>
                                <?= $user['name'] ?>
                                <small>Terdaftar sejak : <?= $user['created_at'] ?></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?= base_url('index.php/backend/cuser_profile/view/' . $user['id_user']) ?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?= base_url('index.php/cauth/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>