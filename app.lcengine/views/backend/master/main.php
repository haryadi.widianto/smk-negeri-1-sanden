<!DOCTYPE html>
<html>
    <head>
        <?= $this->load->view('backend/master/meta', $data); ?>
        <?= $this->load->view('backend/master/css'); ?>
        <?= $this->load->view('backend/master/js'); ?>
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <?= $this->load->view('backend/master/header'); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?= $this->load->view('backend/master/sidebar'); ?>

            <?php if (!empty($data['data']['content'])): ?>
                <?= $this->load->view($data['file'],  $data); ?>
            <?php else: ?>
                <?= $this->load->view($data['file']); ?>
            <?php endif; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <!-- /.right-side -->
        </div><!-- ./wrapper -->

    </body>
</html>