<!DOCTYPE html>
<html>
    
    <head>
        <?= $this->load->view('backend/master/meta', $data); ?>
        <?= $this->load->view('backend/master/css'); ?>
        <?= $this->load->view('backend/master/js'); ?>
    </head>
    
    <body class="skin-black" style="background-color: black;">

        <div class="form-box" id="login-box">
            
            <div class="header">Sign In</div>
            
            <?= form_open('cauth/login_process') ?>
            
            <div class="body bg-gray">
                
                <div class="form-group">
                    <input type="text" name="username" class="form-control" placeholder="Username"/>
                </div>
                
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password"/>
                </div>       
                
                <div class="form-group">
                    <input type="checkbox" name="remember_me"/> Remember me
                </div>
                
            </div>
            
            <div class="footer">                                                              
                
                <button type="submit" class="btn bg-olive btn-block">Sign me in</button>  

                <p><a href="#">I forgot my password</a></p>

                <a href="register.html" class="text-center">Register a new membership</a>
                
            </div>
            
            <?= form_close() ?>

            <div class="margin text-center">
                <span>SMK Negeri 1 Sanden Administrator Dashboard</span>
            </div>
            
        </div>

    </body>
</html>