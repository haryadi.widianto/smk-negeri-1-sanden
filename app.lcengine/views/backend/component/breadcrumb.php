<ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
    <?php if (!empty($breadcrumb)): ?>
        <?php foreach ($breadcrumb as $key => $value) : ?>
    <li class="active"><a href="#"><?= ucwords(ltrim(str_replace('_', ' ', $value), '\c'))  ?></a></li>
        <?php endforeach; ?>
    <?php endif; ?>
</ol>
