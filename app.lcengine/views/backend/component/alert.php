<?php
$icon = '';
$btn = '';

switch ($mode) {
    case 'alert-info':
        $icon = 'fa-info';
        $btn = 'btn-info';
        break;
    case 'alert-danger':
        $icon = 'fa-ban';
        $btn = 'btn-danger';
        break;
    case 'alert-warning':
        $icon = 'fa-warning';
        $btn = 'btn-warning';
        break;
    case 'alert-success':
        $icon = 'fa-check';
        $btn = 'btn-success';
        break;
}
?>

<div class="alert <?= $mode ?> alert-dismissable" style="margin-left: 0px; padding-left: 15px;">
    
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <?= $message ?>

    <?php if (!empty($button)): ?>
        <br>
        <a href="<?= $button['url'] ?>" type="button"class="btn <?= $btn ?>" >Yes</a>
        <a class="btn btn-info" data-dismiss="alert" aria-hidden="true" >Cancel</a>
    <?php endif; ?>

</div>