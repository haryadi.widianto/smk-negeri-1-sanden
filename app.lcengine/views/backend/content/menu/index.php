<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-xs-9" style="height: 1000px;">
                <?= get_alert() ?>
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="dropdown active">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                Navigation Category <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu tab-header">
                                <?php if (!empty($data['nav_category'])): ?>
                                    <?php foreach ($data['nav_category'] as $key => $value) : ?>
                                        <li role="presentation" class="<?=(($this->session->userdata('tab') == $value->id)) ? 'active' : '' ?>" ><a class="anchor-tab" role="menuitem" data-tab="<?= $value->id ?>" data-toggle="tab" tabindex="-1" href="#tab_<?= $value->id ?>"><?= $value->title ?></a></li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </li>
                        <li><a href="#" class="text-muted "><strong id="title-menu">Menu Aktif</strong></a></li>
                    </ul>
                    <div class="tab-content">

                        <?php if (!empty($data['nav_category'])): ?>
                            <?php foreach ($data['nav_category'] as $key => $value) : ?>
                                <div class="tab-pane <?=(($this->session->userdata('tab') == $value->id)) ? 'active' : '' ?>" id="tab_<?= $value->id ?>">
                                    <?php $send['data']['content'] = $data['tab_' . $value->id]; ?>
                                    <?php $send['data']['position'] = $value->id; ?>
                                    <?= $this->load->view('backend/content/menu/table', $send) ?>
                                </div><!-- /.tab-pane -->
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div><!-- nav-tabs-custom -->

            <div class="col-md-3">

                <?php $target = ($this->uri->segment(3) == 'index') ? 'update/' . $this->uri->segment(4) : 'storage'; ?>

                <?= form_open_multipart('backend/cmenu/' . $target) ?>

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title"><?= ($this->uri->segment(3) == 'index') ? 'Edit' : 'Add' ?></h3>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label>Menu : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-sitemap"></i>
                                </div>
                                <input name="menu"  value="<?= (isset($data['content_edit']['menu'])) ? $data['content_edit']['menu'] : '' ?>" type="text" class="form-control" placeholder="Menu">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="furl">
                            <label>Position : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-puzzle-piece"></i>
                                </div>
                                <select name="position" class="form-control">
                                    <option value="null" >-- Select Position --</option>
                                    <?php if (!empty($data['nav_category'])): ?>
                                        <?php foreach ($data['nav_category'] as $key => $value) : ?>
                                            <option value="<?= $value->id ?>" <?= (isset($data['content_edit']['position']) && $data['content_edit']['position'] == $value->id) ? 'selected="selected"' : '' ?> >
                                                <?= $value->title ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group"  >
                            <label>Template : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-book"></i>
                                </div>
                                <select name="template" class="form-control">
                                    <option value="null">-- Select Template --</option>
                                    <?php if (!empty($data['template'])): ?>
                                        <?php foreach ($data['template'] as $key => $value) : ?>
                                            <option value="<?= str_replace('.php', '', $value) ?>" <?= (isset($data['content_edit']['template']) && $data['content_edit']['template'] == str_replace('.php', '', $value) ) ? 'selected="selected"' : '' ?> >
                                                <?= str_replace('.php', '', $value) ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="furl">
                            <label>Type : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-random"></i>
                                </div>
                                <select name="type" class="form-control">
                                    <option value="null" >-- Select Type --</option>
                                    <option value="page"    <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'page' ) ? 'selected="selected"' : '' ?> >Page</option>
                                    <option value="post"    <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'post' ) ? 'selected="selected"' : '' ?> >Post</option>
                                    <option value="category"    <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'category' ) ? 'selected="selected"' : '' ?> >Category</option>
                                    <option value="raw"    <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'raw' ) ? 'selected="selected"' : '' ?> >Raw</option>
                                    <option value="url"     <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'url' ) ? 'selected="selected"' : '' ?> >URL</option>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="post" >
                            <label>Posting : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-pencil"></i>
                                </div>
                                <select name="post" class="form-control">
                                    <option value="" >-- Select Post --</option>
                                    <?php if (!empty($data['post'])): ?>
                                        <?php foreach ($data['post'] as $key => $value) : ?>
                                            <option value="<?= $value->slug ?>" <?= (isset($data['content_edit']['data']) && str_replace($data['content_edit']['template'] . '/', '', $data['content_edit']['data']) == $value->slug && $data['content_edit']['type'] == 'post' ) ? 'selected="selected"' : '' ?> >
                                                <?= $value->title ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="category" >
                            <label>Category : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-tag"></i>
                                </div>
                                <select name="category" class="form-control">
                                    <option value="" >-- Select Category --</option>
                                    <?php if (!empty($data['category'])): ?>
                                        <?php foreach ($data['category'] as $key => $value) : ?>
                                            <option value="<?= $value->slug ?>" <?= (isset($data['content_edit']['data']) && str_replace($data['content_edit']['template'] . '/', '', $data['content_edit']['data']) == $value->slug && $data['content_edit']['type'] == 'category' ) ? 'selected="selected"' : '' ?> >
                                                <?= $value->post_category ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="raw" >
                            <label>Raw Data : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-link"></i>
                                </div>
                                <input name="raw"  value="<?= (isset($data['content_edit']['data']) && $data['content_edit']['type'] == 'raw' ) ? str_replace($data['content_edit']['template'] . '/', '', $data['content_edit']['data']) : '' ?>" type="text" class="form-control" placeholder="Raw Data">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="url" >
                            <label>URL : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-link"></i>
                                </div>
                                <input name="url"  value="<?= (isset($data['content_edit']['data']) && $data['content_edit']['type'] == 'url' ) ? $data['content_edit']['data'] : '' ?>" type="text" class="form-control" placeholder="URL">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Image : </label>
                            <div class="input-group" style="width: 100%">

                                <a href="#" class="thumbnail" id="photo_profile" style="width: 100%" >
                                    <?php if (!isset($data['content_edit']['image']) || $data['content_edit']['image'] == ''): ?>
                                        <img style="width: 100%; height: 200px;" data-src="holder.js/300x200" alt="300x200" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQQAAAC0CAYAAABytVLLAAAK/ElEQVR4Xu2aB4tUSxdFa8wJA+acc86K+YermBWMmDBizo6KOX/fLrhNzzgzbz/RR8NeF4ZpZk7frrNO1boVuqu7u/tn4YIABCDwfwJdCIF+AAEINAQQAn0BAhBoEUAIdAYIQAAh0AcgAIFfCTBDoFdAAALMEOgDEIAAMwT6AAQgMAABlgx0DwhAgCUDfQACEGDJQB+AAARYMtAHIAABhwB7CA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIXRYob99+1Zu375dnj59Wr5+/VpGjhxZpk2bVubOnVsGDRrUau2LFy/KrVu3yvv378vQoUNrzKJFi0pXV1cr5s6dO+XBgwfly5cvZcSIEWXevHllxowZ/zrjT58+lStXrpTu7u4yZMiQsnfv3h730P3V5pcvX5bPnz+XYcOGlUmTJtX2KL65/lR7/nUCvMEmgBBsVP9N4MWLF6sMNLA1iD9+/Fg/ePbs2WXp0qX1tQbeuXPn6uvhw4fXAf/z58864DUIdd27d69cv379l/usXbu2TJ482U7m2bNn5erVq1VOuvoSwpkzZ8qrV69qW8aPH1/FofgpU6aUNWvW/NH22A0n8LcIIITfwvZ33qTBf+zYsXrzzZs3l3HjxpVHjx7Vp7NmB3oy6/epU6fK27dv6+CXBCQIxWgwrl69ur7/yJEj9WndCKARhGI2bdpkJ3D48OEqgenTp9cZSW8haPZw9OjRer/t27eX0aNH9xDWvn37yuDBg/9Ye+yGE/hbBBDCb2H7O2/S4NJSQANIA1DXu3fvysmTJ+vrPXv2lO/fv9cBqJjdu3fX370vLSNOnDhR5aEBqdlG+8DV3548eVKf/HqqayBroCtG79NnrF+/vkycOLEuBbRcef36dZ2V9BZCe/uawd98vtqlNmq28E/t6SuPv0OZuw5EACF0eP+4efNm0dpbA3fXrl1VGOfPny9jxowpK1asKA8fPqwDWMuAqVOn1myaGC05du7cWf+mJcX+/ft7PMnPnj1bp/dz5swpS5YsKRcuXCjPnz8vM2fOLMuXL+9Bplmm9BaC7ithSQK6h/Yo1F79NLMRtz0dXoqI5iGEDi6z9hK0p6BLg1+DrVlCSBB68v748aOVwfz588vChQvrHoTep+m7nv7NdeDAgRrfLEc0I9BgllD0PslHm5hbt27tsRmo9/cnBP1PS51Lly6VN2/etD5Ls4uVK1fWDUa3PR1cipimIYQOLbUGvqb0egLPmjWrLFu2rLa0EYJeN09knSRoMGuJoBmBNvj6EsLBgwfr4G+EoHs8fvy4XL58uUVB+wt6sve+BhLCjRs3yt27d6tMxo4dW8Wg/QsJasGCBf0Koa/2dGg5YpqFEDqw1FoGSAa6tH5fvHhxq5XN07Z96i5pHDp0qOjIcuPGjXXQa1nR35Jh27ZtdcmhS7HagNR7NaPQ/9qPLpsP7k8Iko9OGdSeHTt21CNQyUCbo5qN6H6aibjt6cByRDUJIXRYuTXwNHg0yDUD0Pq+/dLpgk4Z2jcM9X8JQUuIDRs21Ce1BmR7THOCocGuzb/mOw3Xrl0r9+/fr5uTkkNfn6n79yeE5vRCM4MtW7a0mtqccmjZoBmH254OK0dccxBCB5VcA/L48eP1Cdts9PVunkShUwbFaONPG4DNYNVg15JB+wu6z4cPH+r3APR9AG3yaVmhtb1OEHTp5OD06dN1nb9u3br6WvfQwNZsof3qTwjtMxZ9tmYKapvaqLZqxjJhwgSrPR1UitimIIQOKn37/oAGafs3E9XMVatW1adte5wGrp7+mp63nw40Me1fcNJrzSA0QCUfzTQkDT3FdcypI0b96PsP2ktQvE4eNLA1+9DegP4mqejSJqfupftoWdDsIUg0koLapg1K5fFP7emgMkQ3BSF0UPmb6Xd/TWq+G6D/a4BpI08y0LpdMtAmXvv6X/fTj77JOGrUqHqS0HxLUd9i1P80oPUU1yVJ6NRB92yWDjqqlBD6urS3oT0OxWv2of0EiUMykzT0eZqtNNdA7emgMkQ3BSFEl5/kIdCTAEKgR0AAAi0CCIHOAAEIIAT6AAQg8CsBZgj0CghAgBkCfQACEGCGQB+AAAQGIMCSge4BAQiwZKAPQAACLBnoAxCAAEsG+gAEIOAQYA/BoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwC/wNER+9UOtmw8QAAAABJRU5ErkJggg==" >
                                    <?php else: ?>
                                        <img style="width: 100%; height: 200px;" data-src="holder.js/300x200" alt="300x200" src="<?= base_url('pub.lcengine/upload/media/' . $data['content_edit']['image']) ?>" >
                                    <?php endif; ?>
                                </a>

                                <input id="photo_upload" name="userfile" style="display: none;" type="file" class="btn btn-info" >
                                <?php if ($this->uri->segment(3) == 'index'): ?>
                                    <input type="checkbox" name="delete_image" /> Delete Image
                                <?php endif; ?>

                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" >
                            <label>Description : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                                <textarea style="width: 100%; height: 100px;" name="description"><?= (isset($data['content_edit']['description'])) ? $data['content_edit']['description'] : '' ?></textarea>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <div class="input-group">
                                <input type="submit" class="btn btn-info" value="Save"/> &nbsp;
                                <a href="<?= base_url('index.php/backend/cmenu') ?>" class="btn btn-default" >Clear</a>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->


                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <?= form_close() ?>

            </div><!-- /.col (left) -->

        </div>

    </section><!-- /.content -->

    <script type="text/javascript">
        $(function() {
            $("#table_top").dataTable();
            $("#table_bottom").dataTable();
            $("#table_right").dataTable();
            $("#table_left").dataTable();


            $("a#photo_profile").click(function() {
                $("input#photo_upload").trigger("click");
            });

            $("input#photo_upload").change(function() {

                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) {
                    return;
                }

                if (/^image/.test(files[0].type)) {

                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);

                    reader.onloadend = function() {

                        $('a#photo_profile > img').remove();
                        var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                        $('a#photo_profile').append(html);

                    }

                }

            });

            hide();

            var type = $('select[name="type"]').val();
            $('div#' + type).show();

            /* Select */
            $('select[name="type"]').change(function() {
                hide();
                $('div#' + $(this).val()).show();
            });

            /* Tab set session */
            $('.anchor-tab').click(function() {
                $.get("<?= site_url('backend/cmenu/save_last_tab') ?>/" + $(this).attr('data-tab'));
                $('#title-menu').text($(this).text());
            });
            
            /* Menu aktif */
            $('#title-menu').text($('.tab-header').find('.active a').text());
            

        });


        function hide() {
            $('div#post').hide();
            $('div#category').hide();
            $('div#raw').hide();
            $('div#url').hide();
        }
    </script>



</aside>

