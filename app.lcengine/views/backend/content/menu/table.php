<div class="box-body table-responsive">
    <table id="table_<?= $data['position'] ?>" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th width="0%" style="display: none;" >ID</th>
                <th width="40%" >Menu</th>
                <th  width="20%" >Parent</th>
                <th width="10%" >Type</th>
                <th width="10%" >Template</th>
                <th width="10%" >Order</th>
                <th width="10%" >Action</th>
            </tr>
        </thead>
        <tbody>
            <?= $this->menu_backend->make($data['content']) ?>
        </tbody>
    </table>
</div><!-- /.box-body -->

<script type="text/javascript">
    $(document).ready(function() {
        $('select.parent').change(function() {
            var id_menu = $(this).parent().find('input[name="id_menu"]').val();
            var id_parent = this.value;
            
            window.location.href = "<?= site_url('backend/cmenu/parent_order') ?>/"+id_menu+'/'+id_parent;
        });
    });
</script>