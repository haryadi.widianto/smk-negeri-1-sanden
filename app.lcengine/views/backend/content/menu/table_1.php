<div class="box-body table-responsive">
    <table id="table_<?= $data['position'] ?>" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th width="0%" style="display: none;" >ID</th>
                <th width="40%" >Menu</th>
                <th  width="20%"  >Parent</th>
                <th width="10%" >Type</th>
                <th width="10%" >Template</th>
                <th width="10%" >Order</th>
                <th width="10%" >Action</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($data['content'])): ?>
                <?php foreach ($data['content'] as $key => $item): ?>
                    <tr>
                        <td style="display: none;" ><?= $item->id ?></td>
                        <td><?= $item->menu ?></td>
                        <td>
                            <input type="hidden" name="id_menu" value="<?= $item->id ?>" />
                            <select class="form-control parent" >
                                <option value="0" <?= ($item->id == 0) ? 'selected="selected"' : '' ?>>-- No Parent --</option>
                                <?php if (!empty($data['content'])): ?>
                                    <?php foreach ($data['content'] as $k => $value) : ?>
                                        <?php if ($item->id != $value->id): ?>
                                            <option value="<?= $value->id ?>" <?= ($value->id == $item->parents) ? 'selected="selected"' : '' ?> ><?= $value->menu ?></option>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td><?= $item->type ?></td>
                        <td><?= $item->template ?></td>
                        <td align="center">
                            <div class="btn-group">
                                <a href="<?= base_url("index.php/backend/cmenu/down/$item->id/$item->position") ?>" class="btn btn-info btn-xs <?= ($key + 1 == count($data['content'])) ? 'disabled' : '' ?>"   ><i class="fa fa-arrow-down"></i></a>
                                <a href="<?= base_url("index.php/backend/cmenu/up/$item->id/$item->position") ?>" class="btn btn-info btn-xs <?= ($key == 0) ? 'disabled' : '' ?> "  ><i class="fa fa-arrow-up"></i></a>
                            </div>
                        </td>

                        <td align="center">
                            <div class="btn-group">
                                <a href="<?= base_url('index.php/backend/cmenu/index/' . $item->id) ?>" class="btn btn-info  btn-xs" ><i class="fa fa-pencil"></i></a>
                                <a href="<?= base_url("index.php/backend/cmenu/destroy/$item->id/false") ?>" class="btn btn-danger  btn-xs"><i class="fa fa-times"></i></a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
</div><!-- /.box-body -->

<script type="text/javascript">
    $(document).ready(function() {
        $('select.parent').change(function() {
            var id_menu = $(this).parent().find('input[name="id_menu"]').val();
            var id_parent = this.value;
            
            window.location.href = "<?= site_url('backend/cmenu/parent_order') ?>/"+id_menu+'/'+id_parent;
        });
    });
</script>