<aside class="right-side">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <?= get_alert() ?>

                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3>
                    </div>
                    <div class="box-body">

                        <?= form_open($data['save-link']) ?>

                        <div class="form-group">
                            <label>Product Category : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-barcode"></i>
                                </div>
                                <input name="product_category"  value="<?= set_value('product_category') ?>" type="text" class="form-control" />
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Parent Category : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-sitemap"></i>
                                </div>
                                <select name="id_parent" class="form-control">
                                    <option value="0">Uncategory</option>
                                    <?php foreach ($data['option'] as $key => $item): ?>
                                        <option value="<?= $item->id_product_category ?>"><?= $item->product_category ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Description : </label>
                            <div class='box-body pad' style="margin-left: -9px;margin-right: -9px;">
                                <textarea id="editor1" name="description" rows="15" cols="90"></textarea>                        
                            </div>
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <div class="input-group">
                                <input type="submit" class="btn btn-info" value="<?= $data['save-title'] ?>"/>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <?= form_close() ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->


        </div><!-- /.row -->                    

    </section><!-- /.content -->
</aside><!-- /.right-side -->

<script type="text/javascript">
    $(document).ready(function() {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
    });
</script>