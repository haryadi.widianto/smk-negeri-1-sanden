<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                <?= get_alert() ?>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3><br>
                        <div style="float: right; margin-right: 10px;">
                            <a href="<?= $data['add-link'] ?>"><button type="button" class="btn btn-info"><i class="fa fa-pencil"></i> <?= $data['add-title'] ?></button></a>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" >ID</th>
                                    <th width="20%" >Product Category</th>
                                    <th width="20%" >Parent Category</th>
                                    <th width="15%" >Created By</th>
                                    <th width="15%" >Created At</th>
                                    <th width="15%" >Updated At</th>
                                    <th width="10%" >Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($data['content'] as $key => $item): ?>
                                    <tr>
                                        <td><?= $item->id_product_category ?></td>
                                        <td><?= $item->product_category ?></td>
                                        <td><?= $item->id_parent ?></td>
                                        <td><?= $item->name ?></td>
                                        <td><?= $item->created_at ?></td>
                                        <td><?= $item->updated_at ?></td>

                                        <td align="center">
                                            <div class="btn-group">
                                                <a href="<?= base_url('index.php/backend/cproduct_category/edit/' . $item->id_product_category) ?>" class="btn btn-info" ><i class="fa fa-pencil"></i></a>
                                                <a href="<?= base_url("index.php/backend/cproduct_category/destroy/$item->id_product_category/false") ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>

                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section><!-- /.content -->

    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        });
    </script>

</aside>

