<style type="text/css">
    .tnl_table  tr  th,td{
        height: 30px;
    }
</style>

<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                <?= get_alert() ?>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3><br>
                        <div style="float: right; margin-right: 10px;">
                            <a href="<?= $data['add-link'] ?>"><button type="button" class="btn btn-info"><i class="fa fa-pencil"></i> <?= $data['add-title'] ?></button></a>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%"  >Image</th>
                                    <th width="38%" >Event</th>
                                    <th width="21%" >Time & Location</th>
                                    <th width="21%" >Activity</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($data['content'] as $key => $item): ?>
                                    <tr>
                                        <td>
                                            <a href="#" class="thumbnail" id="photo_profile">
                                                <img  style="width: 100%; max-height: 100px;" src="<?= set_image($item->image) ?>">
                                            </a>
                                        </td>
                                        <td>
                                            <div style="border-bottom: #c9c9c9; margin-bottom: 10px;">
                                                <strong><?= $item->title ?></strong>
                                                <?php if ($item->status == 'publish'): ?> 
                                                    <small class="badge bg-blue"><i class="fa fa-check"></i> Publish</small>
                                                <?php else: ?>
                                                    <small class="badge bg-yellow"><i class="fa fa-pencil"></i> Draft</small>
                                                <?php endif; ?>
                                                <br/>
                                                <i class="fa fa-tag"></i> <?= $item->tag ?>
                                            </div>
                                            <p><?= word_limiter(strip_tags($item->description), 30) ?></p>
                                        </td>
                                        <td>
                                            <table class="tnl_table">
                                                <tr>
                                                    <th style="width: 60px;"><i class="fa fa-calendar"></i> Start</th>
                                                    <td><?= date('j F Y', strtotime($item->day_start)) ?></td>
                                                </tr>
                                                <tr>
                                                    <th style="width: 60px;"><i class="fa fa-calendar"></i> End</th>
                                                    <td><?= date('j F Y', strtotime($item->day_end)) ?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <i class="fa fa-clock-o"></i> 
                                                        <?= date('H:i', strtotime($item->time_start)) ?> - 
                                                        <?= date('H:i', strtotime($item->time_end)) ?> WIB
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <strong><i class="fa fa-map-marker"></i></strong>
                                                        <?= $item->location ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table class="tnl_table">
                                                <tr>
                                                    <th><i class="fa fa-user"></i> </th>
                                                    <td><?= $item->name ?></td>
                                                </tr>
                                                <tr>
                                                    <th ><i class="fa fa-pencil"></i></th>
                                                    <td><?= date('j M Y - H:i', strtotime($item->created_at)) ?></td>
                                                </tr>
                                                <tr>
                                                    <th ><i class="fa fa-repeat"></i></th>
                                                    <td>
                                                        <?=($item->updated_at != '0000-00-00 00:00:00')? date('j M Y - H:i', strtotime($item->updated_at)) :'-' ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th ><i class="fa fa-cogs"></i></th>
                                                    <td >
                                                        <div class="btn-group btn-xs">
                                                            <a href="<?= base_url('index.php/backend/cevent/edit/' . $item->id) ?>" class="btn btn-info  btn-xs" ><i class="fa fa-pencil"></i> Edit</a>
                                                            <a href="<?= base_url("index.php/backend/cevent/destroy/$item->id/false") ?>" class="btn btn-danger  btn-xs"><i class="fa fa-times"></i> Delete</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section><!-- /.content -->

    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        });
    </script>



</aside>

