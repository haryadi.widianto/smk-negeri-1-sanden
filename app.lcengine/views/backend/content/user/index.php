<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                <?= get_alert() ?>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3><br>
                        <div style="float: right; margin-right: 10px;">
                            <a href="<?= $data['add-link'] ?>"><button type="button" class="btn btn-info"><i class="fa fa-pencil"></i> <?= $data['add-title'] ?></button></a>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>User Level</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($data['content'] as $key => $item): ?>
                                    <tr>
                                        <td><?= $item->name ?></td>
                                        <td><?= $item->username ?></td>
                                        <td><?= ucwords($item->level) ?></td>
                                        <td><?= $item->email ?></td>
                                        <td align="center" >
                                            <div class="btn-group">
                                                <?php if ($item->status == 'active'): ?>
                                                    <small class="badge pull-right bg-green"><i class="fa fa-sun-o"></i> Active</small>
                                                <?php else: ?>
                                                    <small class="badge pull-right bg-red"><i class="fa fa-circle"></i> Deactive</small>
                                                <?php endif; ?>
                                            </div>
                                        </td>
                                        <td align="center">
                                            <div class="btn-group btn-group-xs">
                                                <a href="<?= base_url('index.php/backend/cuser/edit/' . $item->id_user) ?>" class="btn btn-info btn-xs" ><i class="fa fa-pencil"></i></a>
                                                <a href="<?= base_url('index.php/backend/cuser_profile/view/' . $item->id_user) ?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
                                                <a href="<?= base_url("index.php/backend/cuser/destroy/$item->id_user/false") ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section><!-- /.content -->

    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        });
    </script>

</aside>

