<aside class="right-side">
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                
                <?= get_alert() ?>

                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3>
                    </div>
                    <div class="box-body">
                        
                        <?= form_open($data['save-link']) ?>

                        <div class="form-group">
                            <label>Name : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-credit-card"></i>
                                </div>
                                <input name="name" value="<?= $data['content']['name'] ?>" type="text" class="form-control"  />
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Email : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <input name="email" value="<?= $data['content']['email'] ?>" type="email" class="form-control"  />
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Username : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </div>
                                <input name="username"  value="<?= $data['content']['username'] ?>" type="text" class="form-control" />
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>New Password : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input name="password"  value="" type="password" class="form-control" />
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Level : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-list-ol"></i>
                                </div>
                                <select name="level" class="form-control">
                                    <option value="admin" <?= ($data['content']['level'] == 'admin')?'selected="selected"':'' ?> >Admin</option>
                                    <option value="staf"  <?= ($data['content']['level'] == 'staf')?'selected="selected"':'' ?>  >Staf</option>
                                    <option value="teacher"  <?= ($data['content']['level'] == 'teacher')?'selected="selected"':'' ?>  >Teacher</option>
                                    <option value="student"  <?= ($data['content']['level'] == 'student')?'selected="selected"':'' ?>  >Student</option>
                                    <option value="pre-student"  <?= ($data['content']['level'] == 'pre-student')?'selected="selected"':'' ?>  >Pre-Student</option>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Status : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-star-half-empty"></i>
                                </div>
                                <select name="status" class="form-control">
                                    <option value="active" <?= ($data['content']['status'] == 'active')?'selected="selected"':'' ?> >Active</option>
                                    <option value="deactive" <?= ($data['content']['status'] == 'deactive')?'selected="selected"':'' ?> >Deactive</option>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <div class="input-group">
                                <input type="submit" class="btn btn-info" value="<?= $data['save-title'] ?>"/>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <?= form_close() ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->


        </div><!-- /.row -->                    

    </section><!-- /.content -->
</aside><!-- /.right-side -->