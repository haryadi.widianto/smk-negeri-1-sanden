<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-xs-8">

                <?= get_alert() ?>

                <div class="box">

                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    Album List <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu album_list">
                                    <?php if (!empty($data['album'])): ?>
                                        <?php foreach ($data['album'] as $key => $value) : ?>
                                            <li role="presentation" data-value="<?= $value->id ?>" >
                                                <a role="menuitem" data-toggle="tab" tabindex="-1" href="#tab_<?= $value->id ?>">
                                                    <?= $value->title ?>  <?= ($value->set_at_home == 'set') ? '<span class="badge bg-blue"> Set at Home</span>' : '' ?>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </li>
                            <li class="pull-right dropdown">
                                <a href="#"   data-toggle="dropdown"  class="dropdown-toggle"><i class="fa fa-gear"></i> Action</a>
                                <ul class="dropdown-menu">
                                    <li role="presentation">
                                        <a id="edit_album_link" href="" >Edit</a>
                                    </li>
                                    <li role="presentation">
                                        <a id="delete_album_link" href="" >Delete</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <?php if (!empty($data['album'])): ?>
                                <?php foreach ($data['album'] as $key => $value) : ?>
                                    <div class="tab-pane <?= ($key == 0) ? 'active' : '' ?>" id="tab_<?= $value->id ?>">
                                        <?php $send['data']['content'] = $data['tab_' . $value->id]; ?>
                                        <?php $send['data']['position'] = $value->id; ?>
                                        <?= $this->load->view('backend/content/gallery/table', $send) ?>
                                    </div><!-- /.tab-pane -->
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div><!-- /.box -->
            </div>

            <div class="col-md-4">

                <?php $target = ($this->uri->segment(3) == 'index') ? 'update/' . $this->uri->segment(4) : 'storage'; ?>

                <?= form_open_multipart('backend/cgallery/' . $target) ?>

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title"><?= ($this->uri->segment(3) == 'index') ? 'Edit Image' : 'Add Image' ?></h3>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label>Image Title : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-pencil"></i>
                                </div>
                                <input value="<?= (isset($data['content_edit']['title'])) ? $data['content_edit']['title'] : '' ?>" name="title" type="text" class="form-control" placeholder="Image Title">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Type : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-picture-o"></i>
                                </div>
                                <select name="type" class="form-control">
                                    <option value="image" <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'image') ? "selected='selected'" : '' ?>  >Image</option>
                                    <option value="video" <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'video') ? "selected='selected'" : '' ?>  >Video</option>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Album : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-folder-open"></i>
                                </div>
                                <select name="id_album" class="form-control">
                                    <?php if (!empty($data['album'])): ?>
                                        <?php foreach ($data['album'] as $key => $value) : ?>
                                            <option value="<?= $value->id ?>" <?= (isset($data['content_edit']['id_album']) && $data['content_edit']['id_album'] == $value->id) ? "selected='selected'" : '' ?>  >
                                                <?= $value->title ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" >
                            <label>Description : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                                <textarea style="width: 100%; height: 100px;" name="description"><?= (isset($data['content_edit']['description'])) ? $data['content_edit']['description'] : '' ?></textarea>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="image-form">
                            <label>Image : </label>
                            <div class="input-group" style="width: 100%">

                                <a href="#" class="thumbnail" id="photo_profile" style="width: 100%" >
                                    <?php if (!isset($data['content_edit']['image']) || $data['content_edit']['image'] == ''): ?>
                                        <img style="width: 100%; height: 200px;" data-src="holder.js/300x200" alt="300x200" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQQAAAC0CAYAAABytVLLAAAK/ElEQVR4Xu2aB4tUSxdFa8wJA+acc86K+YermBWMmDBizo6KOX/fLrhNzzgzbz/RR8NeF4ZpZk7frrNO1boVuqu7u/tn4YIABCDwfwJdCIF+AAEINAQQAn0BAhBoEUAIdAYIQAAh0AcgAIFfCTBDoFdAAALMEOgDEIAAMwT6AAQgMAABlgx0DwhAgCUDfQACEGDJQB+AAARYMtAHIAABhwB7CA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIXRYob99+1Zu375dnj59Wr5+/VpGjhxZpk2bVubOnVsGDRrUau2LFy/KrVu3yvv378vQoUNrzKJFi0pXV1cr5s6dO+XBgwfly5cvZcSIEWXevHllxowZ/zrjT58+lStXrpTu7u4yZMiQsnfv3h730P3V5pcvX5bPnz+XYcOGlUmTJtX2KL65/lR7/nUCvMEmgBBsVP9N4MWLF6sMNLA1iD9+/Fg/ePbs2WXp0qX1tQbeuXPn6uvhw4fXAf/z58864DUIdd27d69cv379l/usXbu2TJ482U7m2bNn5erVq1VOuvoSwpkzZ8qrV69qW8aPH1/FofgpU6aUNWvW/NH22A0n8LcIIITfwvZ33qTBf+zYsXrzzZs3l3HjxpVHjx7Vp7NmB3oy6/epU6fK27dv6+CXBCQIxWgwrl69ur7/yJEj9WndCKARhGI2bdpkJ3D48OEqgenTp9cZSW8haPZw9OjRer/t27eX0aNH9xDWvn37yuDBg/9Ye+yGE/hbBBDCb2H7O2/S4NJSQANIA1DXu3fvysmTJ+vrPXv2lO/fv9cBqJjdu3fX370vLSNOnDhR5aEBqdlG+8DV3548eVKf/HqqayBroCtG79NnrF+/vkycOLEuBbRcef36dZ2V9BZCe/uawd98vtqlNmq28E/t6SuPv0OZuw5EACF0eP+4efNm0dpbA3fXrl1VGOfPny9jxowpK1asKA8fPqwDWMuAqVOn1myaGC05du7cWf+mJcX+/ft7PMnPnj1bp/dz5swpS5YsKRcuXCjPnz8vM2fOLMuXL+9Bplmm9BaC7ithSQK6h/Yo1F79NLMRtz0dXoqI5iGEDi6z9hK0p6BLg1+DrVlCSBB68v748aOVwfz588vChQvrHoTep+m7nv7NdeDAgRrfLEc0I9BgllD0PslHm5hbt27tsRmo9/cnBP1PS51Lly6VN2/etD5Ls4uVK1fWDUa3PR1cipimIYQOLbUGvqb0egLPmjWrLFu2rLa0EYJeN09knSRoMGuJoBmBNvj6EsLBgwfr4G+EoHs8fvy4XL58uUVB+wt6sve+BhLCjRs3yt27d6tMxo4dW8Wg/QsJasGCBf0Koa/2dGg5YpqFEDqw1FoGSAa6tH5fvHhxq5XN07Z96i5pHDp0qOjIcuPGjXXQa1nR35Jh27ZtdcmhS7HagNR7NaPQ/9qPLpsP7k8Iko9OGdSeHTt21CNQyUCbo5qN6H6aibjt6cByRDUJIXRYuTXwNHg0yDUD0Pq+/dLpgk4Z2jcM9X8JQUuIDRs21Ce1BmR7THOCocGuzb/mOw3Xrl0r9+/fr5uTkkNfn6n79yeE5vRCM4MtW7a0mtqccmjZoBmH254OK0dccxBCB5VcA/L48eP1Cdts9PVunkShUwbFaONPG4DNYNVg15JB+wu6z4cPH+r3APR9AG3yaVmhtb1OEHTp5OD06dN1nb9u3br6WvfQwNZsof3qTwjtMxZ9tmYKapvaqLZqxjJhwgSrPR1UitimIIQOKn37/oAGafs3E9XMVatW1adte5wGrp7+mp63nw40Me1fcNJrzSA0QCUfzTQkDT3FdcypI0b96PsP2ktQvE4eNLA1+9DegP4mqejSJqfupftoWdDsIUg0koLapg1K5fFP7emgMkQ3BSF0UPmb6Xd/TWq+G6D/a4BpI08y0LpdMtAmXvv6X/fTj77JOGrUqHqS0HxLUd9i1P80oPUU1yVJ6NRB92yWDjqqlBD6urS3oT0OxWv2of0EiUMykzT0eZqtNNdA7emgMkQ3BSFEl5/kIdCTAEKgR0AAAi0CCIHOAAEIIAT6AAQg8CsBZgj0CghAgBkCfQACEGCGQB+AAAQGIMCSge4BAQiwZKAPQAACLBnoAxCAAEsG+gAEIOAQYA/BoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwC/wNER+9UOtmw8QAAAABJRU5ErkJggg==" >
                                    <?php else: ?>
                                        <img style="width: 100%; height: 200px;" data-src="holder.js/300x200" alt="300x200" src="<?= set_image($data['content_edit']['image'], $data['content_edit']['type']) ?>" >
                                    <?php endif; ?>
                                </a>

                                <input id="photo_upload" name="userfile" style="display: none;" type="file" class="btn btn-info" >
                                <?php if ($this->uri->segment(3) == 'index'): ?>
                                    <input type="checkbox" name="delete_image" /> Delete Image
                                <?php endif; ?>

                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="video-form">
                            <label>Youtube Video ID : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-youtube-play"></i>
                                </div>
                                <input value="<?= (isset($data['content_edit']['image'])) ? $data['content_edit']['image'] : '' ?>" name="video" type="text" class="form-control" placeholder="Youtube Video URL">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <div class="input-group">
                                <input type="submit" class="btn btn-info" value="Save"/> &nbsp;
                                <a href="<?= base_url('index.php/backend/cgallery') ?>" class="btn btn-default" >Clear</a>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->


                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <?= form_close() ?>


                <?php $target = ($this->uri->segment(4) == 'album') ? 'update/' . $this->uri->segment(5) : 'storage'; ?>

                <?= form_open_multipart('backend/calbum/' . $target) ?>

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title"><?= ($this->uri->segment(3) == 'index') ? 'Edit Album' : 'Add Album' ?></h3>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label>Album Title : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-folder-open"></i>
                                </div>
                                <input value="<?= (isset($data['edit_album']['title'])) ? $data['edit_album']['title'] : '' ?>" name="title" type="text" class="form-control" placeholder="Album Title">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" >
                            <label>Description : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-file-text-o"></i>
                                </div>
                                <textarea style="width: 100%; height: 100px;" name="description"><?= (isset($data['edit_album']['description'])) ? $data['edit_album']['description'] : '' ?></textarea>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->  

                        <div class="form-group">
                            <label>Set At Home : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa  fa-home"></i>
                                </div>
                                <select name ="set_at_home" class="form-control">
                                    <option value="set" <?= (isset($data['edit_album']['set_at_home']) && $data['edit_album']['set_at_home'] == 'set') ? "selected='selected'" : '' ?>  >Set at Home</option>
                                    <option value="unset" <?= (isset($data['edit_album']['set_at_home']) && $data['edit_album']['set_at_home'] == 'unset') ? "selected='selected'" : '' ?> >Unset from Home</option>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <div class="input-group">
                                <input type="submit" class="btn btn-info" value="Save"/> &nbsp;
                                <a href="<?= base_url('index.php/backend/cgallery') ?>" class="btn btn-default" >Clear</a>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <?= form_close() ?>

            </div><!-- /.col (left) -->

        </div>

    </section><!-- /.content -->

    <script type="text/javascript">
        $(function() {

            /* Global variable */
            var edit_album_url = "<?= site_url('backend/cgallery/index/album/') ?>";
            var delete_album_url = "<?= site_url('backend/calbum/destroy/') ?>";

            $("#example1").dataTable();
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": false,
                "bInfo": true,
                "bAutoWidth": false
            });

            $("a#photo_profile").click(function() {
                $("input#photo_upload").trigger("click");
            });

            $("input#photo_upload").change(function() {

                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) {
                    return;
                }

                if (/^image/.test(files[0].type)) {

                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);

                    reader.onloadend = function() {

                        $('a#photo_profile > img').remove();
                        var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                        $('a#photo_profile').append(html);

                    }

                }

            });

            $('span.abv').tooltip();

            $('.album_list li').click(function() {
                var value_ = $(this).attr('data-value');
                $('a#edit_album_link').attr('href', edit_album_url + '/' + value_);
                $('a#delete_album_link').attr('href', delete_album_url + '/' + value_);
            });

            /* Type */
            var type = $('select[name="type"] option:selected').val();
            if (type == 'image') {
                $('div#image-form').show();
                $('div#video-form').hide();
            } else if (type== 'video') {
                $('div#image-form').hide();
                $('div#video-form').show();
            }

            $('select[name="type"]').change(function() {
                if (this.value == 'image') {
                    $('div#image-form').show();
                    $('div#video-form').hide();
                } else if (this.value == 'video') {
                    $('div#image-form').hide();
                    $('div#video-form').show();
                }
            });

        });


    </script>



</aside>

