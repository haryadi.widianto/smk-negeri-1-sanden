<div class="box-body">
    <div class="row" style="margin-left: 0px ! important; margin-right: 0px ! important;">
        <?php if (!empty($data['content'])): ?>
            <?php foreach ($data['content'] as $key => $value) : ?>
                <div class="col-md-4" >
                    <div class="thumbnail">
                        <span style="cursor: pointer;" class="abv" rel="tooltip" data-placement="right" data-original-title="<?= $value->title ?>" ><?= word_limiter($value->title, 3) ?></span>
                        <img  alt="260x180" src="<?= set_image($value->image, $value->type) ?>" style="width: 260px; height: 180px;">
                        <div class="btn-group btn-xs" style="margin-left: -5px;">
                            <a href="<?= site_url('backend/cgallery/index/' . $value->id) ?>" class="btn btn-info  btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                            <a href="<?= site_url('backend/cgallery/destroy/' . $value->id) ?>" class="btn btn-danger  btn-xs"><i class="fa fa-times"></i> Delete</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div><!-- /.box-body -->