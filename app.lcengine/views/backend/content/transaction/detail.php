<style type="text/css">
    .tbody-session tr td, th.product{
        text-align: center;
    }

    .tbody-session tr th.price{
        padding-left: 25px;
    }
</style>


<aside class="right-side">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">Advanced Elements</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?= form_open_multipart($data['save-link']) ?>

        <div class="row">
            <div class="col-md-8">
                <?= get_alert() ?>

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3>
                    </div>
                    <div class="box-body">


                        <table class="table">
                            <tbody class="tbody-session">
                                <tr>
                                    <th class="product" style="width: 25%" >Product</th>
                                    <th style="width: 35%" >Description</th>
                                    <th style="width: 10%" >Quantity</th>
                                    <th class="price" style="width: 15%" >Price</th>
                                    <th class="price" style="width: 15%" >Total</th>
                                </tr>

                                <?php $item = 0; ?>
                                <?php $price = 0; ?>

                                <?php if (!empty($data['content'])): ?>
                                    <?php foreach ($data['content'] as $key => $value) : ?>

                                        <tr>
                                            <td>
                                                <?php if ($value->image != ''): ?>
                                                    <img width="60" src="<?= base_url('pub.lcengine/upload/media/' . $value->image) ?>" alt=""><br>
                                                <?php else: ?>
                                                    <img width="60" src="<?= base_url("pub.lcengine/upload/media/noimage.png") ?>" alt=""><br>
                                                <?php endif; ?>
                                                <strong><?= $value->product ?></strong>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <?= word_limiter($value->description, 25); ?>
                                            </td>
                                            <td>
                                                <?= $value->qty ?>
                                                <?php $item += $value->qty; ?>
                                            </td>
                                            <td>
                                                Rp. <?= $value->price ?>
                                            </td>
                                            <td>
                                                Rp. <?= $value->total ?>
                                                <?php $price += $value->total; ?>

                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                <?php endif; ?>

                                <tr>
                                    <td colspan="2" style="text-align:right"><strong>Total Item </strong></td>
                                    <td  style="text-align:center"><strong><?= $item ?></strong></td>
                                    <td  style="text-align:right"><strong>Total Price </strong></td>
                                    <td ><strong>Rp. <?php echo $price; ?></strong></td>
                                </tr>


                            </tbody>
                        </table>



                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->

            <?php $sub_content = $data['sub-content']; ?>

            <div class="col-md-4">

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Detail Session</h3>
                    </div>
                    <div class="box-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td><strong>Name</strong></td>
                                    <td>
                                        <?= $sub_content['name'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>No Resensi</strong></td>
                                    <td>
                                        <h4>
                                            <span class="label label-warning">
                                                <?= $sub_content['no_resensi'] ?>
                                            </span>
                                        </h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Session Date</strong></td>
                                    <td>
                                        <?= $sub_content['session_date'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Submit Date</strong></td>
                                    <td>
                                        <?= $sub_content['submit_date'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td><strong>Deliver Date</strong></td>
                                    <td>
                                        <?= $sub_content['deliver_date'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>

                                        <?php if ($sub_content['deliver_date'] == NULL): ?>

                                            <a href="<?= site_url('backend/ctransaction/deliver/' . $sub_content['id_session']) ?>">
                                                <button type="button" class="btn btn-info">
                                                    <i class="fa fa-truck"></i> Deliver
                                                </button>
                                            </a>

                                        <?php else: ?>

                                            <button type="button" class="btn btn-success">
                                                <i class="fa fa-thumbs-o-up"></i> Delivered
                                            </button>

                                        <?php endif; ?>

                                        <a href="<?= site_url('backend/ctransaction/') ?>">
                                            <button type="button" class="btn btn-default">
                                                <i class="fa fa-reply"></i> Back
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div><!-- /.row -->       
        <?= form_close() ?>
    </section><!-- /.content -->
</aside><!-- /.right-side -->