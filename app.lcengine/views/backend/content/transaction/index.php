<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Tables</a></li>
            <li class="active">Data tables</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-xs-12">

                <?= get_alert() ?>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3><br>
                        <button class="btn btn-warning pull-right" style="margin-right: 10px;" id="daterange-btn">
                            <i class="fa  fa-bar-chart-o"></i> Create Report
                            <i class="fa fa-caret-down"></i>
                        </button>
                        <form method="post" id="report" action="<?= base_url('index.php/backend/ctransaction/report') ?>">
                            <input type="hidden" name="start_date" />
                            <input type="hidden" name="end_date" />
                        </form>
                    </div><!-- /.box-header -->

                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="25%" >Account</th>
                                    <th width="15%" >Total Item</th>
                                    <th width="20%" >Total Price</th>
                                    <th width="10%" >Session Date</th>
                                    <th width="10%" >Submit Date</th>
                                    <th width="10%" >Deliver Date</th>

                                    <th width="10%" >Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($data['content'] as $key => $item): ?>
                                    <tr>
                                        <td align="left">
                                            <?= $item['status'] ?><br/>
                                            <strong>Name :</strong><?= $item['name'] ?><br/>
                                            <strong>No Ref :</strong><?= $item['no_resensi'] ?>
                                        </td>
                                        <td><?= $item['total_item'] ?></td>
                                        <td><?= $item['total_price'] ?></td>
                                        <td><?= $item['session_date'] ?></td>
                                        <td><?= $item['submit_date'] ?></td>
                                        <td><?= $item['deliver_date'] ?></td>
                                        <td align="center">
                                            <div class="btn-group">
                                                <a href="<?= base_url('index.php/backend/ctransaction/detail/' . $item['id_session']) ?>" class="btn btn-info" >
                                                    <i class="fa fa-search-plus"></i>
                                                </a>
                                                <a href="<?= base_url('index.php/backend/ctransaction/destroy/' . $item['id_session']) ?>" class="btn btn-danger" >
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>

                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section><!-- /.content -->

    <script type="text/javascript">


        $(function() {
            $("#example1").dataTable();
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });

        });

        $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                        'Last 7 Days': [moment().subtract('days', 6), moment()],
                        'Last 30 Days': [moment().subtract('days', 29), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                    },
                    startDate: moment().subtract('days', 29),
                    endDate: moment()
                }, function(start, end) {
                $('input[name="start_date"]').val(start.format('YYYY-MM-DD HH:mm:ss'));
                $('input[name="end_date"]').val(end.format('YYYY-MM-DD HH:mm:ss'));
                $('#report').submit();
                //$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        });

    </script>



</aside>

