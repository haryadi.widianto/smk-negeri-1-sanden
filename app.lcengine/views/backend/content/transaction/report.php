<aside class="right-side">                
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Laporan
            <small><?= date('j F Y') ?></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content invoice">                    
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> SMK Kesehatan Bantul
                    <small class="pull-right">Tanggal : <?= date('j F Y') ?></small>
                </h2>                            
            </div><!-- /.col -->
        </div>
        <!-- info row -->
       

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 20%;" >Product</th>
                            <th style="width: 10%;" >Qty</th>
                            <th style="width: 55%;" >Description</th>
                            <th style="width: 15%;" >Subtotal</th>
                        </tr>                                   
                    </thead>
                    <tbody>
                        
                        <?php $subtotal = 0; ?>
                        <?php $item = 0; ?>
                        <?php $product = 0; ?>
                        
                        <?php if (!empty($data['content'])): ?>
                            <?php foreach ($data['content'] as $key => $value) : ?>
                                <tr>
                                    <td><?= $value->product ?></td>
                                    <td><?= $value->total ?></td>
                                    <td><?= $value->description ?></td>
                                    <td>Rp. <?= $value->subtotal ?></td>
                                </tr>
                                
                                <?php $subtotal += $value->subtotal; ?>
                                <?php $item += $value->qty; ?>
                                <?php $product = count($data['content']); ?>
                            <?php endforeach; ?>
                        <?php else: ?>
                                <tr><td colspan="4" style="text-align: center;">Data tidak tersedia</td></tr>
                        <?php endif; ?>

                    </tbody>
                </table>                            
            </div><!-- /.col -->
        </div><!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            
            <div class="col-xs-7">
                <p class="lead">Transaksi : <?= $data['date'] ?></p>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Total Transaksi</th>
                            <td>Rp. <?= $subtotal ?></td>
                        </tr>
                        <tr>
                            <th>Total Item</th> 
                            <td><?= $item ?> Item</td>
                        </tr>
                        <tr>
                            <th>Total Product</th>
                            <td><?= $product ?> Product</td>
                        </tr>
                    </table>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">
                <a href="<?= site_url('backend/ctransaction/') ?>" class="btn btn-primary pull-right" style="margin-right: 5px;">
                    <i class="fa fa-undo"></i> Back to List Transaction
                </a>
            </div>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->