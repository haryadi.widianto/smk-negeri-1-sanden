<script type="text/javascript">

    $(document).ready(function() {

        /* Global variable */
        var selected = parseInt("<?= (isset($data['content']['post_at_home'])) ? $data['content']['post_at_home'] : '' ?>");
        var description = parseInt("<?= (isset($data['content']['post_at_home'])) ? $data['content']['description'] : '' ?>");

        /* Logo */
        $("a#logo").click(function() {
            $("input#logo_upload").trigger("click");
        });

        $("input#logo_upload").change(function() {

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) {
                return;
            }

            if (/^image/.test(files[0].type)) {

                var reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onloadend = function() {

                    $('a#logo > img').remove();
                    var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                    $('a#logo').append(html);

                }

            }

        });

        /* Favicon */
        $("a#favicon").click(function() {
            $("input#favicon_upload").trigger("click");
        });

        $("input#favicon_upload").change(function() {

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) {
                return;
            }

            if (/^image/.test(files[0].type)) {

                var reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onloadend = function() {

                    $('a#favicon > img').remove();
                    var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                    $('a#favicon').append(html);

                }

            }

        });

        /* Hook home post */
        /* -- Default */
        var value_ = $(this).find('option:selected').val();
        $.ajax({
            url: "<?php echo site_url('backend/ccontact_setting/get_post_bycategory'); ?>/" + value_,
            async: false,
            type: "POST",
            success: function(data) {

                /* Post Combo */
                $('div#post_at_home').remove();
                $('div#post_description').remove();

                $('div#post_category').after(data);

                $('select[name="post"] option').each(function() {
                    if ($(this).val() == selected) {
                        $(this).attr('selected', 'selected');
                    }
                });

                $('select[name="description"] option').each(function() {
                    if ($(this).val() == description) {
                        $(this).attr('selected', 'selected');
                    }
                });

            }
        });

        $('select[name="post_category"]').change(function() {

            var value_ = $(this).find('option:selected').val();
            $.ajax({
                url: "<?php echo site_url('backend/ccontact_setting/get_post_bycategory'); ?>/" + value_,
                async: false,
                type: "POST",
                success: function(data) {

                    /* Post Combo */
                    $('div#post_at_home').remove();
                    $('div#post_description').remove();

                    $('div#post_category').after(data);
                    $('select[name="post"] option').each(function() {
                        if ($(this).val() == selected) {
                            $(this).attr('selected', 'selected');
                        }
                    });

                    $('select[name="description"] option').each(function() {
                        if ($(this).val() == description) {
                            $(this).attr('selected', 'selected');
                        }
                    });

                }
            });
        });




        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();

    });
</script>

<aside class="right-side">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <?= form_open_multipart($data['save-link']) ?>

        <div class="row">
            <div class="col-md-12">
                <?= get_alert() ?>

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label>Form Title : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-angle-double-right"></i>
                                </div>
                                <input name="form_title" value="<?= (isset($data['content']['form_title'])) ? $data['content']['form_title'] : '' ?>" type="text" class="form-control" placeholder="Form Title">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Button Title : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-angle-double-right"></i>
                                </div>
                                <input name="button_title" value="<?= (isset($data['content']['button_title'])) ? $data['content']['button_title'] : '' ?>" type="text" class="form-control" placeholder="Button Title">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="post_category">
                            <label>Post Category : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-barcode"></i>
                                </div>
                                <select name="post_category" class="form-control">
                                    <option>Uncategory</option>
                                    <?php foreach ($data['post_category'] as $key => $item): ?>
                                        <option value="<?= $item->id_post_category ?>"  <?= ((isset($data['content']['post_category'])) && ($data['content']['post_category'] == $item->id_post_category)) ? 'selected="selected"' : '' ?> >
                                            <?= $item->post_category ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->
                        
                        <div class="form-group">
                            <label>Address : </label>
                            <div class='box-body pad' style="margin-left: -9px;margin-right: -9px;">
                                <textarea id="editor1" name="address" rows="15" cols="90"><?= (isset($data['content']['address'])) ? $data['content']['address'] : '' ?></textarea>                        
                            </div>
                        </div><!-- /.form group -->
                        
                        <div class="form-group">
                            <label>Telephone : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-angle-double-right"></i>
                                </div>
                                <input name="telp" value="<?= (isset($data['content']['telp'])) ? $data['content']['telp'] : '' ?>" type="text" class="form-control" placeholder="Telephone">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->
                        
                        <div class="form-group">
                            <label>Fax : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-angle-double-right"></i>
                                </div>
                                <input name="fax" value="<?= (isset($data['content']['fax'])) ? $data['content']['fax'] : '' ?>" type="text" class="form-control" placeholder="Fax">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->
                        
                        <div class="form-group">
                            <label>Email : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-angle-double-right"></i>
                                </div>
                                <input name="email" value="<?= (isset($data['content']['email'])) ? $data['content']['email'] : '' ?>" type="text" class="form-control" placeholder="Email">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Langitude Area : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-angle-double-right"></i>
                                </div>
                                <input name="lang_area" value="<?= (isset($data['content']['lang_area'])) ? $data['content']['lang_area'] : '' ?>" type="text" class="form-control" placeholder="Langitude Area">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Longitude Area : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-angle-double-right"></i>
                                </div>
                                <input name="long_area" value="<?= (isset($data['content']['long_area'])) ? $data['content']['long_area'] : '' ?>" type="text" class="form-control" placeholder="Longitude Area">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Langitude Point : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-angle-double-right"></i>
                                </div>
                                <input name="lang_point" value="<?= (isset($data['content']['lang_point'])) ? $data['content']['lang_point'] : '' ?>" type="text" class="form-control" placeholder="Langitude Point">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Longitude Point : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-angle-double-right"></i>
                                </div>
                                <input name="long_point" value="<?= (isset($data['content']['long_point'])) ? $data['content']['long_point'] : '' ?>" type="text" class="form-control" placeholder="Longitude Point">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <div class="input-group">
                                <input type="submit"  class="btn btn-info" value="<?= $data['save-title'] ?>"/>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->


        </div><!-- /.row -->       

        <?= form_close() ?>

    </section><!-- /.content -->
</aside><!-- /.right-side -->