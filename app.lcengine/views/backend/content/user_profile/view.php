<script type="text/javascript">

    $(document).ready(function() {

        $("a#photo_profile").click(function() {
            $("input#photo_upload").trigger("click");
        });

        $("input#photo_upload").change(function() {

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) {
                return;
            }

            if (/^image/.test(files[0].type)) {

                var reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onloadend = function() {

                    $('a#photo_profile > img').remove();
                    var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                    $('a#photo_profile').append(html);

                }

            }

        });

    });
</script>

<aside class="right-side">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-9">

                <?= get_alert() ?>

                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3>
                    </div>
                    <div class="box-body">

                        <div class="form-group">
                            <label>Gender : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-female"></i>
                                </div>
                                <select name="gender" class="form-control" disabled="disabled" readonly="readonly">
                                    <option> Choose Gender </option>
                                    <option value="male" <?= ((isset($data['content']['gender'])) && ($data['content']['gender'] == 'male')) ? 'selected="selected"' : '' ?> >Male</option>
                                    <option value="female" <?= ((isset($data['content']['gender'])) && ($data['content']['gender'] == 'female')) ? 'selected="selected"' : '' ?> >Female</option>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Address : </label>
                            <div class="input-group">
                                <div class="input-group-addon" style="vertical-align: top;">
                                    <i class="fa fa-map-marker"></i>
                                </div>
                                <textarea readonly="readonly" name="address" class="form-control" rows="5"><?= (isset($data['content']['address'])) ? $data['content']['address'] : '' ?></textarea>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Telepon : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-phone"></i>
                                </div>
                                <input readonly="readonly" name="telp"  value="<?= (isset($data['content']['telp'])) ? $data['content']['telp'] : '' ?>" type="text" class="form-control" />
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Post Code : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-lock"></i>
                                </div>
                                <input readonly="readonly" name="post_code"  value="<?= (isset($data['content']['post_code'])) ? $data['content']['post_code'] : '' ?>" type="text" class="form-control" />
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <div class="input-group">
                                <a href="<?= base_url('index.php/backend/cuser_profile/edit/' . $data['id']) ?>" class="btn btn-info" ><?= $data['save-title'] ?></a>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->

            <div class="col-md-3">

                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title">User Photo Profile</h3>
                    </div>
                    <div class="box-body">

                        <a href="#" class="thumbnail" id="photo_profile">
                            <?php if (!isset($data['content']['foto']) || $data['content']['foto'] == ''): ?>
                                <img data-src="holder.js/260x180" alt="260x180" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQQAAAC0CAYAAABytVLLAAAK/ElEQVR4Xu2aB4tUSxdFa8wJA+acc86K+YermBWMmDBizo6KOX/fLrhNzzgzbz/RR8NeF4ZpZk7frrNO1boVuqu7u/tn4YIABCDwfwJdCIF+AAEINAQQAn0BAhBoEUAIdAYIQAAh0AcgAIFfCTBDoFdAAALMEOgDEIAAMwT6AAQgMAABlgx0DwhAgCUDfQACEGDJQB+AAARYMtAHIAABhwB7CA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIXRYob99+1Zu375dnj59Wr5+/VpGjhxZpk2bVubOnVsGDRrUau2LFy/KrVu3yvv378vQoUNrzKJFi0pXV1cr5s6dO+XBgwfly5cvZcSIEWXevHllxowZ/zrjT58+lStXrpTu7u4yZMiQsnfv3h730P3V5pcvX5bPnz+XYcOGlUmTJtX2KL65/lR7/nUCvMEmgBBsVP9N4MWLF6sMNLA1iD9+/Fg/ePbs2WXp0qX1tQbeuXPn6uvhw4fXAf/z58864DUIdd27d69cv379l/usXbu2TJ482U7m2bNn5erVq1VOuvoSwpkzZ8qrV69qW8aPH1/FofgpU6aUNWvW/NH22A0n8LcIIITfwvZ33qTBf+zYsXrzzZs3l3HjxpVHjx7Vp7NmB3oy6/epU6fK27dv6+CXBCQIxWgwrl69ur7/yJEj9WndCKARhGI2bdpkJ3D48OEqgenTp9cZSW8haPZw9OjRer/t27eX0aNH9xDWvn37yuDBg/9Ye+yGE/hbBBDCb2H7O2/S4NJSQANIA1DXu3fvysmTJ+vrPXv2lO/fv9cBqJjdu3fX370vLSNOnDhR5aEBqdlG+8DV3548eVKf/HqqayBroCtG79NnrF+/vkycOLEuBbRcef36dZ2V9BZCe/uawd98vtqlNmq28E/t6SuPv0OZuw5EACF0eP+4efNm0dpbA3fXrl1VGOfPny9jxowpK1asKA8fPqwDWMuAqVOn1myaGC05du7cWf+mJcX+/ft7PMnPnj1bp/dz5swpS5YsKRcuXCjPnz8vM2fOLMuXL+9Bplmm9BaC7ithSQK6h/Yo1F79NLMRtz0dXoqI5iGEDi6z9hK0p6BLg1+DrVlCSBB68v748aOVwfz588vChQvrHoTep+m7nv7NdeDAgRrfLEc0I9BgllD0PslHm5hbt27tsRmo9/cnBP1PS51Lly6VN2/etD5Ls4uVK1fWDUa3PR1cipimIYQOLbUGvqb0egLPmjWrLFu2rLa0EYJeN09knSRoMGuJoBmBNvj6EsLBgwfr4G+EoHs8fvy4XL58uUVB+wt6sve+BhLCjRs3yt27d6tMxo4dW8Wg/QsJasGCBf0Koa/2dGg5YpqFEDqw1FoGSAa6tH5fvHhxq5XN07Z96i5pHDp0qOjIcuPGjXXQa1nR35Jh27ZtdcmhS7HagNR7NaPQ/9qPLpsP7k8Iko9OGdSeHTt21CNQyUCbo5qN6H6aibjt6cByRDUJIXRYuTXwNHg0yDUD0Pq+/dLpgk4Z2jcM9X8JQUuIDRs21Ce1BmR7THOCocGuzb/mOw3Xrl0r9+/fr5uTkkNfn6n79yeE5vRCM4MtW7a0mtqccmjZoBmH254OK0dccxBCB5VcA/L48eP1Cdts9PVunkShUwbFaONPG4DNYNVg15JB+wu6z4cPH+r3APR9AG3yaVmhtb1OEHTp5OD06dN1nb9u3br6WvfQwNZsof3qTwjtMxZ9tmYKapvaqLZqxjJhwgSrPR1UitimIIQOKn37/oAGafs3E9XMVatW1adte5wGrp7+mp63nw40Me1fcNJrzSA0QCUfzTQkDT3FdcypI0b96PsP2ktQvE4eNLA1+9DegP4mqejSJqfupftoWdDsIUg0koLapg1K5fFP7emgMkQ3BSF0UPmb6Xd/TWq+G6D/a4BpI08y0LpdMtAmXvv6X/fTj77JOGrUqHqS0HxLUd9i1P80oPUU1yVJ6NRB92yWDjqqlBD6urS3oT0OxWv2of0EiUMykzT0eZqtNNdA7emgMkQ3BSFEl5/kIdCTAEKgR0AAAi0CCIHOAAEIIAT6AAQg8CsBZgj0CghAgBkCfQACEGCGQB+AAAQGIMCSge4BAQiwZKAPQAACLBnoAxCAAEsG+gAEIOAQYA/BoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwC/wNER+9UOtmw8QAAAABJRU5ErkJggg==" style="width: 260px; height: 180px;">
                            <?php else: ?>
                                <img data-src="holder.js/260x180" alt="260x180" src="<?= base_url('pub.lcengine/upload/media/' . $data['content']['foto']) ?>" style="width: 260px; height: 180px;">
                            <?php endif; ?>
                        </a>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div>


        </div><!-- /.row -->                    

    </section><!-- /.content -->
</aside><!-- /.right-side -->