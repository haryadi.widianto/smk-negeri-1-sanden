<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-xs-8">

                <?= get_alert() ?>

                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3><br>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="30%" >Title</th>
                                    <th width="20%" >Type</th>
                                    <th width="20%" >Menu Category</th>
                                    <th width="15%" >Order</th>
                                    <th width="15%" >Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($data['content'] as $key => $item): ?>
                                    <tr>
                                        <td><?= $item->title ?></td>
                                        <td><?= ucwords(str_replace('-', ' ', $item->type)) ?></td>
                                        <td><?= get_data_title("navigation_category", $item->id_menu_category) ?></td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="<?= base_url("index.php/backend/csocial_media/down/$item->id") ?>" class="btn btn-info <?= ($key + 1 == count($data['content'])) ? 'disabled' : '' ?>"   ><i class="fa fa-arrow-down"></i></a>
                                                <a href="<?= base_url("index.php/backend/csocial_media/up/$item->id") ?>" class="btn btn-info <?= ($key == 0) ? 'disabled' : '' ?> "  ><i class="fa fa-arrow-up"></i></a>
                                            </div>
                                        </td>

                                        <td align="center">
                                            <div class="btn-group">
                                                <a href="<?= base_url('index.php/backend/cbottom_navigation/index/' . $item->id) ?>" class="btn btn-info" ><i class="fa fa-pencil"></i></a>
                                                <a href="<?= base_url("index.php/backend/cbottom_navigation/destroy/$item->id/false") ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>

                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>

            <div class="col-md-4">

                <?php $target = ($this->uri->segment(3) == 'index') ? 'update/' . $this->uri->segment(4) : 'storage'; ?>

                <?= form_open_multipart('backend/cbottom_navigation/' . $target) ?>

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title"><?= ($this->uri->segment(3) == 'index') ? 'Edit' : 'Add' ?></h3>
                    </div>
                    <div class="box-body">

                        <div class="form-group" id="furl">
                            <label>Title : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-tag"></i>
                                </div>
                                <input value="<?= (isset($data['content_edit']['title'])) ? $data['content_edit']['title'] : '' ?>" name="title" type="text" class="form-control" placeholder="Link">
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Type : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-folder"></i>
                                </div>
                                <select name="type"  class="form-control">
                                    <option <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'with-icon' ) ? 'selected="selected"' : '' ?>  value="with-icon">With Icon</option>
                                    <option <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'icon-only' ) ? 'selected="selected"' : '' ?> value="icon-only">Icon Only</option>
                                    <option <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'text-only' ) ? 'selected="selected"' : '' ?> value="text-only">Text Only</option>
                                    <option <?= (isset($data['content_edit']['type']) && $data['content_edit']['type'] == 'desc-only' ) ? 'selected="selected"' : '' ?> value="desc-only">Description Only</option>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <label>Navigation Category : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-sitemap"></i>
                                </div>
                                <select name="id_menu_category"  class="form-control">
                                    <?php if (!empty($option)): ?>
                                        <?php foreach ($option as $key => $value) : ?>
                                            <option <?= (isset($data['content_edit']['id_menu_category']) && $data['content_edit']['id_menu_category'] == $value->id ) ? 'selected="selected"' : '' ?>  value="<?=  $value->id ?>">
                                                <?= $value->title ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group" id="furl">
                            <label>Description : </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-file-text"></i>
                                </div>
                                <textarea rows="5" class="form-control" name="description"><?= (isset($data['content_edit']['description'])) ? $data['content_edit']['description'] : '' ?></textarea>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <div class="form-group">
                            <div class="input-group">
                                <input type="submit" class="btn btn-info" value="Save"/> &nbsp;
                                <a href="<?= base_url('index.php/backend/cbottom_navigation') ?>" class="btn btn-default" >Clear</a>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->


                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <?= form_close() ?>

            </div><!-- /.col (left) -->

        </div>

    </section><!-- /.content -->

    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable();
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": false,
                "bInfo": true,
                "bAutoWidth": false
            });

            $("a#photo_profile").click(function() {
                $("input#photo_upload").trigger("click");
            });

            $("input#photo_upload").change(function() {

                var files = !!this.files ? this.files : [];
                if (!files.length || !window.FileReader) {
                    return;
                }

                if (/^image/.test(files[0].type)) {

                    var reader = new FileReader();
                    reader.readAsDataURL(files[0]);

                    reader.onloadend = function() {

                        $('a#photo_profile > img').remove();
                        var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                        $('a#photo_profile').append(html);

                    }

                }

            });

        });
    </script>



</aside>

