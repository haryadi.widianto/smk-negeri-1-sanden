
<aside class="right-side">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <?= get_alert() ?>

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">IDENTITAS CALON SISWA</h3><br>
                    </div>
                    <div class="box-body">
                        <div>
                            <div class="col-md-4">
                                <a href="#" class="thumbnail col-md-">
                                    <img data-src="holder.js/260x180" src="<?= image('ppdb/'.$data['content']['image']) ?>" style="width: 100%; height: 150px;" alt="260x180">
                                </a>
                            </div>
                            <div class="col-md-8" style="padding-right: 0px;" >
                                <table style="font-size: 16px;"  class="table table-striped">
                                    <tr>
                                        <th width="40%">Nama Lengkap</td> 
                                        <td width="60%" ><?= $data['content']['sname'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>NISN</th> 
                                        <td><?= $data['content']['nisn'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Jenis Kelamin</th> 
                                        <td><?= ucwords($data['content']['gender']) ?></td>
                                    </tr>
                                    <tr>
                                        <th>Agama</th> 
                                        <td><?= ucwords($data['content']['sreligion']) ?></td>
                                    </tr>

                                </table>
                            </div>
                        </div>

                        <table style="font-size: 16px;"  class="table table-striped">
                            <tr>
                                <th width="25%">Tempat Lahir</th> 
                                <td width="70%" ><?= $data['content']['birth_place'] ?></td>
                            </tr>
                            <tr>
                                <th>Tanggal Lahir</th> 
                                <td><?= date('j F Y', strtotime($data['content']['birth_date'])) ?></td>
                            </tr>
                            <tr>
                                <th>Alamat Asal</th> 
                                <td><?= $data['content']['home_address'] ?></td>
                            </tr>
                            <tr>
                                <th>Alamat Tinggal</th> 
                                <td><?= $data['content']['stay_address'] ?></td>
                            </tr>
                            <tr>
                                <th>No. Telp / HP</th> 
                                <td><?= $data['content']['stelp'] ?></td>
                            </tr>
                        </table>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">IDENTITAS ORANG TUA</h3><br>
                    </div>
                    <div class="box-body">

                        <table style="font-size: 16px;"  class="table table-striped">

                            <tr>
                                <th width="30%">Nama Lengkap</th> 
                                <td width="70%" ><?= $data['content']['pname'] ?></td>
                            </tr>
                            <tr>
                                <th>Alamat Lengkap</th> 
                                <td><?= $data['content']['paddress'] ?></td>
                            </tr>
                            <tr>
                                <th>Agama</th> 
                                <td><?= ucwords($data['content']['preligion']) ?></td>
                            </tr>
                            <tr>
                                <th>Pekerjaan</th> 
                                <td><?= $data['content']['poccupation'] ?></td>
                            </tr>

                        </table>
                        <hr>
                        <table style="font-size: 16px;"  class="table table-striped">
                            <tr>
                                <th colspan="2">Wali Siswa</th> 
                            </tr>
                            <tr>
                                <th width="30%">Nama Lengkap</th> 
                                <td width="70%" ><?= $data['content']['alt_name'] ?></td>
                            </tr>
                            <tr>
                                <th>Alamat Lengkap</th> 
                                <td><?= $data['content']['alt_address'] ?></td>
                            </tr>
                            <tr>
                                <th>Agama</th> 
                                <td><?= ucwords($data['content']['alt_religion']) ?></td>
                            </tr>
                            <tr>
                                <th>Pekerjaan</th> 
                                <td><?= $data['content']['alt_occupation'] ?></td>
                            </tr>

                        </table>



                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->



            <div class="col-md-6">

                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            <?= $data['content']['register_number'] ?>
                        </h3>
                        <p>
                            Registration Code
                        </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Registration code <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">ASAL SEKOLAH</h3><br>
                    </div>
                    <div class="box-body">

                        <table style="font-size: 16px;"  class="table table-striped">

                            <tr>
                                <th width="40%">Nama Sekolah Asal</th> 
                                <td width="60%" ><?= $data['content']['school_name'] ?></td>
                            </tr>
                            <tr>
                                <th>Alamat Sekolah Asal</th> 
                                <td><?= $data['content']['school_address'] ?></td>
                            </tr>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Pilihan Konpetensi Keahlian</h3><br>
                    </div>
                    <div class="box-body">
                        <div>
                            <table style="font-size: 16px;"  class="table table-striped">
                                <tr>
                                    <td width="5%">1</td> 
                                    <td width="95%" ><?= ucwords(str_replace('-', ' ', $data['content']['option_1'])) ?></td>
                                </tr>
                                <tr>
                                    <td>2</td> 
                                    <td><?= ucwords(str_replace('-', ' ', $data['content']['option_2'])) ?></td>
                                </tr>
                                <tr>
                                    <td>3</td> 
                                    <td><?= ucwords(str_replace('-', ' ', $data['content']['option_3'])) ?></td>
                                </tr>
                            </table>
                        </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Nilai Raport</h3><br>
                    </div>
                    <div class="box-body">
                        <div>
                            <table class="table table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Mata Pelajaran</th>
                                        <th>I</th>
                                        <th>II</th>
                                        <th>III</th>
                                        <th>IV</th>
                                        <th>V</th>
                                    </tr>
                                    <tr>
                                        <td>Bahasa Indonesia</td>
                                        <td><?= $data['content']['bi1'] ?></td>
                                        <td><?= $data['content']['bi3'] ?></td>
                                        <td><?= $data['content']['bi3'] ?></td>
                                        <td><?= $data['content']['bi4'] ?></td>
                                        <td><?= $data['content']['bi5'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Bahasa Inggris</td>
                                        <td><?= $data['content']['be1'] ?></td>
                                        <td><?= $data['content']['be3'] ?></td>
                                        <td><?= $data['content']['be3'] ?></td>
                                        <td><?= $data['content']['be4'] ?></td>
                                        <td><?= $data['content']['be5'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Matematika</td>
                                        <td><?= $data['content']['mt1'] ?></td>
                                        <td><?= $data['content']['mt3'] ?></td>
                                        <td><?= $data['content']['mt3'] ?></td>
                                        <td><?= $data['content']['mt4'] ?></td>
                                        <td><?= $data['content']['mt5'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>IPA</td>
                                        <td><?= $data['content']['ipa1'] ?></td>
                                        <td><?= $data['content']['ipa3'] ?></td>
                                        <td><?= $data['content']['ipa3'] ?></td>
                                        <td><?= $data['content']['ipa4'] ?></td>
                                        <td><?= $data['content']['ipa5'] ?></td>
                                    </tr>

                                </tbody>
                            </table>

                            <hr/>


                            <table class="table table-striped table-bordered">
                                <tbody>

                                    <tr>
                                        <th>NEM</th>
                                        <th colspan="5"><?= $data['content']['nem'] ?></th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box box-info">
                   
                    <div class="box-body">
                        <?= form_open($data['save-link']) ?>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="submit" class="btn btn-info" value="<?= $data['save-title'] ?>"/>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <?= form_close() ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->


        </div><!-- /.row -->           



    </section><!-- /.content -->
</aside><!-- /.right-side -->