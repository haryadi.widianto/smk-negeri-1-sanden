<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-xs-12">
                <?= get_alert() ?>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3><br>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="25%" >Siswa</th>
                                    <th width="40%" >Nilai Raport</th>
                                    <th width="20%" >Kompetensi Keahlian</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($data['content'] as $key => $item): ?>
                                    <tr>
                                        <td>
                                            <a href="#" class="thumbnail col-md-5" >
                                                <img data-src="holder.js/260x180" src="<?= image('ppdb/'.$item->image) ?>" style="width: 100%; height: 100px;" alt="260x180">
                                            </a> 
                                            &nbsp;<small class="badge bg-yellow"><i class="fa fa-pencil-square"></i> <?= $item->register_number ?></small><br/>
                                            &nbsp;<strong><?= $item->sname ?></strong><br/>
                                            &nbsp;NISP : <?= $item->nisn ?><br/>
                                            &nbsp;Gender : <?= ucwords($item->gender) ?><br/>
                                            &nbsp;<?= $item->year_session ?><br/><br/>

                                            <table class="table table-striped">
                                                <tbody>
                                                    <tr>
                                                        <th>Sekolah Asal :</th>
                                                    </tr>
                                                    <tr>
                                                        <th>
                                                                <?= $item->school_name ?>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </td>
                                        <td>
                                            <div class="box box-info">

                                                <div class="box-body no-padding">
                                                    <table class="table table-striped table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <th>Mata Pelajaran</th>
                                                                <th>I</th>
                                                                <th>II</th>
                                                                <th>III</th>
                                                                <th>IV</th>
                                                                <th>V</th>
                                                            </tr>
                                                            <tr>
                                                                <td>Bahasa Indonesia</td>
                                                                <td><?= $item->bi1 ?></td>
                                                                <td><?= $item->bi2 ?></td>
                                                                <td><?= $item->bi3 ?></td>
                                                                <td><?= $item->bi4 ?></td>
                                                                <td><?= $item->bi5 ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Bahasa Inggris</td>
                                                                <td><?= $item->be1 ?></td>
                                                                <td><?= $item->be2 ?></td>
                                                                <td><?= $item->be3 ?></td>
                                                                <td><?= $item->be4 ?></td>
                                                                <td><?= $item->be5 ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Matematika</td>
                                                                <td><?= $item->mt1 ?></td>
                                                                <td><?= $item->mt2 ?></td>
                                                                <td><?= $item->mt3 ?></td>
                                                                <td><?= $item->mt4 ?></td>
                                                                <td><?= $item->mt5 ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>IPA</td>
                                                                <td><?= $item->ipa1 ?></td>
                                                                <td><?= $item->ipa2 ?></td>
                                                                <td><?= $item->ipa3 ?></td>
                                                                <td><?= $item->ipa4 ?></td>
                                                                <td><?= $item->ipa5 ?></td>
                                                            </tr>
                                                            <tr>
                                                                <th>NEM</th>
                                                                <th colspan="5"><?= $item->nem ?></th>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div><!-- /.box-body -->
                                            </div>

                                        </td>
                                        <td>

                                            <div class="box box-info">
                                                <div class="box-body no-padding">
                                                    <table class="table table-striped">
                                                        <tbody>
                                                            <tr>
                                                                <td width="10%" >1</td>
                                                                <td  width="90%"><?= ucwords(str_replace('-', ' ', $item->option_1)) ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td><?= ucwords(str_replace('-', ' ', $item->option_2)) ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>3</td>
                                                                <td><?= ucwords(str_replace('-', ' ', $item->option_3)) ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div><!-- /.box-body -->
                                            </div>
                                            <div class="btn-group">
                                                <a href="<?= base_url('index.php/backend/cregistration/view/' . $item->id) ?>" class="btn btn-info" ><i class="fa fa- fa-search-plus"></i></a>
                                                <a href="<?= base_url("index.php/backend/cregistration/destroy/$item->id/false") ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>


                                    </tr>
                                <?php endforeach; ?>

                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section><!-- /.content -->

    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable({
                "bSort": false
            });
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        });
    </script>

</aside>

