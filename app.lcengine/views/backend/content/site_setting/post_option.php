<div id="post_at_home" class="form-group">
    <label>Main Contact : </label>
    <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-angle-double-right"></i>
        </div>
        <select name="post" class="form-control">
            <?php if (!empty($post_option)): ?>
                <?php foreach ($post_option as $key => $value) : ?>
                    <option value="<?= $value->id_post ?>"><?= $value->title ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div><!-- /.input group -->
</div><!-- /.form group -->

<div id="post_description" class="form-group">
    <label>Contact Description : </label>
    <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-angle-double-right"></i>
        </div>
        <select name="description" class="form-control">
            <?php if (!empty($post_option)): ?>
                <?php foreach ($post_option as $key => $value) : ?>
                    <option value="<?= $value->id_post ?>"><?= $value->title ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </div><!-- /.input group -->
</div><!-- /.form group -->


