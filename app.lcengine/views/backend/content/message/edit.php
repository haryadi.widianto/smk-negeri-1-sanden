<aside class="right-side">

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <?= get_alert() ?>

                <div class="box box-danger">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['content']['subject'] ?></h3><br>
                        
                    </div>
                    <div class="box-body">
                        <div>
                            <table style="font-size: 16px;">
                                    <tr>
                                        <td style="width: 70px;">Name</td> 
                                        <td>:</td>
                                        <td><?= $data['content']['name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>:</td>
                                        <td><?= $data['content']['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Occuppation</td>
                                        <td>:</td>
                                        <td><?= $data['content']['occuppation'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Send</td>
                                        <td>:</td>
                                        <td><?= $data['content']['date'] ?></td>
                                    </tr>
                                    <tr>
                                        <td>Type</td>
                                        <td>:</td>
                                        <td><?= ucwords($data['content']['type'])  ?></td>
                                    </tr>
                                </table>
                        </div>
                        
                        <hr>

                        <div><?= $data['content']['content'] ?></div>
                        <hr>
                        <?= form_open($data['save-link']) ?>

                        <div class="form-group">
                            <div class="input-group">
                                <input type="submit" class="btn btn-info" value="<?= $data['save-title'] ?>"/>
                            </div><!-- /.input group -->
                        </div><!-- /.form group -->

                        <?= form_close() ?>

                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col (left) -->


        </div><!-- /.row -->                    

    </section><!-- /.content -->
</aside><!-- /.right-side -->