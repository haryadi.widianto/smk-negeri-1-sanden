<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= $data['header'] ?>
            <small><?= $data['sub-header'] ?></small>
        </h1>
        <?= get_breadcrumb() ?>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="row">

            <div class="col-xs-12">
                <?= get_alert() ?>
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?= $data['box-title'] ?></h3><br>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="20%" >Name</th>
                                    <th width="15%" >Email</th>
                                    <th width="15%" >Subject</th>
                                     <th width="10%" >Type</th>
                                    <th width="15%" >Date</th>
                                    <th width="10%" >Status</th>
                                    <th width="15%" >Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php foreach ($data['content'] as $key => $item): ?>
                                    <tr>
                                        <td><?= $item->name ?></td>
                                        <td><?= $item->email ?></td>
                                        <td><?= $item->subject ?></td>
                                        <td><strong><?= ucwords($item->type) ?></strong></td>
                                        <td><?= $item->date ?></td>
                                        <td><?= ($item->status == 'new') ? '<small class="badge bg-red"><i class="fa fa-fw fa-bell-o"></i> New</small>' : '<small class="badge bg-green"><i class="fa fa-fw fa-check-square-o"></i> Read</small>' ?></td>

                                        <td align="center">
                                            <div class="btn-group">
                                                <a href="<?= base_url('index.php/backend/cmessage/view/' . $item->id) ?>" class="btn btn-info" ><i class="fa fa- fa-search-plus"></i></a>
                                                <a href="<?= base_url("index.php/backend/cmessage/destroy/$item->id/false") ?>" class="btn btn-danger"><i class="fa fa-times"></i></a>
                                            </div>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>

                            </tbody>

                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>

    </section><!-- /.content -->

    <script type="text/javascript">
        $(function() {
            $("#example1").dataTable({
                "bSort": false
            });
            $('#example2').dataTable({
                "bPaginate": true,
                "bLengthChange": false,
                "bFilter": false,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": false
            });
        });
    </script>

</aside>

