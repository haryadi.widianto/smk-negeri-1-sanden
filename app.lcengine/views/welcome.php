<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- meta data -->
        <?= $this->load->view('frontend/component/meta') ?>
        <?= $this->load->view('frontend/component/css') ?>
        <?= $this->load->view('frontend/component/js') ?>
    </head>
    <body class="home">
        
        <!-- main header -->
        <?= $this->load->view('frontend/component/header') ?>
        <!-- slide show -->
        <?= $this->load->view('frontend/component/carousel') ?>
        
        <div id="content-container">
            <div id="content" class="clearfix">
                <!-- banner -->
                <?= $this->load->view('frontend/component/banner') ?>
                <div id="main-content">
                    
                    <!-- intro article -->
                    <?= $this->load->view('frontend/component/intro_article') ?>
                    <div id="sidebar-homepage-left" class="sidebar-homepage">
                    
                        <!-- blog list -->
                        <?= $this->load->view('frontend/component/blog_list') ?>
                    </div>
                    <div id="sidebar-homepage-middle" class="sidebar-homepage">
                        <!-- event list -->
                        <?= $this->load->view('frontend/component/event_list') ?>
                    </div>
                </div>
                <div id="sidebar-homepage-right" class="sidebar-homepage">
                    
                   <!-- side navbar -->
                   <?= $this->load->view('frontend/component/side_navbar') ?>
                   
                </div>
                
                <!-- intro-principal-article -->
                <?= $this->load->view('frontend/component/intro_principal_article') ?>
            </div>
        </div>
        <footer id="main-footer" style="background:url(images/img-8.jpg) no-repeat 50% 0;">
            <?= $this->load->view('frontend/component/footer') ?>
        </footer>
    </body>
</html>