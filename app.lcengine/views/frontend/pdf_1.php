<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

    <head>
        <!-- Header -->
        <?= $this->views->render($header) ?>
    </head> 

    <body>
        <?php $prestasi = $data['prestasi']; ?>
        <?php $data = $data['student']; ?>
        <div class="container-fluid center-block">

        <div class="row col-md-8">
            
            <div class="col-md-4">
                <button type="submit" class="btn btn-cta btn-block" style="font-size: 17px;">No. Registrasi : <?= $data['register_number'] ?></button><br/>

                <a href="components.1#" class="thumbnail">
                    <img data-src="holder.js/100%x180" alt="100%x180" src="<?= image('ppdb/' . $data['image']) ?>" data-holder-rendered="true" style="height: 250px; width: 100%; display: block;">
                </a>

                <div class="box box-border page-row">
                    <ul class="list-unstyled">
                        <li><strong>Pilihan Jurusan:</strong></li>
                        <li>1. <?= $data['option_1'] ?></li>
                        <li>2. <?= $data['option_2'] ?></li>
                        <li>3. <?= $data['option_3'] ?></li>
                    </ul>                                
                </div>

            </div>
            <div class="col-md-8">
                <div class="table-responsive">                    
                    <table class="table" style="font-size: 15px;">
                        <thead>
                            <tr>
                                <th colspan="3">Data Diri Siswa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 35%;">NISN</td>
                                <td style="width: 5%;" >:</td>
                                <td style="width: 60%;" ><?= $data['nisn'] ?></td>
                            </tr>
                            <tr>
                                <td>Nama Lengkap</td>
                                <td>:</td>
                                <td><?= $data['sname'] ?></td>
                            </tr>
                            <tr>
                                <td>Tempat & Tanggal Lahir</td>
                                <td>:</td>
                                <td><?= $data['birth_place'] ?>, <?= date('d F Y', strtotime($data['birth_date'])) ?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?= $data['home_address'] ?></td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td><?= ucwords($data['gender']) ?></td>
                            </tr>
                            <tr>
                                <td>Agama</td>
                                <td>:</td>
                                <td><?= ucwords($data['religion']) ?></td>
                            </tr>
                            <tr>
                                <td>Alamat Tinggal</td>
                                <td>:</td>
                                <td><?= $data['stay_address'] ?></td>
                            </tr>
                        </tbody>
                    </table><!--//table-->
                </div>

                <div class="table-responsive">                    
                    <table class="table" style="font-size: 15px;">
                        <thead>
                            <tr>
                                <th colspan="3">Data Asal Sekolah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 35%;">Nama Sekolah</td>
                                <td style="width: 5%;" >:</td>
                                <td style="width: 60%;" ><?= $data['school_name'] ?></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                <td><?= ucwords($data['status']) ?></td>
                            </tr>
                            <tr>
                                <td>Alamat Sekolah</td>
                                <td>:</td>
                                <td><?= $data['school_address'] ?></td>
                            </tr>

                        </tbody>
                    </table><!--//table-->
                </div>

                <div class="table-responsive">                    
                    <table class="table" style="font-size: 15px;">
                        <thead>
                            <tr>
                                <th colspan="3">Data Orang Tua dan Wali Siswa</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 35%;">Nama Orang Tua</td>
                                <td style="width: 5%;" >:</td>
                                <td style="width: 60%;" ><?= $data['pname'] ?></td>
                            </tr>
                            <tr>
                                <td>Pekerjaan</td>
                                <td>:</td>
                                <td><?= $data['poccupation'] ?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?= $data['paddress'] ?></td>
                            </tr>
                            <tr>
                                <td style="width: 35%;">Nama Wali</td>
                                <td style="width: 5%;" >:</td>
                                <td style="width: 60%;" ><?= $data['alt_name'] ?></td>
                            </tr>
                            <tr>
                                <td>Pekerjaan</td>
                                <td>:</td>
                                <td><?= $data['alt_occupation'] ?></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?= $data['alt_address'] ?></td>
                            </tr>
                        </tbody>
                    </table><!--//table-->
                </div>

                <div class="table-responsive">                    
                    <table class="table" style="font-size: 15px;">
                        <thead>
                            <tr>
                                <th colspan="3">Bakat Prestasi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($prestasi)): ?>
                                <?php foreach ($prestasi as $key => $value) : ?>
                                    <tr>
                                        <td style="width: 5%;" ><?= $key + 1 ?></td>
                                        <td style="width: 95%;" ><?= $value->prestasi ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                    </table><!--//table-->
                </div>

            </div>
        </div>


        </div>

    </body>
</html> 

