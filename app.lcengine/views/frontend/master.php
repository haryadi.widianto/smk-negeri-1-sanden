<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  

    <head>
        <!-- Header -->
        <?= $this->views->render($header) ?>
    </head> 

    <body <?= ( $this->uri->segment(3) == '') ? 'class="home-page"' : '' ?> >
        <div class="wrapper">

            <!-- Pre Content -->
            <?= $this->views->render((isset($precontent)) ? $precontent : '') ?>

            <!-- ******CONTENT****** --> 
            <div class="content container">
                <?= $this->views->render((isset($content)) ? $content : '') ?>

                <div class="page-wrapper">
                    <?= $this->views->render((isset($page_header)) ? $page_header : '') ?>
                    <div class="page-content">
                        <div class="row page-row">
                            <?= $this->views->render((isset($page_content)) ? $page_content : '') ?>
                            <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1 col-xs-12">    
                                <?= $this->views->render((isset($page_sidebar)) ? $page_sidebar : '') ?>
                            </aside>
                        </div><!--//page-row-->

                        <?= $this->views->render((isset($page_bottom)) ? $page_bottom : '') ?>

                    </div><!--//page-content-->
                </div><!--//page--> 

                <div class="row cols-wrapper row-home">

                    <?php if (!isset($leftcontent9)) : ?>
                        <div class="col-md-3">
                            <?= $this->views->render((isset($leftsidebar)) ? $leftsidebar : '') ?>
                        </div><!--//col-md-3-->
                        <div class="col-md-6">
                            <?= $this->views->render((isset($innersidebar)) ? $innersidebar : '') ?>
                        </div>
                    <?php else: ?>
                        <div class="col-md-9 col-leftcontent">
                            <?= $this->views->render((isset($leftcontent9)) ? $leftcontent9 : '') ?>
                        </div>
                    <?php endif; ?>
                        
                    <div class="col-md-3">
                        <?= $this->views->render((isset($rightsidebar)) ? $rightsidebar : '') ?>
                    </div><!--//col-md-3-->
                    
                </div><!--//cols-wrapper-->
                <?= $this->views->render((isset($outsidebar)) ? $outsidebar : '') ?>
            </div><!--//content-->
        </div><!--//wrapper-->

        <!-- ******FOOTER****** --> 
        <footer class="footer">
            <div class="footer-content">
                <div class="container">
                    <div class="row">
                        <?= $this->views->render((isset($bottomside)) ? $bottomside : '') ?>
                    </div>   
                </div>        
            </div><!--//footer-content-->
            <?= $this->views->render((isset($footer)) ? $footer : '') ?>
        </footer><!--//footer-->

    </body>
</html> 

