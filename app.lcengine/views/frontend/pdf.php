<?php $prestasi = $data['prestasi']; ?>
<?php $site_setting = $data['site_setting']; ?>
<?php $data = $data['student']; ?>


<style>
    html,body {
        font-family: sans-serif;
        font-size: 11px;
    }

    .img-logo {
        width: 60px;
        height: 100%;
    }

    .img-logo2 {
        width: 80px;
        height: 100%;
    }

    .table-1 {
        margin-top: 0px;
        width: 100%;
        border-bottom: 1px solid #ddd;
        padding-bottom: -10px; 
        padding-top: -10px; 
    }

    .table-1 tr td {
        padding: 0px 15px;
    }

    .info-table-1 {
        text-align: center;
    }

    .info-table-1 span {
        font-size: 9px;
        display: block;
        margin-top: 5px;
    }


    table.list tr td {
        padding: 5px 0px;
        width: 150px; 
    }

    .list-title {
        width: 100px;
        color: #999;
    }

    /**/

    .table-pesanan {
        margin-top: 10px;
        width: 100%; 
        border-bottom: 1px solid #ddd;
        padding-bottom: 15px;
    }

    .table-pesanan tr td {
        padding: 12px;
        border-bottom: 3px double #ddd;
    }


    .list-pesanan label {
        padding-left: 10px;
        border-left: 3px solid #f47b00;
    }

    .text-right {
        text-align: right; 
    }

    .back-total {
        background: #f0f0f0;
    }

    .thead td {
        background: #f5f5f5;
    }


</style>
<table class="table-1" style="border-bottom: solid #3d558b 5px; line-height: 1px;"  >
    <tr>
        <td style="width: 20%;">
            <img src="<?= set_image('logo-bantul.jpg') ?>"  class="img-logo">
        </td>
        <td style="width: 80%;line-height:3px;" class="info-table-1">
            <h3 style="font-size: 15px;">PEMERINTAH KABUPATEN BANTUL</h3>
            <h3 style="font-size: 15px;" >DINAS PENDIDIKAN MENENGAH DAN NONFORMAL</h3>
            <h1 style="position: relative; top: 7px;">SMK NEGERI 1 SANDEN</h1>
            <div style="position: relative;top: -12px; font-size: 13px; line-height:2px;">
                <h5>Jln. Samas Km 11, Ngemplak, Srigading, Sanden, Bantul (0274) 7475172</h5>
                <h5>Fax : (0274) 367677, Website : http://www.smknegeri1sanden.sch.id</h5>
            </div>
        </td>
        <td style="width: 20%;" >
            <img src="<?= set_image('logo-smkn1sanden.png') ?>"  class="img-logo2">
        </td>
    </tr>
</table>

<table class="table" style="margin-top: 6px; width: 100%;">
    <tr>
        <td style="text-align: center; line-height: 10px;">
            <h2>BUKTI PENDAFTARAN PESERTA DIDIK BARU</h2>
            <h2 style="font-size: 15px;">TAHUN PELAJARAN 2014/2015</h2>
        </td>
    </tr>
</table>

<table class="table" style="margin-top: 7px; width: 100%;" >
    <tr>
        <td style="width: 25%;">
            <table class="list">
                <tbody>
                    <tr>
                        <td><img src="<?= image('ppdb/' . $data['image']) ?>" style="width: 150px; height: 220px;"/></td>
                    </tr>
                    <tr  >
                        <td style="text-align: center; font-size: 15px; font-weight: bold; background-color: #3d558b; color: white;" >
                            No Registrasi<br/>
                            <?= $data['register_number'] ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td style="width: 75%;" >
            <table class="list" style="font-size: 14px; width: 100%;">
                <tr>
                    <td colspan="2" style="border-bottom: #3d558b solid 3px;">
                        <strong style="font-size: 16px;">Data Diri Siswa</strong>
                    </td>
                </tr>
                <tr>
                    <td style="">
                        <table class="list" style="width: 100%; font-size: 14px;" >
                            <tbody>
                                <tr><td><strong>NISN :</strong></td></tr>
                                <tr><td><?= $data['nisn'] ?></td></tr>
                                <tr><td><strong>Nama Lengkap :</strong></td></tr>
                                <tr><td><?= $data['sname'] ?></td></tr>
                                <tr><td><strong>Tempat Tanggal Lahir :</strong></td></tr>
                                <tr><td><?= $data['birth_place'] ?>, <?= date('d F Y', strtotime($data['birth_date'])) ?></td></tr>
                                <tr><td><strong>Alamat Asal :</strong></td></tr>
                                <tr><td><?= $data['home_address'] ?></td></tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="padding-left: 20px;" >
                        <table class="list" style="font-size: 14px;">
                            <tbody>
                                <tr><td><strong>Jenis Kelamin :</strong></td></tr>
                                <tr><td><?= ucwords($data['gender']) ?></td></tr>
                                <tr><td><strong>Ageama :</strong></td></tr>
                                <tr><td><?= ucwords($data['religion']) ?></td></tr>
                                <tr><td><strong>Alamat Tinggal :</strong></td></tr>
                                <tr><td><?= $data['stay_address'] ?></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>

<table class="table" style="margin-top: 7px; width: 100%;" >
    <tr>
        <td>
            <table class="list" style="width: 100%;">
                <tbody>
                    <tr>
                        <td colspan="2" style="border-bottom: #3d558b solid 3px;">
                            <strong style="font-size: 16px;">Data Asal Sekolah</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="list" style="font-size: 14px; ">
                                <tbody>
                                    <tr><td><strong> Nama Sekolah :</strong></td></tr>
                                    <tr><td><?= $data['school_name'] ?></td></tr>
                                    <tr><td><strong>Status :</strong></td></tr>
                                    <tr><td><?= ucwords($data['status']) ?></td></tr>
                                    <tr><td><strong>Alamat :</strong></td></tr>
                                    <tr><td><?= $data['school_address'] ?></td></tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
            <table class="list" style="width: 100%;">
                <tbody>
                    <tr>
                        <td colspan="2" style="border-bottom: #3d558b solid 3px;">
                            <strong style="font-size: 16px;">Data Orang Tua dan Wali</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="list" style="font-size: 14px;">
                                <tbody>
                                    <tr><td><strong>Nama Orang Tua :</strong></td></tr>
                                    <tr><td><?= $data['pname'] ?></td></tr>
                                    <tr><td><strong>Pekerjaan :</strong></td></tr>
                                    <tr><td><?= $data['poccupation'] ?></td></tr>
                                    <tr><td><strong>Alamat :</strong></td></tr>
                                    <tr><td><?= $data['paddress'] ?></td></tr>
                                   
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="list" style="font-size: 14px;">
                                <tbody>
                                  
                                    <tr><td><strong>Nama Orang Wali :</strong></td></tr>
                                    <tr><td><?= $data['alt_name'] ?></td></tr>
                                    <tr><td><strong>Pekerjaan :</strong></td></tr>
                                    <tr><td><?= $data['alt_occupation'] ?></td></tr>
                                    <tr><td><strong>Alamat :</strong></td></tr>
                                    <tr><td><?= $data['alt_address'] ?></td></tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

<table class="table" style="margin-top: 7px; width: 100%;" >
    <tr>
        <td>
            <table class="list" style="width: 100%;">
                <tbody>
                    <tr>
                        <td colspan="3" style="border-bottom: #3d558b solid 3px;">
                            <strong style="font-size: 16px;">Pilihan Jurusan</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table class="list" style="font-size: 14px; ">
                                <tbody>
                                    <tr><td><strong> Jurusan 1 :</strong></td></tr>
                                    <tr><td><?= $data['option_1'] ?></td></tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="list" style="font-size: 14px; ">
                                <tbody>
                                    <tr><td><strong>Jurusan 2 :</strong></td></tr>
                                    <tr><td><?= $data['option_2'] ?></td></tr>
                                </tbody>
                            </table>
                        </td>
                        <td>
                            <table class="list" style="font-size: 14px; ">
                                <tbody>
                                    <tr><td><strong> Jurusan 3 :</strong></td></tr>
                                    <tr><td><?= $data['option_3'] ?></td></tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
        
    </tr>
</table>

<hr style="height: 10px; background-color: #3d558b;"/>
