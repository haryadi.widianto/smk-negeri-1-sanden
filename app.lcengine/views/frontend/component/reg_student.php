<article class="contact-form col-md-8 col-sm-7  page-row">                            
    <h3 class="title">Progres Pendaftaran PPDB</h3>
    <div class="progress progress-striped active">
        <div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
            20%
        </div>
    </div>
    <hr/>
    <h3 class="title">Data Sekolah</h3>
    <?= form_open_multipart('routing/index/post-data-siswa') ?>
    <div class="form-group name">
        <?php echo get_alert(); ?>
        <div class="row">
            <div class="col-md-6">
                <label for="name">NISN</label>
                <input name="nisn" value="<?= set_value('nisn') ?>" id="name" type="text" class="form-control" placeholder="Masukkan NISN Anda">
            </div>
            <div class="col-md-6">
                <label for="name">Nama Lengkap</label>
                <input name="name" value="<?= set_value('name') ?>" id="name" type="text" class="form-control" placeholder="Masukkan nama lengkap Anda">
            </div>
        </div>

    </div><!--//form-group-->

    <div class="form-group message">
        <label >Alamat Asal<span class="required">*</span></label>
        <textarea  name="home_address" class="form-control" rows="6" placeholder="Masukkan alamat asal Anda"><?= set_value('home_address') ?></textarea>
    </div><!--//form-group-->

    <div class="form-group email">
        <div class="row">
            <div class="col-md-6">
                <label for="email">Tempat Lahir<span class="required">*</span></label>
                <input  name="birth_place" value="<?= set_value('birth_place') ?>" type="text"  id="email"  class="form-control" placeholder="Masukkan Tempat Lahir Anda">
            </div>
            <div class="col-md-6">
                <label for="email">Tanggal Lahir<span class="required">*</span></label>
                <input  name="birth_date" value="<?= set_value('birth_date') ?>" type="date" class="form-control" placeholder="Masukkan Tanggal Lahir Anda">
            </div>
        </div>
    </div><!--//form-group-->

    <div class="form-group email">
        <div class="row">
            <div class="col-md-6">
                <label for="email">Jenis Kelamin<span class="required">*</span></label><br/>

                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-warning <?= (set_value('gender') == 'laki-laki') ? 'active' : '' ?>">
                        <input type="radio" id="option1" autocomplete="on" name="gender" value="laki-laki"  <?= (set_value('gender') == 'laki-laki') ? 'checked' : '' ?> >
                        <i class="fa fa-male"></i> Laki-laki
                    </label>
                    <label class="btn btn-warning <?= (set_value('gender') == 'perempuan') ? 'active' : '' ?>">
                        <input type="radio" id="option2" autocomplete="on" name="gender" value="perempuan"  <?= (set_value('gender') == 'perempuan') ? 'checked' : '' ?> >
                        <i class="fa fa-female"></i> Perempuan
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <label for="email">Agama<span class="required">*</span></label>
                <select class="form-control" name="religion" >
                    <option> -- Pilih Agama -- </option>
                    <option value="islam" <?= (set_value('religion') == 'islam') ? 'selected="selected"' : '' ?>  >Islam</option>
                    <option value="kristen" <?= (set_value('religion') == 'kristen') ? 'selected="selected"' : '' ?>  >Kristen</option>
                    <option value="katolik" <?= (set_value('religion') == 'katolik') ? 'selected="selected"' : '' ?> >Katolik</option>
                    <option value="budha" <?= (set_value('religion') == 'budha') ? 'selected="selected"' : '' ?> >Budha</option>
                    <option value="hindu" <?= (set_value('religion') == 'hindu') ? 'selected="selected"' : '' ?> >Hindu</option>
                    <option value="lainnya" <?= (set_value('religion') == 'lainnya') ? 'selected="selected"' : '' ?> >Lainnya</option>
                </select>
            </div>
        </div>
    </div><!--//form-group-->

    <div class="form-group message">
        <label for="message">Alamat Rumah<span class="required">*</span></label>
        <textarea  name="stay_address" id="message" class="form-control" rows="6" placeholder="Masukkan alamat rumah Anda"><?= set_value('stay_address') ?></textarea>
    </div><!--//form-group-->

    <div class="form-group message">
        <label for="message">Kota<span class="required">*</span></label>
        <input name="kota" value="<?= set_value('kota') ?>" id="name" type="text" class="form-control" placeholder="Masukkan kota alamat rumah Anda">
    </div><!--//form-group-->

    <div class="form-group email">
        <div class="row">
            <div class="col-md-6">
                <label for="email">Nomor Telepon / HP<span class="required">*</span></label>
                <input  name="telp" value="<?= set_value('telp') ?>"  type="text"  id="email"  class="form-control" placeholder="Masukkan Tempat Lahir Anda">
            </div>
            <div class="col-md-6">
                <label for="email">Email <span class="required">*</span></label>
                <input  name="email" value="<?= set_value('email') ?>"  type="email" class="form-control" placeholder="Masukkan Email Anda">
            </div>
        </div>
    </div><!--//form-group-->
    
     <div class="form-group name">
        <div class="row">
            <div class="col-md-4">
               <label for="name">Pilihan 1</label>
               <select class="form-control" name="option_1" >
                   <option> -- Pilih Pilihan 1 -- </option>
                    <option value="Teknik Bodi Mobil" <?= (set_value('option_1') == 'Teknik Bodi Mobil') ? 'selected="selected"' : '' ?>  >Teknik Bodi Mobil (Otomotif)</option>
                    <option value="Teknik Kendaraan Ringan" <?= (set_value('option_1') == 'Teknik Kendaraan Ringan') ? 'selected="selected"' : '' ?>  >Teknik Kendaraan Ringan (Otomotif)</option>
                    <option value="Rekayasa Perangkat Lunak" <?= (set_value('option_1') == 'Rekayasa Perangkat Lunak') ? 'selected="selected"' : '' ?> >Rekayasa Perangkat Lunak (Komputer)</option>
                    <option value="Nautika Kapal Penangkap Ikan" <?= (set_value('option_1') == 'Nautika Kapal Penangkap Ikan') ? 'selected="selected"' : '' ?> >Nautika Kapal Penangkap Ikan (Kelautan)</option>
                    <option value="Teknologi Pengolahan Hasil Perikanan" <?= (set_value('option_1') == 'Teknologi Pengolahan Hasil Perikanan') ? 'selected="selected"' : '' ?> >Teknologi Pengolahan Hasil Perikanan</option>
                </select>
            </div>
            <div class="col-md-4">
               <label for="name">Pilihan 2</label>
               <select class="form-control" name="option_2" >
                    <option> -- Pilih Pilihan 2 -- </option>
                    <option value="Teknik Bodi Mobil" <?= (set_value('option_2') == 'Teknik Bodi Mobil') ? 'selected="selected"' : '' ?>  >Teknik Bodi Mobil (Otomotif)</option>
                    <option value="Teknik Kendaraan Ringan" <?= (set_value('option_2') == 'Teknik Kendaraan Ringan') ? 'selected="selected"' : '' ?>  >Teknik Kendaraan Ringan (Otomotif)</option>
                    <option value="Rekayasa Perangkat Lunak" <?= (set_value('option_2') == 'Rekayasa Perangkat Lunak') ? 'selected="selected"' : '' ?> >Rekayasa Perangkat Lunak (Komputer)</option>
                    <option value="Nautika Kapal Penangkap Ikan" <?= (set_value('option_2') == 'Nautika Kapal Penangkap Ikan') ? 'selected="selected"' : '' ?> >Nautika Kapal Penangkap Ikan (Kelautan)</option>
                    <option value="Teknologi Pengolahan Hasil Perikanan" <?= (set_value('option_2') == 'Teknologi Pengolahan Hasil Perikanan') ? 'selected="selected"' : '' ?> >Teknologi Pengolahan Hasil Perikanan</option>
                </select>
            </div>
            <div class="col-md-4">
               <label for="name">Pilihan 3</label>
               <select class="form-control" name="option_3" >
                    <option> -- Pilih Pilihan 3 -- </option>
                    <option value="Teknik Bodi Mobil" <?= (set_value('option_3') == 'Teknik Bodi Mobil') ? 'selected="selected"' : '' ?>  >Teknik Bodi Mobil (Otomotif)</option>
                    <option value="Teknik Kendaraan Ringan" <?= (set_value('option_3') == 'Teknik Kendaraan Ringan') ? 'selected="selected"' : '' ?>  >Teknik Kendaraan Ringan (Otomotif)</option>
                    <option value="Rekayasa Perangkat Lunak" <?= (set_value('option_3') == 'Rekayasa Perangkat Lunak') ? 'selected="selected"' : '' ?> >Rekayasa Perangkat Lunak (Komputer)</option>
                    <option value="Nautika Kapal Penangkap Ikan" <?= (set_value('option_3') == 'Nautika Kapal Penangkap Ikan') ? 'selected="selected"' : '' ?> >Nautika Kapal Penangkap Ikan (Kelautan)</option>
                    <option value="Teknologi Pengolahan Hasil Perikanan" <?= (set_value('option_3') == 'Teknologi Pengolahan Hasil Perikanan') ? 'selected="selected"' : '' ?> >Teknologi Pengolahan Hasil Perikanan</option>
                </select>
            </div>
        </div>
    </div>

    <div class="form-group message">
        <div class="row">
            <div class="col-md-4">
                <label for="message">Upload Foto<span class="required">*</span></label>
                <a href="#" class="thumbnail" id="photo_profile">
                    <?php if (!isset($data['content']['image']) || $data['content']['image'] == ''): ?>
                        <img data-src="holder.js/300x200" alt="300x200" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQQAAAC0CAYAAABytVLLAAAK/ElEQVR4Xu2aB4tUSxdFa8wJA+acc86K+YermBWMmDBizo6KOX/fLrhNzzgzbz/RR8NeF4ZpZk7frrNO1boVuqu7u/tn4YIABCDwfwJdCIF+AAEINAQQAn0BAhBoEUAIdAYIQAAh0AcgAIFfCTBDoFdAAALMEOgDEIAAMwT6AAQgMAABlgx0DwhAgCUDfQACEGDJQB+AAARYMtAHIAABhwB7CA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIXRYob99+1Zu375dnj59Wr5+/VpGjhxZpk2bVubOnVsGDRrUau2LFy/KrVu3yvv378vQoUNrzKJFi0pXV1cr5s6dO+XBgwfly5cvZcSIEWXevHllxowZ/zrjT58+lStXrpTu7u4yZMiQsnfv3h730P3V5pcvX5bPnz+XYcOGlUmTJtX2KL65/lR7/nUCvMEmgBBsVP9N4MWLF6sMNLA1iD9+/Fg/ePbs2WXp0qX1tQbeuXPn6uvhw4fXAf/z58864DUIdd27d69cv379l/usXbu2TJ482U7m2bNn5erVq1VOuvoSwpkzZ8qrV69qW8aPH1/FofgpU6aUNWvW/NH22A0n8LcIIITfwvZ33qTBf+zYsXrzzZs3l3HjxpVHjx7Vp7NmB3oy6/epU6fK27dv6+CXBCQIxWgwrl69ur7/yJEj9WndCKARhGI2bdpkJ3D48OEqgenTp9cZSW8haPZw9OjRer/t27eX0aNH9xDWvn37yuDBg/9Ye+yGE/hbBBDCb2H7O2/S4NJSQANIA1DXu3fvysmTJ+vrPXv2lO/fv9cBqJjdu3fX370vLSNOnDhR5aEBqdlG+8DV3548eVKf/HqqayBroCtG79NnrF+/vkycOLEuBbRcef36dZ2V9BZCe/uawd98vtqlNmq28E/t6SuPv0OZuw5EACF0eP+4efNm0dpbA3fXrl1VGOfPny9jxowpK1asKA8fPqwDWMuAqVOn1myaGC05du7cWf+mJcX+/ft7PMnPnj1bp/dz5swpS5YsKRcuXCjPnz8vM2fOLMuXL+9Bplmm9BaC7ithSQK6h/Yo1F79NLMRtz0dXoqI5iGEDi6z9hK0p6BLg1+DrVlCSBB68v748aOVwfz588vChQvrHoTep+m7nv7NdeDAgRrfLEc0I9BgllD0PslHm5hbt27tsRmo9/cnBP1PS51Lly6VN2/etD5Ls4uVK1fWDUa3PR1cipimIYQOLbUGvqb0egLPmjWrLFu2rLa0EYJeN09knSRoMGuJoBmBNvj6EsLBgwfr4G+EoHs8fvy4XL58uUVB+wt6sve+BhLCjRs3yt27d6tMxo4dW8Wg/QsJasGCBf0Koa/2dGg5YpqFEDqw1FoGSAa6tH5fvHhxq5XN07Z96i5pHDp0qOjIcuPGjXXQa1nR35Jh27ZtdcmhS7HagNR7NaPQ/9qPLpsP7k8Iko9OGdSeHTt21CNQyUCbo5qN6H6aibjt6cByRDUJIXRYuTXwNHg0yDUD0Pq+/dLpgk4Z2jcM9X8JQUuIDRs21Ce1BmR7THOCocGuzb/mOw3Xrl0r9+/fr5uTkkNfn6n79yeE5vRCM4MtW7a0mtqccmjZoBmH254OK0dccxBCB5VcA/L48eP1Cdts9PVunkShUwbFaONPG4DNYNVg15JB+wu6z4cPH+r3APR9AG3yaVmhtb1OEHTp5OD06dN1nb9u3br6WvfQwNZsof3qTwjtMxZ9tmYKapvaqLZqxjJhwgSrPR1UitimIIQOKn37/oAGafs3E9XMVatW1adte5wGrp7+mp63nw40Me1fcNJrzSA0QCUfzTQkDT3FdcypI0b96PsP2ktQvE4eNLA1+9DegP4mqejSJqfupftoWdDsIUg0koLapg1K5fFP7emgMkQ3BSF0UPmb6Xd/TWq+G6D/a4BpI08y0LpdMtAmXvv6X/fTj77JOGrUqHqS0HxLUd9i1P80oPUU1yVJ6NRB92yWDjqqlBD6urS3oT0OxWv2of0EiUMykzT0eZqtNNdA7emgMkQ3BSFEl5/kIdCTAEKgR0AAAi0CCIHOAAEIIAT6AAQg8CsBZgj0CghAgBkCfQACEGCGQB+AAAQGIMCSge4BAQiwZKAPQAACLBnoAxCAAEsG+gAEIOAQYA/BoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwC/wNER+9UOtmw8QAAAABJRU5ErkJggg==" style="width: 300px; height: 200px;">
                    <?php else: ?>
                        <img data-src="holder.js/300x200" alt="300x200" src="<?= base_url('pub.lcengine/upload/media/' . $data['content']['image']) ?>" style="width: 300px; height: 200px;">
                    <?php endif; ?>
                </a>
                <input id="photo_upload" name="userfile" style="display: none;" type="file" class="btn btn-info" >
            </div>
        </div>

    </div><!--//form-group-->

    <hr/>
    <button type="submit" class="btn btn-cta"><i class="fa fa-arrow-circle-right"></i> Selanjutnya</button>
    <?= form_close() ?>               
</article>

<script type="text/javascript">
    $(document).ready(function() {
        $('.file-trigger').click(function() {
            $('.file-target').trigger('click');
        });

        $("a#photo_profile").click(function() {
            $("input#photo_upload").trigger("click");
        });

        $("input#photo_upload").change(function() {

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) {
                return;
            }

            if (/^image/.test(files[0].type)) {

                var reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onloadend = function() {

                    $('a#photo_profile > img').remove();
                    var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                    $('a#photo_profile').append(html);

                }

            }

        });

    });
</script>