<article class="contact-form col-md-8 col-sm-7  page-row">                            
    <h3 class="title">Progres Pendaftaran PPDB</h3>
    <div class="progress progress-striped active">
        <div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
            40%
        </div>
    </div>
    <hr/>
    <h3 class="title">Data Siswa</h3>
    <?= form_open_multipart('routing/index/post-data-school-from') ?>
    <div class="form-group name">
        <?php echo get_alert(); ?>

        <input type="hidden" name="id_student" value="<?= $this->session->userdata('id_student') ?>" />

        <div class="row">
            <div class="col-md-6">
                <label for="name">Nama Sekolah</label>
                <input name="school_name" value="<?= set_value('school_name') ?>" id="name" type="text" class="form-control" placeholder="Masukkan Nama Sekolah Anda">
            </div>
            <div class="col-md-6">
                <label for="name">Status Sekolah</label>
                <div>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-warning <?= (set_value('status') == 'negeri') ? 'active' : '' ?>">
                            <input type="radio" id="option1" autocomplete="on" name="status" value="negeri"  <?= (set_value('status') == 'negeri') ? 'checked' : '' ?> >
                            <i class="fa fa-flag-checkered"></i> Negeri
                        </label>
                        <label class="btn btn-warning <?= (set_value('status') == 'swasta') ? 'active' : '' ?>">
                            <input type="radio" id="option2" autocomplete="on" name="status" value="swasta"  <?= (set_value('status') == 'swasta') ? 'checked' : '' ?> >
                            <i class="fa fa-flag"></i> Swasta
                        </label>
                    </div>
                </div>
            </div>
        </div>

    </div><!--//form-group-->

    <div class="form-group message">
        <label >Alamat Sekolah<span class="required">*</span></label>
        <textarea  name="school_address" class="form-control" rows="6" placeholder="Masukkan alamat sekolah asal Anda"><?= set_value('school_address') ?></textarea>
    </div><!--//form-group-->

    <hr/>
    <button type="submit" class="btn btn-cta"><i class="fa fa-arrow-circle-right"></i> Selanjutnya</button>
    <?= form_close() ?>               
</article>
