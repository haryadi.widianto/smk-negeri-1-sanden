<?php $slider = $data['slider']; ?>

<div id="promo-slider" class="slider flexslider">
    <ul class="slides">
        <?php if (!empty($slider)): ?>
            <?php foreach ($slider as $key => $value) : ?>
                <li>
                    <img src="<?= set_image($value['image']) ?>"  alt="<?= $value['title'] ?>" />
                    <p class="flex-caption">
                        <span class="main" ><?= $value['title'] ?></span>
                        <br />
                        <span class="secondary clearfix" ><?= $value['sub_title'] ?></span>
                    </p>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul><!--//slides-->
</div><!--//flexslider-->
