<?php
$data = array();
if (isset($leftcontent['breadcrumb']['data'])) {
    $data = $leftcontent['breadcrumb']['data'];
}
if (isset($headcontent['breadcrumb']['data'])) {
    $data = $headcontent['breadcrumb']['data'];
}
?>

<div id="breadcrumbs" class="clearfix">
    <div itemscope itemtype="">
        <a href="<?= base_url() ?>" itemprop="url" class="icon-home">
            <span itemprop="title">Home</span>
        </a> <span class="arrow">&gt;</span>
    </div>

    <?php if (!empty($data)): ?>
        <?php foreach ($data as $key => $value) : ?>
            <?php if (count($data) == $key + 1): ?>
                <span class="last-breadcrumbs">
                    <?= $value['title'] ?>
                </span>
            <?php else: ?>
                <div itemscope itemtype="">
                    <a href="<?= (isset($value['slug']) && isset($value['type'])) ? site_url('/routing/index/' . $value['type'] . '/' . $value['slug']) : "#" ?>" itemprop="url">
                        <span itemprop="title"><?= $value['title'] ?></span>
                    </a> <span class="arrow">&gt;</span>
                </div>  
            <?php endif; ?>
        <?php endforeach; ?>
    <?php endif; ?>


</div>