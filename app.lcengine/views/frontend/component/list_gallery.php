<?php $data = $data['gallery_list']; ?>
<div class="gallery-wrapper col-md-8 col-sm-7">
    <div class="page-row">
        <p></p>    
    </div>
    <div class="row page-row">
        <?php if (!empty($data)): ?>
            <?php foreach ($data as $key => $value) : ?>
                <?php if ($value->set_at_home == 'unset'): ?>
                    <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                        <div class="album-cover" style="height: 380px;">
                            <a href="<?= set_route('detail_gallery/' . $value->id) ?>" align="center">
                                <?php $cover = get_album_cover($value->id); ?>
                                <?php $coverimage = isset($cover['image']) ? $cover['image'] : ''; ?>
                                <img class="img-responsive" style="height: 250px; width: 100%;" src="<?= set_image($coverimage, 'image') ?>" alt="" />
                            </a>
                            <div class="desc">
                                <h4><small><a href="<?= set_route('detail_gallery/' . $value->id) ?>"><?= $value->title ?></a></small></h4>
                                <p><?= $value->description ?></p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div><!--//page-row-->
    <?= $this->pagination->create_links() ?>
</div><!--//gallery-wrapper-->