<ul id="nav-sidebar" class="clearfix">

    <?php if (!empty($sidebar_home['side_navbar']['data'])): ?>
        <?php foreach ($sidebar_home['side_navbar']['data'] as $key => $value) : ?>
            <li>
                <a href="<?= site_url('/routing/index/'.$value->data) ?>" class="clearfix">
                    <figure><img src="<?= base_url('pub.smkkbantul/upload/media/'.$value->image) ?>" alt="<?= $value->menu ?>" /></figure>
                    <strong class="title-nav-sidebar"> <?= $value->menu ?></strong>
                    <?= $value->description ?>
                </a>
            </li>
        <?php endforeach; ?>
    <?php endif; ?>



</ul>
