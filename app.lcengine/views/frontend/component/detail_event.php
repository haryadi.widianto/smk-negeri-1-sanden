<?php $data = $data['detail_event']; ?>

<div class="course-wrapper col-md-8 col-sm-7">                         
    <article class="course-item">
        <p class="featured-image page-row">
            <img class="img-responsive" src="<?= set_image($data['image']) ?>" alt="<?= $data['title'] ?>"/>
        </p>
        <div class="page-row box box-border">
            <ul class="list-unstyled no-margin-bottom">
                <li>
                    <strong>Tanggal :</strong> 
                    <em> <?= date('d F Y', strtotime($data['day_start'])) ?> - <?= date('d F Y', strtotime($data['day_end'])) ?> </em>
                </li>
                <li>
                    <strong>waktu : </strong> 
                    <em><?= date('H:i', strtotime($data['time_start'])) ?> - <?= date('H:i', strtotime($data['time_end'])) ?></em>
                </li>
                <li>
                    <strong>Location : </strong> 
                    <em><?= $data['location'] ?></em>
                </li>
            </ul>
        </div><!--//page-row-->
        <div class="page-row">
            <?= $data['description'] ?>
        </div><!--//page-row-->

    </article><!--//course-item-->                                              
</div><!--//course-wrapper-->