<?php $site_setting = $data['site_setting']; ?>
<?php $social_media = $data['social_media']; ?>
<?php $contact_setting = $data['contact_setting']; ?>
<?php $sub_topmenu = $data['sub_topmenu']; ?>

<!-- ******HEADER****** --> 
<header class="header">  
    <div class="top-bar">
        <div class="container">              
            <ul class="social-icons col-md-6 col-sm-6 col-xs-12 hidden-xs">

                <?php if (!empty($social_media)): ?>
                    <?php foreach ($social_media as $key => $value) : ?>
                        <li <?= (count($social_media) - 1 == ($key)) ? 'class="row-end"' : "" ?> ><a href="<?= $value->link ?>" ><i class="fa fa-<?= $value->social_media ?>"></i></a></li>
                    <?php endforeach; ?>
                <?php endif; ?>

            </ul><!--//social-icons-->
            <form class="pull-right search-form" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search the site...">
                </div>
                <button type="submit" class="btn btn-theme">Go</button>
            </form>         
        </div>      
    </div><!--//to-bar-->
    <div class="header-main container">
        <h1 class="logo col-md-4 col-sm-4">
            <a href="<?= site_url('/') ?>"><img id="logo" src="<?= set_image($site_setting['logo']) ?>" alt="Logo"></a>
        </h1><!--//logo-->           
        <div class="info col-md-8 col-sm-8">
            <ul class="menu-top navbar-right hidden-xs">
                <?php if (!empty($sub_topmenu)): ?>
                    <?php foreach ($sub_topmenu as $key => $value) : ?>
                        <li <?= (count($sub_topmenu) - 1 != ($key)) ? 'class="divider"' : "" ?>  >
                            <?php if ($value->type == 'url'): ?>
                                <a href="<?=  $value->data ?>"><?= $value->menu ?></a>
                            <?php else: ?>
                                <a href="<?= site_url('/routing/index/' . $value->data) ?>"><?= $value->menu ?></a>
                            <?php endif; ?>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>

            </ul><!--//menu-top-->
            <br />
            <div class="contact pull-right">
                <p class="phone" style="position: relative; top: -4px;"><i class="fa fa-phone"></i><?= $contact_setting['telp'] ?></p> 
                <p class="fax" style="margin-right: 25px;"><i class="fa fa-fax"></i><?= $contact_setting['fax'] ?></p> 
                <p class="email"><i class="fa fa-envelope"></i><a href="#"><?= $contact_setting['email'] ?></a></p>
            </div><!--//contact-->
        </div><!--//info-->
    </div><!--//header-main-->
</header><!--//header-->