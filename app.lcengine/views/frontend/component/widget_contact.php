<?php $data = $data['widget_contact']; ?>

<div class="footer-col col-md-3 col-sm-12 contact">
    <div class="footer-col-inner">
        <h3>Hubungi Kami</h3>
        <div class="row">
            <p class="adr col-md-12 col-sm-4"><i class="fa fa-map-marker pull-left"></i><?= strip_tags($data['address'])  ?></p>
            <p class="tel col-md-12 col-sm-4"><i class="fa fa-phone"></i><?= $data['telp'] ?></p>
            <p class="tel col-md-12 col-sm-4"><i class="fa fa-fax"></i><?= $data['fax'] ?></p>
            <p class="email col-md-12 col-sm-4"><i class="fa fa-envelope"></i><a href="#"><?= $data['email'] ?></a></p>  
        </div> 
    </div><!--//footer-col-inner-->            
</div><!--//foooter-col-->   