<?php $data = $data['detail_gallery']; ?>

<div class="album-wrapper col-md-8 col-sm-7"> 
    <div class="row">
        <?php if (!empty($data)): ?>
            <?php foreach ($data as $key => $value) : ?>
                <a class="prettyphoto col-md-3 col-sm-4 col-xs-6" rel="prettyPhoto[gallery]" title="<?= $value->title ?>" href="<?= set_image($value->image, $value->type) ?>">
                    <img style="height: 120px; width: 100%;"  class="img-responsive img-thumbnail" src="<?= set_image($value->image, $value->type) ?>" alt="<?= $value->title ?>" />
                </a>
                <?php endforeach; ?>
            <?php endif; ?>
    </div>
</div>