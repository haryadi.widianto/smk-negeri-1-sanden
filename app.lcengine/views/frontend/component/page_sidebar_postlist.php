<?php $data = $data['latest_post']; ?>

<section class="widget has-divider">
    <h3 class="title">Berita Terbaru</h3>

    <?php if (!empty($data)): ?>
        <?php foreach ($data as $key => $value) : ?>

            <article class="news-item row">       
                <figure class="thumb col-md-2 col-sm-3 col-xs-3">
                    <img src="<?= set_image($value->image, 'image', 'square') ?>" alt="<?= $value->title ?>" />
                </figure>
                <div class="details col-md-10 col-sm-9 col-xs-9">
                    <h4 class="title"><a href="<?= set_route('detail_post/'.$value->slug) ?>"><?= $value->title ?></a></h4>
                </div>
            </article><!--//news-item-->

        <?php endforeach; ?>
    <?php endif; ?>

</section><!--//widget-->