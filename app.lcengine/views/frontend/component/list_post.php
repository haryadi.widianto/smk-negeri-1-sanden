<?php $data = $data['post_list']; ?>

<div class="news-wrapper col-md-8 col-sm-7">   

    <?php if (!empty($data)): ?>
        <?php foreach ($data as $key => $value) : ?>

            <article class="news-item page-row has-divider clearfix row">       
                <figure class="thumb col-md-2 col-sm-3 col-xs-4">
                    <img class="img-responsive" src="<?= set_image($value->image, 'image', 'thumb') ?>" alt="<?= $value->title ?>" />
                </figure>
                <div class="details col-md-10 col-sm-9 col-xs-8">
                    <h3 class="title"><a href="<?= set_route('detail_post/' . $value->slug) ?>"><?= $value->title ?></a></h3>
                    <p><?= word_limiter(strip_tags($value->content), 50) ?></p>
                    <a class="btn btn-theme read-more" href="<?= set_route('detail_post/' . $value->slug) ?>">Selengkapnya<i class="fa fa-chevron-right"></i></a>
                </div>
            </article><!--//news-item-->

        <?php endforeach; ?>
    <?php endif; ?>

    <?= $this->pagination->create_links() ?>

</div><!--//news-wrapper-->