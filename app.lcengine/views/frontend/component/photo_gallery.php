<?php $data = $sidebar_home['photo_gallery']['data']; ?>

<aside id="gw_gallery-5" class="widget-container widget_gw_gallery">
    <div class="widget-wrapper clearfix">
        <h3 class="widget-title">Galeri Foto</h3>  
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $("#gw_gallery-5-slide").flexslider({
                    animation: "slide",
                    animationLoop: false,
                    pauseOnAction: true
                });
            });
        </script>
        <div id="gw_gallery-5-slide" class="flexslider">
            <ul class="slides">
                <?php if (!empty($data)): ?>
                    <?php foreach ($data as $key => $value) : ?>
                        <li>
                            <div class="slides-image">
                                <a href="<?= set_image($value->image) ?>" data-rel="prettyPhoto[pp-gw_gallery-5]">
                                    <img src="<?= set_image($value->image) ?>" alt="<?= $value->title ?>"  data-retina="<?= set_image($value->image) ?>" /></a>
                            </div>
                            <h4><a href="<?= set_image($value->image) ?>" data-rel="prettyPhoto[pp-gw_gallery-5-slide]"><?= $value->description ?></a></h4>
                        </li>
                    <?php endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</aside>