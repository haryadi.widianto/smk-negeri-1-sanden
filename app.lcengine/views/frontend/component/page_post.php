<?php $data = $data['post']; ?>

<div class="news-wrapper col-md-8 col-sm-7">                         

    <article class="news-item">

        <p class="meta text-muted">
            By: <a href="#"><?= $data['name'] ?>     </a> | Diposting pada : <?= date('d F Y') ?>
        </p>

        <?php if ($data['image'] != '') : ?>
            <p class="featured-image">
                <img style="margin-bottom: 40px;" class="img-responsive" src="<?= set_image($data['image']) ?>" alt="<?= $data['title'] ?>" />
            </p>
        <?php endif; ?>

        <?= $data['content'] ?>                       

    </article><!--//news-item-->

</div><!--//news-wrapper-->