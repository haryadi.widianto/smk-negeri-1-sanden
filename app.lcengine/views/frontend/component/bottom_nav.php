<?php $btm_nav_category = $data['btm_nav_category']; ?>
<?php $bottom_nav = $data['bottom_nav']; ?>

<div class="footer-col col-md-3 col-sm-4 about">
    <div class="footer-col-inner">
        <h3><?= $btm_nav_category['title'] ?></h3>
        <ul>
            <?php if (!empty($bottom_nav)): ?>
                <?php foreach ($bottom_nav as $key => $value) : ?>
                    <li>
                        <a href="<?= set_route($value->data) ?>">
                            <i class="fa fa-caret-right"></i>
                            <?= $value->menu ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div><!--//footer-col-inner-->
    
</div><!--//foooter-col-->