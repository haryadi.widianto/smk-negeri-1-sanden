

<article class="static-page">
    <h1 id="main-title">Pendaftaran Siswa Baru</h1>
    <p>Silakan masukkan data diri anda ke dalam kolom yang telah disediakan.</p>

    <?php if ($this->session->flashdata('falert') == 'fail'): ?>
        <div class="errordiv">
            Maaf ada kesalahan penginputan
        </div>
    <?php endif; ?>

    <form method="post" action="<?= site_url('post/send_application') ?>" id="form-contact" class="clearfix">
        <div>
            <label for="text-name">Nama Lengkap<span>*</span></label>
            <input type="text" name="name" class="input" id="text-name" placeholder="Nama Lengkap" /><br />
            <label for="text-email">Tempat dan Tanggal Lahir</label>
            <input style="width: 220px;" type="text" name="place_birth" class="input" id="text-email" placeholder="Tempat"/> - 
            <input  style="width: 135px;" type="date" name="date_birth" class="input" id="text-email" placeholder="Tanggal" />
            <br />
            <label for="text-phone">Asal Sekolah</label>
            <input type="text" name="school_from" class="input" id="text-phone" placeholder="Asal Sekolah" /><br />
            <label for="text-comment">Alamat <span>*</span></label>
            <textarea name="address" cols="10" rows="20" class="input textarea" id="text-comment"></textarea><br />
            <label for="text-phone">Nilai Rata-rata Raport</label>
            <input type="text" name="avg_raport" class="input" id="text-phone" placeholder="Nilai Rata-rata Rapot" /><br />
            <label for="text-phone">Nilai Rata-rata NEM</label>
            <input type="text" name="avg_nem" class="input" id="text-phone" placeholder="Nilai Rata-rata NEM" /><br />
            <label for="text-comment">Jurusan yang akan dipilih : <span>*</span></label>
            <select name="department" class="input" >
                <option > -- Pilih Jurusan -- </option>
                <option value="keperawatan">Keperawatan</option>
                <option value="analis_kesehatan">Analis Kesehatan</option>
                <option value="farmasi">Farmasi</option>
            </select>
            <input type="submit" class="button" value="Daftar" />
        </div>
    </form>
</article>