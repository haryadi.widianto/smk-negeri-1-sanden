<!--Less styles -->
<?php $site_setting = $data['site_setting']; ?>

<link rel="shortcut icon" href="<?= set_image($site_setting['favicon']) ?>">  
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>   
<!-- Global CSS -->
<link rel="stylesheet" href="<?= base_url('pub.lcengine/frontend/') ?>/plugins/bootstrap/css/bootstrap.min.css">   
<!-- Plugins CSS -->    
<link rel="stylesheet" href="<?= base_url('pub.lcengine/frontend/') ?>/plugins/font-awesome/css/font-awesome.css">
<link rel="stylesheet" href="<?= base_url('pub.lcengine/frontend/') ?>/plugins/flexslider/flexslider.css">
<link rel="stylesheet" href="<?= base_url('pub.lcengine/frontend/') ?>/plugins/pretty-photo/css/prettyPhoto.css"> 
<!-- Theme CSS -->  
<link id="theme-style" rel="stylesheet" href="<?= base_url('pub.lcengine/frontend/') ?>/css/styles.css">
<style type="text/css" >

    .header{
        background-image: url(<?= base_url('pub.lcengine/upload/') ?>/header/header_02.jpg);
        background-repeat: no-repeat;
        background-position: 0% 27%;
        background-size: 100%;
    }

    .header .social-icons a {
        background: #6091ba;
    }

    .header .social-icons a li {
        color: white;
    }

    .header .top-bar {
        background: #2f506c3b;
    }

    .header .social-icons a .fa {
        color: rgba(255, 255, 255, 0.77);
    }

    .header .menu-top li a {
        display: inline-block;
        color: #fbe73d;
        padding: 0px 10px;
        text-shadow: 2px 2px 5px #252525;
        font-size: 12px;
        font-weight: bold;
    }

    .header .menu-top li a:hover{
        color: #fbe73d;
    }

    .header .contact .fa {
        color: #f8e248;
        margin-right: 8px;
        font-size: 18px;
        position: relative;
        top: 1px;
    }

    .header div.contact p {
        color: #fbe840;
        font-size: 13px;
        font-weight: bold;
        text-shadow: 2px 2px 5px #252525;
    }

    .header div.contact p a {
        color: #fbe840;
        font-size: 13px;
        font-weight: bold;
        text-shadow: 2px 2px 5px #252525;
    }

    .carousel-inner div ul{
        display: flex;
        align-items: center;
    }

    .carousel-control{
        display: flex;
        align-items: center;
    }

</style>


<style type="text/css" >
    .row-home{
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
    }

    .col-leftcontent{
        display: flex;
        flex-direction: column;
    }

    .row-video{
        height: 100%;
    }
</style>