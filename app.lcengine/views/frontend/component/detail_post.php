<?php $data = $leftcontent['detail_post']['data']; ?>

<article class="static-page news">
    <header class="clearfix">
        <figure>
            <img style="width: 350px; height: 230px;" src="<?= set_image($data['image']) ?>" data-retina="<?= set_image($data['image']) ?>" alt="<?= $data['title'] ?>" />
        </figure>
        <aside>
            <h1 id="news-title"> <?= $data['title'] ?></h1>
            <p id="link-category"><a href="newsdetail.html#"> <?= $data['tag'] ?></a></p>
            <p id="blog-time" class="clearfix">
                <time datetime="2013-11-26"><?= date('j F Y', strtotime($data['created_at'])) ?></time> 
                <!--<a href="newsdetail.html#" id="link-comment-header">Comments (6)</a>-->
            </p>
            <ul id="social-link" class="clearfix">
<!--                <li>
                    <div id="fb-root"></div>
                    <script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
                    <fb:like href="http://cubicthemes.com" send="false" layout="button_count" width="40" show_faces="false" font=""></fb:like></li>
                <li>
                    <a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal">Tweet</a><script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
                </li>-->
                <li class="last">
                    <div class="g-plusone" data-size="medium"></div>
                    <script type="text/javascript">
                        (function() {
                            var po = document.createElement('script');
                            po.type = 'text/javascript';
                            po.async = true;
                            po.src = 'https://apis.google.com/js/plusone.js';
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(po, s);
                        })();
                    </script>
                </li>
            </ul>

        </aside>
    </header>
    <?= $data['content'] ?>
</article>
