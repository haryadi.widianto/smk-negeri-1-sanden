<header class="page-heading clearfix">
    <h1 class="heading-title pull-left"><?= $data ?></h1>
    <div class="breadcrumbs pull-right">
        <ul class="breadcrumbs-list">
            <li class="breadcrumbs-label">You are here:</li>
            <li><a href="<?= site_url('/') ?>">Home</a><i class="fa fa-angle-right"></i></li>
            <li><a href="index.html">News</a><i class="fa fa-angle-right"></i></li>
            <li class="current">Morbi bibendum consectetuer vulputate sollicitudin</li>
        </ul>
    </div><!--//breadcrumbs-->
</header>