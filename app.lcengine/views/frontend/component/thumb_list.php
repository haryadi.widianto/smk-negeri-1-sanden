<?php $thumb_list = $data['thumb_list']; ?>

<section class="awards">
    <div class="row">
        <div class="col-md-12" style="text-align: center;margin-bottom: -20px;margin-top: -5px;">
            <h2 class="section-heading text-highlight" style="margin: 0px 0px 0px 0px; color: #2f506c;">MITRA KAMI<h2>
        </div>
    </div>
    <div id="awards-carousel" class="awards-carousel carousel slide">
        <div class="carousel-inner">

            <?php if (!empty($thumb_list)): ?>
                <?php foreach ($thumb_list as $key => $value) : ?>

                    <?php if (($key + 1) == 1 || ($key) % 6 == 0): ?>
                        <div class="item <?= (($key + 1) == 1) ? 'active' : '' ?>"> 
                            <ul class="logos">
                            <?php endif; ?>
                            <li class="col-md-2 col-sm-2 col-xs-4">
                                <a href="<?= $value->data ?>"><img class="img-responsive" src="<?= set_image($value->image) ?>"  alt="<?= $value->menu ?>" /></a>
                            </li>
                            <?php if (($key + 1) % 6 == 0): ?>
                            </ul><!--//slides-->
                        </div><!--//item-->
                    <?php endif; ?>

                <?php endforeach; ?>
            <?php endif; ?>

        </div><!--//carousel-inner-->

        <a class="left carousel-control" href="#awards-carousel" data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a class="right carousel-control" href="#awards-carousel" data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>

    </div>
</section>