<?php $data = $leftcontent['detail_team']['data']; ?>

<article class="static-page">
    <div id="profile-team">
        <img src="<?= set_image($data['image']) ?>" data-retina="<?= set_image($data['image']) ?>" alt="" />
        <ul id="list-social-team">
            <li class="facebook-team"><a href="<?= $data['facebook'] ?>" target="_blank"><span></span>Facebook</a></li>
            <li class="twitter-team"><a href="<?= $data['twitter'] ?>" target="_blank" ><span></span>Twitter</a></li>
            <li class="gplus-team"><a href="<?= $data['gplus'] ?>" target="_blank" ><span></span>Google+</a></li>
            <li class="linkedin-team"><a href="<?= $data['linkedin'] ?>" target="_blank" ><span></span>Linkedin</a></li>
            <li class="position-team"><?= $data['position'] ?></li>
            <li class="email-team"><?= $data['email'] ?></li>
        </ul>
    </div>
    <h1 id="main-title"><?= $data['name'] ?></h1>
    <?= $data['resume']  ?>
</article>