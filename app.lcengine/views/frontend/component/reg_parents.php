<article class="contact-form col-md-8 col-sm-7  page-row">                            
    <h3 class="title">Progres Pendaftaran PPDB</h3>
    <div class="progress progress-striped active">
        <div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
            60%
        </div>
    </div>
    <hr/>
    <h3 class="title">Data Orang Tua Siswa</h3>
    <?= form_open_multipart('routing/index/post-data-orang-tua') ?>
    <div class="form-group name">
        <?php echo get_alert(); ?>

        <input type="hidden" name="id_student" value="<?= $this->session->userdata('id_student') ?>" />

        <div class="row">
            <div class="col-md-6">
                <label for="name">Nama Orang Tua</label>
                <input name="name" value="<?= set_value('name') ?>" id="name" type="text" class="form-control" placeholder="Masukkan nama orang tua">
            </div>
            <div class="col-md-6">
                <label for="name">Pekerjaan Orang Tua</label>
                <input name="occupation" value="<?= set_value('occupation') ?>" id="name" type="text" class="form-control" placeholder="Masukkan pekerjaan orang tua">
            </div>
        </div>

    </div><!--//form-group-->

    <div class="form-group message">
        <label >Alamat Orang Tua<span class="required">*</span></label>
        <textarea  name="address" class="form-control" rows="6" placeholder="Masukkan alamat orang tua"><?= set_value('address') ?></textarea>
    </div><!--//form-group-->
    
    <hr/>
    
    
    <h3 class="title">Data Wali Siswa</h3>
    <?= form_open_multipart('routing/index/post-data-orang-tua') ?>
    <div class="form-group name">
        
        <div class="row">
            <div class="col-md-6">
                <label for="name">Nama Wali Siswa</label>
                <input name="alt_name" value="<?= set_value('alt_name') ?>" id="name" type="text" class="form-control" placeholder="Masukkan wali siswa">
            </div>
            <div class="col-md-6">
                <label for="name">Pekerjaan Wali Siswa</label>
                <input name="alt_occupation" value="<?= set_value('alt_occupation') ?>" id="name" type="text" class="form-control" placeholder="Masukkan pekerjaan wali siswa">
            </div>
        </div>

    </div><!--//form-group-->

    <div class="form-group message">
        <label >Alamat Wali Siswa<span class="required">*</span></label>
        <textarea  name="alt_address" class="form-control" rows="6" placeholder="Masukkan alamat wali siswa"><?= set_value('alt_address') ?></textarea>
    </div><!--//form-group-->

    <hr/>
    <button type="submit" class="btn btn-cta"><i class="fa fa-arrow-circle-right"></i> Selanjutnya</button>
    <?= form_close() ?>               
</article>
