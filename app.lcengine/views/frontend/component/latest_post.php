<?php $latest_post = $data['latest_post']; ?>

<section class="news">
    <h1 class="section-heading text-highlight"><span class="line">Berita Terbaru</span></h1>     
    <div class="carousel-controls">
        <a class="prev" href="#news-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
        <a class="next" href="#news-carousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
    </div><!--//carousel-controls--> 
    <div class="section-content clearfix">
        <div id="news-carousel" class="news-carousel carousel slide">
            <div class="carousel-inner" style="height: 130px;">
                <?php if (!empty($latest_post)): ?>
                    <?php foreach ($latest_post as $key => $value) : ?>

                        <?php if (($key + 1) == 1 || ($key) % 3 == 0): ?>
                            <div class="item <?= (($key + 1) == 1)?'active':'' ?>"> 
                            <?php endif; ?>

                            <div class="col-md-4 news-item">
                                <h2 class="title"><a href="<?= set_route('detail_post/' . $value->slug) ?>"><?= word_limiter(strip_tags($value->title), 15) ?></a></h2>
                                <img class="thumb" height="100" width="100" src="<?= set_image($value->image, 'image', 'thumb') ?>"  alt="" />
                                <a class="read-more" href="<?= set_route('detail_post/' . $value->slug) ?>">Selengkapnya<i class="fa fa-chevron-right"></i></a>                
                            </div><!--//news-item-->

                            <?php if (($key + 1) % 3 == 0): ?>
                            </div><!--//item-->
                        <?php endif; ?>

                    <?php endforeach; ?>
                <?php endif; ?>
            </div><!--//carousel-inner-->
        </div><!--//news-carousel-->  
    </div><!--//section-content-->     
</section><!--//news-->