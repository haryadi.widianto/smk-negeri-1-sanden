<?php $data = $leftcontent['intro_article']['data']; ?>

<article id="intro">
    <h1><?= $data['title'] ?></h1>
    <figure>
        <img  style="width: 300px; max-width: 363px; max-height: 250px;" src="<?= set_image($data['image']) ?>" alt="<?= $data['title'] ?>" />
    </figure>
    <p><?= word_limiter($data['content'], 40)  ?></p>
    <a href="<?= site_url('/routing/index/page/'.$data['slug']) ?>" class="more-intro">- Learn More</a>
</article>