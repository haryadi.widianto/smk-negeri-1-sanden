<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Javascript -->          
<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/plugins/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/plugins/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/plugins/bootstrap/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/plugins/bootstrap-hover-dropdown.min.js"></script> 
<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/plugins/back-to-top.js"></script>
<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/plugins/jquery-placeholder/jquery.placeholder.js"></script>
<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/plugins/flexslider/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/plugins/jflickrfeed/jflickrfeed.min.js"></script> 

<script type="text/javascript" src="<?= base_url('pub.lcengine/frontend/') ?>/js/main.js"></script>
