<?php $meta = $data['video_meta']; ?>
<?php $data = $data['video']; ?>

<section class="video row-video" >
    <h1 class="section-heading text-highlight"><span class="line"><?= $meta['title'] ?></span></h1>
    <div class="carousel-controls">
        <a class="prev" href="#videos-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
        <a class="next" href="#videos-carousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
    </div><!--//carousel-controls-->
    <div class="section-content" style="height: 100%;" >    
        <div id="videos-carousel" class="videos-carousel carousel slide" style="height: 100%;">
            <div class="carousel-inner" style="height: 100%;">
                <?php if (!empty($data)): ?>
                    <?php foreach ($data as $key => $value) : ?>
                        <div class="item <?= ($key == 0)?'active':'' ?> " style="height: 100%;">            
                            <iframe style="height: 85%;" class="video-iframe" src="//www.youtube.com/embed/<?= $value->image ?>?rel=0&amp;wmode=transparent" frameborder="0" allowfullscreen=""></iframe>
                        </div><!--//item-->
                    <?php endforeach; ?>
                <?php endif; ?>
            </div><!--//carousel-inner-->
        </div><!--//videos-carousel-->                            
        <!--<p class="description"><?= $meta['description'] ?></p>-->
    </div><!--//section-content-->
</section><!--//video-->