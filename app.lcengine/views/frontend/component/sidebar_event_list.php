<?php $sidebar_event_list = $data['sidebar_event_list']; ?>

<section class="events">
    <h1 class="section-heading text-highlight"><span class="line">Agenda</span></h1>
    <div class="section-content">

        <?php if (!empty($sidebar_event_list)): ?>
            <?php foreach ($sidebar_event_list as $key => $value) : ?>

                <div class="event-item">
                    <p class="date-label">
                        <span class="month"><?= date('M', strtotime($value->day_start)) ?></span>
                        <span class="date-number"><?= date('d', strtotime($value->day_start)) ?></span>
                    </p>
                    <div class="details">
                        <h2 class="title">
                            <a style="color: #7d7d7d; font-family: 'open sans', arial, sans-serif; font-weight: 300; margin-bottom: 20px; font-size: 16px;" href="<?= set_route('detail_event/'.$value->slug) ?>"><?= $value->title ?></a>
                        </h2>
                        <p class="time">
                            <i class="fa fa-clock-o"></i>
                            <?= date('H:i', strtotime($value->time_start)) ?> - 
                            <?= date('H:i', strtotime($value->time_end)) ?>
                        </p>
                        <p class="location">
                            <i class="fa fa-map-marker"></i>
                            <?= $value->location ?>
                        </p>                            
                    </div><!--//details-->
                </div><!--event-item-->  

            <?php endforeach; ?>
        <?php endif; ?>

        <a class="read-more" href="<?= set_route('list_event') ?>">All events<i class="fa fa-chevron-right"></i></a>
    </div><!--//section-content-->
</section><!--//events-->