<?php $data = $footercontent['principal_article']['data']; ?>
<article id="intro-principal" class="static-page">
    <h3 id="title-principal"><?= $data['title'] ?></h3>
    <figure>
        <img style="max-width: 300px; margin-bottom: 40px;" src="<?= set_image( $data['image']) ?>" data-retina="<?= set_image( $data['image']) ?>" alt="<?= $data['title'] ?>" />
    </figure>
    <div id="content-principal">
        <?= word_limiter($data['content'], 100)  ?>
        <p><a href="<?= site_url('/routing/index/page/'.$data['slug']) ?>" class="more-intro">- Learn More</a></p>
    </div>
</article>