<?php $site_setting = $data['site_setting']; ?>
<?php $top_navigation = $data['top_navigation']; ?>


<!-- ******NAV****** -->
<nav class="main-nav" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button><!--//nav-toggle-->
        </div><!--//navbar-header-->            
        <div class="navbar-collapse collapse" id="navbar-collapse">
            <?= $this->menu_frontend->make($top_navigation) ?>
        </div><!--//navabr-collapse-->
    </div><!--//container-->
</nav><!--//main-nav-->

