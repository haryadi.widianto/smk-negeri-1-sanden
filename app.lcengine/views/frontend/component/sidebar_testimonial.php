<?php $sidebar_testimonial = $data['sidebar_testimonial']; ?>

<section class="testimonials">
    <h1 class="section-heading text-highlight"><span class="line"> Testimonial</span></h1>
    <div class="carousel-controls">
        <a class="prev" href="#testimonials-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
        <a class="next" href="#testimonials-carousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
    </div><!--//carousel-controls-->
    <div class="section-content">
        <div id="testimonials-carousel" class="testimonials-carousel carousel slide">
            <div class="carousel-inner">

                <?php if (!empty($sidebar_testimonial)): ?>
                    <?php foreach ($sidebar_testimonial as $key => $value) : ?>

                        <div class="item <?=($key == 0)?'active':''?>">
                            <blockquote class="quote">                                  
                                <p>
                                    <i class="fa fa-quote-left"></i>
                                    <?= $value->content ?>
                                </p>
                            </blockquote>                
                            <div class="row">
                                <p class="people col-md-8 col-sm-3 col-xs-8">
                                    <span class="name"><?= $value->name ?></span><br />
                                    <span class="title"><?= $value->occuppation ?></span></p>
                                <img class="profile col-md-4 pull-right" src="<?= get_gravatar($value->email) ?>"  alt="<?= $value->name ?>" />
                            </div>                               
                        </div><!--//item-->
                        
                    <?php endforeach; ?>
                <?php endif; ?>

            </div><!--//carousel-inner-->
        </div><!--//testimonials-carousel-->
    </div><!--//section-content-->
</section><!--//testimonials-->