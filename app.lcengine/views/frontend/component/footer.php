<?php $social_media = $data['footer']; ?>

<div class="bottom-bar">
    <div class="container">
        <div class="row">
            <small class="copyright col-md-6 col-sm-12 col-xs-12">Copyright @ 2015 Develop by <a href="http://linecastle.com">LineCastle</a></small>
            <ul class="social pull-right col-md-6 col-sm-12 col-xs-12">
                <?php if (!empty($social_media)): ?>
                    <?php foreach ($social_media as $key => $value) : ?>
                        <li <?= (count($social_media) - 1 == ($key)) ? 'class="row-end"' : "" ?> ><a href="<?= $value->link ?>" ><i class="fa fa-<?= $value->social_media ?>"></i></a></li>
                            <?php endforeach; ?>
                        <?php endif; ?>
            </ul><!--//social-->
        </div><!--//row-->
    </div><!--//container-->
</div><!--//bottom-bar-->