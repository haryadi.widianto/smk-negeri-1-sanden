<?php $banner = $data['banner']; ?>

<section class="promo box box-dark">        
    <div class="col-md-9">
        <h1 class="section-heading"><?= $banner['banner_title'] ?></h1>
        <p><?= $banner['banner_description'] ?></p>   
    </div>  
    <div class="col-md-3">
        <a target="_blank" class="btn btn-cta" href="<?= $banner['banner_link'] ?>"><i class="fa fa-play-circle"></i><?= $banner['banner_link_title'] ?></a>  
    </div>
</section><!--//promo-->