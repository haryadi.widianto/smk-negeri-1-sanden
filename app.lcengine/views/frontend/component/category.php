
<ul class="breadcrumb">
    <li><a href="#">Home</a> <span class="divider">/</span></li>
    <li class="active"><?= $main['category']['page']['product_category'] ?></li>
</ul>
<h3> <?= $main['category']['page']['product_category'] ?> <small class="pull-right"> 40 products are available </small></h3>	
<hr class="soft"/>
<p><?= $main['category']['page']['description'] ?></p>
<hr class="soft"/>
<form class="form-horizontal span6">
    <div class="control-group">
        <label class="control-label alignL">Sort By </label>
        <select>
            <option>Priduct name A - Z</option>
            <option>Priduct name Z - A</option>
            <option>Priduct Stoke</option>
            <option>Price Lowest first</option>
        </select>
    </div>
</form>

<div id="myTab" class="pull-right">
    <a href="#listView" data-toggle="tab"><span class="btn btn-large"><i class="icon-list"></i></span></a>
    <a href="#blockView" data-toggle="tab"><span class="btn btn-large btn-primary"><i class="icon-th-large"></i></span></a>
</div>
<br class="clr"/>

<div class="tab-content">

    <div class="tab-pane" id="listView">



        <?php if (!empty($main['category']['data'])): ?>
            <?php foreach ($main['category']['data'] as $key => $value) : ?>

                <div class="row">	  
                    <div class="span2">
                        <?php if ($value->image != ''): ?>
                            <img src="<?= base_url('/pub.keramik/upload/media/' . $value->image) ?>" alt="<?= $value->product ?>"/>
                        <?php else: ?>
                            <img src="<?= base_url('/pub.keramik/upload/media/noimage.png') ?>" alt=""/>
                        <?php endif; ?>
                    </div>
                    <div class="span4">
                        <h3><?= ($value->new_tag == 1) ? 'New |' : '' ?> <?= $value->status ?></h3>				
                        <hr class="soft"/>
                        <h5><?= $value->product ?> </h5>
                        <p><?= word_limiter($value->description, 15); ?></p>
                        <a class="btn btn-small pull-right" href="<?= base_url("index.php/routing/index/product-detail/$value->slug") ?>">View Details</a>
                        <br class="clr"/>
                    </div>
                    <div class="span3 alignR">
                        <form class="form-horizontal qtyFrm">
                            <h3>Rp. <?= $value->price ?></h3>
                            <label class="checkbox">
                                <input type="checkbox">  Adds product to compair
                            </label><br/>

                            <?php if ($this->session->userdata('login')): ?>
                                <a class="btn btn-large btn-primary" href="<?= base_url("index.php/transaction/put_item/$value->id_product") ?>">Add to <i class="icon-shopping-cart"></i>
                                </a>
                            <?php else: ?>
                                <a role="modal" data-toggle="modal" class="btn btn-large btn-primary" href="#login">Add to <i class="icon-shopping-cart"></i>
                                </a>
                            <?php endif; ?>
                            <a href="<?= base_url("index.php/routing/index/product-detail/$value->slug") ?>" class="btn btn-large"><i class="icon-zoom-in"></i></a>
                        </form>
                    </div>
                </div>
                <hr class="soft"/>

            <?php endforeach; ?>
        <?php endif; ?>

    </div>

    <div class="tab-pane  active" id="blockView">
        <ul class="thumbnails">
            <?php if (!empty($main['category']['data'])): ?>
                <?php foreach ($main['category']['data'] as $key => $value) : ?>
                    <li class="span3">
                        <div class="thumbnail">
                            <?php if ($value->new_tag == 1): ?>
                                <i class="tag"></i>
                            <?php endif; ?>
                            <a href="<?= base_url("index.php/routing/index/product-detail/$value->slug") ?>">
                                <?php if ($value->image != ''): ?>
                                    <img style="height: 200px;" src="<?= base_url('/pub.keramik/upload/media/' . $value->image) ?>" alt="<?= $value->product ?>"/>
                                <?php else: ?>
                                    <img style="height: 200px;" src="<?= base_url('/pub.keramik/upload/media/noimage.png') ?>" alt=""/>
                                <?php endif; ?>
                            </a>
                            <div class="caption">
                                <h5><?= $value->product ?></h5>
                                <p><?= word_limiter($value->description, 15); ?></p>
                                <h4 style="text-align:center">
                                    <a class="btn" href="<?= base_url("index.php/routing/index/product-detail/$value->slug") ?>"> 
                                        <i class="icon-zoom-in"></i>
                                    </a> 
                                    <?php if ($this->session->userdata('login')): ?>
                                        <a class="btn" href="<?= base_url("index.php/transaction/put_item/$value->id_product") ?>">Add to <i class="icon-shopping-cart"></i>
                                        </a>
                                    <?php else: ?>
                                        <a role="modal" data-toggle="modal" class="btn" href="#login">Add to <i class="icon-shopping-cart"></i>
                                        </a>
                                    <?php endif; ?>
                                    <a class="btn btn-primary" href="#">Rp. <?= $value->price ?></a>
                                </h4>
                            </div>
                        </div>
                    </li>

                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
        <hr class="soft"/>
    </div>
</div>


<div class="pagination">
    <?= $this->pagination->create_links() ?>
</div>
<br class="clr"/>