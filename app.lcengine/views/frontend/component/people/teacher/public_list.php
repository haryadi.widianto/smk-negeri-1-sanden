<?php $dataguru = $data['dataguru']; ?>

<?php $oldinput = get_oldinput(); ?>

<article class="contact-form col-md-12 col-sm-7  page-row">                            

    <div class="row">
        <div class="col-md-12" style="text-align: center; margin-bottom: 25px;" >
            <h3 class="title" style="margin-top: 0px; margin-bottom: 0px;">Daftar <?= $data['position-title'] ?></h3>
        </div>
    </div>

    <?php if ($dataguru): ?>

        <?php foreach ($dataguru as $key => $item): ?>

            <?php if ($key == 0 || $key % 4 == 0): ?>
                <div class="row">
                <?php endif; ?>

                <div class="col-md-3">
                    <a href="<?= set_route('detail/' . $item->id_user) ?>">
                        <div class="thumbnail" style="border: none;">
                            <img class="img-circle" src="<?= set_image($item->photo, 'image', 'square') ?>" alt="<?= $item->specifically_assignments ?>" style="height: 150px; width:150px;">
                            <div class="caption center" style="text-align: center;">
                                <h5 style="font-weight: bold; line-height: 20px;"><?= $item->fullname ?></h5>
                                <h5 style="line-height: 20px; margin-top: -5px;">
                                    <?php if ($item->specifically_assignments != ''): ?>
                                        <?= $data['position-title'] == 'Guru' ? $data['position-title'] : 'Staf' ?>
                                    <?php endif; ?>
                                    <?= $item->specifically_assignments ?>
                                </h5>
                            </div>
                        </div>
                    </a>
                </div>

                <?php if (($key + 1) % 4 == 0 || ($key + 1) == count($dataguru)): ?>
                </div>
            <?php endif; ?>

        <?php endforeach; ?>

    <?php else: ?>

        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-info alert-dismissable" style="text-align: center; padding-top: 25px;">
                    <h4>Data Tidak Tersedia</h4>
                </div>
            </div>
        </div>

    <?php endif; ?>

</article>
