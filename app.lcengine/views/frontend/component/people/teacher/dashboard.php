<?php $data = $data['dataguru']; ?>
<?php $oldinput = get_oldinput(); ?>
<?php $visibility = json_decode($data['visibility'], true); ?>

<style type="text/css">
    .visibility{
        position: absolute;
        margin-top: 2px;
        margin-left: 4px;
    }
</style>

<article class="contact-form col-md-12  page-row">                            

    <?= form_open('routing/index/update-visibility-guru', ['class' => 'form-visibility']) ?>

    <br/>

    <div class="row">
        <div class="col-md-8" >
            <h3 class="title" style="margin-top: 0px; margin-bottom: 0px;">Dashboard Guru Dan Karyawan</h3>
        </div>
        <div class="col-md-4 pull-right" style="height: max-content;">
            <a href="<?= site_url('routing/index/update-guru') ?>" class="btn btn-info pull-right">
                <i class="fa fa-edit"></i> Perbarui
            </a>
            <button type="submit" class="btn btn-info pull-right" style="margin-right: 10px;">
                <i class="fa fa-user-secret"></i> Perbarui Privasi
            </button>
        </div>
    </div>

    <hr/>


    <div class="form-group name">
        <?php echo get_alert(); ?>
        <div class="row">
            <div class="col-md-3">
                <a href="#" class="thumbnail" id="photo_profile">
                    <img data-src="holder.js/300x200" alt="300x200" src="<?= set_image($data['photo'], 'image') ?>" style="width: 100%;">
                </a>
            </div>
            <div class="col-md-9">
                <table class="table table-striped table-bordered">
                    <tr>
                        <th width="25%">Posisi</th>
                        <td width="60%" ><?= appear($data['position']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="position" type="checkbox" value="1" <?= isValued($visibility, 'position') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Nama Lengkap dan Gelar</th>
                        <td><?= appear($data['fullname']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="fullname" type="checkbox" value="1"  <?= isValued($visibility, 'fullname') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>NIP Lama</th>
                        <td><?= appear($data['old_nip']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="old_nip" type="checkbox" value="1" <?= isValued($visibility, 'old_nip') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>NIP Baru</th>
                        <td><?= appear($data['new_nip']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="new_nip" type="checkbox" value="1" <?= isValued($visibility, 'new_nip') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Tempat / Tanggal Lahir</th>
                        <td><?= appear($data['birth_place']) . ', ' . appear($data['birth_day']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="birth_place" type="checkbox" value="1" <?= isValued($visibility, 'birth_place') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Instansi Induk</th>
                        <td><?= appear_alt($data['parent_institute'], $data['other_parent_institute']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="parent_institute" type="checkbox" value="1" <?= isValued($visibility, 'parent_institute') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Unit Organisasi</th>
                        <td><?= appear_alt($data['organization_unit'], $data['other_organization_unit']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="organization_unit" type="checkbox" value="1" <?= isValued($visibility, 'organization_unit') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Unit Kerja</th>
                        <td><?= appear_alt($data['work_unit'], $data['other_work_unit']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="work_unit" type="checkbox" value="1" <?= isValued($visibility, 'work_unit') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Pangkat / Golongan / Ruang CPNS</th>
                        <td><?= appear($data['cpns_grade']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="cpns_grade" type="checkbox" value="1" <?= isValued($visibility, 'cpns_grade') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Pangkat / Golongan / Ruang Terakhir</th>
                        <td><?= appear($data['last_grade']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="last_grade" type="checkbox" value="1" <?= isValued($visibility, 'last_grade') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>TMT Eselon</th>
                        <td><?= appear($data['tmt_eselon']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="tmt_eselon" type="checkbox" value="1" <?= isValued($visibility, 'tmt_eselon') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Jabatan Terakhir</th>
                        <td><?= appear($data['last_position']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="last_position" type="checkbox" value="1" <?= isValued($visibility, 'last_position') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Masa Kerja Golongan (tahun-bulan)</th>
                        <td><?= appear($data['working_group_period']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="working_group_period" type="checkbox" value="1" <?= isValued($visibility, 'working_group_period') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <th>Email</th>
                        <td><?= appear($data['email']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="email" type="checkbox" value="1" <?= isValued($visibility, 'email') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                </table>


                <table class="table table-striped table-bordered" >
                    <tr>
                        <th colspan="3">
                            <h4>DIKLAT Pra / Jabatan Terakhir</h4>
                        </th>
                    </tr>
                    <tr>
                        <th width="25%" >Nomor SK</th>
                        <td width="60%" ><?= appear($data['sk_number']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="sk_number" type="checkbox" value="1" <?= isValued($visibility, 'sk_number') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Jumlah Jam</th>
                        <td><?= appear($data['total_hour']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="total_hour" type="checkbox" value="1" <?= isValued($visibility, 'total_hour') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Tempat Pelaksanaan</th>
                        <td><?= appear($data['execution_place']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="execution_place" type="checkbox" value="1" <?= isValued($visibility, 'execution_place') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                </table>

                <table class="table table-striped table-bordered" >
                    <tr>
                        <th colspan="3">
                            <h4>Pendidikan</h4>
                        </th>
                    </tr>
                    <tr>
                        <th width="25%" >Pendidikan Saat CPNS</th>
                        <td width="60%" ><?= appear($data['education_when_cpns']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="education_when_cpns" type="checkbox" value="1" <?= isValued($visibility, 'education_when_cpns') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Pendidikan Terakhir</th>
                        <td><?= appear_alt($data['last_education'], $data['other_last_education']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="last_education" type="checkbox" value="1" <?= isValued($visibility, 'last_education') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Pendidikan Terakhir :<br/>Jurusan</th>
                        <td><?= appear($data['education_field']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="education_field" type="checkbox" value="1" <?= isValued($visibility, 'education_field') ?>  />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Pendidikan Terakhir :<br/>Nama Sekolah / Lembaga</th>
                        <td><?= appear($data['education_institute']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="education_institute" type="checkbox" value="1" <?= isValued($visibility, 'education_institute') ?>  />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                </table>

                <table class="table table-striped table-bordered" >
                    <tr>
                        <th width="25%" >
                            Jenis Kelamin
                        </th>
                        <td width="60%" >
                            <?= appear($data['gender']) ?>
                        </td>
                        <td width="15%" >
                            <input class="visibility-options" name="gender" type="checkbox" value="1" <?= isValued($visibility, 'gender') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Agama</th>
                        <td><?= appear_alt($data['religion'], $data['other_religion']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="religion" type="checkbox" value="1" <?= isValued($visibility, 'religion') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Kedudukan Hukum</th>
                        <td><?= appear_alt($data['legal_position'], $data['other_legal_position']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="legal_position" type="checkbox" value="1" <?= isValued($visibility, 'legal_position') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>TMT Kepegawaian</th>
                        <td><?= appear($data['tmt_staffing']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="tmt_staffing" type="checkbox" value="1" <?= isValued($visibility, 'tmt_staffing') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Nomor KARPEG</th>
                        <td><?= appear($data['karpeg_number']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="karpeg_number" type="checkbox" value="1" <?= isValued($visibility, 'karpeg_number') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>NUPTK</th>
                        <td><?= appear($data['nuptk']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="nuptk" type="checkbox" value="1" <?= isValued($visibility, 'nuptk') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>No. Registrasi Guru / NRG</th>
                        <td><?= appear($data['nrg']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="nrg" type="checkbox" value="1" <?= isValued($visibility, 'nrg') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>NIK</th>
                        <td><?= appear($data['nik']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="nik" type="checkbox" value="1" <?= isValued($visibility, 'nik') ?>  />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>NPWP</th>
                        <td><?= appear($data['npwp']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="npwp" type="checkbox" value="1" <?= isValued($visibility, 'npwp') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Nomor ASKES</th>
                        <td><?= appear($data['askes_number']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="askes_number" type="checkbox" value="1" <?= isValued($visibility, 'askes_number') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Nomor Seri TASPEN</th>
                        <td><?= appear($data['taspen_serial_number']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="taspen_serial_number" type="checkbox" value="1" <?= isValued($visibility, 'taspen_serial_number') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Khusus Tugas Guru, <br/>Mengajar Pada Mata Diklat</th>
                        <td><?= appear($data['specifically_assignments']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="specifically_assignments" type="checkbox" value="1" <?= isValued($visibility, 'specifically_assignments') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Khusus Tugas Karyawan,<br/>Penugasan Pokok</th>
                        <td><?= appear_alt($data['principal_assignment'], $data['other_principal_assignment']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="principal_assignment" type="checkbox" value="1" <?= isValued($visibility, 'principal_assignment') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Kemampuan Berbahasa</th>
                        <td><?= appear_alt($data['language_ability'], $data['other_language_ability']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="language_ability" type="checkbox" value="1" <?= isValued($visibility, 'language_ability') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Alamat Lengkap</th>
                        <td><?= appear($data['address']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="address" type="checkbox" value="1" <?= isValued($visibility, 'address') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Nomor HP Aktif</th>
                        <td><?= appear($data['phone_number']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="phone_number" type="checkbox" value="1" <?= isValued($visibility, 'phone_number') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                </table>

                <table class="table table-striped table-bordered" >
                    <tr>
                        <th colspan="3">
                            <h4>Keterangan Badan</h4>
                        </th>
                    </tr>
                    <tr>
                        <th width="25%" >Berat Badan ( Kg )</th>
                        <td width="60%" ><?= appear($data['body_weight']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="body_weight" type="checkbox" value="1" <?= isValued($visibility, 'body_weight') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Tinggi Badan ( cm ) </th>
                        <td><?= appear($data['body_height']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="body_height" type="checkbox" value="1" <?= isValued($visibility, 'body_height') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Golongan Darah</th>
                        <td><?= appear_alt($data['body_blood_type'], $data['other_body_blood_type']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="body_blood_type" type="checkbox" value="1" <?= isValued($visibility, 'body_blood_type') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <th>Warna Kulit</th>
                        <td><?= appear_alt($data['skin_color'], $data['other_skin_color']) ?></td>
                        <td width="15%" >
                            <input class="visibility-options" name="skin_color" type="checkbox" value="1" <?= isValued($visibility, 'skin_color') ?> />
                            <span class="visibility">
                                Tampilkan 
                            </span>
                        </td>
                    </tr>
                </table>
                
                <table class="table table-striped table-bordered" >
                    <tr>
                        <th >
                            <h4>Keterangan Lain </h4>
                        </th>
                    </tr>
                    <tr>
                        <td><?= appear($data['note']) ?></td>
                    </tr>
                </table>

            </div>


        </div>
    </div><!--//form-group-->

    <hr/>

    <a href="<?= site_url('routing/index/logout-guru') ?>" class="btn btn-cta"><i class="fa fa-sign-in"></i> Logout</a>    
    <?= form_close() ?>    

</article>