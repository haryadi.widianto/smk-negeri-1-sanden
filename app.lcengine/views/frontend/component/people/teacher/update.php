<?php $data = $data['dataguru']; ?>
<?php $oldinput = get_oldinput(); ?>

<article class="contact-form col-md-12 col-sm-7  page-row">                            

    <?= form_open_multipart('routing/index/update-guru') ?>

    <!-- Hidden input list -->
    <input type="hidden" name="user_id" value="<?= $data['user_id']; ?>" />
    <input type="hidden" name="teacher_id" value="<?= $data['id']; ?>" />

    <div class="row">
        <div class="col-md-12">
            <?= get_alert(); ?>
        </div>
    </div>

    <div class="row">

        <div class="col-md-3">

            <h3 class="title">Foto Profil</h3>

            <div class="form-group message">
                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="thumbnail" id="photo_profile" style="margin-bottom: 5px;">
                            <?php if (!isset($data['photo']) || $data['photo'] == ''): ?>
                                <img data-src="holder.js/300x200" alt="300x200" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQQAAAC0CAYAAABytVLLAAAK/ElEQVR4Xu2aB4tUSxdFa8wJA+acc86K+YermBWMmDBizo6KOX/fLrhNzzgzbz/RR8NeF4ZpZk7frrNO1boVuqu7u/tn4YIABCDwfwJdCIF+AAEINAQQAn0BAhBoEUAIdAYIQAAh0AcgAIFfCTBDoFdAAALMEOgDEIAAMwT6AAQgMAABlgx0DwhAgCUDfQACEGDJQB+AAARYMtAHIAABhwB7CA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIXRYob99+1Zu375dnj59Wr5+/VpGjhxZpk2bVubOnVsGDRrUau2LFy/KrVu3yvv378vQoUNrzKJFi0pXV1cr5s6dO+XBgwfly5cvZcSIEWXevHllxowZ/zrjT58+lStXrpTu7u4yZMiQsnfv3h730P3V5pcvX5bPnz+XYcOGlUmTJtX2KL65/lR7/nUCvMEmgBBsVP9N4MWLF6sMNLA1iD9+/Fg/ePbs2WXp0qX1tQbeuXPn6uvhw4fXAf/z58864DUIdd27d69cv379l/usXbu2TJ482U7m2bNn5erVq1VOuvoSwpkzZ8qrV69qW8aPH1/FofgpU6aUNWvW/NH22A0n8LcIIITfwvZ33qTBf+zYsXrzzZs3l3HjxpVHjx7Vp7NmB3oy6/epU6fK27dv6+CXBCQIxWgwrl69ur7/yJEj9WndCKARhGI2bdpkJ3D48OEqgenTp9cZSW8haPZw9OjRer/t27eX0aNH9xDWvn37yuDBg/9Ye+yGE/hbBBDCb2H7O2/S4NJSQANIA1DXu3fvysmTJ+vrPXv2lO/fv9cBqJjdu3fX370vLSNOnDhR5aEBqdlG+8DV3548eVKf/HqqayBroCtG79NnrF+/vkycOLEuBbRcef36dZ2V9BZCe/uawd98vtqlNmq28E/t6SuPv0OZuw5EACF0eP+4efNm0dpbA3fXrl1VGOfPny9jxowpK1asKA8fPqwDWMuAqVOn1myaGC05du7cWf+mJcX+/ft7PMnPnj1bp/dz5swpS5YsKRcuXCjPnz8vM2fOLMuXL+9Bplmm9BaC7ithSQK6h/Yo1F79NLMRtz0dXoqI5iGEDi6z9hK0p6BLg1+DrVlCSBB68v748aOVwfz588vChQvrHoTep+m7nv7NdeDAgRrfLEc0I9BgllD0PslHm5hbt27tsRmo9/cnBP1PS51Lly6VN2/etD5Ls4uVK1fWDUa3PR1cipimIYQOLbUGvqb0egLPmjWrLFu2rLa0EYJeN09knSRoMGuJoBmBNvj6EsLBgwfr4G+EoHs8fvy4XL58uUVB+wt6sve+BhLCjRs3yt27d6tMxo4dW8Wg/QsJasGCBf0Koa/2dGg5YpqFEDqw1FoGSAa6tH5fvHhxq5XN07Z96i5pHDp0qOjIcuPGjXXQa1nR35Jh27ZtdcmhS7HagNR7NaPQ/9qPLpsP7k8Iko9OGdSeHTt21CNQyUCbo5qN6H6aibjt6cByRDUJIXRYuTXwNHg0yDUD0Pq+/dLpgk4Z2jcM9X8JQUuIDRs21Ce1BmR7THOCocGuzb/mOw3Xrl0r9+/fr5uTkkNfn6n79yeE5vRCM4MtW7a0mtqccmjZoBmH254OK0dccxBCB5VcA/L48eP1Cdts9PVunkShUwbFaONPG4DNYNVg15JB+wu6z4cPH+r3APR9AG3yaVmhtb1OEHTp5OD06dN1nb9u3br6WvfQwNZsof3qTwjtMxZ9tmYKapvaqLZqxjJhwgSrPR1UitimIIQOKn37/oAGafs3E9XMVatW1adte5wGrp7+mp63nw40Me1fcNJrzSA0QCUfzTQkDT3FdcypI0b96PsP2ktQvE4eNLA1+9DegP4mqejSJqfupftoWdDsIUg0koLapg1K5fFP7emgMkQ3BSF0UPmb6Xd/TWq+G6D/a4BpI08y0LpdMtAmXvv6X/fTj77JOGrUqHqS0HxLUd9i1P80oPUU1yVJ6NRB92yWDjqqlBD6urS3oT0OxWv2of0EiUMykzT0eZqtNNdA7emgMkQ3BSFEl5/kIdCTAEKgR0AAAi0CCIHOAAEIIAT6AAQg8CsBZgj0CghAgBkCfQACEGCGQB+AAAQGIMCSge4BAQiwZKAPQAACLBnoAxCAAEsG+gAEIOAQYA/BoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwC/wNER+9UOtmw8QAAAABJRU5ErkJggg==" style="width: 100%;">
                            <?php else: ?>
                                <img data-src="holder.js/300x200" alt="300x200" src="<?= base_url('pub.lcengine/upload/media/' . $data['photo']) ?>" style="width: 100%;">
                            <?php endif; ?>
                        </a>
                        <input id="photo_upload" name="userfile" style="display: none;" type="file" class="btn btn-info" >
                        <input type="checkbox" class="form-input" name="delete_image" /> Delete Image
                    </div>
                </div>
            </div><!--//form-group-->
        </div>

        <div class="col-md-9">

            <h3 class="title">Biodata Guru / Karyawan</h3>

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Posisi</label>
                        <select class="form-control" name="position" >
                            <option value="" <?= (switchInput($data['position'], $oldinput['position']) == '') ? 'selected="selected"' : '' ?>> -- Pilih Posisi -- </option>
                            <option value="ASN Guru" <?= (switchInput($data['position'], $oldinput['position']) == 'ASN Guru') ? 'selected="selected"' : '' ?>  >ASN Guru</option>
                            <option value="ASN Karyawan" <?= (switchInput($data['position'], $oldinput['position']) == 'ASN Karyawan') ? 'selected="selected"' : '' ?>  >ASN Karyawan</option>
                            <option value="GTT" <?= (switchInput($data['position'], $oldinput['position']) == 'GTT') ? 'selected="selected"' : '' ?>  >GTT</option>
                            <option value="PTT" <?= (switchInput($data['position'], $oldinput['position']) == 'PTT') ? 'selected="selected"' : '' ?>  >PTT</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Nama Lengkap dan Gelar</label>
                        <input name="fullname" value="<?= switchInput($data['fullname'], $oldinput['fullname']); ?>" id="name" type="text" class="form-control" placeholder="Masukkan nama lengkap dan gelar">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">NIP Lama</label>
                        <input name="old_nip" value="<?= switchInput($data['old_nip'], $oldinput['old_nip']); ?>" id="name" type="text" class="form-control" placeholder="NIM Lama">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">NIP Baru</label>
                        <input name="new_nip" value="<?= switchInput($data['new_nip'], $oldinput['new_nip']) ?>" id="name" type="text" class="form-control" placeholder="NIP Baru">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="birth_place">Tempat Lahir</label>
                        <input name="birth_place" value="<?= switchInput($data['birth_place'], $oldinput['birth_place']); ?>" id="name" type="text" class="form-control" placeholder="Masukkan Tempat lahir Anda">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="birth_date">Tanggal Lahir</label>
                        <input  name="birth_day" value="<?= switchInput($data['birth_day'], $oldinput['birth_day']); ?>" type="date" class="form-control" placeholder="Masukkan Tanggal Lahir Anda">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="birth_place">Instansi Induk</label>
                        <div class="row">
                            <div class="col-md-6">
                                <select class="form-control select-other" name="parent_institute" >
                                    <option value="" > -- Pilih Instansi -- </option>
                                    <option value="PEMPROV DIY" <?= (switchInput($data['parent_institute'], $oldinput['parent_institute']) == 'PEMPROV DIY') ? 'selected="selected"' : '' ?>  >PEMPROV DIY</option>
                                    <option value="other" <?= (switchInput($data['parent_institute'], $oldinput['parent_institute']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input name="other_parent_institute" value="<?= switchInput($data['other_parent_institute'], isset($oldinput['other_parent_institute']) ? $oldinput['other_parent_institute'] : '' ); ?>" id="name" type="text" class="form-control form-other" placeholder="Selainnya..">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="birth_place">Unit Organisasi</label>
                        <div class="row">
                            <div class="col-md-6">
                                <select class="form-control select-other" name="organization_unit" >
                                    <option value="" > -- Pilih Unit Organisasi -- </option>
                                    <option value="DIKPORA" <?= (switchInput($data['organization_unit'], $oldinput['organization_unit']) == 'DIKPORA') ? 'selected="selected"' : '' ?>  >DIKPORA</option>
                                    <option value="DEPAG" <?= (switchInput($data['organization_unit'], $oldinput['organization_unit']) == 'DEPAG') ? 'selected="selected"' : '' ?>  >DEPAG</option>
                                    <option value="other" <?= (switchInput($data['organization_unit'], $oldinput['organization_unit']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input name="other_organization_unit" value="<?= switchInput($data['other_organization_unit'], isset($oldinput['other_organization_unit']) ? $oldinput['other_organization_unit'] : ''); ?>" id="name" type="text" class="form-control form-other" placeholder="Selainnya..">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="work_unit">Unit Kerja</label>
                        <div class="row">
                            <div class="col-md-6">
                                <select class="form-control select-other" name="work_unit" >
                                    <option value="" > -- Pilih Unit Kerja -- </option>
                                    <option value="SMK N 1 SANDEN" <?= (switchInput($data['work_unit'], $oldinput['work_unit']) == 'SMK N 1 SANDEN') ? 'selected="selected"' : '' ?>  >SMK N 1 SANDEN</option>
                                    <option value="other" <?= (switchInput($data['work_unit'], $oldinput['work_unit']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <input name="other_work_unit" value="<?= switchInput($data['other_work_unit'], isset($oldinput['other_work_unit']) ? $oldinput['other_work_unit'] : '' ); ?>" type="text" class="form-control form-other" placeholder="Selainnya..">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">


                <div class="col-md-6">
                    <div class="form-group">
                        <label for="cpns_grade">PANGKAT/GOLONGAN/RUANG CPNS</label>
                        <select class="form-control" name="cpns_grade" >
                            <option value="" > -- Pilih Golongan -- </option>
                            <option value="Pembina Utama/IV/E" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Pembina Utama/IV/E') ? 'selected="selected"' : '' ?>  >Pembina Utama/IV/E</option>
                            <option value="Pembina Utama Madya/IV/D" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Pembina Utama Madya/IV/D') ? 'selected="selected"' : '' ?>  >Pembina Utama Madya/IV/D</option>
                            <option value="Pembina Utama Muda/IV/C" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Pembina Utama Muda/IV/C') ? 'selected="selected"' : '' ?>  >Pembina Utama Muda/IV/C</option>
                            <option value="Pembina Tingkat. 1/IV/B" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Pembina Tingkat. 1/IV/B') ? 'selected="selected"' : '' ?>  >Pembina Tingkat. 1/IV/B</option>
                            <option value="Pembina/IV/A" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Pembina/IV/A') ? 'selected="selected"' : '' ?>  >Pembina/IV/A</option>
                            <option value="Penata Tingkat. 1/III/D" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Penata Tingkat. 1/III/D') ? 'selected="selected"' : '' ?>  >Penata Tingkat. 1/III/D</option>
                            <option value="Penata/III/C" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Penata/III/C') ? 'selected="selected"' : '' ?>  >Penata/III/C</option>
                            <option value="Penata Muda Tingkat. 1/III/B" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Penata Muda Tingkat. 1/III/B') ? 'selected="selected"' : '' ?>  >Penata Muda Tingkat. 1/III/B</option>
                            <option value="Penata Muda/III/A" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Penata Muda/III/A') ? 'selected="selected"' : '' ?>  >Penata Muda/III/A</option>
                            <option value="Pengatur Tingkat. 1/II/D" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Pengatur Tingkat. 1/II/D') ? 'selected="selected"' : '' ?>  >Pengatur Tingkat. 1/II/D</option>
                            <option value="Pengatur/II/C" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Pengatur/II/C') ? 'selected="selected"' : '' ?>  >Pengatur/II/C</option>
                            <option value="Pengatur Muda Tingkat. 1/II/B" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Pengatur Muda Tingkat. 1/II/B') ? 'selected="selected"' : '' ?>  >Pengatur Muda Tingkat. 1/II/B</option>
                            <option value="Pengatur/II/A" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Pengatur/II/A') ? 'selected="selected"' : '' ?>  >Pengatur/II/A</option>
                            <option value="Juru Tingkat. 1/I/D" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Juru Tingkat. 1/I/D') ? 'selected="selected"' : '' ?>  >Juru Tingkat. 1/I/D</option>
                            <option value="Juru I/C" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Juru I/C') ? 'selected="selected"' : '' ?>  >Juru I/C</option>
                            <option value="Juru Muda Tingkat. 1/I/B" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Juru Muda Tingkat. 1/I/B') ? 'selected="selected"' : '' ?>  >Juru Muda Tingkat. 1/I/B</option>
                            <option value="Juru Muda/I/A" <?= (switchInput($data['cpns_grade'], $oldinput['cpns_grade']) == 'Juru Muda/I/A') ? 'selected="selected"' : '' ?>  >Juru Muda/I/A</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="last_grade">PANGKAT/GOLONGAN/RUANG TERAKHIR</label>
                        <select class="form-control" name="last_grade" >
                            <option value="" > -- Pilih Golongan -- </option>
                            <option value="Pembina Utama/IV/E" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Pembina Utama/IV/E') ? 'selected="selected"' : '' ?>  >Pembina Utama/IV/E</option>
                            <option value="Pembina Utama Madya/IV/D" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Pembina Utama Madya/IV/D') ? 'selected="selected"' : '' ?>  >Pembina Utama Madya/IV/D</option>
                            <option value="Pembina Utama Muda/IV/C" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Pembina Utama Muda/IV/C') ? 'selected="selected"' : '' ?>  >Pembina Utama Muda/IV/C</option>
                            <option value="Pembina Tingkat. 1/IV/B" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Pembina Tingkat. 1/IV/B') ? 'selected="selected"' : '' ?>  >Pembina Tingkat. 1/IV/B</option>
                            <option value="Pembina/IV/A" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Pembina/IV/A') ? 'selected="selected"' : '' ?>  >Pembina/IV/A</option>
                            <option value="Penata Tingkat. 1/III/D" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Penata Tingkat. 1/III/D') ? 'selected="selected"' : '' ?>  >Penata Tingkat. 1/III/D</option>
                            <option value="Penata/III/C" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Penata/III/C') ? 'selected="selected"' : '' ?>  >Penata/III/C</option>
                            <option value="Penata Muda Tingkat. 1/III/B" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Penata Muda Tingkat. 1/III/B') ? 'selected="selected"' : '' ?>  >Penata Muda Tingkat. 1/III/B</option>
                            <option value="Penata Muda/III/A" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Penata Muda/III/A') ? 'selected="selected"' : '' ?>  >Penata Muda/III/A</option>
                            <option value="Pengatur Tingkat. 1/II/D" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Pengatur Tingkat. 1/II/D') ? 'selected="selected"' : '' ?>  >Pengatur Tingkat. 1/II/D</option>
                            <option value="Pengatur/II/C" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Pengatur/II/C') ? 'selected="selected"' : '' ?>  >Pengatur/II/C</option>
                            <option value="Pengatur Muda Tingkat. 1/II/B" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Pengatur Muda Tingkat. 1/II/B') ? 'selected="selected"' : '' ?>  >Pengatur Muda Tingkat. 1/II/B</option>
                            <option value="Pengatur/II/A" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Pengatur/II/A') ? 'selected="selected"' : '' ?>  >Pengatur/II/A</option>
                            <option value="Juru Tingkat. 1/I/D" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Juru Tingkat. 1/I/D') ? 'selected="selected"' : '' ?>  >Juru Tingkat. 1/I/D</option>
                            <option value="Juru I/C" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Juru I/C') ? 'selected="selected"' : '' ?>  >Juru I/C</option>
                            <option value="Juru Muda Tingkat. 1/I/B" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Juru Muda Tingkat. 1/I/B') ? 'selected="selected"' : '' ?>  >Juru Muda Tingkat. 1/I/B</option>
                            <option value="Juru Muda/I/A" <?= (switchInput($data['last_grade'], $oldinput['last_grade']) == 'Juru Muda/I/A') ? 'selected="selected"' : '' ?>  >Juru Muda/I/A</option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="tmt_eselon">TMT Eselon</label>
                        <input  name="tmt_eselon" value="<?= switchInput($data['tmt_eselon'], $oldinput['tmt_eselon']); ?>" type="date" class="form-control" placeholder="TMT Eselon">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label >Jabatan Terakhir</label>
                        <select class="form-control" name="last_position" >
                            <option value="" > -- Pilih Jabatan -- </option>
                            <option value="Guru Pertama" <?= (switchInput($data['last_position'], $oldinput['last_position']) == 'Guru Pertama') ? 'selected="selected"' : '' ?>  >Guru Pertama</option>
                            <option value="Guru Muda" <?= (switchInput($data['last_position'], $oldinput['last_position']) == 'Guru Muda') ? 'selected="selected"' : '' ?>  >Guru Muda</option>
                            <option value="Guru Madya" <?= (switchInput($data['last_position'], $oldinput['last_position']) == 'Guru Madya') ? 'selected="selected"' : '' ?>  >Guru Madya</option>
                            <option value="Guru Utama" <?= (switchInput($data['last_position'], $oldinput['last_position']) == 'Guru Utama') ? 'selected="selected"' : '' ?>  >Guru Utama</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Masa Kerja Golongan </label>
                        <input  name="working_group_period" value="<?= switchInput($data['working_group_period'], $oldinput['working_group_period']); ?>" type="text" class="form-control" placeholder="Masa Kerja Golongan">
                    </div>
                </div>

            </div>

            <hr/>

            <h4 class="title" >DIKLAT Pra/Jabatan Terakhir : </h4>

            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Nomor SK </label>
                        <input  name="sk_number" value="<?= switchInput($data['sk_number'], $oldinput['sk_number']); ?>" type="text" class="form-control" placeholder="Nomor SK">
                    </div>                    
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Jumlah Jam </label>
                        <input  name="total_hour" value="<?= switchInput($data['total_hour'], $oldinput['total_hour']); ?>" type="text" class="form-control" placeholder="Jumlah Jam">
                    </div>                    
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tempat Pelaksanaan </label>
                        <input  name="execution_place" value="<?= switchInput($data['execution_place'], $oldinput['execution_place']); ?>" type="text" class="form-control" placeholder="Tempat Pelaksanaan">
                    </div>                    
                </div>
            </div>

            <hr/>

            <h4 class="title" >Pendidikan : </h4>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pendidikan Saat CPNS </label>
                        <input  name="education_when_cpns" value="<?= switchInput($data['education_when_cpns'], $oldinput['education_when_cpns']); ?>" type="text" class="form-control" placeholder="Pendidikan Saat CPNS">
                    </div>
                </div>

                <div class="col-md-6">
                    <label>Pendidikan Terakhir </label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control select-other" name="last_education" >
                                    <option value="" > -- Pilih Pendidikan -- </option>
                                    <option value="S3" <?= (switchInput($data['last_education'], $oldinput['last_education']) == 'S3') ? 'selected="selected"' : '' ?>  >S3</option>
                                    <option value="S2" <?= (switchInput($data['last_education'], $oldinput['last_education']) == 'S2') ? 'selected="selected"' : '' ?>  >S2</option>
                                    <option value="S1" <?= (switchInput($data['last_education'], $oldinput['last_education']) == 'S1') ? 'selected="selected"' : '' ?>  >S1</option>
                                    <option value="other" <?= (switchInput($data['last_education'], $oldinput['last_education']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>        
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input  name="other_last_education" value="<?= switchInput($data['other_last_education'], isset($oldinput['other_last_education']) ? $oldinput['other_last_education'] : '' ); ?>" type="text" class="form-control form-other" placeholder="Selainnya">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pendidikan Terakhir : Jurusan </label>
                        <input  name="education_field" value="<?= switchInput($data['education_field'], $oldinput['education_field']) ?>" type="text" class="form-control" placeholder="Jurusan">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Pendidikan Terakhir : Nama Sekolah / Lembaga </label>
                        <input  name="education_institute" value="<?= switchInput($data['education_institute'], $oldinput['education_institute']); ?>" type="text" class="form-control" placeholder="Nama Sekolah / Lembaga">
                    </div>
                </div>

            </div>

            <hr/>

            <div class="row">

                <div class="col-md-6">
                    <div class="form-group">
                        <label >Jenis Kelamin</label><br/>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-warning <?= (switchInput($data['gender'], isset($oldinput['gender']) ? $oldinput['gender'] : '' ) == 'laki-laki') ? 'active' : '' ?>">
                                <input type="radio" id="option1" autocomplete="on" name="gender" value="laki-laki"  <?= (switchInput($data['gender'], isset($oldinput['gender']) ? $oldinput['gender'] : '' ) == 'laki-laki') ? 'checked' : '' ?> >
                                <i class="fa fa-male"></i> Laki-laki
                            </label>
                            <label class="btn btn-warning <?= (switchInput($data['gender'], isset($oldinput['gender']) ? $oldinput['gender'] : '' ) == 'perempuan') ? 'active' : '' ?>">
                                <input type="radio" id="option2" autocomplete="on" name="gender" value="perempuan"  <?= (switchInput($data['gender'], isset($oldinput['gender']) ? $oldinput['gender'] : '' ) == 'perempuan') ? 'checked' : '' ?> >
                                <i class="fa fa-female"></i> Perempuan
                            </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <label >Agama</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control select-other" name="religion" >
                                    <option value="" > -- Pilih Agama -- </option>
                                    <option value="Islam" <?= (switchInput($data['religion'], $oldinput['religion']) == 'Islam') ? 'selected="selected"' : '' ?>  >Islam</option>
                                    <option value="Kristen" <?= (switchInput($data['religion'], $oldinput['religion']) == 'Kristen') ? 'selected="selected"' : '' ?>  >Kristen</option>
                                    <option value="Katolik" <?= (switchInput($data['religion'], $oldinput['religion']) == 'Katolik') ? 'selected="selected"' : '' ?>  >Katolik</option>
                                    <option value="Hindu" <?= (switchInput($data['religion'], $oldinput['religion']) == 'Hindu') ? 'selected="selected"' : '' ?>  >Hindu</option>
                                    <option value="Budha" <?= (switchInput($data['religion'], $oldinput['religion']) == 'Budha') ? 'selected="selected"' : '' ?>  >Budha</option>
                                    <option value="other" <?= (switchInput($data['religion'], $oldinput['religion']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>        
                        </div>
                        <div class="col-md-6">
                            <input  name="other_religion" value="<?= switchInput($data['other_religion'], isset($oldinput['other_religion']) ? $oldinput['other_religion'] : '' ) ?>" type="text" class="form-control form-other" placeholder="Selainnya">
                        </div>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <label >Kedudukan Hukum</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control select-other" name="legal_position" >
                                    <option value="" > -- Pilih Negara -- </option>
                                    <option value="Indonesia" <?= (switchInput($data['legal_position'], $oldinput['legal_position']) == 'Indonesia') ? 'selected="selected"' : '' ?>  >Indonesia</option>
                                    <option value="other" <?= (switchInput($data['legal_position'], $oldinput['legal_position']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>        
                        </div>
                        <div class="col-md-6">
                            <input  name="other_legal_position" value="<?= switchInput($data['other_legal_position'], isset($oldinput['other_legal_position']) ? $oldinput['other_legal_position'] : '' ) ?>" type="text" class="form-control form-other" placeholder="Selainnya">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>TMT Kepegawaian </label>
                        <input  name="tmt_staffing" value="<?= switchInput($data['tmt_staffing'], $oldinput['tmt_staffing']); ?>" type="text" class="form-control" placeholder="TMT Kepegawaian">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Nomor KARPEG </label>
                        <input  name="karpeg_number" value="<?= switchInput($data['karpeg_number'], $oldinput['karpeg_number']); ?>" type="text" class="form-control" placeholder="Nomor KARPEG">
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-3">
                    <div class="form-group">
                        <label>NUPTK </label>
                        <input  name="nuptk" value="<?= switchInput($data['nuptk'], $oldinput['nuptk']); ?>" type="text" class="form-control" placeholder="NUPTK">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>No. Registrasi Guru/NRG </label>
                        <input  name="nrg" value="<?= switchInput($data['nrg'], $oldinput['nrg']); ?>" type="text" class="form-control" placeholder="No. Registrasi Guru/NRG">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>NIK </label>
                        <input  name="nik" value="<?= switchInput($data['nik'], $oldinput['nik']); ?>" type="text" class="form-control" placeholder="NIK">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>NPWP </label>
                        <input  name="npwp" value="<?= switchInput($data['npwp'], $oldinput['npwp']); ?>" type="text" class="form-control" placeholder="NPWP">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-3">

                    <div class="form-group">
                        <label>Nomor ASKES </label>
                        <input  name="askes_number" value="<?= switchInput($data['askes_number'], $oldinput['askes_number']); ?>" type="text" class="form-control" placeholder="Nomor ASKES">
                    </div>

                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Nomor Seri TASPEN </label>
                        <input  name="taspen_serial_number" value="<?= switchInput($data['taspen_serial_number'], $oldinput['taspen_serial_number']); ?>" type="text" class="form-control" placeholder="Nomor Seri TASPEN">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Khusus Tugas Guru, Mengajar Pada Mata Diklat : </label>
                        <input  name="specifically_assignments" value="<?= switchInput($data['specifically_assignments'], $oldinput['specifically_assignments']); ?>" type="text" class="form-control" placeholder="Khusus Tugas Guru, Mengajar Pada Mata Diklat">
                    </div>
                </div>

            </div>

            <div class="row">

                <div class="col-md-6">
                    <label>Khusus Tugas Karyawan, Penugasan Pokok : </label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group select-other">
                                <select class="form-control select-other" name="principal_assignment" >
                                    <option value="" > -- Pilih Tugas Pokok -- </option>
                                    <option value="Tata Usaha" <?= (switchInput($data['principal_assignment'], $oldinput['principal_assignment']) == 'Tata Usaha') ? 'selected="selected"' : '' ?>  >Tata Usaha</option>
                                    <option value="HUBIN" <?= (switchInput($data['principal_assignment'], $oldinput['principal_assignment']) == 'HUBIN') ? 'selected="selected"' : '' ?>  >HUBIN</option>
                                    <option value="Kurikulum" <?= (switchInput($data['principal_assignment'], $oldinput['principal_assignment']) == 'Kurikulum') ? 'selected="selected"' : '' ?>  >Kurikulum</option>
                                    <option value="Sarana Prasarana" <?= (switchInput($data['principal_assignment'], $oldinput['principal_assignment']) == 'Sarana Prasarana') ? 'selected="selected"' : '' ?>  >Sarana Prasarana</option>
                                    <option value="Perpustakaan" <?= (switchInput($data['principal_assignment'], $oldinput['principal_assignment']) == 'Perpustakaan') ? 'selected="selected"' : '' ?>  >Perpustakaan</option>
                                    <option value="Keamanan" <?= (switchInput($data['principal_assignment'], $oldinput['principal_assignment']) == 'Keamanan') ? 'selected="selected"' : '' ?>  >Keamanan</option>
                                    <option value="Jurusan" <?= (switchInput($data['principal_assignment'], $oldinput['principal_assignment']) == 'Jurusan') ? 'selected="selected"' : '' ?>  >Jurusan</option>
                                    <option value="other" <?= (switchInput($data['principal_assignment'], $oldinput['principal_assignment']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>        
                        </div>
                        <div class="col-md-6">
                            <input  name="other_principal_assignment" value="<?= switchInput($data['other_principal_assignment'], isset($oldinput['other_principal_assignment']) ? $oldinput['other_principal_assignment'] : '' ); ?>" type="text" class="form-control form-other" placeholder="Selainnya">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <label>Kemampuan Berbahasa </label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control select-other" name="language_ability" >
                                    <option value="" > -- Pilih Bahasa -- </option>
                                    <option value="Bahasa Indonesia" <?= (switchInput($data['language_ability'], $oldinput['language_ability']) == 'Bahasa Indonesia') ? 'selected="selected"' : '' ?>  >Bahasa Indonesia</option>
                                    <option value="Bahasa Inggris" <?= (switchInput($data['language_ability'], $oldinput['language_ability']) == 'Bahasa Inggris') ? 'selected="selected"' : '' ?>  >Bahasa Inggris</option>
                                    <option value="Bahasa Daerah Asal" <?= (switchInput($data['language_ability'], $oldinput['language_ability']) == 'Bahasa Daerah Asal') ? 'selected="selected"' : '' ?>  >Bahasa Daerah Asal</option>
                                    <option value="other" <?= (switchInput($data['language_ability'], $oldinput['language_ability']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>        
                        </div>
                        <div class="col-md-6">
                            <input  name="other_language_ability" value="<?= switchInput($data['other_legal_position'], isset($oldinput['other_legal_position']) ? $oldinput['other_legal_position'] :  '' ) ?>" type="text" class="form-control form-other" placeholder="Selainnya">
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Alamat Rumah Lengkap</label>
                        <input  name="address" value="<?= switchInput($data['address'], $oldinput['address']); ?>" type="text" class="form-control" placeholder="Alamat Rumah Lengkap">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label>Nomor HP Aktif</label>
                        <input  name="phone_number" value="<?= switchInput($data['phone_number'], $oldinput['phone_number']) ?>" type="text" class="form-control" placeholder="Nomor HP Aktif">
                    </div>
                </div>

            </div>

            <hr/>

            <h4 class="title" >Keterangan Badan : </h4>

            <div class="row">

                <div class="col-md-6">

                    <div class="form-group">
                        <label>Berat Badan ( Kg )</label>
                        <input  name="body_weight" value="<?= switchInput($data['body_weight'], $oldinput['body_weight']); ?>" type="text" class="form-control" placeholder="Berat Badan">
                    </div>

                </div>

                <div class="col-md-6">

                    <div class="form-group">
                        <label>Tinggi Badan ( cm )</label>
                        <input  name="body_height" value="<?= switchInput($data['body_height'], $oldinput['body_height']); ?>" type="text" class="form-control" placeholder="Tinggi Badan">
                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-6">
                    <label>Golongan Darah </label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control select-other" name="body_blood_type" >
                                    <option value="" > -- Pilih Golongan Darah -- </option>
                                    <option value="A" <?= (switchInput($data['body_blood_type'], $oldinput['body_blood_type']) == 'A') ? 'selected="selected"' : '' ?>  >A</option>
                                    <option value="B" <?= (switchInput($data['body_blood_type'], $oldinput['body_blood_type']) == 'B') ? 'selected="selected"' : '' ?>  >B</option>
                                    <option value="O" <?= (switchInput($data['body_blood_type'], $oldinput['body_blood_type']) == 'O') ? 'selected="selected"' : '' ?>  >O</option>
                                    <option value="other" <?= (switchInput($data['body_blood_type'], $oldinput['body_blood_type']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>        
                        </div>
                        <div class="col-md-6">
                            <input  name="other_body_blood_type" value="<?= switchInput($data['other_body_blood_type'], isset($oldinput['other_body_blood_type']) ? $oldinput['other_body_blood_type'] : '' ) ?>" type="text" class="form-control form-other" placeholder="Selainnya">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <label>Warna Kulit</label>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control select-other" name="skin_color" >
                                    <option value="" > -- Pilih Warna Kulit -- </option>
                                    <option value="Sawo Matang" <?= (switchInput($data['skin_color'], $oldinput['skin_color']) == 'Sawo Matang') ? 'selected="selected"' : '' ?>  >Sawo Matang</option>
                                    <option value="Kuning" <?= (switchInput($data['skin_color'], $oldinput['skin_color']) == 'Kuning') ? 'selected="selected"' : '' ?>  >Kuning</option>
                                    <option value="Gelap" <?= (switchInput($data['skin_color'], $oldinput['skin_color']) == 'Gelap') ? 'selected="selected"' : '' ?>  >Gelap</option>
                                    <option value="other" <?= (switchInput($data['skin_color'], $oldinput['skin_color']) == 'other') ? 'selected="selected"' : '' ?>  >Selainnya</option>
                                </select>
                            </div>        
                        </div>
                        <div class="col-md-6">
                            <input  name="other_skin_color" value="<?= switchInput($data['other_skin_color'], isset($oldinput['other_skin_color']) ? $oldinput['other_skin_color'] : '' ) ?>" type="text" class="form-control form-other" placeholder="Selainnya">
                        </div>
                    </div>
                </div>

            </div>


            <h4 class="title" >Keterangan Lain : </h4>

            <div class="row">
                <div class="col-md-12">
                    <textarea name="note" class="form-control" rows="7"><?= switchInput($data['note'], $oldinput['note']); ?></textarea>
                </div>
            </div>

            <hr/>

            <h3 class="title">Akun Guru / Karyawan</h3>

            <div class="form-group name">

                <div class="row">
                    <div class="col-md-6">
                        <label for="name">Email</label>
                        <input name="username" value="<?= switchInput($data['username'], $oldinput['username']) ?>" id="name" type="text" class="form-control" placeholder="Masukkan email">
                    </div>
                    <div class="col-md-6">
                        <label for="">Password Baru</label>
                        <input name="password" value="" id="name" type="password" class="form-control" placeholder="Masukkan password baru" >
                    </div>
                </div>
            </div><!--//form-group-->
        </div>

    </div>



    <hr/>
    <a href="<?= site_url('routing/index/dashboard-guru') ?>" class="btn btn-cta"><i class="fa fa-dashboard"></i> Dashboard</a>    
    <button type="submit" class="btn btn-cta"><i class="fa fa-save"></i> Simpan</button>
    <?= form_close() ?>               
</article>

<script type="text/javascript">
    $(document).ready(function () {

        // Hidden form other
        $('.form-other').attr('disabled', true)
        $('.select-other').each(function () {

            // Variable initialization
            var name = $(this).attr('name');
            var value = $(this).find('option:selected').val();
            var dom = 'other_' + name;

            console.log(value);

            if (value == 'other') {
                $('input[name="' + dom + '"]').removeAttr('disabled');
            } else {
                $('input[name="' + dom + '"]').attr('disabled', true);
            }

        });

        $('body').on('change', '.select-other', function () {

            // Variable initiailzation
            var name = $(this).attr('name');
            var value = $(this).find('option:selected').val();
            var dom = 'other_' + name;

            if (value == 'other') {
                $('input[name="' + dom + '"]').removeAttr('disabled');
            } else {
                $('input[name="' + dom + '"]').attr('disabled', true)
            }

        });

        $('.file-trigger').click(function () {
            $('.file-target').trigger('click');
        });

        $("a#photo_profile").click(function () {
            $("input#photo_upload").trigger("click");
        });

        $("input#photo_upload").change(function () {

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) {
                return;
            }

            if (/^image/.test(files[0].type)) {

                var reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onloadend = function () {

                    $('a#photo_profile > img').remove();
                    var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 100%;">';
                    $('a#photo_profile').append(html);

                }

            }

        });

    });



</script>