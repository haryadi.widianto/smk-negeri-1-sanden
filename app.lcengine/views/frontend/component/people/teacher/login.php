<?php $oldinput = get_oldinput(); ?>

<article class="contact-form col-md-12 col-sm-7  page-row">                            

    <?= form_open_multipart('routing/index/login-guru') ?>

    <div class="col-md-3"></div>

    <div class="col-md-6">

        <div class="heading-title" style="text-align: center;">
            <h2 class="title">Login Guru dan Karyawan</h2>
            <hr/>
        </div>
        <div class="form-group name">
            <?= get_alert(); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Email</label>
                                <input name="username" value="<?= get_oldinput_trim($oldinput, 'username') ?>" id="name" type="text" class="form-control" placeholder="Masukkan email">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Password</label>
                                <input name="password" value="" id="name" type="password" class="form-control" placeholder="Masukkan password">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div><!--//form-group-->

        <hr/>
        <button type="submit" class="btn btn-cta"><i class="fa fa-sign-in"></i> Login</button>
        <a href="<?= site_url('routing/index/registration-guru') ?>" class="btn btn-cta"><i class="fa fa-user-plus"></i> Registrasi</a>

    </div>

    <div class="col-md-3"></div>



    <?= form_close() ?>               
</article>

<script type="text/javascript">
    $(document).ready(function () {
        $('.file-trigger').click(function () {
            $('.file-target').trigger('click');
        });

        $("a#photo_profile").click(function () {
            $("input#photo_upload").trigger("click");
        });

        $("input#photo_upload").change(function () {

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) {
                return;
            }

            if (/^image/.test(files[0].type)) {

                var reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onloadend = function () {

                    $('a#photo_profile > img').remove();
                    var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 260px; height: 180px;">';
                    $('a#photo_profile').append(html);

                }

            }

        });

    });
</script>