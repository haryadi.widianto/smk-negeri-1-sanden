<?php $data = $data['datasiswa']; ?>
<?php $oldinput = get_oldinput(); ?>

<article class="contact-form col-md-12  page-row">                            

    <?= form_open_multipart('routing/index/login-siswa') ?>

    <br/>

    <div class="row">
        <div class="col-md-8" >
            <h3 class="title" style="margin-top: 0px; margin-bottom: 0px;">Dashboard Siswa</h3>
        </div>
        <div class="col-md-4" style="height: max-content;">
            <a href="<?= site_url('routing/index/update-siswa') ?>" class="btn btn-info pull-right">
                <i class="fa fa-edit"></i> Perbarui
            </a>
        </div>
    </div>

    <hr/>


    <div class="form-group name">
        <?php echo get_alert(); ?>
        <div class="row">
            <div class="col-md-3">
                <a href="#" class="thumbnail" id="photo_profile">
                    <img data-src="holder.js/300x200" alt="300x200" src="<?= set_image($data['photo'], 'image') ?>" style="width: 100%;">
                </a>
                <input id="photo_upload" name="userfile" style="display: none;" type="file" class="btn btn-info" >
            </div>
            <div class="col-md-9">
                <table class="table table-striped table-bordered">
                    <tr>
                        <th width="20%">Nama Lengkap</th>
                        <td width="80%" ><?= $data['name'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">Nama Ibu Kandung</th>
                        <td width="80%" ><?= $data['mother_name'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">Jenis Kelamin</th>
                        <td width="80%" ><?= ucwords($data['gender']) ?></td>
                    </tr>
                    <tr>
                        <th width="20%">Tempat Lahir</th>
                        <td width="80%" ><?= $data['birth_place'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">Tanggal Lahir</th>
                        <td width="80%" ><?= $data['birth_date'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">Alamat Rumah</th>
                        <td width="80%" ><?= $data['address'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">Nomor Telp / HP</th>
                        <td width="80%" ><?= $data['phone_number'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">NIK</th>
                        <td width="80%" ><?= $data['nik'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">NISN</th>
                        <td width="80%" ><?= $data['nisn'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">Tingkat</th>
                        <td width="80%" ><?= $data['grade'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">Rombongan</th>
                        <td width="80%" ><?= $data['group'] ?></td>
                    </tr>
                    <tr>
                        <th width="20%">Tahun Angkatan</th>
                        <td width="80%" >
                            <?= $data['year_force'] ?> &nbsp;
                            <?php if ($data['alumni'] == 1): ?> 
                                <b>( Sudah Lulus )</b>
                            <?php endif; ?>
                        </td>
                    </tr>

                    <!-- Show data when status is alumni -->
                    <?php if($data['alumni'] == 1): ?>
                        <tr>
                            <th width="20%">Tahun Lulus</th>
                            <td width="80%" ><?= $data['graduation_year'] ?></td>
                        </tr>
                        <tr>
                            <th width="20%">Pekerjaan / Kantor</th>
                            <td width="80%" ><?= $data['work'] ?></td>
                        </tr>
                    <?php endif; ?>

                    <tr>
                        <th width="20%">Email</th>
                        <td width="80%" ><?= $data['email'] ?></td>
                    </tr>
                </table>

            </div>


        </div>
    </div><!--//form-group-->

    <hr/>
    <a href="<?= site_url('routing/index/logout-siswa') ?>" class="btn btn-cta"><i class="fa fa-sign-in"></i> Logout</a>    
    <?= form_close() ?>               
</article>
