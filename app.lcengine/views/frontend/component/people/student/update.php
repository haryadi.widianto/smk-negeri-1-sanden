<?php $data = $data['datasiswa']; ?>
<?php $oldinput = get_oldinput(); ?>

<article class="contact-form col-md-12 col-sm-7  page-row">                            

    <?= form_open_multipart('routing/index/update-siswa') ?>

    <!-- Hidden input list -->
    <input type="hidden" name="user_id" value="<?= $data['user_id']; ?>" />
    <input type="hidden" name="student_id" value="<?= $data['id']; ?>" />

    <div class="row">
        <div class="col-md-12">
            <?= get_alert(); ?>
        </div>
    </div>

    <div class="row">

        <div class="col-md-3">

            <h3 class="title">Foto Profil</h3>

            <div class="form-group message">
                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="thumbnail" id="photo_profile" style="margin-bottom: 5px;">
                            <?php if (!isset($data['photo']) || $data['photo'] == ''): ?>
                                <img data-src="holder.js/300x200" alt="300x200" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQQAAAC0CAYAAABytVLLAAAK/ElEQVR4Xu2aB4tUSxdFa8wJA+acc86K+YermBWMmDBizo6KOX/fLrhNzzgzbz/RR8NeF4ZpZk7frrNO1boVuqu7u/tn4YIABCDwfwJdCIF+AAEINAQQAn0BAhBoEUAIdAYIQAAh0AcgAIFfCTBDoFdAAALMEOgDEIAAMwT6AAQgMAABlgx0DwhAgCUDfQACEGDJQB+AAARYMtAHIAABhwB7CA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIYQUmjQh4BBACA4lYiAQQgAhhBSaNCHgEEAIDiViIBBCACGEFJo0IeAQQAgOJWIgEEIAIXRYob99+1Zu375dnj59Wr5+/VpGjhxZpk2bVubOnVsGDRrUau2LFy/KrVu3yvv378vQoUNrzKJFi0pXV1cr5s6dO+XBgwfly5cvZcSIEWXevHllxowZ/zrjT58+lStXrpTu7u4yZMiQsnfv3h730P3V5pcvX5bPnz+XYcOGlUmTJtX2KL65/lR7/nUCvMEmgBBsVP9N4MWLF6sMNLA1iD9+/Fg/ePbs2WXp0qX1tQbeuXPn6uvhw4fXAf/z58864DUIdd27d69cv379l/usXbu2TJ482U7m2bNn5erVq1VOuvoSwpkzZ8qrV69qW8aPH1/FofgpU6aUNWvW/NH22A0n8LcIIITfwvZ33qTBf+zYsXrzzZs3l3HjxpVHjx7Vp7NmB3oy6/epU6fK27dv6+CXBCQIxWgwrl69ur7/yJEj9WndCKARhGI2bdpkJ3D48OEqgenTp9cZSW8haPZw9OjRer/t27eX0aNH9xDWvn37yuDBg/9Ye+yGE/hbBBDCb2H7O2/S4NJSQANIA1DXu3fvysmTJ+vrPXv2lO/fv9cBqJjdu3fX370vLSNOnDhR5aEBqdlG+8DV3548eVKf/HqqayBroCtG79NnrF+/vkycOLEuBbRcef36dZ2V9BZCe/uawd98vtqlNmq28E/t6SuPv0OZuw5EACF0eP+4efNm0dpbA3fXrl1VGOfPny9jxowpK1asKA8fPqwDWMuAqVOn1myaGC05du7cWf+mJcX+/ft7PMnPnj1bp/dz5swpS5YsKRcuXCjPnz8vM2fOLMuXL+9Bplmm9BaC7ithSQK6h/Yo1F79NLMRtz0dXoqI5iGEDi6z9hK0p6BLg1+DrVlCSBB68v748aOVwfz588vChQvrHoTep+m7nv7NdeDAgRrfLEc0I9BgllD0PslHm5hbt27tsRmo9/cnBP1PS51Lly6VN2/etD5Ls4uVK1fWDUa3PR1cipimIYQOLbUGvqb0egLPmjWrLFu2rLa0EYJeN09knSRoMGuJoBmBNvj6EsLBgwfr4G+EoHs8fvy4XL58uUVB+wt6sve+BhLCjRs3yt27d6tMxo4dW8Wg/QsJasGCBf0Koa/2dGg5YpqFEDqw1FoGSAa6tH5fvHhxq5XN07Z96i5pHDp0qOjIcuPGjXXQa1nR35Jh27ZtdcmhS7HagNR7NaPQ/9qPLpsP7k8Iko9OGdSeHTt21CNQyUCbo5qN6H6aibjt6cByRDUJIXRYuTXwNHg0yDUD0Pq+/dLpgk4Z2jcM9X8JQUuIDRs21Ce1BmR7THOCocGuzb/mOw3Xrl0r9+/fr5uTkkNfn6n79yeE5vRCM4MtW7a0mtqccmjZoBmH254OK0dccxBCB5VcA/L48eP1Cdts9PVunkShUwbFaONPG4DNYNVg15JB+wu6z4cPH+r3APR9AG3yaVmhtb1OEHTp5OD06dN1nb9u3br6WvfQwNZsof3qTwjtMxZ9tmYKapvaqLZqxjJhwgSrPR1UitimIIQOKn37/oAGafs3E9XMVatW1adte5wGrp7+mp63nw40Me1fcNJrzSA0QCUfzTQkDT3FdcypI0b96PsP2ktQvE4eNLA1+9DegP4mqejSJqfupftoWdDsIUg0koLapg1K5fFP7emgMkQ3BSF0UPmb6Xd/TWq+G6D/a4BpI08y0LpdMtAmXvv6X/fTj77JOGrUqHqS0HxLUd9i1P80oPUU1yVJ6NRB92yWDjqqlBD6urS3oT0OxWv2of0EiUMykzT0eZqtNNdA7emgMkQ3BSFEl5/kIdCTAEKgR0AAAi0CCIHOAAEIIAT6AAQg8CsBZgj0CghAgBkCfQACEGCGQB+AAAQGIMCSge4BAQiwZKAPQAACLBnoAxCAAEsG+gAEIOAQYA/BoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwCCMGhRAwEQggghJBCkyYEHAIIwaFEDARCCCCEkEKTJgQcAgjBoUQMBEIIIISQQpMmBBwC/wNER+9UOtmw8QAAAABJRU5ErkJggg==" style="width: 100%;">
                            <?php else: ?>
                                <img data-src="holder.js/300x200" alt="300x200" src="<?= base_url('pub.lcengine/upload/media/' . $data['photo']) ?>" style="width: 100%;">
                            <?php endif; ?>
                        </a>
                        <input id="photo_upload" name="userfile" style="display: none;" type="file" class="btn btn-info" >
                        <input type="checkbox" class="form-input" name="delete_image" /> Delete Image
                    </div>
                </div>
            </div><!--//form-group-->
        </div>

        <div class="col-md-9">



            <h3 class="title">Biodata Siswa / Alumni</h3>

            <div class="form-group name">
                <div class="row">
                    <div class="col-md-6">
                        <label for="name">Nama Lengkap</label>
                        <input name="name" value="<?= $data['name'] ?>" id="name" type="text" class="form-control" placeholder="Masukkan nama lengkap Anda">
                    </div>
                    <div class="col-md-6">
                        <label for="">Nama Ibu Kandung</label>
                        <input name="mother_name" value="<?= $data['mother_name'] ?>" id="name" type="text" class="form-control" placeholder="Masukkan nama Ibu kandung">
                    </div>
                </div>
            </div><!--//form-group-->


            <div class="form-group name">
                <div class="row">
                    <div class="col-md-6">
                        <label for="name">NIK</label>
                        <input name="nik" value="<?= $data['nik'] ?>" id="name" type="text" class="form-control" placeholder="Masukkan NIK Anda">
                    </div>
                    <div class="col-md-6">
                        <label for="nisn">NISN</label>
                        <input name="nisn" value="<?= $data['nisn'] ?>" id="name" type="text" class="form-control" placeholder="Masukkan NISN Anda">
                    </div>
                </div>
            </div><!--//form-group-->

            <div class="form-group email">
                <div class="row">
                    <div class="col-md-3">
                        <label>Tempat Lahir<span class="required">*</span></label>
                        <input  name="birth_place" value="<?= $data['birth_place'] ?>" type="text" class="form-control" placeholder="Tempat Lahir">
                    </div>
                    <div class="col-md-3">
                        <label for="birth_date">Tanggal Lahir<span class="required">*</span></label>
                        <input  name="birth_date" value="<?= $data['birth_date'] ?>" type="date" class="form-control" placeholder="Tanggal Lahir">
                    </div>
                    <div class="col-md-6">
                        <label for="gender">Jenis Kelamin<span class="required">*</span></label><br/>

                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-warning <?= ($data['gender'] == 'laki-laki') ? 'active' : '' ?>">
                                <input type="radio" id="option1" autocomplete="on" name="gender" value="laki-laki"  <?= ($data['gender'] == 'laki-laki') ? 'checked' : '' ?> >
                                <i class="fa fa-male"></i> Laki-laki
                            </label>
                            <label class="btn btn-warning <?= ($data['gender'] == 'perempuan') ? 'active' : '' ?>">
                                <input type="radio" id="option2" autocomplete="on" name="gender" value="perempuan"  <?= ($data['gender'] == 'perempuan') ? 'checked' : '' ?> >
                                <i class="fa fa-female"></i> Perempuan
                            </label>
                        </div>
                    </div>
                </div>
            </div><!--//form-group-->

            <div class="form-group name">
                <div class="row">
                    <div class="col-md-6">
                        <label>Alamat Rumah</label>
                        <input name="address" value="<?= $data['address'] ?>"  type="text" class="form-control" placeholder="Alamat Rumah">
                    </div>
                    <div class="col-md-6">
                        <label>Nomor Telp / HP</label>
                        <input name="phone_number" value="<?= $data['phone_number'] ?>" type="text" class="form-control" placeholder="Nomor Telp / HP">
                    </div>
                </div>
            </div><!--//form-group-->

            <div class="form-group name">
                <div class="row">

                    <div class="col-md-3">
                        <label for="grade">Tingkat</label>
                        <select class="form-control" name="grade" >
                            <option value="" > -- Pilih Tingkat -- </option>
                            <option value="X" <?= ($data['grade'] == 'X') ? 'selected="selected"' : '' ?>  >X</option>
                            <option value="XI" <?= ($data['grade'] == 'XI') ? 'selected="selected"' : '' ?>  >XI</option>
                            <option value="XII" <?= ($data['grade'] == 'XII') ? 'selected="selected"' : '' ?>  >XII</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="group">Jurusan</label>
                        <select class="form-control" name="group" >
                            <option value="" > -- Pilih Jurusan -- </option>
                            <option value="APAT" <?= ($data['group'] == 'APAT') ? 'selected="selected"' : '' ?>  >APAT</option>
                            <option value="APHPI" <?= ($data['group'] == 'APHPI') ? 'selected="selected"' : '' ?>  >APHPI</option>
                            <option value="NKPI" <?= ($data['group'] == 'NKPI') ? 'selected="selected"' : '' ?>  >NKPI</option>
                            <option value="OTO" <?= ($data['group'] == 'OTO') ? 'selected="selected"' : '' ?>  >OTO</option>
                            <option value="RPL" <?= ($data['group'] == 'RPL') ? 'selected="selected"' : '' ?>  >RPL</option>
                            <option value="TKPI" <?= ($data['group'] == 'TKPI') ? 'selected="selected"' : '' ?>  >TKPI</option>
                            <option value="BDP" <?= ($data['group'] == 'BDP') ? 'selected="selected"' : '' ?>  >BDP</option>
                            <option value="TKR" <?= ($data['group'] == 'TKR') ? 'selected="selected"' : '' ?>  >TKR</option>
                            <option value="TPBO" <?= ($data['group'] == 'TPBO') ? 'selected="selected"' : '' ?>  >TPBO</option>
                            <option value="TPHPI" <?= ($data['group'] == 'TPHPI') ? 'selected="selected"' : '' ?>  >TPHPI</option>
                            <option value="BDY" <?= ($data['group'] == 'BDY') ? 'selected="selected"' : '' ?>  >BDY</option>
                        </select>
                    </div>

                    <div class="col-md-6">
                        <label for="year_force">Tahun Angkatan</label>
                        <div class="row">
                            <div class="col-md-6">
                                <select class="form-control" name="year_force" >
                                    <option value="" > -- Pilih Tahun -- </option>
                                    <?php for ($i = date('Y') - 20; $i < date('Y'); $i++): ?>
                                        <option value="<?= $i ?>" <?= ($data['year_force'] == $i) ? 'selected="selected"' : '' ?>  ><?= $i ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" style="display: block;">
                                    <input type="checkbox" name="alumni" value="1" <?= ($data['alumni'] == 1 )? 'checked="true"' : ''; ?> />
                                    <label for="group">Sudah Lulus</label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <h3 class="title">Khusus Alumni</h3>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label >Pekerjaan / Kantor</label>
                        <input name="work" value="<?= $data['work'] ?>" type="text" class="form-control" placeholder="Pekerjaan / Kantor">
                    </div>
                    <div class="col-md-6">
                        <label >Tahun Lulus</label>
                        <input name="graduation_year" value="<?= $data['graduation_year'] ?>" type="text" class="form-control" placeholder="Tahun Lulus">
                    </div>
                </div>
            </div><!--//form-group-->

            <h3 class="title">Akun Siswa / Alumni</h3>

            <div class="form-group name">
                <div class="row">
                    <div class="col-md-6">
                        <label for="name">Email</label>
                        <input name="username" value="<?= $data['username'] ?>" id="name" type="text" class="form-control" placeholder="Masukkan email">
                    </div>
                    <div class="col-md-6">
                        <label for="">Password Baru</label>
                        <input name="password" value="" id="name" type="password" class="form-control" placeholder="Masukkan password baru">
                    </div>
                </div>
            </div><!--//form-group-->

        </div>

    </div>

    <hr/>
    <a href="<?= site_url('routing/index/dashboard-siswa') ?>" class="btn btn-cta"><i class="fa fa-dashboard"></i> Dashboard</a>    
    <button type="submit" class="btn btn-cta"><i class="fa fa-save"></i> Simpan</button>
    <?= form_close() ?>               
</article>

<script type="text/javascript">
    $(document).ready(function () {
        $('.file-trigger').click(function () {
            $('.file-target').trigger('click');
        });

        $("a#photo_profile").click(function () {
            $("input#photo_upload").trigger("click");
        });

        $("input#photo_upload").change(function () {

            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) {
                return;
            }

            if (/^image/.test(files[0].type)) {

                var reader = new FileReader();
                reader.readAsDataURL(files[0]);

                reader.onloadend = function () {

                    $('a#photo_profile > img').remove();
                    var html = '<img data-src="holder.js/260x180" alt="260x180" src="' + this.result + '" style="width: 100%;">';
                    $('a#photo_profile').append(html);

                }

            }

        });

    });
</script>