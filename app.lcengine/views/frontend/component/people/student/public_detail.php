<?php $data = $data['dataguru']; ?>

<article class="contact-form col-md-12  page-row">                            

    <div class="row">
        <div class="col-md-8" >
            <h3 class="title" style="margin-top: 0px; margin-bottom: 0px;">Detail Guru</h3>
        </div>
    </div>

    <hr/>

    <div class="form-group name">
        <?php echo get_alert(); ?>
        <div class="row">
            <div class="col-md-3">
                <a href="#" class="thumbnail" id="photo_profile">
                    <img data-src="holder.js/300x200" alt="300x200" src="<?= set_image($data['photo'], 'image') ?>" style="width: 100%;">
                </a>
                <input id="photo_upload" name="userfile" style="display: none;" type="file" class="btn btn-info" >
            </div>
            <div class="col-md-9">
                <table class="table table-striped table-bordered">
                    <tr>
                        <th width="25%">Posisi</th>
                        <td width="75%" ><?= appear($data['position']) ?></td>
                    </tr>
                    <tr>
                        <th>Nama Lengkap dan Gelar</th>
                        <td><?= appear($data['fullname']) ?></td>
                    </tr>
                    <tr>
                        <th>NIP Lama</th>
                        <td><?= appear($data['old_nip']) ?></td>
                    </tr>
                    <tr>
                        <th>NIP Baru</th>
                        <td><?= appear($data['new_nip']) ?></td>
                    </tr>
                    <tr>
                        <th>Tempat / Tanggal Lahir</th>
                        <td><?= appear($data['birth_place']) . ', ' . appear($data['birth_day']) ?></td>
                    </tr>
                    <tr>
                        <th>Instansi Induk</th>
                        <td><?= appear_alt($data['parent_institute'], $data['other_parent_institute']) ?></td>
                    </tr>
                    <tr>
                        <th>Unit Organisasi</th>
                        <td><?= appear_alt($data['organization_unit'], $data['other_organization_unit']) ?></td>
                    </tr>
                    <tr>
                        <th>Unit Kerja</th>
                        <td><?= appear_alt($data['work_unit'], $data['other_work_unit']) ?></td>
                    </tr>
                    <tr>
                        <th>Pangkat / Golongan / Ruang CPNS</th>
                        <td><?= appear($data['cpns_grade']) ?></td>
                    </tr>
                    <tr>
                        <th>Pangkat / Golongan / Ruang Terakhir</th>
                        <td><?= appear($data['last_grade']) ?></td>
                    </tr>
                    <tr>
                        <th>TMT Eselon</th>
                        <td><?= appear($data['tmt_eselon']) ?></td>
                    </tr>
                    <tr>
                        <th>Jabatan Terakhir</th>
                        <td><?= appear($data['last_position']) ?></td>
                    </tr>
                    <tr>
                        <th>Masa Kerja Golongan (tahun-bulan)</th>
                        <td><?= appear($data['working_group_period']) ?></td>
                    </tr>

                    <tr>
                        <th>Email</th>
                        <td><?= appear($data['email']) ?></td>
                    </tr>
                </table>


                <table class="table table-striped table-bordered" >
                    <tr>
                        <th colspan="2">
                            <h4>DIKLAT Pra / Jabatan Terakhir</h4>
                        </th>
                    </tr>
                    <tr>
                        <th width="25%" >Nomor SK</th>
                        <td width="75%" ><?= appear($data['sk_number']) ?></td>
                    </tr>
                    <tr>
                        <th>Jumlah Jam</th>
                        <td><?= appear($data['total_hour']) ?></td>
                    </tr>
                    <tr>
                        <th>Tempat Pelaksanaan</th>
                        <td><?= appear($data['execution_place']) ?></td>
                    </tr>
                </table>

                <table class="table table-striped table-bordered" >
                    <tr>
                        <th colspan="2">
                            <h4>Pendidikan</h4>
                        </th>
                    </tr>
                    <tr>
                        <th width="25%" >Pendidikan Saat CPNS</th>
                        <td width="75%" ><?= appear($data['education_when_cpns']) ?></td>
                    </tr>
                    <tr>
                        <th>Pendidikan Terakhir</th>
                        <td><?= appear_alt($data['last_education'], $data['other_last_education']) ?></td>
                    </tr>
                    <tr>
                        <th>Pendidikan Terakhir :<br/>Jurusan</th>
                        <td><?= appear($data['education_field']) ?></td>
                    </tr>
                    <tr>
                        <th>Pendidikan Terakhir :<br/>Nama Sekolah / Lembaga</th>
                        <td><?= appear($data['education_institute']) ?></td>
                    </tr>
                </table>

                <table class="table table-striped table-bordered" >
                    <tr>
                        <th width="25%" >
                            Jenis Kelamin
                        </th>
                        <td width="75%" >
                            <?= appear($data['gender']) ?>
                        </td>
                    </tr>
                    <tr>
                        <th>Agama</th>
                        <td><?= appear_alt($data['religion'], $data['other_religion']) ?></td>
                    </tr>
                    <tr>
                        <th>Kedudukan Hukum</th>
                        <td><?= appear_alt($data['legal_position'], $data['other_legal_position']) ?></td>
                    </tr>
                    <tr>
                        <th>TMT Kepegawaian</th>
                        <td><?= appear($data['tmt_staffing']) ?></td>
                    </tr>
                    <tr>
                        <th>Nomor KARPEG</th>
                        <td><?= appear($data['karpeg_number']) ?></td>
                    </tr>
                    <tr>
                        <th>NUPTK</th>
                        <td><?= appear($data['nuptk']) ?></td>
                    </tr>
                    <tr>
                        <th>No. Registrasi Guru / NRG</th>
                        <td><?= appear($data['nrg']) ?></td>
                    </tr>
                    <tr>
                        <th>NIK</th>
                        <td><?= appear($data['nik']) ?></td>
                    </tr>
                    <tr>
                        <th>NPWP</th>
                        <td><?= appear($data['npwp']) ?></td>
                    </tr>
                    <tr>
                        <th>Nomor ASKES</th>
                        <td><?= appear($data['askes_number']) ?></td>
                    </tr>
                    <tr>
                        <th>Nomor Seri TASPEN</th>
                        <td><?= appear($data['taspen_serial_number']) ?></td>
                    </tr>
                    <tr>
                        <th>Khusus Tugas Guru, <br/>Mengajar Pada Mata Diklat</th>
                        <td><?= appear($data['specifically_assignments']) ?></td>
                    </tr>
                    <tr>
                        <th>Khusus Tugas Karyawan,<br/>Penugasan Pokok</th>
                        <td><?= appear_alt($data['principal_assignment'], $data['other_principal_assignment']) ?></td>
                    </tr>
                    <tr>
                        <th>Kemampuan Berbahasa</th>
                        <td><?= appear_alt($data['language_ability'], $data['other_language_ability']) ?></td>
                    </tr>
                    <tr>
                        <th>Alamat Lengkap</th>
                        <td><?= appear($data['address']) ?></td>
                    </tr>
                    <tr>
                        <th>Nomor HP Aktif</th>
                        <td><?= appear($data['phone_number']) ?></td>
                    </tr>
                </table>

                <table class="table table-striped table-bordered" >
                    <tr>
                        <th colspan="2">
                            <h4>Keterangan Badan</h4>
                        </th>
                    </tr>
                    <tr>
                        <th width="25%" >Berat Badan ( Kg )</th>
                        <td width="75%" ><?= appear($data['body_weight']) ?></td>
                    </tr>
                    <tr>
                        <th>Tinggi Badan ( cm ) </th>
                        <td><?= appear($data['body_height']) ?></td>
                    </tr>
                    <tr>
                        <th>Golongan Darah</th>
                        <td><?= appear_alt($data['body_blood_type'], $data['other_body_blood_type']) ?></td>
                    </tr>
                    <tr>
                        <th>Warna Kulit</th>
                        <td><?= appear_alt($data['skin_color'], $data['other_skin_color']) ?></td>
                    </tr>
                </table>
                
            </div>


        </div>
    </div><!--//form-group-->

</article>
