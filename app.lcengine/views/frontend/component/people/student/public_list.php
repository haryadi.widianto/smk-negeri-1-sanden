<?php $datasiswa = $data['datasiswa']; ?>
<?php $oldinput = get_oldinput(); ?>

<style type="text/css">
    .student-list-table tr th{
        text-align: center;
    }
</style>

<article class="contact-form col-md-12 col-sm-7  page-row">                            

    <div class="row">
        <div class="col-md-12" style="text-align: center; margin-bottom: 25px;" >
            <h3 class="title" style="margin-top: 0px; margin-bottom: 0px;">Daftar <?= $data['position-title'] ?></h3>
        </div>
    </div>

    <table class="table table-bordered table-striped table-responsive student-list-table">
        <tr>
            <th>Nama</th>
            <th>L / P</th>
            <th>Tanggal Lahir</th>
            <th>NIK</th>
            <th>NISN</th>
            <th>Tingkat</th>
            <th>Jurusan</th>
            <th>Tahun Angkatan</th>
        </tr>

        <?php if ($datasiswa): ?>
            <?php foreach ($datasiswa as $key => $item) : ?>
                <tr>
                    <td><?= $item->name ?></td>
                    <td><?= ucwords($item->gender) ?></td>
                    <td><?= inadate($item->birth_date) ?></td>
                    <td><?= $item->nik ?></td>
                    <td><?= $item->nisn ?></td>
                    <td><?= ($data['position-title'] == 'Siswa') ? $item->grade : '-' ?></td>
                    <td><?= $item->group ?></td>
                    <td><?= $item->year_force ?></td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>

            <tr>
                <th colspan="8">
                    Data Tidak Tersedia
                </th>
            </tr>

        <?php endif; ?>
    </table>

</article>
