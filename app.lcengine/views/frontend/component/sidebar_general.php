<div id="sidebar">
    
    <aside class="widget-container">
        <div class="widget-wrapper clearfix">
            <h3 class="widget-title">Latest Blog</h3>
            <ul class="menu news-sidebar">			
                <li class="clearfix">
                    <img src="images/img-4.jpg" data-retina="images/img-4-retina.jpg" alt="We Sent our Student to Himalaya to Study and Hiking" class="imgframe alignleft" />
                    <h4><a href="about.html#">We Sent our Student to Himalaya to Study and Hiking</a></h4>
                    <span class="date-news">October 5, 2013</span>
                </li>
                <li class="clearfix">
                    <img src="images/img-5.jpg" data-retina="images/img-5-retina.jpg" alt="We are opening Modelling Night Course" class="imgframe alignleft" />
                    <h4><a href="about.html#">We are opening Modelling Night Course</a></h4>
                    <span class="date-news">October 2, 2013</span>
                </li>
                <li class="clearfix">
                    <img src="images/img-6.jpg" data-retina="images/img-6-retina.jpg" alt="Photography Club 120 Year Birthday Beach Party" class="imgframe alignleft" />
                    <h4><a href="about.html#">Photography Club 120 Year Birthday Beach Party</a></h4>
                    <span class="date-news">September 30, 2013</span>
                </li>
                <li class="clearfix">
                    <img src="images/img-7.jpg" data-retina="images/img-7-retina.jpg" alt="Photography Club 120 Year Birthday Beach Party" class="imgframe alignleft" />
                    <h4><a href="about.html#">We are opening Medical Faculty in our new Campus</a></h4>
                    <span class="date-news">September 30, 2013</span>
                </li>
            </ul>
            <a href="newslist.html" class="button-more">Read More Blog Post</a>
        </div>
    </aside>
    <aside class="widget-container">
        <div class="widget-wrapper clearfix">
            <h3 class="widget-title">Latest Event</h3>
            <ul class="menu event-sidebar">			
                <li class="clearfix">
                    <div class="event-date-widget">
                        <strong>28</strong>
                        <span>Nov</span>
                    </div>
                    <div class="event-content-widget">
                        <article>
                            <h4><a href="about.html#">Musicfest 2013</a></h4>
                            <p>November 28, 2013 - November 30, 2013<br />
                                9:00 AM - 10:00 AM
                            </p>
                            <em>Campus Auditorium</em>
                        </article>
                        <article>
                            <h4><a href="about.html#">HTML5 &amp; CSS3 Workshop</a></h4>
                            <p>November 28, 2013<br />
                                11:00 AM - 1:00 PM
                            </p>
                            <em>IT Center Building E</em>
                        </article>
                        <article>
                            <h4><a href="about.html#">Free TOEFL &amp; IBT Test</a></h4>
                            <p>November 28, 2013<br />
                                2:00 PM - 2:00 PM
                            </p>
                            <em>Main English Classroom</em>
                        </article>
                    </div>
                </li>
                <li class="clearfix">
                    <div class="event-date-widget">
                        <strong>25</strong>
                        <span>Nov</span>
                    </div>
                    <div class="event-content-widget">
                        <article>
                            <h4><a href="about.html#">Foodfest 2013</a></h4>
                            <p>November 25, 2013 - November 27, 2013<br />
                                9:00 AM - 10:00 AM
                            </p>
                            <em>Campus Canteen</em>
                        </article>
                        <article>
                            <h4><a href="about.html#">Student Festival</a></h4>
                            <p>November 25, 2013 - November 26, 2013<br />
                                8:00 AM - 9:00 AM
                            </p>
                            <em>Basketball Court</em>
                        </article>
                        <article>
                            <h4><a href="about.html#">Free TOEFL &amp; IBT Test</a></h4>
                            <p>November 28, 2013<br />
                                2:00 PM - 2:00 PM
                            </p>
                            <em>Main English Classroom</em>
                        </article>
                    </div>
                </li>
            </ul>
            <a href="eventlist.html" class="button-more">More Event</a>
        </div>
    </aside>
</div>