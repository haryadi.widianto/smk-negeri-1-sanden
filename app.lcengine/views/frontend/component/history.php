<article class="static-page">
    <h1 id="main-title">School Fun Historical Moment</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse ornare, ipsum quis aliquam lobortis, felis sapien rutrum risus, posuere ultrices quam nisi nec nulla. Vestibulum ipsum diam, congue in nunc et, placerat faucibus nunc.</p>
    <div id="history-container">
        <h3 class="history-year">1980</h3>
        <div class="history-moment clearfix">
            <header>
                January 11, 1980
            </header>
            <aside>
                <h2 class="history-title">First Stone Signed by President John Doe</h2>
                <p>Vestibulum ipsum diam, congue in nunc et, placerat faucibus nunc. Proin consectetur mauris quis tincidunt faucibus. Suspendisse lobortis blandit aliquet. Cras a luctus orci.</p>
            </aside>
        </div>
        <div class="history-moment clearfix">
            <header>
                September 23, 1980
            </header>
            <aside>
                <h2 class="history-title">Our Club win their first kayak race</h2>
                <img src="images/img-19.jpg" data-retina="images/img-19-retina.jpg" alt="" class="alignleft img-history" />
                <p><strong>Morbi scelerisque magna ac elit accumsan fringilla</strong>. Vivamus enim massa, egestas quis viverra ut, adipiscing eget metus. Etiam neque orci, cursus vitae sem in, rhoncus vestibulum dolor. Cras consectetur, tellus vel auctor venenatis, tortor ante.</p>
            </aside>
        </div>
        <h3 class="history-year">1985</h3>
        <div class="history-moment clearfix">
            <header>
                February 21, 1985
            </header>
            <aside>
                <h2 class="history-title">We are opening new campus</h2>
                <p>Suspendisse lobortis blandit aliquet. Cras a luctus orci. Aenean pretium venenatis gravida. Morbi varius erat erat, vitae sollicitudin tortor ornare tincidunt.</p>
            </aside>
        </div>
        <div class="history-moment clearfix">
            <header>
                March 11, 1985
            </header>
            <aside>
                <h2 class="history-title">We sent our student to Olympic</h2>
                <p>Vivamus enim massa, egestas quis viverra ut, adipiscing eget metus. Etiam neque orci, cursus vitae sem in, rhoncus vestibulum dolor.</p>
            </aside>
        </div>
        <div class="history-moment clearfix">
            <header>
                December 25, 1985
            </header>
            <aside>
                <h2 class="history-title">First Christmas in Campus</h2>
                <img src="images/img-18.jpg" data-retina="images/img-18-retina.jpg" alt="" class="alignleft img-history" />
                <p>Nullam nec elit risus. Aliquam ac lectus cursus, rhoncus velit vel, molestie lacus. Cras tempus vel diam vel vehicula. Morbi tincidunt porttitor turpis, nec accumsan felis pharetra sit amet. Morbi varius erat erat, vitae sollicitudin tortor ornare tincidunt.</p>
            </aside>
        </div>
        <h3 class="history-year">2013</h3>
        <div class="history-moment clearfix">
            <header>
                December 21, 2013
            </header>
            <aside>
                <h2 class="history-title">We have website now!</h2>
                <p>Quisque sed placerat lacus. Donec lacinia tempus nibh, egestas varius nisi dignissim ut. Proin sollicitudin consequat euismod. Phasellus sed purus in ipsum dignissim imperdiet. Mauris pharetra erat non nibh mollis vulputate. Nullam mauris dui, fringilla et tempus at, faucibus et nisl. In ultricies massa ante, nec congue mauris fringilla eu. Maecenas consequat aliquam enim at commodo.</p>
            </aside>
        </div>
    </div>
</article>