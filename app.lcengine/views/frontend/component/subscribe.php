
<div class="footer-col col-md-6 col-sm-8 newsletter">
    <div class="footer-col-inner">
        <h3>Bergabung Grup Email</h3>
        <p>Dapatkan informasi terkini dari SMK Negeri 1 Sanden yang langsung kami kirimkan ke kotak email Anda</p>
        <?php $attributes = array('class' => 'subscribe-form'); ?>
        <?= form_open_multipart('post/send_message', $attributes) ?>
        <div class="form-group">
            <input type="email" name="email" class="form-control" placeholder="Masukkan alamat email Anda" />
            <input type="hidden" value="subscribe" name="type"/>
        </div>
        <input class="btn btn-theme btn-subscribe" type="submit" value="Bergabung">
        <?= form_close() ?>
    </div><!--//footer-col-inner-->
</div><!--//foooter-col--> 

