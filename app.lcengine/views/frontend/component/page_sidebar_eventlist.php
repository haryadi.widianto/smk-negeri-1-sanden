<?php $data = $data['event_list']; ?>

<section class="widget has-divider">
    <h3 class="title">Agenda Terkini</h3>

    <?php if (!empty($data)): ?>
        <?php foreach ($data as $key => $value) : ?>
            <article class="events-item row page-row">                                    
                <div class="date-label-wrapper col-md-3 col-sm-4 col-xs-4">
                    <p class="date-label">
                        <span class="month"><?= date('M', strtotime($value->day_start)) ?></span>
                        <span class="date-number"><?= date('d', strtotime($value->day_start)) ?></span>
                    </p>
                </div><!--//date-label-wrapper-->
                <div class="details col-md-9 col-sm-8 col-xs-8">
                    <a href="<?= set_route('detail_event/'.$value->slug) ?>"><h5 class="title"><?= $value->title ?></h5></a>
                    <p class="time text-muted">
                        <?= date('H:i', strtotime($value->time_start)) ?> - 
                        <?= date('H:i', strtotime($value->time_end)) ?>
                        <br><?= $value->location ?></p>                  
                </div><!--//details-->                                    
            </article>
        <?php endforeach; ?>
    <?php endif; ?>

</section><!--//widget-->