<?php $data = $data['event_list']; ?>

<div class="events-wrapper col-md-8 col-sm-7">   

    <?php if (!empty($data)): ?>
        <?php foreach ($data as $key => $value) : ?>

            <article class="events-item page-row has-divider clearfix">
                <div class="date-label-wrapper col-md-1 col-sm-2">
                    <p class="date-label">
                        <span class="month"><?= date('M', strtotime($value->day_start)) ?></span>
                        <span class="date-number"><?= date('d', strtotime($value->day_start)) ?></span>
                    </p>
                </div><!--//date-label-wrapper-->
                <div class="details col-md-11 col-sm-10">
                    <h3 class="title"><?= $value->title ?></h3>
                    <p class="meta"><span class="time">
                            <i class="fa fa-clock-o"></i>
                            <?= date('H:i', strtotime($value->time_start)) ?> - 
                            <?= date('H:i', strtotime($value->time_end)) ?>
                        </span>
                        <span class="location">
                            <i class="fa fa-map-marker"></i>
                            <a href="#"><?= $value->location ?></a>
                        </span>
                    </p>  
                    <p class="desc">
                        <?= word_limiter(strip_tags($value->description), 50) ?>
                    </p>                      
                    <a class="btn btn-theme read-more" href="<?= set_route('detail_event/' . $value->slug) ?>">Selengkapnya<i class="fa fa-chevron-right"></i></a>
                </div><!--//details-->
            </article><!--//events-item-->

        <?php endforeach; ?>
    <?php endif; ?>

     <?= $this->pagination->create_links() ?>

</div>