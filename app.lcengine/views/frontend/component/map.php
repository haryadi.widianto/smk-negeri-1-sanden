<?php $contact_setting = $data['contact']; ?>

<div class="page-row">
    <article class="map-section">
        <h3 class="title">Lokasi SMK Negeri 1 Sanden</h3>
        <div id="maps" style="height: 300px;"></div><!--//map-->
    </article><!--//map-->
</div><!--//page-row-->

<script src="<?= base_url('pub.lcengine/frontend/plugins/gmaps/gmap3.min.js') ?>" type="text/javascript"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#maps").gmap3({
            map: {
                options: {
                    center: [<?= $contact_setting['lang_area'] ?>, <?= $contact_setting['long_area'] ?>],
                    zoom: 15
                }
            },
            marker: {
                latLng: [<?= $contact_setting['lang_point'] ?>, <?= $contact_setting['long_point'] ?>],
                callback: function() {

                }
            }
        });
    });
</script>

