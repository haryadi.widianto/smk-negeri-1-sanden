<?php $data = $leftcontent['page']['data']['content']; ?>
<article class="static-page">
    <h1 id="main-title"><?= $data['title'] ?></h1>
    <img style="max-height: 300px; max-width: 300px;" src="<?= set_image($data['image']) ?>" alt="<?= $data['title'] ?>" class="alignleft" />
    <?= $data['content'] ?>
</article>