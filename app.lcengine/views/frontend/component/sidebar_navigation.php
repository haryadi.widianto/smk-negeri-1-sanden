<?php $sidebar_navigation = $data['sidebar_navigation']; ?>

<section class="links">
    <h1 class="section-heading text-highlight"><span class="line">Link Terkait</span></h1>
    <div class="section-content">
        <?php if (!empty($sidebar_navigation)): ?>
            <?php foreach ($sidebar_navigation as $key => $value) : ?>
                <p>
                    <a href="<?= site_url('/routing/index/' . $value->data) ?>">
                        <i class="fa fa-caret-right"></i>
                        <?=$value->menu  ?>
                    </a>
                </p>
            <?php endforeach; ?>
        <?php endif; ?>
    </div><!--//section-content-->
</section><!--//links-->