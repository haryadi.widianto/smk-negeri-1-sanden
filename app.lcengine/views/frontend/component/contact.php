<?php $meta = $data['meta']; ?>
<?php $data = $data['contact']; ?>

<article class="contact-form col-md-8 col-sm-7  page-row">                            
    <h3 class="title"><?= $meta['title'] ?></h3>
    <p><?= $meta['content'] ?></p>
    <?= form_open('post/send_message') ?>

    <div class="form-group name">
        <?php echo get_alert(); ?>
        <label for="name">Nama Lengkap</label>
        <input name="name" id="name" type="text" class="form-control" placeholder="Masukkan nama lengkap Anda">
    </div><!--//form-group-->
    <div class="form-group email">
        <label for="email">Email<span class="required">*</span></label>
        <input  name="email" id="email" type="email" class="form-control" placeholder="Masukkan email Anda">
    </div><!--//form-group-->
    <div class="form-group phone">
        <label for="phone">Topik</label>
        <input  name="subject" id="phone" type="tel" class="form-control" placeholder="Masukkan topik Anda">
    </div><!--//form-group-->
    <div class="form-group phone">
        <label for="phone">Jenis Pesan</label>
        <select name="type" class="form-control subject">
            <option value="sugest">Saran</option>
            <option value="critical" >Kritik</option>
            <option value="testimonial" >Testimonial</option>
            <option value="question" >Pertanyaan</option>
        </select>
    </div><!--//form-group-->
    <div class="form-group message">
        <label for="message">Pesan<span class="required">*</span></label>
        <textarea  name="content" id="message" class="form-control" rows="6" placeholder="Tulisan saran, kritik, testimonial dan pertanyaan Anda di sini.."></textarea>
    </div><!--//form-group-->
    <button type="submit" class="btn btn-theme">Kirim Pesan</button>
    <?= form_close() ?>               
</article>