<article class="contact-form col-md-8 col-sm-7  page-row">                            
    <h3 class="title">Progres Pendaftaran PPDB</h3>
    <div class="progress progress-striped active">
        <div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
            80%
        </div>
    </div>
    <hr/>
    <h3 class="title">Data Prestasi Bakat</h3>
    <?= form_open_multipart('routing/index/post-data-prestasi-bakat') ?>
    <div class="form-group form-prestasi">
        <?php echo get_alert(); ?>

        <input type="hidden" name="id_student" value="<?= $this->session->userdata('id_student') ?>" />

        <div class="row" style="margin-bottom: 20px;">
            <div class="col-md-4">
                <label for="name">Bidang</label>
                <select name="bidang[]" class="form-control">
                    <option value="Olahraga" >Olahraga</option>
                    <option value="Seni" >Seni</option>
                    <option value="Sains" >Sains</option>
                    <option value="Lainnya" >Lainnya</option>
                </select>
            </div>
            <div class="col-md-8">
                <label for="name">Prestasi</label>
                <input name="prestasi[]" value="<?= set_value('prestasi') ?>" id="name" type="text" class="form-control" placeholder="Masukkan Prestasi">
            </div>
        </div>


    </div><!--//form-group-->


    <div class="form-group message">
        <button type="button" class="btn btn-info btn-add"><i class="fa fa-plus"></i> Tambah</button>
    </div><!--//form-group-->

    <button type="submit" class="btn btn-cta"><i class="fa fa-arrow-circle-right"></i> Selanjutnya</button>
    <?= form_close() ?>               
</article>

<script type="text/javascript">
    $(document).ready(function() {
        $('.btn-add').click(function() {
            var html = '<div class="row" style="margin-bottom: 20px;">';
            html += '<div class="col-md-4">';
            html += '<label for="name">Bidang</label>';
            html += '<select name="bidang[]" class="form-control">';
            html += '<option value="Olahraga" >Olahraga</option>';
            html += '<option value="Seni" >Seni</option>';
            html += '<option value="Sains" >Sains</option>';
            html += '<option value="Lainnya" >Lainnya</option>';
            html += '</select>';
            html += '</div>';
            html += '<div class="col-md-8">';
            html += '<label for="name">Prestasi</label>';
            html += '<input name="prestasi[]" id="name" type="text" class="form-control" placeholder="Masukkan Prestasi">';
            html += '</div>';
            html += '</div>';
            
            $('.form-prestasi').append(html);
        });
    });
</script>
