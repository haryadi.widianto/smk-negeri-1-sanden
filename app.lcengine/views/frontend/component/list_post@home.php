<?php
$data = array();
if(isset($middlecontent)){
     $data = $middlecontent['list_post@home']['data'];
}
if(isset($sidebar_normal)){
     $data = $sidebar_normal['list_post@home']['data'];
}
?>

<aside class="widget-container">
    <div class="widget-wrapper clearfix">
        <h3 class="widget-title">Berita Terbaru</h3>
        <ul class="menu news-sidebar">
            <?php if (!empty($data)): ?>
                <?php foreach ($data as $key => $value) : ?>
                    <li class="clearfix">
                        <img style="width: 123px; max-width: 123px; max-height: 94px;" src="<?= set_image($value->image) ?>" data-retina="<?= set_image($value->image) ?>" alt="<?= $value->title ?>" class="imgframe alignleft" />
                        <h4><a href="<?= set_route('detail_post/' . $value->slug) ?>"><?= $value->title ?></a></h4>
                        <span class="date-news"><?= date('j F Y', strtotime($value->created_at))?></span>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>

        </ul>
        <a href="<?= set_route('list_post/blog-2015-02-13-074733') ?>" class="button-more">Read More Blog Post</a>
    </div>
</aside>
