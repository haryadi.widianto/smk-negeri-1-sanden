<?php $data = $data['list_team']; ?>

<div class="team-wrapper col-md-8 col-sm-7">

    <?php if (!empty($data)): ?>
        <?php foreach ($data as $key => $value) : ?>

            <div class="row page-row" >
                <figure class="thumb col-md-3 col-sm-4 col-xs-6">
                    <img class="img-responsive" src="<?= set_image($value->image) ?>" alt="<?= $value->name ?>" />
                </figure>
                <div class="details col-md-9 col-sm-8 col-xs-6">
                    <h3 class="title"><?= $value->name ?></h3>
                    <h4><?= $value->position ?></h4>
                    <p><?= $value->resume ?></p>                                 
                </div>                               
            </div>

        <?php endforeach; ?>
    <?php endif; ?>
    
     <?= $this->pagination->create_links() ?>

</div>