<?php $array = $headcontent['sub_navigation']['data']; ?>
<nav id="nav-sub-container" class="clearfix">
    <ul id="nav-sub">
        <?php if (!empty($array)): ?>
            <?php foreach ($array as $key => $value) : ?>
                <li><a href="<?= set_route($value->data) ?>"><?= $value->menu ?></a></li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
</nav>