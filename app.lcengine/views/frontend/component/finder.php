<?php $array = $data['category']; ?>

<section class="course-finder">
    <h1 class="section-heading text-highlight"><span class="line">Cari Artikel</span></h1>
    <div class="section-content">
        <form class="course-finder-form" action="#" method="get">
            <div class="row">
                <div class="col-md-5 col-sm-5 subject">
                    <select class="form-control subject">
                        <?php if (!empty($array)): ?>
                            <?php foreach ($array as $key => $value) : ?>
                                <option value="<?= $value->id_post_category ?>"><?= $value->post_category ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div> 
                <div class="col-md-7 col-sm-7 keywords">
                    <input class="form-control pull-left" type="text" placeholder="Search keywords" />
                    <button type="submit" class="btn btn-theme"><i class="fa fa-search"></i></button>
                </div> 
            </div>                     
        </form><!--//course-finder-form-->
        <a class="read-more" href="courses.html">View all our courses<i class="fa fa-chevron-right"></i></a>
    </div><!--//section-content-->
</section><!--//course-finder-->