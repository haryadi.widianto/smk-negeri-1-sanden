<?php

class Post extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('muser');
        $this->load->model('muser_profile');
        $this->load->model('mcontact');
        $this->load->model('mregistration');
        $this->load->model('frontend/msession');
    }

    public function register() {

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|max_length[100]'
            ), array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|max_length[100]'
            ), array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|max_length[250]'
            ), array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|max_length[100]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('routing/register', 'refresh');
        } else {

            $data['username'] = $this->input->post('username');
            $data['email'] = $this->input->post('email');
            $data['password'] = $this->encrypt->encode($this->input->post('password'), get_key());
            $data['name'] = $this->input->post('name');
            $data['status'] = 'deactive';
            $data['level'] = 'costumer';
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['token'] = $this->encrypt->encode(date('Y-m-d H:i:s') . $this->input->post('email'), get_key());

            /* Check availabe username & password */
            if ($this->muser->check_available_user($data, 'add')) {
                $this->muser->insert_data($data);
                set_alert('alert-info', "New account with name <strong>{$data['name']}</strong> is success created ");
                redirect('routing/index/register-success', 'refresh');
            } else {
                set_alert('alert-danger', 'Sorry, username is already exist');
                redirect('routing/index/register', 'refresh');
            }
        }
    }

    public function update_user($id = '') {

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|max_length[100]'
            ), array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|max_length[100]'
            ), array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|max_length[100]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('routing/index/dashboard', 'refresh');
        } else {

            $data_user['id_user'] = $id;
            $data_user['name'] = $this->input->post('name');
            $data_user['email'] = $this->input->post('email');
            $data_user['username'] = $this->input->post('username');

            if ($this->input->post('new_password') != '') {
                $data_user['password'] = $this->encrypt->encode($this->input->post('new_password'), get_key());
            }

            /* Check availabe username & password */
            if ($this->muser->check_available_user($data_user, 'edit', $id)) {
                $this->muser->update_data($data_user);
            } else {
                set_alert('alert-danger', 'Sorry, username is already exist');
                redirect('routing/index/dashboard', 'refresh');
            }

            $data['id_user'] = $id;
            $data['gender'] = $this->input->post('gender');
            $data['address'] = $this->input->post('address');
            $data['telp'] = $this->input->post('telp');
            $data['post_code'] = $this->input->post('post_code');

            if ($this->muser_profile->user_profile_exist($id) > 0) {

                if ($_FILES['userfile']['name'] != '') {

                    /* Deleting old file */
                    if ($this->muser_profile->get_old_filename($id) != '') {
                        unlink("./pub.lcengine/upload/user_photo_profile/" . $this->muser_profile->get_old_filename($id));
                    }
                    $data['foto'] = $this->save_photo_profile($id);
                }

                /* If image delted */
                if ($this->input->post('delete_photo')) {
                    if ($this->muser_profile->get_old_filename($id) != '') {
                        unlink("./pub.lcengine/upload/user_photo_profile/" . $this->muser_profile->get_old_filename($id));
                    }
                    $data['foto'] = '';
                }

                $this->muser_profile->update_data($data);
            } else {

                if ($_FILES['userfile']['name'] != '') {
                    $data['foto'] = $this->save_photo_profile($id);
                }

                /* If image deleted */
                if ($this->input->post('delete_photo')) {
                    if ($this->muser_profile->get_old_filename($id) != '') {
                        unlink("./pub.lcengine/upload/user_photo_profile/" . $this->muser_profile->get_old_filename($id));
                    }
                    $data['foto'] = '';
                }

                $this->muser_profile->insert_data($data);
            }

            set_alert('alert-info', "User with name {$this->muser->get_per_field($id, 'name')} is success updated");
            redirect('routing/index/dashboard', 'refresh');
        }
    }

    public function login_process() {

        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
        );

        if ($this->auth->login($data)) {

            $user = $this->session->userdata('login');
            if ($user['level'] == 'admin')
                redirect('backend/dashboard', 'refresh');

            if ($user['level'] == 'costumer') {
                $this->msession->set_session();
                redirect('routing/index/dashboard', 'refresh');
            }
        } else {
            redirect('routing', 'refresh');
        }
    }

    public function logout() {

        $this->session->sess_destroy();

        redirect('routing', 'refresh');
    }

    public function save_photo_profile($id = '') {

        $config['upload_path'] = './pub.lcengine/upload/user_photo_profile/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '500';
        $config['max_width'] = '500';
        $config['max_height'] = '500';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('routing/index/dashboard', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }
    }

    public function send_message() {

        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'subject' => $this->input->post('subject'),
            'content' => $this->input->post('content'),
            'type' => $this->input->post('type'),
            'status' => 'new',
            'date' => date('Y-m-d H:i:s'),
        );

        $this->mcontact->insert_data($data);

        set_alert('alert-success', "Terimakasih atas pesan yang Anda sampaikan.");

        redirect(set_route('contact'), 'refresh');
    }

    public function send_application() {

        $data = array(
            'name' => $this->input->post('name'),
            'place_birth' => $this->input->post('place_birth'),
            'date_birth' => $this->input->post('date_birth'),
            'school_from' => $this->input->post('school_from'),
            'address' => $this->input->post('address'),
            'avg_raport' => $this->input->post('avg_raport'),
            'avg_nem' => $this->input->post('avg_nem'),
            'registration_code' => get_random_string(),
            'year_session' => date('Y'),
            'department' => $this->input->post('department'),
            'created_at' => date('Y-m-d H:i:s'),
        );

        $this->mregistration->insert_data($data);

        $this->session->set_flashdata('falert', 'success');
        $this->session->set_flashdata('data_verification', $data);

        redirect(set_route('register_success'), 'refresh');
    }

    public function subsribe() {

        $data = array(
            'email' => $this->input->post('email'),
            'type' => $this->input->post('type'),
            'status' => 'new',
            'date' => date('Y-m-d H:i:s'),
        );

        $this->mcontact->insert_data($data);

        return true;
    }

    public function pdf() {
        Pdf::write('hello', "report.pdf");
    }

}

?>
