<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Cauth extends CI_Controller{
    
    public function __construct() {
        parent::__construct();
        $this->load->library('auth');
        $this->load->model('mauth');
        $this->load->model('frontend/msession');
    }
    
    public function index() {
        
        $data['data']['title'] = 'Login Page';
        $this->load->view('backend/static-page/login-page', $data);
        
    }
    
    public function login_process() {
        
        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password')
        );
        
        if($this->auth->login($data)){
            
            $user = $this->session->userdata('login');
            if($user['level'] == 'admin') redirect('backend/dashboard', 'refresh');
            if($user['level'] == 'costumer'){
                $this->msession->set_session($user['id_user']);
                redirect('routing/index/dashboard', 'refresh');
            }
            
        }else{
            redirect('cauth', 'refresh');
        }
        
    }
    
    public function logout() {
        
        $this->auth->logout('cauth');
        
    }
    
}

?>
