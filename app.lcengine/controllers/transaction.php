<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Transaction extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('muser');
        $this->load->model('frontend/mchart');
        $this->load->model('frontend/msession');
        $this->load->model('mproduct');
    }

    public function put_item($id_product = '', $qty = 0) {

        $qty_ = 0;
        if ($this->input->post('qty')) {
            $qty_ = $this->input->post('qty');
        } else {
            $qty_ = $qty;
        }

        $this->mchart->set_chart($id_product, $qty_);
        $product = $this->mproduct->get_per_field($id_product, 'product');

        set_alert('alert-success', "Add item <strong>{$product}</strong> is success");

        redirect(str_replace(base_url('index.php'), '', $_SERVER['HTTP_REFERER']), 'refresh');
    }

    public function add_item($id_product = '', $qty = 0) {

        $qty_ = 0;
        if ($this->input->post('qty')) {
            $qty_ = $this->input->post('qty');
        } else {
            $qty_ = $qty;
        }

        $this->mchart->set_chart($id_product, $qty_);

        return false;
    }

    public function delete_item($id_product = '') {

        $this->mchart->delete_item($id_product);
        return false;
    }

    public function submit_session() {

        return $this->load->view('frontend/component/dashboard/confirm-submit');
    }

    public function set_no_resensi() {

        $data = json_decode($this->input->post('data'), true);

        if ($data['no_resensi'] != '') {
            $this->msession->submit_session($data);
            $this->msession->set_session();
        }
        
        return $this->load->view('frontend/component/dashboard/success-submit');
        
    }

    public function get_item() {

        print_r($this->session->userdata('d'));
        return false;
    }

}

?>
