<?php

class Registration extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('registration/student');
        $this->load->model('registration/parents');
        $this->load->model('registration/raport');
        $this->load->model('registration/school_from');
    }

    public function set_student() {
        
        /* Validation */
        $config = array(
            array('field' => 'name', 'label' => 'Nama ', 'rules' => 'trim|required'),
            array('field' => 'nisn', 'label' => 'NISN ', 'rules' => 'required'),
            array('field' => 'home_address', 'label' => 'Alamat Asal', 'rules' => 'required'),
            array('field' => 'stay_address', 'label' => 'Alamat Tinggal', 'rules' => 'required'),
            array('field' => 'gender', 'label' => 'Jenis Kelamin', 'rules' => 'required'),
            array('field' => 'religion', 'label' => 'Agama', 'rules' => 'required'),
            array('field' => 'telp', 'label' => 'Telepon', 'rules' => 'required'),
            array('field' => 'birth_place', 'label' => 'Tempat Lahir', 'rules' => 'required'),
            array('field' => 'birth_date', 'label' => 'Tanggal Lahir', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        $this->form_validation->set_message('required', 'Kolom %s wajib diisi');

        if ($this->form_validation->run() == FALSE) {
             set_alert('alert-danger', validation_errors());
            redirect('routing/index/data-siswa', 'refresh');
        } else {

            $data = array(
                'name' => $this->input->post('name'),
                'nisn' => $this->input->post('nisn'),
                'home_address' => $this->input->post('home_address'),
                'stay_address' => $this->input->post('stay_address'),
                'gender' => $this->input->post('gender'),
                'religion' => $this->input->post('religion'),
                'telp' => $this->input->post('telp'),
                'birth_place' => $this->input->post('birth_place'),
                'birth_date' => $this->input->post('birth_date'),
                'register_number' => get_random_string(),
                'year_session' => date('Y'),
                'created_at' => date('Y-m-d H:i:s')
            );

            /* Image upload */
//            if ($_FILES['userfile']['name'] != '')
//                $data['image'] = upload_file(set_route('data-siswa'));

            $this->student->insert_data($data);

//            $this->session->set_flashdata('id_student', mysql_insert_id());
            
            return "sukses";

         //   redirect('routing/index/reg_parents', 'refresh');
        }
    }

    public function set_parents($id_student = 0) {

        /* Validation */
        $config = array(
            array('field' => 'name', 'label' => 'Nama Lengkap', 'rules' => 'required'),
            array('field' => 'address', 'label' => 'Alamat Lengkap', 'rules' => 'required'),
            array('field' => 'telp', 'label' => 'Telepon', 'rules' => 'required'),
            array('field' => 'religion', 'label' => 'Agama', 'rules' => 'required'),
            array('field' => 'occupation', 'label' => 'Pekerjaan', 'rules' => 'required'),
            array('field' => 'alt_name', 'label' => 'Nama Lengkap Wali', 'rules' => 'required'),
            array('field' => 'alt_address', 'label' => 'Alamat Lengkap Wali', 'rules' => 'required'),
            array('field' => 'alt_telp', 'label' => 'Telepon Wali', 'rules' => 'required'),
            array('field' => 'alt_religion', 'label' => 'Agama Wali', 'rules' => 'required'),
            array('field' => 'alt_occupation', 'label' => 'Pekerjaan Wali', 'rules' => 'required')
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('fail', validation_errors());
            $this->session->set_flashdata('id_student', $id_student);

            redirect('routing/index/reg_parents', 'refresh');
        } else {

            $data = array(
                'id_student' => $id_student,
                'name' => $this->input->post('name'),
                'address' => $this->input->post('address'),
                'telp' => $this->input->post('telp'),
                'religion' => $this->input->post('religion'),
                'occupation' => $this->input->post('occupation'),
                'alt_name' => $this->input->post('alt_name'),
                'alt_address' => $this->input->post('alt_address'),
                'alt_telp' => $this->input->post('alt_telp'),
                'alt_religion' => $this->input->post('alt_religion'),
                'alt_occupation' => $this->input->post('alt_occupation')
            );

            $this->parents->insert_data($data);

            $this->session->set_flashdata('id_student', $id_student);

            redirect('routing/index/reg_school_from', 'refresh');
        }
    }

    public function set_school_from($id_student = 0) {

        /* Validation */
        $config = array(
            array('field' => 'bi1', 'label' => 'Bahasa Indonesia Semester I', 'rules' => 'required'),
            array('field' => 'bi2', 'label' => 'Bahasa Indonesia Semester II', 'rules' => 'required'),
            array('field' => 'bi3', 'label' => 'Bahasa Indonesia Semester III', 'rules' => 'required'),
            array('field' => 'bi4', 'label' => 'Bahasa Indonesia Semester IV', 'rules' => 'required'),
            array('field' => 'bi5', 'label' => 'Bahasa Indonesia Semester v', 'rules' => 'required'),
            array('field' => 'be1', 'label' => 'Bahasa Inggris Semester I', 'rules' => 'required'),
            array('field' => 'be2', 'label' => 'Bahasa Inggris Semester II', 'rules' => 'required'),
            array('field' => 'be3', 'label' => 'Bahasa Inggris Semester III', 'rules' => 'required'),
            array('field' => 'be4', 'label' => 'Bahasa Inggris Semester IV', 'rules' => 'required'),
            array('field' => 'be5', 'label' => 'Bahasa Inggris Semester V', 'rules' => 'required'),
            array('field' => 'mt1', 'label' => 'Matematika Semester I', 'rules' => 'required'),
            array('field' => 'mt2', 'label' => 'Matematika Semester II', 'rules' => 'required'),
            array('field' => 'mt3', 'label' => 'Matematika Semester III', 'rules' => 'required'),
            array('field' => 'mt4', 'label' => 'Matematika Semester IV', 'rules' => 'required'),
            array('field' => 'mt5', 'label' => 'Matematika Semester V', 'rules' => 'required'),
            array('field' => 'ipa1', 'label' => 'IPA I', 'rules' => 'required'),
            array('field' => 'ipa2', 'label' => 'IPA II', 'rules' => 'required'),
            array('field' => 'ipa3', 'label' => 'IPA III', 'rules' => 'required'),
            array('field' => 'ipa4', 'label' => 'IPA IV', 'rules' => 'required'),
            array('field' => 'ipa5', 'label' => 'IPA V', 'rules' => 'required'),
            array('field' => 'nem', 'label' => 'NEM', 'rules' => 'required'),
       
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('fail', validation_errors());
            $this->session->set_flashdata('id_student', $id_student);

            redirect('routing/index/reg_school_from', 'refresh');
            
        } else {

            /* School From Data */
            $school_from = array(
                'id_student' => $id_student,
                'school_name' => $this->input->post('school_name'),
                'school_address' => $this->input->post('school_address')
            );

            $this->school_from->insert_data($school_from);

            /* Raport Data */
            $raport = array(
                'id_student' => $id_student,
                /* Bahasa Indonesia */
                'bi1' => $this->input->post('bi1'),
                'bi2' => $this->input->post('bi2'),
                'bi3' => $this->input->post('bi3'),
                'bi4' => $this->input->post('bi4'),
                'bi5' => $this->input->post('bi5'),
                /* Bahasa Inggris */
                'be1' => $this->input->post('be1'),
                'be2' => $this->input->post('be2'),
                'be3' => $this->input->post('be3'),
                'be4' => $this->input->post('be4'),
                'be5' => $this->input->post('be5'),
                /* Matematika */
                'mt1' => $this->input->post('mt1'),
                'mt2' => $this->input->post('mt2'),
                'mt3' => $this->input->post('mt3'),
                'mt4' => $this->input->post('mt4'),
                'mt5' => $this->input->post('mt5'),
                /* IPA */
                'ipa1' => $this->input->post('ipa1'),
                'ipa2' => $this->input->post('ipa2'),
                'ipa3' => $this->input->post('ipa3'),
                'ipa4' => $this->input->post('ipa4'),
                'ipa5' => $this->input->post('ipa5'),
                /* NEM */
                'nem' => $this->input->post('nem')
            );

            $this->raport->insert_data($raport);

            $this->session->set_flashdata('id_student', $id_student);

            redirect('routing/index/register_success/'.$id_student, 'refresh');
        }
    }
    
     public function upload_file($path = '') {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '7000';
        $config['max_width'] = '7000';
        $config['max_height'] = '7000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect($path, 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        
    }

}
