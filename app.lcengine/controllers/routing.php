<?php

class Routing extends CI_Controller
{

    private $parsing = array();
    private $limit = 10;
    private $index = 0;
    private $root = '';
    private $site_setting = array();
    private $product_category = array();
    private $path = '';
    private $page = '';
    private $slug = '';
    private $user_id = '';

    public function __construct()
    {
        parent::__construct();
        /* List of model that used */
        $this->path = '/index.php/routing/index/';
        $this->load->model('muser');
        $this->load->model('mmenu');
        $this->load->model('mmenu_category');
        $this->load->model('mpost');
        $this->load->model('mpost_category');
        $this->load->model('msite_setting');
        $this->load->model('mslider');
        $this->load->model('mevent');
        $this->load->model('malbum');
        $this->load->model('mgallery');
        $this->load->model('mcontact');
        $this->load->model('mcontact_setting');
        $this->load->model('msocial_media');
        $this->load->model('mbottom_navigation');
        $this->load->model('people/mstudent');
        $this->load->model('people/mteacher');

        $this->load->library('registration');
        $this->load->library('people');
        $this->load->library('teacher');
        $this->load->library('auth');

        // Load field input data
        set_oldinput($this->input->post());

    }

    public function index($page = '', $slug = '', $index = '')
    {

        $this->page = $page;
        $this->slug = $slug;
        $this->index = $index;

        $this->set_meta('header');
        $this->set_head('precontent');
        $this->set_top_navigation('precontent');

        switch ($page) {

            case 'page':
                $this->set_page_detail_post('page_content');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'list_post':
                $this->set_page_postlist('page_content');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'detail_post':
                $this->set_page_detail_post('page_content');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'list_team':
                $this->set_page_teamlist('page_content');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'detail_team':
                break;

            case 'contact':
                $this->set_page_contact('page_content');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'list_event':
                $this->set_page_eventlist('page_content');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'detail_event':
                $this->set_page_detail_event('page_content');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'list_gallery':
                $this->set_page_list_gallery('page_content');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'detail_gallery':
                $this->set_page_detail_gallery('page_content');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'history':
                break;

            case 'faq':
                break;

            case 'testimonial':
                break;

            // Registration 
            case 'post-data-siswa':

                if ($this->session->userdata('id_student')) {
                    redirect('routing/index/post-data-school-from', 'refresh');
                }

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->registration->set_student();
                }

                $this->compose('page_content', 'reg_student');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'post-data-school-from':

                $step = $this->student->get_per_field($this->session->userdata('id_student'), 'steps');

                if ($step < 1) {
                    redirect('routing/index/post-data-siswa', 'refresh');
                }
                if ($step > 1) {
                    redirect('routing/index/post-data-orang-tua', 'refresh');
                }

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->registration->set_school();
                }

                $this->compose('page_content', 'reg_school_from');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'post-data-orang-tua':

                $step = $this->student->get_per_field($this->session->userdata('id_student'), 'steps');

                if ($step < 2) {
                    redirect('routing/index/post-data-school-from', 'refresh');
                }
                if ($step > 2) {
                    redirect('routing/index/post-data-prestasi-bakat', 'refresh');
                }

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->registration->set_parent();
                }

                $this->compose('page_content', 'reg_parents');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'post-data-prestasi-bakat':

                $step = $this->student->get_per_field($this->session->userdata('id_student'), 'steps');

                if ($step < 3) {
                    redirect('routing/index/post-data-orang-tua', 'refresh');
                }
                if ($step > 3) {
                    redirect('routing/index/data-success', 'refresh');
                }

                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->registration->set_prestasi();
                }
                $this->compose('page_content', 'reg_prestasi_bakat');
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');
                break;

            case 'data-success':

                $step = $this->student->get_per_field($this->session->userdata('id_student'), 'steps');

                if ($step < 4) {
                    redirect('routing/index/post-data-prestasi-bakat', 'refresh');
                }

                $data['student'] = $this->student->get_whole_by($this->session->userdata('id_student'));
                $data['prestasi'] = $this->prestasi->get_by_id($this->session->userdata('id_student'));
                $this->compose('page_content', 'reg_success', $data);
                $this->set_page_siderbar_postlist('page_sidebar');
                $this->set_page_siderbar_eventlist('page_sidebar');

                $this->session->unset_userdata('id_student');

                break;

            // Student, alumni, staff and teacher 
            case 'login-siswa':

                // Authentication
                $this->auth->is_login(set_route('dashboard-siswa'), 'student', 'student_session');

                // Redireting into post people
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->people->student_login();
                }

                // Compose view element
                $this->compose('content', 'people/student/login');


                break;

            case 'logout-siswa':

                // Authentication
                $this->auth->routing_auth(set_route('login-siswa'), 'student', 'student_session');

                // Logout process 
                $this->auth->logout(set_route('login-siswa'), 'student_session');

                break;

            case 'registration-siswa':

                // Redireting into post people
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->people->set_student();
                }

                // Compose view element
                $this->compose('content', 'people/student/registration');


                break;

            case 'dashboard-siswa':

                // Authentication
                $this->auth->routing_auth(set_route('login-siswa'), 'student', 'student_session');

                // Getting user session
                $user_id = $this->auth->get_id('student_session');

                // Getting data
                $data['datasiswa'] = $this->mstudent->get_profile($user_id);

                // Compose view element
                $this->compose('content', 'people/student/dashboard', $data);

                break;

            case 'daftar-siswa':

                // Getting data
                $data['datasiswa'] = $this->mstudent->get_list_student();
                $data['position-title'] = 'Siswa';

                // Compose view element
                $this->compose('content', 'people/student/public_list', $data);

                break;

            case 'daftar-alumni':

                // Getting data
                $data['datasiswa'] = $this->mstudent->get_list_alumni();
                $data['position-title'] = 'Alumni';

                // Compose view element
                $this->compose('content', 'people/student/public_list', $data);

                break;

            case 'detail':

                // Getting data
                $data['dataguru'] = $this->mteacher->get_profile($this->slug);

                // Compose view element
                $this->compose('content', 'people/teacher/public_detail', $data);

                break;

            case 'update-siswa':

                // Authentication
                $this->auth->routing_auth(set_route('login-siswa'), 'student', 'student_session');

                $user_id = $this->auth->get_id('student_session');
                $data['datasiswa'] = $this->mstudent->get_profile($user_id);

                // Redireting into post people
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->people->update_student();
                }

                // Compose view element
                $this->compose('content', 'people/student/update', $data);

                break;

            // Staff and teacher 
            case 'login-guru':

                // Authentication
                $this->auth->is_login(set_route('dashboard-guru'), 'teacher', 'teacher_session');

                // Redireting into post people
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->teacher->login();
                }

                // Compose view element
                $this->compose('content', 'people/teacher/login');

                break;

            case 'logout-guru':

                // Authentication
                $this->auth->routing_auth(set_route('login-guru'), 'teacher', 'teacher_session');

                // Logout process 
                $this->auth->logout(set_route('login-guru'), 'teacher_session');

                break;

            case 'registration-guru':

                // Redireting into post people
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->teacher->insert();
                }

                // Compose view element
                $this->compose('content', 'people/teacher/registration');

                break;

            case 'dashboard-guru':

                // Authentication
                $this->auth->routing_auth(set_route('login-guru'), 'teacher', 'teacher_session');

                // Getting user session
                $user_id = $this->auth->get_id('teacher_session');

                // Getting data
                $data['dataguru'] = $this->mteacher->get_profile($user_id);

                // Compose view element
                $this->compose('content', 'people/teacher/dashboard', $data);

                break;

            case 'daftar-guru':

                // Getting data
                $data['dataguru'] = $this->mteacher->get_list_teacher();
                $data['position-title'] = 'Guru';

                // Compose view element
                $this->compose('content', 'people/teacher/public_list', $data);

                break;

            case 'daftar-karyawan':

                // Getting data
                $data['dataguru'] = $this->mteacher->get_list_staff();
                $data['position-title'] = 'Karyawan';

                // Compose view element
                $this->compose('content', 'people/teacher/public_list', $data);

                break;

            case 'detail':

                // Getting data
                $data['dataguru'] = $this->mteacher->get_profile($this->slug);

                // Compose view element
                $this->compose('content', 'people/teacher/public_detail', $data);

                break;

            case 'update-guru':

                // Authentication
                $this->auth->routing_auth(set_route('login-guru'), 'teacher', 'teacher_session');

                $user_id = $this->auth->get_id('teacher_session');
                $data['dataguru'] = $this->mteacher->get_profile($user_id);

                // Redireting into post people
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->teacher->update();
                }

                // Compose view element
                $this->compose('content', 'people/teacher/update', $data);

                break;

            case 'update-visibility-guru':

                // Authentication
                $this->auth->routing_auth(set_route('login-guru'), 'teacher', 'teacher_session');

                $user_id = $this->auth->get_id('teacher_session');
                $data['dataguru'] = $this->mteacher->get_profile($user_id);

                // Redireting into post people
                if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                    $this->teacher->updateVisibility($data['dataguru']['id']);
                    $data['dataguru'] = $this->mteacher->get_profile($user_id);
                }

                // Compose view element
                $this->compose('content', 'people/teacher/dashboard', $data);

                break;

            default:
                $this->set_slider('content');
                $this->set_banner('content');
                $this->set_latest_post('content');
                $this->set_sidebar_event_list('rightsidebar');
                #$this->set_finder('innersidebar');
                $this->set_video('leftcontent9');
                #$this->set_sidebar_navigation('rightsidebar');
                #$this->set_sidebar_testimonial('rightsidebar');
                $this->set_thumb_list('outsidebar');

                break;
        }

        $this->set_bottom_navigation('bottomside');
        $this->compose('bottomside', 'subscribe');
        $this->set_widget_contact('bottomside');
        $this->set_footer('footer');

        $this->load->view('frontend/master', $this->parsing);

    }

    public function render()
    {
        Pdf::write($this->pdf(), "report.pdf");

    }

    public function pdf($id_student = 0)
    {
        $this->set_meta('header');
        $data['student'] = $this->student->get_whole_by($id_student);
        $data['prestasi'] = $this->prestasi->get_by_id($id_student);
        $data['site_setting'] = $this->msite_setting->get_row();

        $this->parsing['data'] = $data;

        // Get output html
        $html = $this->output->get_output($this->load->view('frontend/pdf', $this->parsing));

        // Load library
        $this->load->library('dompdf_gen');

        // Convert to PDF
        $this->dompdf->load_html($html, true);
        $this->dompdf->render();
        return $this->dompdf->stream("bukti_pendaftaran.pdf");

    }

    public function set_meta($layout_target = '')
    {
        $data['site_setting'] = $this->msite_setting->get_row(1);
        $this->compose($layout_target, 'meta', $data);
        $this->compose($layout_target, 'css', $data);
        $this->compose($layout_target, 'js', $data);

    }

    public function set_head($layout_target = '')
    {
        $data['site_setting'] = $this->msite_setting->get_row();
        $data['social_media'] = $this->msocial_media->get_data();
        $data['contact_setting'] = $this->mcontact_setting->get_row();
        $data['sub_topmenu'] = $this->mmenu->get_data(7);
        $this->compose($layout_target, 'head', $data);

    }

    public function set_top_navigation($layout_target = '')
    {
        $data['top_navigation'] = $this->mmenu->get_data_array(1);
        $data['site_setting'] = $this->msite_setting->get_row();
        $this->compose($layout_target, 'top_navigation', $data);

    }

    public function set_sidebar_event_list($layout_target = '')
    {
        $data['sidebar_event_list'] = $this->mevent->get_data(4);
        $this->compose($layout_target, 'sidebar_event_list', $data);

    }

    public function set_sidebar_navigation($layout_target = '')
    {
        $data['sidebar_navigation'] = $this->mmenu->get_data(5);
        $this->compose($layout_target, 'sidebar_navigation', $data);

    }

    public function set_slider($layout_target = '')
    {
        $data['slider'] = $this->mslider->get_publish(10);
        $this->compose($layout_target, 'slider', $data);

    }

    public function set_banner($layout_target = '')
    {
        $data['banner'] = $this->msite_setting->get_row();
        $this->compose($layout_target, 'banner', $data);

    }

    public function set_latest_post($layout_target = '')
    {
        $data['latest_post'] = $this->mpost->get_row_by_category(15, 9);
        $this->compose($layout_target, 'latest_post', $data);

    }

    public function set_sidebar_testimonial($layout_target = '')
    {
        $data['sidebar_testimonial'] = $this->mcontact->get_data_where('type', 'testimonial');
        $this->compose($layout_target, 'sidebar_testimonial', $data);

    }

    public function set_thumb_list($layout_target = '')
    {
        $data['thumb_list'] = $this->mmenu->get_data(8);
        $this->compose($layout_target, 'thumb_list', $data);

    }

    public function set_bottom_navigation($layout_target = '')
    {
        $data['btm_nav_category'] = $this->mmenu_category->get_row(9);
        $data['bottom_nav'] = $this->mmenu->get_data($data['btm_nav_category']['id']);
        $this->compose($layout_target, 'bottom_nav', $data);

    }

    public function set_widget_contact($layout_target = '')
    {
        $data['widget_contact'] = $this->mcontact_setting->get_row();
        $this->compose($layout_target, 'widget_contact', $data);

    }

    public function set_footer($layout_target = '')
    {
        $data['footer'] = $this->msocial_media->get_data();
        $this->compose($layout_target, 'footer', $data);

    }

    public function set_finder($layout_target = '')
    {
        $data['category'] = $this->mpost_category->get_data();
        $this->compose($layout_target, 'finder', $data);

    }

    public function set_video($layout_target = '')
    {
        $data['video_meta'] = $this->malbum->get_row(10);
        $data['video'] = $this->mgallery->get_data(10);
        $this->compose($layout_target, 'video', $data);

    }

    /* post */
    /* ----------------------------------------------------------------------------------------------------------------------------------------- */

    public function set_page_eventlist($layout_target = '')
    {
        $data['title'] = 'Daftar Agenda Yang Akan Dilaksanakan';
        $data['event_list'] = $this->mevent->get_publish($this->limit, $this->slug);

        $path = '/index.php/routing/index/' . $this->page;
        paging($path, $this->mevent->get_count('publish'), $this->limit, 4);

        $this->compose('page_header', 'page_header', $data['title']);
        $this->compose($layout_target, 'list_event', $data);

    }

    public function set_page_teamlist($layout_target = '')
    {

        if ($this->slug == 'teacher') {
            $data['title'] = "Daftar Guru";
        } else if ($this->slug == 'staf') {
            $data['title'] = "Daftar Karyawan";
        } else if ($this->slug == 'siswa') {
            $data['title'] = "Daftar Siswa";
        } else if ($this->slug == 'alumni') {
            $data['title'] = "Daftar Alumni";
        }

        $data['list_team'] = $this->muser->get_all($this->slug, $this->limit, $this->index);

        $path = '/index.php/routing/index/' . $this->page . '/' . $this->slug;
        paging($path, $this->muser->get_bylevel_count($this->slug), $this->limit, 5);

        $this->compose('page_header', 'page_header', $data['title']);
        $this->compose($layout_target, 'list_team', $data);

    }

    public function set_page_postlist($layout_target = '')
    {
        $data['title'] = $this->mpost_category->get_row_byslug($this->slug)['post_category'];
        $data['post_list'] = $this->mpost->get_byslug_percategory($this->slug, $this->limit, $this->index);

        $path = '/index.php/routing/index/' . $this->page . '/' . $this->slug;
        paging($path, $this->mpost->get_count($this->slug), $this->limit, 5);

        $this->compose('page_header', 'page_header', $data['title']);
        $this->compose($layout_target, 'list_post', $data);

    }

    public function set_page_detail_gallery($layout_target = '')
    {
        $data['detail_gallery'] = $this->mgallery->get_data($this->slug);
        $data['title'] = $this->malbum->get_row($this->slug)['title'];

        $this->compose('page_header', 'page_header', $data['title']);
        $this->compose($layout_target, 'detail_gallery', $data);

    }

    public function set_page_list_gallery($layout_target = '')
    {

        $data['title'] = "Galeri SMK Negeri 1 Sanden";
        $data['gallery_list'] = $this->malbum->get_data($this->limit, $this->slug);

        $path = '/index.php/routing/index/' . $this->page;
        paging($path, $this->malbum->get_count(), $this->limit, 4);

        $this->compose('page_header', 'page_header', $data['title']);
        $this->compose($layout_target, 'list_gallery', $data);

    }

    public function set_page_detail_event($layout_target = '')
    {
        $data['detail_event'] = $this->mevent->get_byslug($this->slug);
        $data['title'] = $data['detail_event']['title'];
        $this->compose('page_header', 'page_header', $data['title']);
        $this->compose($layout_target, 'detail_event', $data);

    }

    public function set_page_detail_post($layout_target = '')
    {
        $data['post'] = $this->mpost->get_row_byslug($this->slug);
        $data['title'] = $data['post']['title'];
        $this->compose('page_header', 'page_header', $data['title']);
        $this->compose($layout_target, 'page_post', $data);

    }

    public function set_page_contact($layout_target = '')
    {
        $data['contact'] = $this->mcontact_setting->get_row();
        $data['title'] = $this->mpost_category->get_row($data['contact']['post_category'])['post_category'];
        $data['meta'] = $this->mpost->get_row($data['contact']['post_at_home']);
        $this->compose('page_header', 'page_header', $data['title']);
        $this->compose('page_bottom', 'map', $data);
        $this->compose($layout_target, 'contact', $data);

    }

    public function set_page_siderbar_postlist($layout_target = '')
    {
        $data['latest_post'] = $this->mpost->get_row_by_category(15, 5);
        $this->compose($layout_target, 'page_sidebar_postlist', $data);

    }

    public function set_page_siderbar_eventlist($layout_target = '')
    {
        $data['event_list'] = $this->mevent->get_data(4);
        $this->compose($layout_target, 'page_sidebar_eventlist', $data);

    }

    /* Registration */
    /* ----------------------------------------------------------------------------------------------------------------------------------------- */

    public function set_reg_parents($layout_target)
    {
        $this->parsing[$layout_target]['registration']['path'] = 'reg_parents';

    }

    public function set_reg_school_from($layout_target)
    {
        $this->parsing[$layout_target]['registration']['path'] = 'reg_school_from';

    }

    public function set_reg_raport($layout_target)
    {
        $this->parsing[$layout_target]['registration']['path'] = 'reg_raport';

    }

    public function set_reg_school_to($layout_target)
    {
        $this->parsing[$layout_target]['registration']['path'] = 'reg_school_to';

    }

    public function set_register_success($layout_target)
    {

        /* Load Model  */
        $this->load->model('registration/student');
        $this->load->model('registration/parents');
        $this->load->model('registration/school_from');
        $this->load->model('registration/raport');

        $data['student'] = $this->student->get_byId($this->slug);
        $data['parents'] = $this->parents->get_byId($this->slug);
        $data['school_from'] = $this->school_from->get_byId($this->slug);
        $data['raport'] = $this->raport->get_byId($this->slug);

        $this->parsing[$layout_target]['register_success']['data'] = $data;
        $this->parsing[$layout_target]['register_success']['path'] = 'register_success';

    }

    /* Asset Function */

    private function compose($position = '', $layout = '', $data = array())
    {
        $key = random_string();
        if (!empty($data)) {
            $this->parsing[$position][$key]['data'] = $data;
        }
        $this->parsing[$position][$key]['path'] = $layout;

    }

}

?>
