<?php

class Cmenu_category extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Menu Category Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Menu Category  Management';
        $this->parsing['data']['sub-header'] = '( control menu category activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mmenu_category');

    }

    public function index($id = 0)
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Menu Category';

        $this->parsing['data']['file'] = 'backend/content/menu_category/index';
        $this->parsing['data']['add-link'] = 'index.php/backend/cmenu_category';
        $this->parsing['data']['add-title'] = 'Add New Menu Category';

        $this->parsing['data']['content'] = $this->mmenu_category->get_data();

        if ($id != 0)
            $this->parsing['data']['content_edit'] = $this->mmenu_category->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function storage()
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title Navigation Category',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cmenu_category', 'refresh');
        } else {

            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');

            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            /* Image upload */
            if ($_FILES['userfile']['name'] != '')
                $data['image'] = $this->upload_file();

            $this->mmenu_category->insert_data($data);

            set_alert('alert-info', "{$data['title']} is success created ");
            redirect('backend/cmenu_category', 'refresh');
        }

    }

    public function update($id = '')
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title Navigation Category',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cmenu_category/index/' . $id, 'refresh');
        } else {

            $data['id'] = $id;
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['updated_at'] = date('Y-m-d H:i:s');

            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($this->mmenu_category->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mmenu_category->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mmenu_category->get_old_filename($id));
                    
                }
                
                $data['image'] = $this->upload_file();
            }

            /* If image delted */
            if ($this->input->post('delete_image')) {
                
                if ($this->mmenu_category->get_old_filename($id) != '') {
                
                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mmenu_category->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mmenu_category->get_old_filename($id));
                    
                }
                
                $data['image'] = '';
                
            }

            $this->mmenu_category->update_data($data);

            set_alert('alert-info', "{$data['title']} is success updated");
            redirect('backend/cmenu_category', 'refresh');
        }

    }

    public function destroy($id = '', $status = 'false')
    {

        $pointer = $this->mmenu_category->get_per_field($id, 'title');

        if ($status == 'false') {

            $btn = array(
                "url" => "cmenu_category/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted data $pointer? <br>", $btn);

            redirect('backend/cmenu_category', 'refresh');
        } else {

            $this->mmenu_category->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cmenu_category', 'refresh');
        }

    }

    public function upload_file()
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/cmenu_category', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }

    }

}

?>
