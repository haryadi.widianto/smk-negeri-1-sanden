<?php

class Cslider extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Image Slider Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Image Slider Management';
        $this->parsing['data']['sub-header'] = '( control image slider activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mslider');
        $this->load->model('mpost');
        $this->load->model('mpost_category');
        $this->load->model('mmenu_category');

    }

    public function index($limit = 0, $index = 0)
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Slider';

        $this->parsing['data']['file'] = 'backend/content/slider/index';
        $this->parsing['data']['add-link'] = 'cslider/add';
        $this->parsing['data']['add-title'] = 'Add New Image Slider';

        $this->parsing['data']['content'] = $this->mslider->get_data();

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function add()
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'New Image Slider Formulir';
        $this->parsing['data']['save-link'] = 'backend/cslider/storage';
        $this->parsing['data']['save-title'] = 'Save New Image Slider';
        $this->parsing['data']['file'] = 'backend/content/slider/add';
        $this->parsing['data']['content'] = '';

        $this->parsing['data']['option_post'] = $this->mpost->get_data();
        $this->parsing['data']['option_post_category'] = $this->mpost_category->get_data();
        $this->parsing['data']['option_menu_category'] = $this->mmenu_category->get_data();

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function storage()
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cslider/add', 'refresh');
        } else {

            $data['title'] = $this->input->post('title');
            $data['sub_title'] = $this->input->post('sub_title');
            $data['status'] = $this->input->post('status');
            $data['description'] = $this->input->post('description');
            $data['data_source'] = $this->input->post('data_source') == ''? NULL : $this->input->post('data_source');
            
            if($data['data_source'] != NULL)
                $data['data'] = $this->input->post($data['data_source']);
            
            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            /* Image upload */
            if ($_FILES['userfile']['name'] != '')
                $data['image'] = $this->upload_file();

            $this->mslider->insert_data($data);

            set_alert('alert-info', "{$data['title']} is success created ");
            redirect('backend/cslider', 'refresh');
        }

    }

    public function edit($id = '')
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Edit Image Slider Formulir';
        $this->parsing['data']['save-link'] = 'backend/cslider/update/' . $id;
        $this->parsing['data']['save-title'] = 'Save Image Slider Edited';
        $this->parsing['data']['file'] = 'backend/content/slider/edit';

        /* Option */
        $this->parsing['data']['option_post'] = $this->mpost->get_data();
        $this->parsing['data']['option_post_category'] = $this->mpost_category->get_data();
        $this->parsing['data']['option_menu_category'] = $this->mmenu_category->get_data();

        $this->parsing['data']['content'] = $this->mslider->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function update($id = '')
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cslider/edit/' . $id, 'refresh');
        } else {

            $data['id'] = $id;
            $data['title'] = $this->input->post('title');
            $data['sub_title'] = $this->input->post('sub_title');
            $data['status'] = $this->input->post('status');
            $data['description'] = $this->input->post('description');
            $data['data_source'] = $this->input->post('data_source') == ''? NULL : $this->input->post('data_source');
            
            if($data['data_source'] != NULL)
                $data['data'] = $this->input->post($data['data_source']);

            $data['updated_at'] = date('Y-m-d H:i:s');

            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($this->mslider->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mslider->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mslider->get_old_filename($id));
                }

                $data['image'] = $this->upload_file();
            }

            /* If image delted */
            if ($this->input->post('delete_image')) {

                if ($this->mslider->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mslider->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mslider->get_old_filename($id));
                }

                $data['image'] = '';
            }

            $this->mslider->update_data($data);

            set_alert('alert-info', "{$data['title']} is success updated");
            redirect('backend/cslider', 'refresh');
        }

    }

    public function destroy($id = '', $status = 'false')
    {

        $pointer = $this->mslider->get_per_field($id, 'title');

        if ($status == 'false') {

            $btn = array(
                "url" => "cslider/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted data $pointer? <br>", $btn);

            redirect('backend/cslider', 'refresh');
        } else {

            $this->mslider->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cslider', 'refresh');
        }

    }

    public function upload_file()
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/cslider/add', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }

    }

}

?>
