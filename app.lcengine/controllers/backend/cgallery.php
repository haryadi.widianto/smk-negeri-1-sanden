<?php

class Cgallery extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Gallery Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Gallery Management';
        $this->parsing['data']['sub-header'] = '( control gallery activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mgallery');
        $this->load->model('malbum');

    }

    public function index($segment = 0, $id = 0)
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Gallery';

        $this->parsing['data']['file'] = 'backend/content/gallery/index';
        $this->parsing['data']['add-link'] = 'index.php/backend/cgallery';
        $this->parsing['data']['add-title'] = 'Back to Album';

        $album = $this->malbum->get_data();
        foreach ($album as $key => $item) {
            $this->parsing['data']['tab_' . $item->id] = $this->mgallery->get_data($item->id);
        }
        $this->parsing['data']['album'] = $album;

        $this->parsing['data']['album'] = $this->malbum->get_data();
        $this->parsing['data']['content'] = $this->mgallery->get_data();

        if ($segment != 0)
            $this->parsing['data']['content_edit'] = $this->mgallery->get_row($segment);
        if ($segment == 'album' && $id != 0)
            $this->parsing['data']['edit_album'] = $this->malbum->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function storage()
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Image Title',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cgallery', 'refresh');
        } else {

            $data['title'] = $this->input->post('title');
            $data['type'] = $this->input->post('type');
            $data['description'] = $this->input->post('description');
            $data['id_album'] = $this->input->post('id_album');

            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            /* Image upload */
            if ($data['type'] == 'image') {
                if ($_FILES['userfile']['name'] != '') {
                    $data['image'] = $this->upload_file();
                }
            } else if ($data['type'] == 'video') {
                $data['image'] = $this->input->post('video');
            }

            $this->mgallery->insert_data($data);

            set_alert('alert-info', "{$data['title']} is success created ");
            redirect('backend/cgallery', 'refresh');
        }

    }

    public function update($id = '')
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Image Title',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cgallery/index/' . $id, 'refresh');
        } else {

            $data['id'] = $id;
            $data['title'] = $this->input->post('title');
            $data['type'] = $this->input->post('type');
            $data['description'] = $this->input->post('description');
            $data['id_album'] = $this->input->post('id_album');

            $data['updated_at'] = date('Y-m-d H:i:s');

            if ($data['type'] == 'image') {
                if ($_FILES['userfile']['name'] != '') {

                    /* Deleting old file */
                    if ($this->mgallery->get_old_filename($id) != '') {

                        // Remove image when it is exist
                        if (file_exists("./pub.lcengine/upload/media/" . $this->mgallery->get_old_filename($id)))
                            unlink("./pub.lcengine/upload/media/" . $this->mgallery->get_old_filename($id));
                    }

                    $data['image'] = $this->upload_file();
                }

                /* If image delted */
                if ($this->input->post('delete_image')) {

                    if ($this->mgallery->get_old_filename($id) != '') {

                        // Remove image when it is exist
                        if (file_exists("./pub.lcengine/upload/media/" . $this->mgallery->get_old_filename($id)))
                            unlink("./pub.lcengine/upload/media/" . $this->mgallery->get_old_filename($id));
                    }

                    $data['image'] = '';
                }
            } else if ($data['type'] == 'video') {

                $data['image'] = $this->input->post('video');
            }

            $this->mgallery->update_data($data);

            set_alert('alert-info', "{$data['title']} is success updated");
            redirect('backend/cgallery', 'refresh');
        }

    }

    public function destroy($id = '', $status = 'false')
    {

        $pointer = $this->mgallery->get_per_field($id, 'title');

        if ($status == 'false') {

            $btn = array(
                "url" => "cgallery/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted data $pointer? <br>", $btn);

            redirect('backend/cgallery', 'refresh');
        } else {

            $this->mgallery->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cgallery', 'refresh');
        }

    }

    public function upload_file()
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '9000';
        $config['max_width'] = '9000';
        $config['max_height'] = '9000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/cgallery', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }

    }

}

?>
