<?php

class Cpost_category extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Post Category Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Post Category Management';
        $this->parsing['data']['sub-header'] = '( control post category activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mpost_category');
    }

    public function index($limit = 0, $index = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Post Category';

        $this->parsing['data']['file'] = 'backend/content/post_category/index';
        $this->parsing['data']['add-link'] = 'cpost_category/add';
        $this->parsing['data']['add-title'] = 'Add New Post Category';

        $this->parsing['data']['content'] = $this->mpost_category->get_data();

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function add() {

        /* Box title */
        $this->parsing['data']['box-title'] = 'New Post Category Formulir';
        $this->parsing['data']['save-link'] = 'backend/cpost_category/storage';
        $this->parsing['data']['save-title'] = 'Save New Post Category';
        $this->parsing['data']['file'] = 'backend/content/post_category/add';
        $this->parsing['data']['content'] = '';

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function storage() {

        $config = array(
            array(
                'field' => 'post_category',
                'label' => 'Post Category',
                'rules' => 'required|max_length[150]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cpost_category/add', 'refresh');
            
        } else {

            $data['post_category'] = $this->input->post('post_category');
            $data['slug'] = set_slug($this->input->post('post_category'));
            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            $this->mpost_category->insert_data($data);
            set_alert('alert-info', "{$data['post_category']} is success created ");
            redirect('backend/cpost_category', 'refresh');
            
        }
    }

    public function edit($id = '') {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Edit Post Category Formulir';
        $this->parsing['data']['save-link'] = 'backend/cpost_category/update/' . $id;
        $this->parsing['data']['save-title'] = 'Save Post Category Edited';
        $this->parsing['data']['file'] = 'backend/content/post_category/edit';
        $this->parsing['data']['content'] = $this->mpost_category->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function update($id = '') {

        $config = array(
            array(
                'field' => 'post_category',
                'label' => 'Post Category',
                'rules' => 'required|max_length[150]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            
            set_alert('alert-danger', validation_errors());
            redirect('backend/cpost_category/edit/' . $id, 'refresh');
            
        } else {

            $data['id_post_category'] = $id;
            $data['post_category'] = $this->input->post('post_category');
            #$data['slug'] = set_slug($this->input->post('post_category'));
            $data['updated_at'] = date('Y-m-d H:i:s');

            $this->mpost_category->update_data($data);
            set_alert('alert-info', "{$data['post_category']} is success updated");
            redirect('backend/cpost_category', 'refresh');
            
        }
    }

    public function destroy($id = '', $status = 'false') {

        $pointer = $this->mpost_category->get_per_field($id, 'post_category');

        if ($status == 'false') {

            $btn = array(
                "url" => "cpost_category/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted category $pointer? <br>", $btn);

            redirect('backend/cpost_category', 'refresh');
            
        } else {

            $this->mpost_category->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cpost_category', 'refresh');
        }
    }

}

?>
