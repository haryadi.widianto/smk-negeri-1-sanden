<?php

class Csite_setting extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        $this->parsing['data']['title'] = 'Site Setting';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Site Setting';
        $this->parsing['data']['sub-header'] = '( control site configuration in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        $this->load->model('msite_setting');
        $this->load->model('mpost');

    }

    public function edit($id = '')
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Site Setting';
        $this->parsing['data']['save-link'] = 'backend/csite_setting/update/' . $id;
        $this->parsing['data']['save-title'] = 'Save Site Setting';
        $this->parsing['data']['file'] = 'backend/content/site_setting/edit';
        $this->parsing['data']['option'] = $this->mpost->get_publish();

        $this->parsing['data']['content'] = $this->msite_setting->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function update($id = '')
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/csite_setting/edit/' . $id, 'refresh');
        } else {

            $data['id'] = $id;
            $data['title'] = $this->input->post('title');
            $data['keyword'] = $this->input->post('keyword');
            $data['description'] = $this->input->post('description');
            $data['street'] = $this->input->post('street');
            $data['top_quotes'] = $this->input->post('top_quotes');
            $data['foreword'] = $this->input->post('foreword');
            $data['preface'] = $this->input->post('preface');
            $data['banner_link'] = $this->input->post('banner_link');
            $data['banner_link_title'] = $this->input->post('banner_link_title');
            $data['banner_title'] = $this->input->post('banner_title');
            $data['banner_description'] = $this->input->post('banner_description');
            $data['bottom_quotes'] = $this->input->post('bottom_quotes');

            /* Logo */
            if ($_FILES['logo']['name'] != '') {

                /* Deleting old file */
                if ($this->msite_setting->get_old_filename($id, 'logo') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'logo')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'logo'));
                }
                $data['logo'] = $this->upload_file('logo');
            }

            /* If logo deleted */
            if ($this->input->post('delete_logo')) {

                if ($this->msite_setting->get_old_filename($id, 'logo') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'logo')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'logo'));
                }

                $data['logo'] = '';
            }


            /* Favicon */
            if ($_FILES['favicon']['name'] != '') {

                /* Deleting old file */
                if ($this->msite_setting->get_old_filename($id, 'favicon') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'favicon')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'favicon'));
                }

                $data['favicon'] = $this->upload_file('favicon');
            }

            /* If favicon delted */
            if ($this->input->post('delete_favicon')) {

                if ($this->msite_setting->get_old_filename($id, 'favicon') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'favicon')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'favicon'));
                }

                $data['favicon'] = '';
            }

            /* Banner */
            if ($_FILES['banner']['name'] != '') {

                /* Deleting old file */
                if ($this->msite_setting->get_old_filename($id, 'banner') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'banner')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'banner'));
                }

                $data['banner'] = $this->upload_file('banner');
            }

            /* If banner deleted */
            if ($this->input->post('delete_banner')) {

                if ($this->msite_setting->get_old_filename($id, 'banner') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'banner')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'banner'));
                }

                $data['banner'] = '';
            }

            /* Footer Logo */
            if ($_FILES['footer_logo']['name'] != '') {

                /* Deleting old file */
                if ($this->msite_setting->get_old_filename($id, 'footer_logo') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'footer_logo')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'footer_logo'));
                }

                $data['footer_logo'] = $this->upload_file('footer_logo');
            }

            /* If banner deleted */
            if ($this->input->post('delete_footer_logo')) {

                if ($this->msite_setting->get_old_filename($id, 'footer_logo') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'footer_logo')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'footer_logo'));
                }
                $data['footer_logo'] = '';
            }


            /* Footer Background */
            if ($_FILES['footer_background']['name'] != '') {

                /* Deleting old file */
                if ($this->msite_setting->get_old_filename($id, 'footer_background') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'footer_background')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'footer_background'));
                }

                $data['footer_background'] = $this->upload_file('footer_background');
            }

            /* If banner deleted */
            if ($this->input->post('delete_footer_background')) {

                if ($this->msite_setting->get_old_filename($id, 'footer_background') != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'footer_background')))
                        unlink("./pub.lcengine/upload/media/" . $this->msite_setting->get_old_filename($id, 'footer_background'));
                }

                $data['footer_background'] = '';
            }

            $this->msite_setting->update_data($data);

            set_alert('alert-info', "Site setting is success updated");
            redirect('backend/csite_setting/edit/' . $id, 'refresh');
        }

    }

    public function upload_file($file_name = '')
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file_name)) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/csite_setting/edit/1', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }

    }

}

?>
