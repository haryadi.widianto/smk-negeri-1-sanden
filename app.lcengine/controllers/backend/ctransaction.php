<?php

class Ctransaction extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Transaction Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Transaction Management';
        $this->parsing['data']['sub-header'] = '( control transaction activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('frontend/msession');
        $this->load->model('frontend/mchart');
    }

    public function index($limit = 0, $index = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Transaction';

        $this->parsing['data']['file'] = 'backend/content/transaction/index';
        $this->parsing['data']['add-link'] = 'ctransaction/add';
        $this->parsing['data']['add-title'] = 'Add New Transaction';

        $this->parsing['data']['content'] = $this->msession->get_transaction_history_all();

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function detail($id = '') {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Detail Chart';
        $this->parsing['data']['save-link'] = 'backend/ctransaction/update/' . $id;
        $this->parsing['data']['save-title'] = '';
        $this->parsing['data']['file'] = 'backend/content/transaction/detail';

        $this->parsing['data']['content'] = $this->mchart->get_chart($id);
        $this->parsing['data']['sub-content'] = $this->msession->get_session_row($id);

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function deliver($id = '') {

        $check_submit = $this->msession->get_session_row($id);

        if ($check_submit['submit_date'] != NULL) {

            $data['id_session'] = $id;
            $data['deliver_date'] = date('Y-m-d H:i:s');

            $this->msession->update($data);

            set_alert('alert-info', "The chart is success delivered");
            redirect('backend/ctransaction/detail/' . $id, 'refresh');
        } else {

            set_alert('alert-danger', "The chart is not yet submit, cannot deliver");
            redirect('backend/ctransaction/detail/' . $id, 'refresh');
        }
    }

    public function destroy($id = '', $status = 'false') {

        if ($status == 'false') {

            $btn = array(
                "url" => "ctransaction/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted this data? <br>", $btn);

            redirect('backend/ctransaction', 'refresh');
            
        } else {

            $this->msession->delete($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/ctransaction', 'refresh');
        }
    }
    
    public function report() {
        
        $result = $this->msession->get_by_date($this->input->post('start_date'), $this->input->post('end_date'));
        /* Box title */
        $this->parsing['data']['box-title'] = 'Detail Chart';
        $this->parsing['data']['save-link'] = 'backend/ctransaction/update/' ;
        $this->parsing['data']['save-title'] = '';
        $this->parsing['data']['file'] = 'backend/content/transaction/report';
        
        $this->parsing['data']['content'] = $this->msession->get_by_date($this->input->post('start_date'), $this->input->post('end_date')); 
        $this->parsing['data']['date'] = date('j F Y', strtotime($this->input->post('start_date'))) .' s/d '.date('j F Y', strtotime($this->input->post('end_date')));  

        $this->load->view('backend/master/main', $this->parsing);
        
    }

    public function upload_file($path = 'add') {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect("backend/cpost/" . $path, 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }
    }

}

?>
