<?php

class Calbum extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Gallery Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Gallery Management';
        $this->parsing['data']['sub-header'] = '( control gallery activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('malbum');
        
    }
    
    public function storage() {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Album Title',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cgallery', 'refresh');
            
        } else {

            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['set_at_home'] = $this->input->post('set_at_home');
            
            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');
            
            $this->malbum->insert_data($data);
            
            set_alert('alert-info', "Album <strong>{$data['title']}</strong> is success created ");
            redirect('backend/cgallery', 'refresh');
            
        }
    }


    public function update($id = '') {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Alhum Title',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            
            set_alert('alert-danger', validation_errors());
            redirect('backend/cgallery/index/' . $id, 'refresh');
            
        } else {

            $data['id'] = $id;
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['set_at_home'] = $this->input->post('set_at_home');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $this->malbum->update_data($data);
            
            set_alert('alert-info', "Album {$data['title']} is success updated");
            redirect('backend/cgallery', 'refresh');
            
        }
    }

    public function destroy($id = '', $status = 'false') {

        $pointer = $this->malbum->get_per_field($id, 'title');

        if ($status == 'false') {

            $btn = array(
                "url" => "calbum/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted data $pointer? <br>", $btn);

            redirect('backend/cgallery', 'refresh');
            
        } else {

            $this->malbum->delete_data($id);

            set_alert('alert-info', "Album $pointer is success deleted");

            redirect('backend/cgallery', 'refresh');
        }
    }
    

}

?>
