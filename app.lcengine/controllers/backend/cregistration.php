<?php

class Cregistration extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Registration Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Registration Management';
        $this->parsing['data']['sub-header'] = '( control registration activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('registration/parents');
        $this->load->model('registration/raport');
        $this->load->model('registration/school_from');
        $this->load->model('registration/student');
        $this->load->model('mregistration');
        
    }

    public function index($limit = 0, $index = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Registration';

        $this->parsing['data']['file'] = 'backend/content/registration/index';
        $this->parsing['data']['add-link'] = 'cregistration/add';
        $this->parsing['data']['add-title'] = 'Add New Post';

        $this->parsing['data']['content'] = $this->student->get_whole();

        $this->load->view('backend/master/main', $this->parsing);
        
    }


    public function view($id = '') {

        /* Box title */
        $this->parsing['data']['box-title'] = 'View Detail Register';
        $this->parsing['data']['save-link'] = 'backend/cregistration';
        $this->parsing['data']['save-title'] = 'Back to List';
        $this->parsing['data']['file'] = 'backend/content/registration/edit';
        
        $this->parsing['data']['content'] = $this->student->get_whole_by($id);

        $this->load->view('backend/master/main', $this->parsing);
        
    }

    public function destroy($id = '', $status = 'false') {

        $pointer = $this->student->get_per_field($id, 'name');

        if ($status == 'false') {

            $btn = array(
                "url" => "cregistration/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted user with username $pointer? <br>", $btn);

            redirect('backend/cregistration', 'refresh');
            
        } else {

            $this->student->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cregistration', 'refresh');
        }
    }
    
}

?>
