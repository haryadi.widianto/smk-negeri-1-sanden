<?php

class Cuser_profile extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'User Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'User Management';
        $this->parsing['data']['sub-header'] = '( control user activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('muser');
        $this->load->model('muser_profile');

    }

    public function view($id = '')
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'View of User Detail';
        $this->parsing['data']['save-link'] = 'backend/cuser/update_detail/' . $id;
        $this->parsing['data']['save-title'] = 'Edit User Profile';
        $this->parsing['data']['file'] = 'backend/content/user_profile/view';
        $this->parsing['data']['id'] = $id;

        $check = $this->muser_profile->user_profile_exist($id);

        if ($check > 0) {
            $this->parsing['data']['content'] = $this->muser_profile->get_row($id);
        } else {
            $this->parsing['data']['content'] = '';
        }

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function edit($id = '')
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Edit of User Detail';
        $this->parsing['data']['save-link'] = 'backend/cuser_profile/update/' . $id;
        $this->parsing['data']['save-title'] = 'Update User Profile';
        $this->parsing['data']['file'] = 'backend/content/user_profile/edit';
        $this->parsing['data']['id'] = $id;

        $check = $this->muser_profile->user_profile_exist($id);

        if ($check > 0) {
            $this->parsing['data']['content'] = $this->muser_profile->get_row($id);
        } else {
            $this->parsing['data']['content'] = '';
        }

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function update($id = '')
    {

        $data['id_user'] = $id;
        $data['gender'] = $this->input->post('gender');
        $data['position'] = $this->input->post('position');
        $data['resume'] = $this->input->post('resume');

        $data['facebook'] = $this->input->post('facebook');
        $data['twitter'] = $this->input->post('twitter');
        $data['gplus'] = $this->input->post('gplus');
        $data['linkedin'] = $this->input->post('linkedin');

        $data['address'] = $this->input->post('address');
        $data['telp'] = $this->input->post('telp');
        $data['post_code'] = $this->input->post('post_code');

        if ($this->muser_profile->user_profile_exist($id) > 0) {

            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($this->muser_profile->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->muser_profile->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->muser_profile->get_old_filename($id));
                }

                $data['foto'] = $this->save_photo_profile($id);
            }

            /* If image delted */
            if ($this->input->post('delete_photo')) {

                if ($this->muser_profile->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->muser_profile->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->muser_profile->get_old_filename($id));
                }

                $data['foto'] = '';
            }

            $this->muser_profile->update_data($data);
        } else {

            if ($_FILES['userfile']['name'] != '') {
                $data['foto'] = $this->save_photo_profile($id);
            }

            /* If image deleted */
            if ($this->input->post('delete_photo')) {

                if ($this->muser_profile->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->muser_profile->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->muser_profile->get_old_filename($id));
                }

                $data['foto'] = '';
            }

            $this->muser_profile->insert_data($data);
        }

        set_alert('alert-info', "User with name {$this->muser->get_per_field($id, 'name')} is success updated");
        redirect('backend/cuser', 'refresh');

    }

    public function save_photo_profile($id = '')
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/cuser_profile/edit/' . $id, 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }

    }

}

?>
