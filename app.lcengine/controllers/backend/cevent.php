<?php

class Cevent extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Event Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Event Management';
        $this->parsing['data']['sub-header'] = '( control event activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mevent');

    }

    public function index($limit = 0, $index = 0)
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Event';

        $this->parsing['data']['file'] = 'backend/content/event/index';
        $this->parsing['data']['add-link'] = 'cevent/add';
        $this->parsing['data']['add-title'] = 'Add New Event';

        $this->parsing['data']['content'] = $this->mevent->get_data();

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function add()
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'New Event Formulir';
        $this->parsing['data']['save-link'] = 'backend/cevent/storage';
        $this->parsing['data']['save-title'] = 'Save New Event';
        $this->parsing['data']['file'] = 'backend/content/event/add';
        $this->parsing['data']['content'] = '';

        $this->parsing['data']['option'] = $this->mevent->get_data();

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function storage()
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required|max_length[150]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cevent/add', 'refresh');
        } else {

            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['status'] = $this->input->post('status');
            $data['tag'] = $this->input->post('tag');
            $data['location'] = $this->input->post('location');
            $data['day_start'] = $this->input->post('day_start');
            $data['day_end'] = $this->input->post('day_end');
            $data['time_start'] = $this->input->post('time_start');
            $data['time_end'] = $this->input->post('time_end');
            $data['slug'] = set_slug($this->input->post('title'));
            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            /* Image upload */
            if ($_FILES['userfile']['name'] != '')
                $data['image'] = $this->upload_file();

            $this->mevent->insert_data($data);

            set_alert('alert-info', "{$data['title']} is success created ");
            redirect('backend/cevent', 'refresh');
        }

    }

    public function edit($id = '')
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Edit Event Formulir';
        $this->parsing['data']['save-link'] = 'backend/cevent/update/' . $id;
        $this->parsing['data']['save-title'] = 'Save Event Edited';
        $this->parsing['data']['file'] = 'backend/content/event/edit';

        $this->parsing['data']['content'] = $this->mevent->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function update($id = '')
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required|max_length[200]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cevent/edit/' . $id, 'refresh');
        } else {

            $data['id'] = $id;
            $data['title'] = $this->input->post('title');
            $data['location'] = $this->input->post('location');
            $data['status'] = $this->input->post('status');
            $data['tag'] = $this->input->post('tag');
            $data['description'] = $this->input->post('description');
            $data['slug'] = set_slug($this->input->post('title'));
            $data['day_start'] = $this->input->post('day_start');
            $data['day_end'] = $this->input->post('day_end');
            $data['time_start'] = $this->input->post('time_start');
            $data['time_end'] = $this->input->post('time_end');

            $data['updated_at'] = date('Y-m-d H:i:s');

            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($this->mevent->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mevent->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mevent->get_old_filename($id));
                }

                $data['image'] = $this->upload_file('edit/' . $id);
            }

            /* If image delted */
            if ($this->input->post('delete_image')) {

                if ($this->mevent->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mevent->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mevent->get_old_filename($id));
                }

                $data['image'] = '';
            }

            $this->mevent->update_data($data);

            set_alert('alert-info', "{$data['title']} is success updated");
            redirect('backend/cevent', 'refresh');
        }

    }

    public function destroy($id = '', $status = 'false')
    {

        $pointer = $this->mevent->get_per_field($id, 'title');

        if ($status == 'false') {

            $btn = array(
                "url" => "cevent/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted user with name $pointer? <br>", $btn);

            redirect('backend/cevent', 'refresh');
        } else {

            $this->mevent->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cevent', 'refresh');
        }

    }

    public function upload_file($path = 'add')
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '7000';
        $config['max_width'] = '7000';
        $config['max_height'] = '7000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect("backend/cpost/" . $path, 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }

    }

}

?>
