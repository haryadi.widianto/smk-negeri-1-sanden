<?php

class Cpayment_port extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Payment Port Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Payment Port Management';
        $this->parsing['data']['sub-header'] = '( control payment port activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mpayment_port');

    }

    public function index($id = 0)
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Payment Port';

        $this->parsing['data']['file'] = 'backend/content/payment_port/index';
        $this->parsing['data']['add-link'] = 'index.php/backend/cpayment_port';
        $this->parsing['data']['add-title'] = 'Add New Payment Port';

        $this->parsing['data']['content'] = $this->mpayment_port->get_data();

        if ($id != 0)
            $this->parsing['data']['content_edit'] = $this->mpayment_port->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function storage()
    {

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cpayment_port', 'refresh');
        } else {

            $data['name'] = $this->input->post('name');
            $data['no_rekening'] = $this->input->post('no_rekening');

            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            /* Image upload */
            if ($_FILES['userfile']['name'] != '')
                $data['image'] = $this->upload_file();

            $this->mpayment_port->insert_data($data);

            set_alert('alert-info', "{$data['name']} is success created ");
            redirect('backend/cpayment_port', 'refresh');
        }

    }

    public function update($id = '')
    {

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cpayment_port/index/' . $id, 'refresh');
        } else {

            $data['id'] = $id;
            $data['name'] = $this->input->post('name');
            $data['no_rekening'] = $this->input->post('no_rekening');
            $data['updated_at'] = date('Y-m-d H:i:s');

            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($this->mpayment_port->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mpayment_port->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mpayment_port->get_old_filename($id));
                }

                $data['image'] = $this->upload_file();
            }

            /* If image delted */
            if ($this->input->post('delete_image')) {
                
                if ($this->mpayment_port->get_old_filename($id) != '') {
                
                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mpayment_port->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mpayment_port->get_old_filename($id));
                    
                }
                
                $data['image'] = '';
                
            }

            $this->mpayment_port->update_data($data);

            set_alert('alert-info', "{$data['name']} is success updated");
            redirect('backend/cpayment_port', 'refresh');
        }

    }

    public function destroy($id = '', $status = 'false')
    {

        $pointer = $this->mpayment_port->get_per_field($id, 'name');

        if ($status == 'false') {

            $btn = array(
                "url" => "cpayment_port/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted data $pointer? <br>", $btn);

            redirect('backend/cpayment_port', 'refresh');
        } else {

            $this->mpayment_port->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cpayment_port', 'refresh');
        }

    }

    public function upload_file()
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/cpayment_port/add', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }

    }

}

?>
