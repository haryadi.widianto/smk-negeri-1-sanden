<?php

require './vendor/autoload.php';

use Intervention\Image\ImageManagerStatic as Image;

class Cpost extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Post Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Post Management';
        $this->parsing['data']['sub-header'] = '( control post activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mpost');
        $this->load->model('mpost_category');

    }

    public function index($limit = 0, $index = 0)
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Post';

        $this->parsing['data']['file'] = 'backend/content/post/index';
        $this->parsing['data']['add-link'] = 'cpost/add';
        $this->parsing['data']['add-title'] = 'Add New Post';

        $this->parsing['data']['content'] = $this->mpost->get_data();

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function add()
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'New Post Formulir';
        $this->parsing['data']['save-link'] = 'backend/cpost/storage';
        $this->parsing['data']['save-title'] = 'Save New Post';
        $this->parsing['data']['file'] = 'backend/content/post/add';
        $this->parsing['data']['content'] = '';

        $this->parsing['data']['option'] = $this->mpost_category->get_data();

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function storage()
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required|max_length[150]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cpost/add', 'refresh');
        } else {

            $data['id_post_category'] = $this->input->post('id_post_category');
            $data['title'] = $this->input->post('title');
            $data['content'] = $this->input->post('content');
            $data['status'] = $this->input->post('status');
            $data['tag'] = $this->input->post('tag');
            $data['slug'] = set_slug($this->input->post('title'));
            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            /* Image upload */
            if ($_FILES['userfile']['name'] != '')
                $data['image'] = $this->upload_file();

            $this->mpost->insert_data($data);

            set_alert('alert-info', "{$data['title']} is success created ");
            redirect('backend/cpost', 'refresh');
        }

    }

    public function edit($id = '')
    {

        $this->parsing['data']['option'] = $this->mpost_category->get_data();

        /* Box title */
        $this->parsing['data']['box-title'] = 'Edit Post Formulir';
        $this->parsing['data']['save-link'] = 'backend/cpost/update/' . $id;
        $this->parsing['data']['save-title'] = 'Save Post Edited';
        $this->parsing['data']['file'] = 'backend/content/post/edit';

        $this->parsing['data']['content'] = $this->mpost->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function update($id = '')
    {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required|max_length[200]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cpost/edit/' . $id, 'refresh');
        } else {

            $data['id_post'] = $id;
            $data['id_post_category'] = $this->input->post('id_post_category');
            $data['title'] = $this->input->post('title');
            $data['content'] = $this->input->post('content');
            $data['status'] = $this->input->post('status');
            $data['tag'] = $this->input->post('tag');
            #  $data['slug'] = set_slug($this->input->post('title'));
            $data['updated_at'] = date('Y-m-d H:i:s');

            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($this->mpost->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mpost->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mpost->get_old_filename($id));
                }
                $data['image'] = $this->upload_file('edit/' . $id);
            }

            /* If image delted */
            if ($this->input->post('delete_image')) {

                if ($this->mpost->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mpost->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mpost->get_old_filename($id));
                }

                $data['image'] = '';
            }

            $this->mpost->update_data($data);

            set_alert('alert-info', "{$data['title']} is success updated");
            redirect('backend/cpost', 'refresh');
        }

    }

    public function destroy($id = '', $status = 'false')
    {

        $pointer = $this->mpost->get_per_field($id, 'title');

        if ($status == 'false') {

            $btn = array(
                "url" => "cpost/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted user with name $pointer? <br>", $btn);

            redirect('backend/cpost', 'refresh');
        } else {

            $this->mpost->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cpost', 'refresh');
        }

    }

    public function upload_file($path = 'add')
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '7000';
        $config['max_width'] = '7000';
        $config['max_height'] = '7000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            set_alert('alert-danger', $this->upload->display_errors());

            redirect("backend/cpost/" . $path, 'refresh');
        } else {

            $data = $this->upload->data();

            $img = Image::make('./pub.lcengine/upload/media/'.$data['file_name']);

            // Create main thumb
            $img->resize(null, 150, function ($constraint) {
                $constraint->aspectRatio();
            })->save('./pub.lcengine/upload/media/thumb_'.$data['file_name']);

            // Create squeare thumb
            $img->crop(150, 150)->save('./pub.lcengine/upload/media/square_'.$data['file_name']);

            return $data['file_name'];
        }

    }

}

?>
