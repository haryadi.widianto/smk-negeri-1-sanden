<?php

class Ccontact_setting extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        $this->parsing['data']['title'] = 'Contact Setting';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Contact Setting';
        $this->parsing['data']['sub-header'] = '( contact site configuration in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        $this->load->model('mcontact_setting');
        $this->load->model('mpost_category');
        $this->load->model('mpost');
    }

    public function edit($id = 1) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Contact Setting';
        $this->parsing['data']['save-link'] = 'backend/ccontact_setting/update/' . $id;
        $this->parsing['data']['save-title'] = 'Save Contact Setting';
        $this->parsing['data']['file'] = 'backend/content/contact_setting/edit';

        $this->parsing['data']['content'] = $this->mcontact_setting->get_row($id);
        $this->parsing['data']['post_category'] = $this->mpost_category->get_data(); 

        $this->load->view('backend/master/main', $this->parsing);
    }
    
    
    public function get_post_bycategory($id_category = '') {
        
          /* Box title */
        $data['post_option'] = $this->mpost->get_row_by_category($id_category);
        echo $this->load->view('backend/content/site_setting/post_option', $data);
    }

    public function update($id = 1) {
        
        $data['id'] = $id;
        $data['form_title'] = $this->input->post('form_title');
        $data['button_title'] = $this->input->post('button_title');
        $data['post_category'] = $this->input->post('post_category');
        $data['post_at_home'] = $this->input->post('post');
        $data['description'] = $this->input->post('description');
        $data['lang_area'] = $this->input->post('lang_area');
        $data['long_area'] = $this->input->post('long_area');
        $data['long_point'] = $this->input->post('long_point');
        $data['lang_point'] = $this->input->post('lang_point');
        $data['address'] = $this->input->post('address');
        $data['telp'] = $this->input->post('telp');
        $data['fax'] = $this->input->post('fax');
        $data['email'] = $this->input->post('email');
        
        $this->mcontact_setting->update_data($data);

        set_alert('alert-info', "contact setting is success updated");
        redirect('backend/ccontact_setting/edit/' . $id, 'refresh');
        
    }

}

?>
