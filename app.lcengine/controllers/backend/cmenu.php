<?php

class Cmenu extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Menu Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Menu Management';
        $this->parsing['data']['sub-header'] = '( control menu activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mmenu');
        $this->load->model('mmenu_title');
        $this->load->model('mpost');
        $this->load->model('mpost_category');
        $this->load->model('mmenu_category');
        $this->load->helper('file');

    }

    public function index($id = 0)
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Menu';

        $this->parsing['data']['file'] = 'backend/content/menu/index';
        $this->parsing['data']['add-link'] = 'index.php/backend/cmenu';
        $this->parsing['data']['add-title'] = 'Add New Menu';

        $nav_category = $this->mmenu_category->get_data();
        foreach ($nav_category as $key => $item) {
            /* Session Tab */
            if (!($this->session->userdata('tab')) && $key == 0) {
                $this->session->set_userdata('tab', $item->id);
            }

            $this->parsing['data']['tab_' . $item->id] = $this->mmenu->get_data_array($item->id);
        }
        $this->parsing['data']['nav_category'] = $nav_category;

        $this->parsing['data']['content_edit_menu'] = $this->mmenu_title->get_row();

        /* Combo Box Content */
        $this->parsing['data']['post'] = $this->mpost->get_data();
        $this->parsing['data']['category'] = $this->mpost_category->get_data();
        $this->parsing['data']['template'] = get_filenames('./app.lcengine/views/frontend/component');

        if ($id != 0)
            $this->parsing['data']['content_edit'] = $this->mmenu->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function storage()
    {

        $config = array(
            array(
                'field' => 'menu',
                'label' => 'Menu',
                'rules' => 'required|max_length[50]'
            ),
            array(
                'field' => 'position',
                'label' => 'Position',
                'rules' => 'required'
            ),
            array(
                'field' => 'type',
                'label' => 'Type',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cmenu', 'refresh');
        } else {

            $data['menu'] = $this->input->post('menu');
            $data['type'] = $this->input->post('type') == 'null' ? NULL : $this->input->post('type');
            $data['template'] = $this->input->post('template') == 'null' ? NULL : $this->input->post('template');
            $data['position'] = $this->input->post('position') == 'null' ? NULL : $this->input->post('position');
            $data['description'] = $this->input->post('description');

            switch ($data['type']) {
                case 'post':
                    $data['data'] = $data['template'] . '/' . $this->input->post('post');
                    break;
                case 'category':
                    $data['data'] = $data['template'] . '/' . $this->input->post('category');
                    break;
                case 'raw':
                    if ($data['template'] != '') {
                        $data['data'] = $data['template'] . '/' . $this->input->post('raw');
                    } else {
                        $data['data'] = $this->input->post('raw');
                    }
                    break;
                case 'url':
                    $data['data'] = $this->input->post('url');
                    break;
                case 'page':
                    $data['data'] = $data['template'];
                    break;
                case NULL:
                    $data['data'] = NULL;
                    break;
            }

            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['deleted_at'] = NULL;

            /* Image upload */
            if ($_FILES['userfile']['name'] != '')
                $data['image'] = $this->upload_file();

            $this->mmenu->insert_data($data);

            set_alert('alert-info', "{$data['menu']} is success created ");
            redirect('backend/cmenu', 'refresh');
        }

    }

    public function update($id = '')
    {

        $config = array(
            array(
                'field' => 'menu',
                'label' => 'Menu',
                'rules' => 'required|max_length[50]'
            ),
            array(
                'field' => 'position',
                'label' => 'Position',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cmenu/index/' . $id, 'refresh');
        } else {

            $data['id'] = $id;
            $data['menu'] = $this->input->post('menu');
            $data['type'] = $this->input->post('type') == 'null' ? NULL : $this->input->post('type');
            $data['template'] = $this->input->post('template') == 'null' ? NULL : $this->input->post('template');
            $data['position'] = $this->input->post('position') == 'null' ? NULL : $this->input->post('position');
            $data['description'] = $this->input->post('description');

            switch ($data['type']) {
                case 'post':
                    $data['data'] = $data['template'] . '/' . $this->input->post('post');
                    break;
                case 'category':
                    $data['data'] = $data['template'] . '/' . $this->input->post('category');
                    break;
                case 'raw':
                    if ($data['template'] != '') {
                        $data['data'] = $data['template'] . '/' . $this->input->post('raw');
                    } else {
                        $data['data'] = $this->input->post('raw');
                    }
                    break;
                case 'url':
                    $data['data'] = $this->input->post('url');
                    break;
                case 'page':
                    $data['data'] = $data['template'];
                    break;
                case NULL:
                    $data['data'] = NULL;
                    break;
            }

            $data['updated_at'] = date('Y-m-d H:i:s');

            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($this->mmenu->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mmenu->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mmenu->get_old_filename($id));
                }

                $data['image'] = $this->upload_file();
            }

            /* If image delted */
            if ($this->input->post('delete_image')) {

                if ($this->mmenu->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mmenu->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mmenu->get_old_filename($id));
                }

                $data['image'] = '';
            }

            $this->mmenu->update_data($data);

            set_alert('alert-info', "{$data['menu']} is success updated");
            redirect('backend/cmenu', 'refresh');
        }

    }

    public function destroy($id = '', $status = 'false')
    {

        $pointer = $this->mmenu->get_per_field($id, 'menu');

        if ($status == 'false') {

            $btn = array(
                "url" => "cmenu/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted data $pointer? <br>", $btn);

            redirect('backend/cmenu', 'refresh');
        } else {

            $this->mmenu->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cmenu', 'refresh');
        }

    }

    public function up($id = 0, $position = '', $parents = '')
    {

        $query = $this->db->query("SELECT * FROM (SELECT * FROM kr_menu ORDER BY kr_menu.id DESC ) AS temp WHERE  id < $id AND deleted_at = '0000-00-00 00:00:00' AND position = '$position' AND parents = '$parents' LIMIT 1  ")->row_array();

        $temp = $query['id'];

        $query = $this->db->query("UPDATE kr_menu SET id = 0 where id = $id");
        $query = $this->db->query("UPDATE kr_menu SET parents = -1 where parents = $id");
        $query = $this->db->query("UPDATE kr_menu SET id = $id  where id = $temp");
        $query = $this->db->query("UPDATE kr_menu SET parents = $id where parents = $temp");
        $query = $this->db->query("UPDATE kr_menu SET id = $temp  where id = 0");
        $query = $this->db->query("UPDATE kr_menu SET parents = $temp where parents = -1");

        /* Change Parent */


        redirect('backend/cmenu', 'refresh');

    }

    public function down($id = 0, $position = '', $parents = '')
    {

        $query = $this->db->query("SELECT * FROM (SELECT * FROM kr_menu ORDER BY kr_menu.id ASC ) AS temp WHERE  id > $id AND deleted_at = '0000-00-00 00:00:00' AND position = '$position'  AND parents = '$parents'  LIMIT 1")->row_array();

        $temp = $query['id'];

        $query = $this->db->query("UPDATE kr_menu SET id = 0 where id = $id");
        $query = $this->db->query("UPDATE kr_menu SET parents = -1 where parents = $id");
        $query = $this->db->query("UPDATE kr_menu SET id = $id  where id = $temp");
        $query = $this->db->query("UPDATE kr_menu SET parents = $id where parents = $temp");
        $query = $this->db->query("UPDATE kr_menu SET id = $temp  where id = 0");
        $query = $this->db->query("UPDATE kr_menu SET parents = $temp where parents = -1");

        redirect('backend/cmenu', 'refresh');

    }

    public function upload_file()
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/cmenu_category', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }

    }

    public function update_title()
    {

        $data = array(
            'id' => 1,
            'top' => $this->input->post('top'),
            'bottom' => $this->input->post('bottom'),
            'right' => $this->input->post('right'),
            'left' => $this->input->post('left')
        );

        $this->mmenu_title->update_data($data);

        set_alert('alert-info', "menu title is success updated");
        redirect('backend/cmenu', 'refresh');

    }

    public function parent_order($id_menu = 0, $id_parent = 0)
    {

        $data = [
            'id' => $id_menu,
            'parents' => $id_parent
        ];

        $this->mmenu->update_data($data);

        redirect('backend/cmenu', 'refresh');

    }

    public function save_last_tab($id_tab = 0)
    {
        $this->session->set_userdata('tab', $id_tab);

    }

}

?>
