<?php

class Cmessage extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Message Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Message Management';
        $this->parsing['data']['sub-header'] = '( control message activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mcontact');
        
    }

    public function index($id = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Message';

        $this->parsing['data']['file'] = 'backend/content/message/index';
        $this->parsing['data']['add-link'] = 'index.php/backend/cmessage';
        $this->parsing['data']['add-title'] = 'Add New Message';

        $this->parsing['data']['content'] = $this->mcontact->get_data();
        
        if($id != 0) $this->parsing['data']['content_edit'] = $this->mcontact->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);
        
    }

    public function add() {

        /* Box title */
        $this->parsing['data']['box-title'] = 'New Message Formulir';
        $this->parsing['data']['save-link'] = 'backend/cmessage/storage';
        $this->parsing['data']['save-title'] = 'Save New Message';
        $this->parsing['data']['file'] = 'backend/content/message/add';
        $this->parsing['data']['content'] = '';
        
        $this->parsing['data']['option'] = $this->mcontact->get_data();

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function storage() {

        $config = array(
            array(
                'field' => 'product_category',
                'label' => 'Product Category',
                'rules' => 'required|max_length[150]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cproduct_category/add', 'refresh');
        } else {

            $data['product_category'] = $this->input->post('product_category');
            $data['id_parent'] = $this->input->post('id_parent');
            $data['description'] = $this->input->post('description');
            $data['slug'] = set_slug($this->input->post('product_category'));
            
            $session = $this->session->userdata('login');
            $data['user_created'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            $this->mcategory_product->insert_data($data);
            set_alert('alert-info', "{$data['product_category']} is success created ");
            redirect('backend/cproduct_category', 'refresh');
        }
    }

    public function view($id = '') {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Read Messsage';
        $this->parsing['data']['save-link'] = 'backend/cmessage/';
        $this->parsing['data']['save-title'] = 'Back to List Message';
        $this->parsing['data']['file'] = 'backend/content/message/edit';
        $this->parsing['data']['content'] = $this->mcontact->get_row($id);
        
        /* Update data read */
        $data = array(
            'id' => $id,
            'status' => 'read'
        );
        $this->mcontact->update_data($data);
         
        $this->load->view('backend/master/main', $this->parsing);
        
    }

    public function destroy($id = '', $status = 'false') {

        $pointer = $this->mcontact->get_per_field($id, 'name');

        if ($status == 'false') {

            $btn = array(
                "url" => "cmessage/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted emall from $pointer? <br>", $btn);

            redirect('backend/cmessage', 'refresh');
        } else {

            $this->mcontact->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cmessage', 'refresh');
        }
    }

}

?>
