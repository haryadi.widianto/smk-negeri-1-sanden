<?php

class Cbottom_navigation extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Bottom Navigation Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Bottom Navigation Management';
        $this->parsing['data']['sub-header'] = '( control bottom navigation activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mbottom_navigation');
        $this->load->model('mmenu_category');
        
    }

    public function index($id = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Bottom Navigation';

        $this->parsing['data']['file'] = 'backend/content/bottom_navigation/index';
        $this->parsing['data']['add-link'] = 'index.php/backend/cbottom_navigation';
        $this->parsing['data']['add-title'] = 'Add New Bottom Navigation';

        $this->parsing['data']['content'] = $this->mbottom_navigation->get_data();
        $this->parsing['data']['option'] = $this->mmenu_category->get_data();
        
        if($id != 0) $this->parsing['data']['content_edit'] = $this->mbottom_navigation->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);
        
    }

    public function storage() {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'required|max_length[100]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cbottom_navigation', 'refresh');
            
        } else {

            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['type'] = $this->input->post('type');
            $data['id_menu_category'] = $this->input->post('id_menu_category');
            
            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');
            
            $this->mbottom_navigation->insert_data($data);
            
            set_alert('alert-info', "{$data['title']} is success created ");
            redirect('backend/cbottom_navigation', 'refresh');
            
        }
    }


    public function update($id = '') {

        $config = array(
            array(
                'field' => 'title',
                'label' => 'Ttitle',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            
            set_alert('alert-danger', validation_errors());
            redirect('backend/cbottom_navigation/index/' . $id, 'refresh');
            
        } else {

            $data['id'] = $id;
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['type'] = $this->input->post('type');
            $data['id_menu_category'] = $this->input->post('id_menu_category');
            
            $data['updated_at'] = date('Y-m-d H:i:s');
            
            $this->mbottom_navigation->update_data($data);
            
            set_alert('alert-info', "{$data['title']} is success updated");
            redirect('backend/cbottom_navigation', 'refresh');
            
        }
    }

    public function destroy($id = '', $status = 'false') {

        $pointer = $this->mbottom_navigation->get_per_field($id, 'title');

        if ($status == 'false') {

            $btn = array(
                "url" => "cbottom_navigation/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted data $pointer? <br>", $btn);

            redirect('backend/cbottom_navigation', 'refresh');
            
        } else {

            $this->mbottom_navigation->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cbottom_navigation', 'refresh');
        }
    }
    
    public function up($id = 0) {
        
        $query = $this->db->query("SELECT id FROM kr_social_media WHERE id < $id AND deleted_at = '0000-00-00 00:00:00' LIMIT 1")->row_array();
        
        $temp = $query['id'];
        
        $query = $this->db->query("UPDATE kr_social_media SET id = 0 where id = $id");
        $query = $this->db->query("UPDATE kr_social_media SET id = $id  where id = $temp");
        $query = $this->db->query("UPDATE kr_social_media SET id = $temp  where id = 0");
        
        redirect('backend/csocial_media', 'refresh');
        
        
    }
    
    public function down($id = 0) {
        
        $query = $this->db->query("SELECT id FROM kr_social_media WHERE id > $id AND deleted_at = '0000-00-00 00:00:00' LIMIT 1")->row_array();
        
        $temp = $query['id'];
        
        $query = $this->db->query("UPDATE kr_social_media SET id = 0 where id = $id");
        $query = $this->db->query("UPDATE kr_social_media SET id = $id  where id = $temp");
        $query = $this->db->query("UPDATE kr_social_media SET id = $temp  where id = 0");
        
        redirect('backend/csocial_media', 'refresh');
        
    }
    
    public function upload_file() {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/csocial_media', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        
    }
    
    
    

}

?>
