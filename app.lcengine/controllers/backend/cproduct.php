<?php

class Cproduct extends CI_Controller
{

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct()
    {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Product Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Product Management';
        $this->parsing['data']['sub-header'] = '( control product activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mproduct');
        $this->load->model('mcategory_product');

    }

    public function index($limit = 0, $index = 0)
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Product';

        $this->parsing['data']['file'] = 'backend/content/product/index';
        $this->parsing['data']['add-link'] = 'cproduct/add';
        $this->parsing['data']['add-title'] = 'Add New Product';

        $this->parsing['data']['content'] = $this->mproduct->get_data();

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function add()
    {

        /* Box title */
        $this->parsing['data']['box-title'] = 'New Product Formulir';
        $this->parsing['data']['save-link'] = 'backend/cproduct/storage';
        $this->parsing['data']['save-title'] = 'Save New Product';
        $this->parsing['data']['file'] = 'backend/content/product/add';
        $this->parsing['data']['content'] = '';

        $this->parsing['data']['option'] = $this->mcategory_product->get_data();

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function storage()
    {

        $config = array(
            array(
                'field' => 'product',
                'label' => 'Product',
                'rules' => 'required|max_length[200]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cproduct/add', 'refresh');
        } else {

            $data['id_product_category'] = $this->input->post('id_product_category');
            $data['product'] = $this->input->post('product');
            $data['description'] = $this->input->post('description');
            $data['price'] = $this->input->post('price');
            $data['qty_stocked'] = $this->input->post('qty_stocked');
            $data['size'] = $this->input->post('size');
            $data['unit'] = $this->input->post('unit');
            $data['tag'] = $this->input->post('tag');
            $data['new_tag'] = $this->input->post('new_tag');
            $data['status_publish'] = $this->input->post('status_publish');
            $data['slug'] = set_slug($this->input->post('product'));
            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            /* Image upload */
            if ($_FILES['userfile']['name'] != '')
                $data['image'] = $this->upload_file();

            $this->mproduct->insert_data($data);

            set_alert('alert-info', "{$data['product']} is success created ");
            redirect('backend/cproduct', 'refresh');
        }

    }

    public function edit($id = '')
    {

        $this->parsing['data']['option'] = $this->mcategory_product->get_data();

        /* Box title */
        $this->parsing['data']['box-title'] = 'Edit Product Formulir';
        $this->parsing['data']['save-link'] = 'backend/cproduct/update/' . $id;
        $this->parsing['data']['save-title'] = 'Save Product Edited';
        $this->parsing['data']['file'] = 'backend/content/product/edit';

        $this->parsing['data']['content'] = $this->mproduct->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);

    }

    public function update($id = '')
    {

        $config = array(
            array(
                'field' => 'product',
                'label' => 'Product',
                'rules' => 'required|max_length[200]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cproduct/edit/' . $id, 'refresh');
        } else {

            $data['id_product'] = $id;
            $data['id_product_category'] = $this->input->post('id_product_category');
            $data['product'] = $this->input->post('product');
            $data['description'] = $this->input->post('description');
            $data['price'] = $this->input->post('price');
            $data['qty_stocked'] = $this->input->post('qty_stocked');
            $data['size'] = $this->input->post('size');
            $data['unit'] = $this->input->post('unit');
            $data['tag'] = $this->input->post('tag');
            $data['new_tag'] = $this->input->post('new_tag');
            $data['status_publish'] = $this->input->post('status_publish');
            #$data['slug'] = set_slug($this->input->post('product'));
            $data['updated_at'] = date('Y-m-d H:i:s');

            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($this->mproduct->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mproduct->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mproduct->get_old_filename($id));
                }

                $data['image'] = $this->upload_file();
            }

            /* If image delted */
            if ($this->input->post('delete_image')) {

                if ($this->mproduct->get_old_filename($id) != '') {

                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->mproduct->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->mproduct->get_old_filename($id));
                }

                $data['image'] = '';
            }

            $this->mproduct->update_data($data);

            set_alert('alert-info', "{$data['product']} is success updated");
            redirect('backend/cproduct', 'refresh');
        }

    }

    public function destroy($id = '', $status = 'false')
    {

        $pointer = $this->mproduct->get_per_field($id, 'product');

        if ($status == 'false') {

            $btn = array(
                "url" => "cproduct/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted user with name $pointer? <br>", $btn);

            redirect('backend/cproduct', 'refresh');
        } else {

            $this->mproduct->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cproduct', 'refresh');
        }

    }

    public function upload_file()
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/cproduct/add', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }

    }

}

?>
