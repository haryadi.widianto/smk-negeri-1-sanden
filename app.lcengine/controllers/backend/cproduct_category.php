<?php

class Cproduct_category extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Product Category Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Product Category Management';
        $this->parsing['data']['sub-header'] = '( control product category activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mcategory_product');
        
    }

    public function index($limit = 0, $index = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Product Category';

        $this->parsing['data']['file'] = 'backend/content/product_category/index';
        $this->parsing['data']['add-link'] = 'cproduct_category/add';
        $this->parsing['data']['add-title'] = 'Add New Product Category';

        $this->parsing['data']['content'] = $this->mcategory_product->get_data();
        
        $this->load->view('backend/master/main', $this->parsing);
        
    }

    public function add() {

        /* Box title */
        $this->parsing['data']['box-title'] = 'New Product Category Formulir';
        $this->parsing['data']['save-link'] = 'backend/cproduct_category/storage';
        $this->parsing['data']['save-title'] = 'Save New Product Category';
        $this->parsing['data']['file'] = 'backend/content/product_category/add';
        $this->parsing['data']['content'] = '';
        
        $this->parsing['data']['option'] = $this->mcategory_product->get_data();

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function storage() {

        $config = array(
            array(
                'field' => 'product_category',
                'label' => 'Product Category',
                'rules' => 'required|max_length[150]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cproduct_category/add', 'refresh');
        } else {

            $data['product_category'] = $this->input->post('product_category');
            $data['id_parent'] = $this->input->post('id_parent');
            $data['description'] = $this->input->post('description');
            $data['slug'] = set_slug($this->input->post('product_category'));
            
            $session = $this->session->userdata('login');
            $data['user_created'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            $this->mcategory_product->insert_data($data);
            set_alert('alert-info', "{$data['product_category']} is success created ");
            redirect('backend/cproduct_category', 'refresh');
        }
    }

    public function edit($id = '') {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Edit Product Category Formulir';
        $this->parsing['data']['save-link'] = 'backend/cproduct_category/update/' . $id;
        $this->parsing['data']['save-title'] = 'Save Product Category Edited';
        $this->parsing['data']['file'] = 'backend/content/product_category/edit';
        $this->parsing['data']['content'] = $this->mcategory_product->get_row($id);
        
        $this->parsing['data']['option'] = $this->mcategory_product->get_data();

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function update($id = '') {

        $config = array(
            array(
                'field' => 'product_category',
                'label' => 'Product Category',
                'rules' => 'required|max_length[150]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cproduct_category/edit/' . $id, 'refresh');
        } else {

            $data['id_product_category'] = $id;
            $data['product_category'] = $this->input->post('product_category');
            $data['id_parent'] = $this->input->post('id_parent');
            $data['description'] = $this->input->post('description');
            #$data['slug'] = set_slug($this->input->post('product_category'));
            $data['updated_at'] = date('Y-m-d H:i:s');

            $this->mcategory_product->update_data($data);
            set_alert('alert-info', "{$data['product_category']} is success updated");
            redirect('backend/cproduct_category', 'refresh');
        }
    }

    public function destroy($id = '', $status = 'false') {

        $pointer = $this->mcategory_product->get_per_field($id, 'product_category');

        if ($status == 'false') {

            $btn = array(
                "url" => "cproduct_category/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted user with name $pointer? <br>", $btn);

            redirect('backend/cproduct_category', 'refresh');
        } else {

            $this->mcategory_product->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/cproduct_category', 'refresh');
        }
    }

}

?>
