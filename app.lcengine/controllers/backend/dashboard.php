<?php

class Dashboard extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Post Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Post Management';
        $this->parsing['data']['sub-header'] = '( control post activity in here )';

        /* Link Load */
        #$this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        #$this->limit = 5;

        /* Model Load */
        $this->parsing['breadcrumb']['class'] = get_class();
    }

    public function index($limit = 0, $index = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Dashboard';

        $this->parsing['data']['file'] = 'backend/content/dashboard/index';
        # $this->parsing['data']['add-link'] = 'cpost/add';
        # $this->parsing['data']['add-title'] = 'Add New Post';
        #$this->parsing['data']['content'] = $this->mpost->get_data();

        $this->load->view('backend/master/main', $this->parsing);
    }

}
?>