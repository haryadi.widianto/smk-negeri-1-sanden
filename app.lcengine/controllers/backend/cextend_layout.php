<?php

class Cextend_layout extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Extend Layout Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Extend Layout Management';
        $this->parsing['data']['sub-header'] = '( control extend layout activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('mextend_layout');
        $this->load->model('mproduct');
    }

    public function index($id = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Extend Layout';

        $this->parsing['data']['file'] = 'backend/content/extend_layout/index';
        $this->parsing['data']['add-link'] = 'index.php/backend/cextend_layout';
        $this->parsing['data']['add-title'] = 'Add New Extend Layout';

        $this->parsing['data']['content'] = $this->mextend_layout->get_data();
        $this->parsing['data']['option'] = $this->mproduct->get_data();

        if ($id != 0)
            $this->parsing['data']['content_edit'] = $this->mextend_layout->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function storage() {

        $config = array(
            array(
                'field' => 'id_product',
                'label' => 'Product',
                'rules' => 'required'
            ), array(
                'field' => 'type',
                'label' => 'Type',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cextend_layout', 'refresh');
        } else {

            $data['id_product'] = $this->input->post('id_product');
            $data['type'] = $this->input->post('type');

            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');

            $this->mextend_layout->insert_data($data);

            set_alert('alert-info', "product with ID {$data['id_product']} is success created ");
            redirect('backend/cextend_layout', 'refresh');
        }
    }

    public function update($id = '') {

        $config = array(
            array(
                'field' => 'id_product',
                'label' => 'Product',
                'rules' => 'required'
            ), array(
                'field' => 'type',
                'label' => 'Type',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cextend_layout/index/' . $id, 'refresh');
        } else {

            $data['id'] = $id;
            $data['id_product'] = $this->input->post('id_product');
            $data['type'] = $this->input->post('type');
            $data['updated_at'] = date('Y-m-d H:i:s');

            $this->mextend_layout->update_data($data);

            set_alert('alert-info', "product with ID {$data['id_product']} is success updated");
            redirect('backend/cextend_layout', 'refresh');
        }
    }

    public function destroy($id = '', $status = 'false') {

        $pointer = $this->mextend_layout->get_per_field($id, 'id_product');

        if ($status == 'false') {

            $btn = array(
                "url" => "cextend_layout/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted data with ID $pointer? <br>", $btn);

            redirect('backend/cextend_layout', 'refresh');
            
        } else {

            $this->mextend_layout->delete_data($id);

            set_alert('alert-info', "Data with ID $pointer is success deleted");

            redirect('backend/cextend_layout', 'refresh');
        }
    }
    

    
}

?>
