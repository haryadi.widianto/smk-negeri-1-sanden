<?php

class Csocial_media extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'Social Media Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'Social Media Management';
        $this->parsing['data']['sub-header'] = '( control social media activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('msocial_media');
        
    }

    public function index($id = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List Social Media';

        $this->parsing['data']['file'] = 'backend/content/social_media/index';
        $this->parsing['data']['add-link'] = 'index.php/backend/csocial_media';
        $this->parsing['data']['add-title'] = 'Add New Social Media';

        $this->parsing['data']['content'] = $this->msocial_media->get_data();
        
        if($id != 0) $this->parsing['data']['content_edit'] = $this->msocial_media->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);
        
    }

    public function storage() {

        $config = array(
            array(
                'field' => 'social_media',
                'label' => 'Social Media',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/csocial_media', 'refresh');
            
        } else {

            $data['social_media'] = $this->input->post('social_media');
            $data['link'] = $this->input->post('link');
            
            $session = $this->session->userdata('login');
            $data['created_by'] = $session['id_user'];
            $data['created_at'] = date('Y-m-d H:i:s');
            
            /* Image upload */
            if ($_FILES['userfile']['name'] != '') $data['icon'] = $this->upload_file();
                
            $this->msocial_media->insert_data($data);
            
            set_alert('alert-info', "{$data['social_media']} is success created ");
            redirect('backend/csocial_media', 'refresh');
            
        }
    }


    public function update($id = '') {

        $config = array(
            array(
                'field' => 'social_media',
                'label' => 'Social Media',
                'rules' => 'required|max_length[250]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {
            
            set_alert('alert-danger', validation_errors());
            redirect('backend/cproduct/index/' . $id, 'refresh');
            
        } else {

            $data['id'] = $id;
            $data['social_media'] = $this->input->post('social_media');
            $data['link'] = $this->input->post('link');
            $data['updated_at'] = date('Y-m-d H:i:s');
            
            if ($_FILES['userfile']['name'] != '') {

                /* Deleting old file */
                if ($this->msocial_media->get_old_filename($id) != '') {
                
                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msocial_media->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->msocial_media->get_old_filename($id));
                    
                }
                
                $data['icon'] = $this->upload_file();
                
            }
            
            /* If image delted */
            if($this->input->post('delete_image')){
                
                if ($this->msocial_media->get_old_filename($id) != '') {
                
                    // Remove image when it is exist
                    if (file_exists("./pub.lcengine/upload/media/" . $this->msocial_media->get_old_filename($id)))
                        unlink("./pub.lcengine/upload/media/" . $this->msocial_media->get_old_filename($id));
                    
                }
                
                $data['icon'] = '';
                
            }

            $this->msocial_media->update_data($data);
            
            set_alert('alert-info', "{$data['social_media']} is success updated");
            redirect('backend/csocial_media', 'refresh');
            
        }
    }

    public function destroy($id = '', $status = 'false') {

        $pointer = $this->msocial_media->get_per_field($id, 'social_media');

        if ($status == 'false') {

            $btn = array(
                "url" => "csocial_media/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted data $pointer? <br>", $btn);

            redirect('backend/csocial_media', 'refresh');
            
        } else {

            $this->msocial_media->delete_data($id);

            set_alert('alert-info', "$pointer is success deleted");

            redirect('backend/csocial_media', 'refresh');
        }
    }
    
    public function up($id = 0) {
        
        $query = $this->db->query("SELECT id FROM kr_social_media WHERE id < $id AND deleted_at = '0000-00-00 00:00:00' LIMIT 1")->row_array();
        
        $temp = $query['id'];
        
        $query = $this->db->query("UPDATE kr_social_media SET id = 0 where id = $id");
        $query = $this->db->query("UPDATE kr_social_media SET id = $id  where id = $temp");
        $query = $this->db->query("UPDATE kr_social_media SET id = $temp  where id = 0");
        
        redirect('backend/csocial_media', 'refresh');
        
        
    }
    
    public function down($id = 0) {
        
        $query = $this->db->query("SELECT id FROM kr_social_media WHERE id > $id AND deleted_at = '0000-00-00 00:00:00' LIMIT 1")->row_array();
        
        $temp = $query['id'];
        
        $query = $this->db->query("UPDATE kr_social_media SET id = 0 where id = $id");
        $query = $this->db->query("UPDATE kr_social_media SET id = $id  where id = $temp");
        $query = $this->db->query("UPDATE kr_social_media SET id = $temp  where id = 0");
        
        redirect('backend/csocial_media', 'refresh');
        
    }
    
    public function upload_file() {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width'] = '2000';
        $config['max_height'] = '2000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            set_alert('alert-danger', $this->upload->display_errors());
            redirect('backend/csocial_media', 'refresh');
        } else {
            $data = $this->upload->data();
            return $data['file_name'];
        }
        
    }
    
    
    

}

?>
