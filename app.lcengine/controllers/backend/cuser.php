<?php

class Cuser extends CI_Controller {

    private $parsing = array();
    private $limit = 0;
    private $root = '';

    public function __construct() {

        parent::__construct();
        $this->auth->routing_auth('cauth');

        /* Title */
        $this->parsing['data']['title'] = 'User Management';

        /* Header & sub header */
        $this->parsing['data']['header'] = 'User Management';
        $this->parsing['data']['sub-header'] = '( control user activity in here )';

        /* Link Load */
        $this->parsing['data']['link'] = array('Home', $this->parsing['data']['title']);

        /* Datagrid Limit */
        $this->limit = 5;

        /* Model Load */
        $this->load->model('muser');
    }

    public function index($limit = 0, $index = 0) {

        /* Box title */
        $this->parsing['data']['box-title'] = 'List user registered';

        $this->parsing['data']['file'] = 'backend/content/user/index';
        $this->parsing['data']['add-link'] = 'cuser/add';
        $this->parsing['data']['add-title'] = 'Add New User';

        $this->parsing['data']['content'] = $this->muser->get_data($limit, $index);

        $this->load->view('backend/master/main', $this->parsing);
        
    }

    public function add() {

        /* Box title */
        $this->parsing['data']['box-title'] = 'New User Formulir';
        $this->parsing['data']['save-link'] = 'backend/cuser/storage';
        $this->parsing['data']['save-title'] = 'Save New User';
        $this->parsing['data']['file'] = 'backend/content/user/add';
        $this->parsing['data']['content'] = '';

        $this->load->view('backend/master/main', $this->parsing);
        
    }

    public function storage() {

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|max_length[100]'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email|max_length[100]'
            ),
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|max_length[100]'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|max_length[100]'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('backend/cuser/add', 'refresh');
        } else {

            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['username'] = $this->input->post('username');
            $data['password'] = $this->encrypt->encode($this->input->post('password'), get_key());
            $data['level'] = $this->input->post('level');
            $data['status'] = $this->input->post('status');
            $data['created_at'] = date('Y-m-d H:m:i');

            /* Check availabe username & password */
            if ($this->muser->check_available_user($data, 'add')) {
                $this->muser->insert_data($data);
                set_alert('alert-info', "User with name {$data['name']} is success created ");
                redirect('backend/cuser', 'refresh');
            } else {
                set_alert('alert-danger', 'Sorry, username is already exist');
                redirect('backend/cuser/add', 'refresh');
            }
            
        }
    }

    public function edit($id = '') {

        /* Box title */
        $this->parsing['data']['box-title'] = 'Edit User Formulir';
        $this->parsing['data']['save-link'] = 'backend/cuser/update/' . $id;
        $this->parsing['data']['save-title'] = 'Save User Edited';
        $this->parsing['data']['file'] = 'backend/content/user/edit';
        $this->parsing['data']['content'] = $this->muser->get_row($id);

        $this->load->view('backend/master/main', $this->parsing);
    }

    public function update($id = '') {

        $config = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|max_length[100]'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|valid_email|max_length[100]'
            ),
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required|max_length[100]'
            ),
            array(
                'field' => 'level',
                'label' => 'Level',
                'rules' => 'required'
            ),
            array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            )
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());
            redirect('backend/cuser/edit/' . $id, 'refresh');
            
        } else {

            $data['id_user'] = $id;
            $data['name'] = $this->input->post('name');
            $data['email'] = $this->input->post('email');
            $data['username'] = $this->input->post('username');

            if ($this->input->post('password') != '') {
                $data['password'] = $this->encrypt->encode($this->input->post('password'), get_key());
            }

            $data['level'] = $this->input->post('level');
            $data['status'] = $this->input->post('status');
            $data['updated_at'] = date('Y-m-d H:m:i');

            /* Check availabe username & password */
            if ($this->muser->check_available_user($data, 'edit', $id)) {
                $this->muser->update_data($data);
                set_alert('alert-info', "User with name {$data['name']} is success updated");
                redirect('backend/cuser', 'refresh');
            } else {
                set_alert('alert-danger', 'Sorry, username is already exist');
                redirect('backend/cuser/edit/' . $id, 'refresh');
            }
            
        }
    }

    public function destroy($id = '', $status = 'false') {

        $name = $this->muser->get_per_field($id, 'name');

        if ($status == 'false') {

            $btn = array(
                "url" => "cuser/destroy/{$id}/true"
            );

            set_alert('alert-danger', "Are sure to deleted user with name $name? <br>", $btn);

            redirect('backend/cuser', 'refresh');
        } else {

            $this->muser->delete_data($id);

            set_alert('alert-info', "User with name $name is success deleted");

            redirect('backend/cuser', 'refresh');
        }
    }

}

?>
