<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require './vendor/autoload.php';

//use Intervention\Image\Image;
use Intervention\Image\ImageManagerStatic as Image;

class Ctest extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        // $this->load->library("Pdf");

    }

    public function index()
    {

        $img = Image::make('./pub.lcengine/upload/media/bbbb.jpg');

        // Create main thumb
        $img->resize(null, 150, function ($constraint) {
            $constraint->aspectRatio();
        })->save('./pub.lcengine/upload/media/thumb_bbbb.jpg');

        // Create squeare thumb
        $img->crop(150, 150)->save('./pub.lcengine/upload/media/square_bbbb.jpg');

        echo "testing";

    }

}

?>
 
