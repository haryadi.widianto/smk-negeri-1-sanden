<?php

if (!function_exists('get_total_message')) {

    function get_total_message() {
        $self = & get_instance();
        $data = $self->load->model('mcontact');
        return $self->mcontact->get_total();
    }

}
if (!function_exists('get_message')) {

    function get_message() {
        $self = & get_instance();
        $data = $self->load->model('mcontact');
        return $self->mcontact->get_data_where('status', 'new');
    }

}

if (!function_exists('get_photo')) {

    function get_photo($id = '') {

        $self = & get_instance();
        $self->load->model('muser_profile');
        return $self->muser_profile->get_old_filename($id);
    }

}

if (!function_exists('get_gravatar')) {

    function get_gravatar($email = '') {

        $gravatar_link = 'http://www.gravatar.com/avatar/' . md5($email) . '?s=128';
        return $gravatar_link;
    }

}

if (!function_exists('get_total_order')) {

    function get_total_order() {
        $self = & get_instance();
        $self->load->model('frontend/msession');
        return $self->msession->get_total_order();
    }

}

if (!function_exists('get_last_order')) {

    function get_last_order() {
        $self = & get_instance();
        $self->load->model('frontend/msession');
        return $self->msession->get_last_order();
    }

}


if (!function_exists('get_name_order')) {

    function get_name_order($id = '') {
        $self = & get_instance();
        $self->load->model('muser');
        return $self->muser->get_per_field($id, 'name');
    }

}


if (!function_exists('get_total_item')) {

    function get_total_item($id_session = '') {
        $self = & get_instance();
        $self->load->model('frontend/mchart');
        return $self->mchart->get_total_item($id_session);
    }

}

if (!function_exists('get_total_user_registered')) {

    function get_total_user_registered() {
        $self = & get_instance();
        $self->load->model('muser');
        return $self->muser->get_total_user_registered();
    }

}

if (!function_exists('get_total_product')) {

    function get_total_product() {
        $self = & get_instance();
        $self->load->model('mproduct');
        return $self->mproduct->get_count();
    }

}

if (!function_exists('get_total_post')) {

    function get_total_post() {
        $self = & get_instance();
        $self->load->model('mpost');
        return $self->mpost->get_count();
    }

}

if (!function_exists('get_total_event')) {

    function get_total_event() {
        $self = & get_instance();
        $self->load->model('mevent');
        return $self->mevent->get_count();
    }

}

if (!function_exists('get_total_student_registered')) {

    function get_total_student_registered() {
        $self = & get_instance();
        $self->load->model('registration/student');
        return $self->student->get_total();
    }

}

if (!function_exists('get_lasted_student_registered')) {

    function get_lasted_student_registered() {
        $self = & get_instance();
        $self->load->model('registration/student');
        return $self->student->get_lasted();
    }

}


if (!function_exists('get_total_transaction')) {

    function get_total_transaction() {
        $self = & get_instance();
        $self->load->model('frontend/msession');
        return $self->msession->get_total_transaction();
    }

}

if (!function_exists('set_image')) {

    function set_image($image = '', $type = 'image', $size = '') {
        if ($type == 'image') {
            $img = 'noimage.png';
            if ($image != '') {

                $img = $image;

                if ($size != '') {
                    $img = $size . '_' . $img;
                }
            }
        } else if ($type == 'video') {
            $img = 'youtube.png';
        }

        return base_url('pub.lcengine/upload/media/' . $img);
    }

}

if (!function_exists('image')) {

    function image($image = '', $type = 'image') {
        if ($type == 'image') {
            $img = 'media/noimage.png';
            if ($image != '') {
                $img = $image;
            }
        } else if ($type == 'video') {
            $img = 'media/youtube.png';
        }

        return base_url('pub.lcengine/upload/' . $img);
    }

}

if (!function_exists('set_route')) {

    function set_route($data = '') {
        return site_url('routing/index/' . $data);
    }

}

if (!function_exists('appear')) {

    function appear($data = '') {
        if ($data == '' || $data == '0') {
            return '-';
        } else {
            return $data;
        }
    }

}

if (!function_exists('appear_alt')) {

    function appear_alt($data = '', $alt = '') {

        if ($data == '' || $data == '0') {
            return '-';
        } else {
            return ($data != 'other' ) ? $data : $alt;
        }
    }

}

if (!function_exists('isValued')) {

    function isValued($data, $key, $return = 'checked="true"') {

        if (isset($data[$key])) {
            return $return;
        }
    }

}

if (!function_exists('get_data_title')) {

    function get_data_title($data_source = '', $id = '') {

        $self = & get_instance();
        $self->load->model('mpost');
        $self->load->model('mpost_category');
        $self->load->model('mmenu_category');


        switch ($data_source) {
            case 'post':
                return $self->mpost->get_per_field($id, 'title');
                break;
            case 'post_category':
                return $self->mpost_category->get_per_field($id, 'post_category');
                break;
            case 'navigation_category':
                return $self->mmenu_category->get_per_field($id, 'title');
                break;
        }
    }

}

if (!function_exists('get_inadate')) {

    function get_inadate() {
        $array = array(
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember'
        );
        return $array;
    }

}

if (!function_exists('inadate')) {

    function inadate($date) {

        $arrDate = explode('-', $date);

        $array = array(
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        );


        return $arrDate[2] . ' ' . $array[$arrDate[1]] . ' ' . $arrDate[0];
    }

}

if (!function_exists('generateRandomString')) {

    function get_random_string($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}


if (!function_exists('upload_file')) {

    function upload_file($path = '') {

        $self = & get_instance();

        $config['upload_path'] = './pub.smkkbantul/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '7000';
        $config['max_width'] = '7000';
        $config['max_height'] = '7000';
        $config['encrypt_name'] = true;

        $self->load->library('upload', $config);

        if (!$self->upload->do_upload()) {
            set_alert('alert-danger', $self->upload->display_errors());
            redirect($path, 'refresh');
        } else {
            $data = $self->upload->data();
            return $data['file_name'];
        }
    }

}

if (!function_exists('get_album_cover')) {

    function get_album_cover($id_album = '') {
        $self = & get_instance();
        $data = $self->load->model('mgallery');
        return $self->mgallery->get_cover($id_album);
    }

}

if (!function_exists('switchInput')) {

    function switchInput($prev = '', $next = '') {
        
        return ($next == '') ? $prev : $next;
        
    }

}
?>