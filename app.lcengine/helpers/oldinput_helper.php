<?php

if (!function_exists('set_oldinput')) {

    function set_oldinput($data = array())
    {

        $self = & get_instance();

        $self->session->set_flashdata('oldinput', $data);

    }

}

if (!function_exists('get_oldinput')) {

    function get_oldinput()
    {

        $self = & get_instance();

        $data = $self->session->flashdata('oldinput');

        return $data;

    }

}

if (!function_exists('get_oldinput_trim')) {

    function get_oldinput_trim($data = array(), $field = '')
    {

        if (!empty($data)) {
            return key_exists($field, $data) ? $data[$field] : '';
        } else {
            return '';
        }

    }

}

?>