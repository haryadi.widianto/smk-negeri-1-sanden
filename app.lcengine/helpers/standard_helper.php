<?php

/* This function use in form */

function paging($path = '', $total_row = 0, $per_page = 0, $uri_segment = 0){
    
    $self =& get_instance();
    
    $self->load->library('pagination');
    
    
    $config['base_url'] = base_url().$path;
    $config['prev_link'] = TRUE;
    $config['total_rows'] = $total_row;
    $config['per_page'] = $per_page;
    $config['uri_segment'] = $uri_segment; 
    $config['first_link'] = 'First';
    $config['last_link'] = 'Last';
    $config['prev_link'] = 'Previous';
    $config['next_link'] = 'Next';
    
    
    $config['full_tag_open'] = '<ul  class="pagination" >';
    $config['full_tag_close'] = '</ul>';
    
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    
    $config['prev_tag_open'] = '<li>';
    $config['prev_tag_close'] = '</li>';
    
    $config['cur_tag_open'] = '<li><a href="#"><b>';
    $config['cur_tag_close'] = '</b></a></li>';
    
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

    
    $self->pagination->initialize($config); 
    
}

?>
