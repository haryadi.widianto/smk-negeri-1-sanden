<?php

if (!function_exists('set_breadcrumb')) {

    function set_breadcrumb($params = array()) {

        $GLOBALS['breadcrumb'] = $params;
    }

}

if (!function_exists('get_breadcrumb')) {

    function get_breadcrumb() {

        $self = & get_instance();

        $data['breadcrumb'] = explode('/', ltrim($self->uri->ruri_string(), '/'));

        if (!empty($data)) {
            return $self->load->view('backend/component/breadcrumb', $data);
        } else {
            return false;
        }
        
    }

}
?>