<?php

if (!function_exists('set_alert')) {

    function set_alert($mode = 'alert-info', $message = '', $button = array()) {

        $self = & get_instance();

        if (!empty($button)) {
            $data = array(
                'mode' => $mode,
                'message' => $message,
                'button' => array(
                    'url' => $button['url'],
                )
            );
        } else {
            $data = array(
                'mode' => $mode,
                'message' => $message,
                'button' => ''
            );
        }

        $self->session->set_flashdata('alert', $data);
    }

}

if (!function_exists('get_alert')) {

    function get_alert() {

        $self = & get_instance();

        $data = $self->session->flashdata('alert');

        if (!empty($data)) {
            return $self->load->view('backend/component/alert', $data);
        } else {
            return false;
        }
    }

}

if (!function_exists('delete_confirm')) {

    function delete_confirm($data = array()) {

        return $self->load->view('backend/component/alert', $data);
    }

}
?>