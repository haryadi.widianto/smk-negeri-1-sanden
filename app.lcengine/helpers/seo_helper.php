<?php

function set_slug($string) {

    // Replace all non letters, numbers, spaces or hypens
    $string = preg_replace('/[^\-\sa-zA-Z0-9]+/', '', mb_strtolower($string));

    // Replace spaces and duplicate hyphens with a single hypen
    $string = preg_replace('/[\-\s]+/', '-', $string);

    // Trim off left and right hypens
    $string = trim($string, '-');

    return $string.'-'.date('Y-m-d-His');
}

function set_textid($string) {

    // Replace all non letters, numbers, spaces or hypens
    $string = preg_replace('/[^\-\sa-zA-Z0-9]+/', '', mb_strtolower($string));

    // Replace spaces and duplicate hyphens with a single hypen
    $string = preg_replace('/[\-\s]+/', '-', $string);

    // Trim off left and right hypens
    $string = trim($string, '-');

    return $string;
}

?>

