<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Migration_Add_kr_product extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            
            'id_product' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
                
            ),
            
            'price' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            )
            
        ));

        $this->dbforge->create_table('kr_product');
    }

    public function down() {
        $this->dbforge->drop_table('kr_product');
    }

}
