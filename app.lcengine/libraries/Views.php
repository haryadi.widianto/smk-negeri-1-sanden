<?php

class Views {

    private $main_path = '';
    private $self;

    public function __construct() {
        $this->self = & get_instance();
        $this->main_path = 'frontend/component/';
    }

    public function render($params = array(), $wrap_open = '', $wrap_close = '') {
        if (!empty($params)) {
            echo $wrap_open;
            foreach ($params as $key => $item) {
                $this->self->load->view($this->main_path . $item['path'], $item);
            }
            echo $wrap_close;
        }
    }

}

?>
