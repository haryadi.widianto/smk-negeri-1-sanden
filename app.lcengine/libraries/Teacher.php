<?php

class Teacher
{

    private $self;

    public function __construct()
    {
        $this->self = & get_instance();
        $this->self->load->model('people/mteacher');
        $this->self->load->model('muser');
        $this->self->load->library('media');
        $this->self->load->library('auth');

    }

    public function insert()
    {

        /* Validation */
        $config = array(
            array('field' => 'username', 'label' => 'Email ', 'rules' => 'trim|required|max_length[100]|valid_email'),
            array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|max_length[100]'),
            array('field' => 'position', 'label' => 'Posisi ', 'rules' => 'trim|required|max_length[100]'),
            array('field' => 'fullname', 'label' => 'Nama Lengka dan Gelar ', 'rules' => 'trim|required|max_length[100]'),
        );

        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');
        $this->self->form_validation->set_message('valid_email', 'Email yang Anda masukkan tidak valid');

        if ($this->self->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('routing/index/registration-guru', 'refresh');
        } else {

            // Create user account
            $user_account = [
                'name' => $this->self->input->post('fullname'),
                'username' => $this->self->input->post('username'),
                'password' => $this->self->encrypt->encode($this->self->input->post('password'), get_key()),
                'email' => $this->self->input->post('username'),
                'level' => 'teacher',
                'status' => 'deactive'
            ];

            // Check availabe username & password 
            if ($this->self->muser->check_available_user($user_account, 'add')) {

                $this->self->muser->insert_data($user_account);

                $data = array(
                    'user_id' => $this->self->db->insert_id(),
                    'position' => $this->self->input->post('position'),
                    'fullname' => $this->self->input->post('fullname')
                );

                // Uploading image
                if ($_FILES['userfile']['name'] != '')
                    $data['photo'] = $this->self->media->upload(set_route('registration-guru'));

                $this->self->mteacher->insert_data($data);

                set_alert('alert-info', "Karyawan / Guru dengan nama {$data['fullname']} telah berhasil di daftarkan, silahkan menghubungi administrator untuk aktivasi akun agar dapat digunakan untuk login");

                redirect('routing/index/login-guru', 'refresh');
            } else {

                set_alert('alert-danger', 'Maaf username sudah digunakan');

                redirect('routing/index/registration-guru', 'refresh');
            }
        }

    }

    public function update()
    {

        // Variable initialization
        $userId = $this->self->input->post('user_id');
        $studentId = $this->self->input->post('teacher_id');

        // Define validation form
        $config = array(
            array('field' => 'username', 'label' => 'Email ', 'rules' => 'trim|required|max_length[100]|valid_email'),
            array('field' => 'position', 'label' => 'Posisi ', 'rules' => 'trim|required'),
            array('field' => 'fullname', 'label' => 'Nama Lengkap dan Gelar ', 'rules' => 'trim|required|max_length[100]'),
            array('field' => 'old_nip', 'label' => 'NIP Lama ', 'rules' => 'trim|required|numeric|max_length[25]'),
            array('field' => 'new_nip', 'label' => 'NIP Baru ', 'rules' => 'trim|required|numeric|max_length[25]')
        );

        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');
        $this->self->form_validation->set_message('valid_email', 'Email yang Anda masukkan tidak valid');

        if ($this->self->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('routing/index/update-guru', 'refresh');
        } else {

            // Create user account
            $user_account = [
                'id_user' => $userId,
                'name' => $this->self->input->post('fullname'),
                'username' => $this->self->input->post('username'),
                'email' => $this->self->input->post('username')
            ];

            if ($this->self->input->post('password') != '') {

                $user_account['password'] = $this->self->encrypt->encode($this->self->input->post('password'), get_key());
            }

            // Check availabe username & password 
            if ($this->self->muser->check_available_user($user_account, 'edit', $userId)) {

                $this->self->muser->update_data($user_account);

                $data = array(
                    'id' => $studentId,
                    'position' => $this->self->input->post('position'),
                    'fullname' => $this->self->input->post('fullname'),
                    'old_nip' => $this->self->input->post('old_nip'),
                    'new_nip' => $this->self->input->post('new_nip'),
                    'birth_place' => $this->self->input->post('birth_place'),
                    'birth_day' => $this->self->input->post('birth_day') == '' ? NULL : $this->self->input->post('birth_day'),
                    'parent_institute' => $this->self->input->post('parent_institute'),
                    'organization_unit' => $this->self->input->post('organization_unit'),
                    'work_unit' => $this->self->input->post('work_unit'),
                    'cpns_grade' => $this->self->input->post('cpns_grade'),
                    'last_grade' => $this->self->input->post('last_grade'),
                    'tmt_eselon' => $this->self->input->post('tmt_eselon') == '' ? NULL : $this->self->input->post('tmt_eselon'),
                    'last_position' => $this->self->input->post('last_position'),
                    'working_group_period' => $this->self->input->post('working_group_period'),
                    'sk_number' => $this->self->input->post('sk_number'),
                    'total_hour' => $this->self->input->post('total_hour'),
                    'execution_place' => $this->self->input->post('execution_place'),
                    'education_when_cpns' => $this->self->input->post('education_when_cpns'),
                    'last_education' => $this->self->input->post('last_education'),
                    'education_field' => $this->self->input->post('education_field'),
                    'education_institute' => $this->self->input->post('education_institute'),
                    'gender' => $this->self->input->post('gender'),
                    'legal_position' => $this->self->input->post('legal_position'),
                    'tmt_staffing' => $this->self->input->post('tmt_staffing'),
                    'karpeg_number' => $this->self->input->post('karpeg_number'),
                    'nuptk' => $this->self->input->post('nuptk'),
                    'nrg' => $this->self->input->post('nrg'),
                    'nik' => $this->self->input->post('nik'),
                    'askes_number' => $this->self->input->post('askes_number'),
                    'taspen_serial_number' => $this->self->input->post('taspen_serial_number'),
                    'specifically_assignments' => $this->self->input->post('specifically_assignments'),
                    'principal_assignment' => $this->self->input->post('principal_assignment'),
                    'language_ability' => $this->self->input->post('language_ability'),
                    'address' => $this->self->input->post('address'),
                    'phone_number' => $this->self->input->post('phone_number'),
                    'body_weight' => $this->self->input->post('body_weight'),
                    'body_height' => $this->self->input->post('body_height'),
                    'body_blood_type' => $this->self->input->post('body_blood_type'),
                    'skin_color' => $this->self->input->post('skin_color'),
                    'implementation_year' => $this->self->input->post('implementation_year'),
                    'religion' => $this->self->input->post('religion'),
                    'npwp' => $this->self->input->post('npwp'),
                    'other_parent_institute' => $this->self->input->post('other_parent_institute') == '' ? NULL : $this->self->input->post('other_parent_institute'),
                    'other_organization_unit' => $this->self->input->post('other_organization_unit') == '' ? NULL : $this->self->input->post('other_organization_unit'),
                    'other_work_unit' => $this->self->input->post('other_work_unit') == '' ? NULL : $this->self->input->post('other_work_unit'),
                    'other_last_education' => $this->self->input->post('other_last_education') == '' ? NULL : $this->self->input->post('other_last_education'),
                    'other_religion' => $this->self->input->post('other_religion') == '' ? NULL : $this->self->input->post('other_religion'),
                    'other_legal_position' => $this->self->input->post('other_legal_position') == '' ? NULL : $this->self->input->post('other_legal_position'),
                    'other_principal_assignment' => $this->self->input->post('other_principal_assignment') == '' ? NULL : $this->self->input->post('other_principal_assignment'),
                    'other_language_ability' => $this->self->input->post('other_language_ability') == '' ? NULL : $this->self->input->post('other_language_ability'),
                    'other_body_blood_type' => $this->self->input->post('other_body_blood_type') == '' ? NULL : $this->self->input->post('other_body_blood_type'),
                    'other_skin_color' => $this->self->input->post('other_skin_color') == '' ? NULL : $this->self->input->post('other_skin_color'),
                    'note' => $this->self->input->post('note')
                );

                // Uploading image
                if ($_FILES['userfile']['name'] != '') {

                    // Deleting old file 
                    $this->self->media->delete($this->self->mteacher->get_old_filename($studentId));

                    // Upload new image and update photo file name on database
                    $data['photo'] = $this->self->media->upload(set_route('update-guru'));
                }

                // If image deleted
                if ($this->self->input->post('delete_image')) {

                    // Deleting old file
                    $this->self->media->delete($this->self->mteacher->get_old_filename($studentId));

                    // Upate photo file name on database
                    $data['photo'] = '';
                }

                $this->self->mteacher->update_data($data);

                set_alert('alert-success', "Data Anda telah berhasil diperbarui");

                redirect('routing/index/dashboard-guru', 'refresh');
            } else {

                set_alert('alert-danger', 'Maaf username sudah digunakan');

                redirect('routing/index/update-guru', 'refresh');
            }
        }

    }

    public function updateVisibility($id = null)
    {

        $input = $this->self->input->post();
        $user_id = $this->self->auth->get_id('teacher_session');

        $data['id'] = $id;
        $data['visibility'] = json_encode($input);

        $this->self->mteacher->update_data($data);
        
        set_alert('alert-success', "Privasi data berhasil diperbarui");

    }

    public function login()
    {

        // Variable initialization
        $username = $this->self->input->post('username');
        $password = $this->self->input->post('password');

        // Define validation 
        $config = array(
            array('field' => 'username', 'label' => 'Email ', 'rules' => 'trim|required|max_length[100]|valid_email'),
            array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|max_length[100]'),
        );

        // Configure setup validation
        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');
        $this->self->form_validation->set_message('valid_email', 'Email tidak valid');

        if ($this->self->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('routing/index/login-guru', 'refresh');
        } else {

            // Setup login parameters
            $params = [
                'username' => $username,
                'password' => $password
            ];

            // Login process
            if ($this->self->auth->login($params, 'teacher_session')) {

                // Success login
                set_alert('alert-success', 'Selamat Datang, Anda telah berhasil masuk ke halaman dashboard guru / karyawan');

                redirect('routing/index/dashboard-guru', 'refresh');
            } else {

                // Failed login
                set_alert('alert-danger', 'Username dan Password Anda tidak sesuai');

                redirect('routing/index/login-guru', 'refresh');
            }
        }

    }

}

?>
