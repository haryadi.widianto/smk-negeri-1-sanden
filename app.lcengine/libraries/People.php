<?php

class People
{

    private $self;

    public function __construct()
    {
        $this->self = & get_instance();
        $this->self->load->model('people/mstudent');
        $this->self->load->model('people/mteacher');
        $this->self->load->model('muser');
        $this->self->load->library('media');
        $this->self->load->library('auth');

    }

    public function set_student()
    {

        /* Validation */
        $config = array(
            array('field' => 'username', 'label' => 'Email ', 'rules' => 'trim|required|max_length[100]|valid_email'),
            array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|max_length[100]'),
            array('field' => 'name', 'label' => 'Nama ', 'rules' => 'trim|required|max_length[100]'),
            array('field' => 'mother_name', 'label' => 'Nama Ibu Kandung ', 'rules' => 'trim|required|max_length[100]'),
            array('field' => 'nik', 'label' => 'NIK ', 'rules' => 'trim|required|numeric|max_length[25]'),
            array('field' => 'nisn', 'label' => 'NISN ', 'rules' => 'trim|required|numeric|max_length[25]'),
            array('field' => 'birth_place', 'label' => 'Tempat Lahir ', 'rules' => 'required'),
            array('field' => 'birth_date', 'label' => 'Tanggal Lahir ', 'rules' => 'required'),
            array('field' => 'gender', 'label' => 'Jenis Kelamin ', 'rules' => 'required'),
            array('field' => 'address', 'label' => 'Alamat Rumah ', 'rules' => 'required'),
            array('field' => 'phone_number', 'label' => 'Nomor Telp / HP ', 'rules' => 'required'),
            array('field' => 'grade', 'label' => 'Tingkat ', 'rules' => 'required'),
            array('field' => 'group', 'label' => 'Jurusan ', 'rules' => 'required')
        );

        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');
        $this->self->form_validation->set_message('valid_email', 'Email yang Anda masukkan tidak valid');

        if ($this->self->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('routing/index/registration-siswa', 'refresh');
        } else {

            // Create user account
            $user_account = [
                'name' => $this->self->input->post('name'),
                'username' => $this->self->input->post('username'),
                'password' => $this->self->encrypt->encode($this->self->input->post('password'), get_key()),
                'email' => $this->self->input->post('username'),
                'level' => 'student',
                'status' => 'deactive'
            ];

            // Check availabe username & password 
            if ($this->self->muser->check_available_user($user_account, 'add')) {

                $this->self->muser->insert_data($user_account);

                $data = array(
                    'user_id' => $this->self->db->insert_id(),
                    'name' => $this->self->input->post('name'),
                    'mother_name' => $this->self->input->post('mother_name'),
                    'nik' => $this->self->input->post('nik'),
                    'nisn' => $this->self->input->post('nisn'),
                    'birth_place' => $this->self->input->post('birth_place'),
                    'birth_date' => $this->self->input->post('birth_date'),
                    'gender' => $this->self->input->post('gender'),
                    'address' => $this->self->input->post('address'),
                    'phone_number' => $this->self->input->post('phone_number'),
                    'grade' => $this->self->input->post('grade'),
                    'group' => $this->self->input->post('group'),
                    'year_force' => $this->self->input->post('year_force'),
                    'graduation_year' => (int) $this->self->input->post('graduation_year'),
                    'work' => $this->self->input->post('work'),
                    'alumni' => $this->self->input->post('alumni')
                );

                // Uploading image
                if ($_FILES['userfile']['name'] != '')
                    $data['photo'] = $this->self->media->upload(set_route('registration-siswa'));

                $this->self->mstudent->insert_data($data);

                set_alert('alert-info', "Siswa dengan nama {$user_account['name']} telah berhasil di daftarkan, silahkan menghubungi administrator untuk aktivasi akun agar dapat digunakan untuk login");

                redirect('routing/index/login-siswa', 'refresh');
            } else {

                set_alert('alert-danger', 'Maaf username sudah digunakan');

                redirect('routing/index/registration-siswa', 'refresh');
            }
        }

    }

    public function update_student()
    {

        // Variable initialization
        $userId = $this->self->input->post('user_id');
        $studentId = $this->self->input->post('student_id');

        // Define validation form
        $config = array(
            array('field' => 'username', 'label' => 'Email ', 'rules' => 'trim|required|max_length[100]|valid_email'),
            array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|max_length[100]'),
            array('field' => 'name', 'label' => 'Nama ', 'rules' => 'trim|required|max_length[100]'),
            array('field' => 'mother_name', 'label' => 'Nama Ibu Kandung ', 'rules' => 'trim|required|max_length[100]'),
            array('field' => 'nik', 'label' => 'NIK ', 'rules' => 'trim|required|numeric|max_length[25]'),
            array('field' => 'nisn', 'label' => 'NISN ', 'rules' => 'trim|requirednumeric|max_length[25]'),
            array('field' => 'birth_place', 'label' => 'Tempat Lahir ', 'rules' => 'required'),
            array('field' => 'birth_date', 'label' => 'Tanggal Lahir ', 'rules' => 'required'),
            array('field' => 'gender', 'label' => 'Jenis Kelamin ', 'rules' => 'required'),
            array('field' => 'address', 'label' => 'Alamat Rumah ', 'rules' => 'required'),
            array('field' => 'phone_number', 'label' => 'Nomor Telp / HP ', 'rules' => 'required'),
            array('field' => 'grade', 'label' => 'Tingkat ', 'rules' => 'required'),
            array('field' => 'group', 'label' => 'Jurusan ', 'rules' => 'required')
        );

        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');
        $this->self->form_validation->set_message('valid_email', 'Email yang Anda masukkan tidak valid');

        if ($this->self->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('routing/index/update-siswa', 'refresh');
        } else {

            // Create user account
            $user_account = [
                'id_user' => $userId,
                'name' => $this->self->input->post('name'),
                'username' => $this->self->input->post('username'),
                'email' => $this->self->input->post('username')
            ];

            if ($this->self->input->post('password') != '') {

                $user_account['password'] = $this->self->encrypt->encode($this->self->input->post('password'), get_key());
            }

            // Check availabe username & password 
            if ($this->self->muser->check_available_user($user_account, 'edit', $userId)) {

                $this->self->muser->update_data($user_account);

                $data = array(
                    'id' => $studentId,
                    'name' => $this->self->input->post('name'),
                    'mother_name' => $this->self->input->post('mother_name'),
                    'nik' => $this->self->input->post('nik'),
                    'nisn' => $this->self->input->post('nisn'),
                    'birth_place' => $this->self->input->post('birth_place'),
                    'birth_date' => $this->self->input->post('birth_date'),
                    'gender' => $this->self->input->post('gender'),
                    'address' => $this->self->input->post('address'),
                    'phone_number' => $this->self->input->post('phone_number'),
                    'grade' => $this->self->input->post('grade'),
                    'group' => $this->self->input->post('group'),
                    'year_force' => $this->self->input->post('year_force'),
                    'graduation_year' => $this->self->input->post('graduation_year'),
                    'work' => $this->self->input->post('work'),
                    'alumni' => $this->self->input->post('alumni')
                );

                // Uploading image
                if ($_FILES['userfile']['name'] != '') {

                    // Deleting old file 
                    $this->self->media->delete($this->self->mstudent->get_old_filename($studentId));

                    // Upload new image and update photo file name on database
                    $data['photo'] = $this->self->media->upload(set_route('update-siswa'));
                }

                // If image deleted
                if ($this->self->input->post('delete_image')) {

                    // Deleting old file
                    $this->self->media->delete($this->self->mstudent->get_old_filename($studentId));

                    // Upate photo file name on database
                    $data['photo'] = '';
                }

                $this->self->mstudent->update_data($data);

                set_alert('alert-success', "Data Anda telah berhasil diperbarui");

                redirect('routing/index/dashboard-siswa', 'refresh');
            } else {

                set_alert('alert-danger', 'Maaf username sudah digunakan');

                redirect('routing/index/update-siswa', 'refresh');
            }
        }

    }

    public function student_login()
    {

        // Variable initialization
        $username = $this->self->input->post('username');
        $password = $this->self->input->post('password');

        // Define validation 
        $config = array(
            array('field' => 'username', 'label' => 'Email ', 'rules' => 'trim|required|max_length[100]|valid_email'),
            array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|max_length[100]'),
        );

        // Configure setup validation
        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');
        $this->self->form_validation->set_message('valid_email', 'Email tidak valid');

        if ($this->self->form_validation->run() == FALSE) {

            set_alert('alert-danger', validation_errors());

            redirect('routing/index/login-siswa', 'refresh');
        } else {

            // Setup login parameters
            $params = [
                'username' => $username,
                'password' => $password
            ];

            // Login process
            if ($this->self->auth->login($params, 'student_session')) {

                // Success login
                set_alert('alert-success', 'Selamat Datang, Anda telah berhasil masuk ke halaman dashboard siswa');

                redirect('routing/index/dashboard-siswa', 'refresh');
            } else {

                // Failed login
                set_alert('alert-danger', 'Username dan Password Anda tidak sesuai');

                redirect('routing/index/login-siswa', 'refresh');
            }
        }

    }

}

?>
