<?php

require './vendor/autoload.php';

use Intervention\Image\ImageManagerStatic as Image;

class Media
{

    private $self;

    public function __construct()
    {

        $this->self = & get_instance();

    }

    public function upload($path = '')
    {

        $config['upload_path'] = './pub.lcengine/upload/media/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $config['encrypt_name'] = true;

        $this->self->load->library('upload', $config);

        if (!$this->self->upload->do_upload()) {

            set_alert('alert-danger', $this->self->upload->display_errors());

            redirect($path, 'refresh');
        } else {

            $data = $this->self->upload->data();

            $img = Image::make('./pub.lcengine/upload/media/' . $data['file_name']);

            // Create main thumb
            $img->resize(null, 300, function ($constraint) {
                $constraint->aspectRatio();
            })->save('./pub.lcengine/upload/media/thumb_' . $data['file_name']);

            // Create squeare thumb
            $img->crop(300, 300)->save('./pub.lcengine/upload/media/square_' . $data['file_name']);

            return $data['file_name'];
        }

    }

    public function delete($filename = '')
    {

        if ($filename != '') {
            
            // Remove main image file
            if (file_exists("./pub.lcengine/upload/media/" . $filename))
                unlink("./pub.lcengine/upload/media/" . $filename);

            // Remove thumb image file
            if (file_exists("./pub.lcengine/upload/media/thumb_" . $filename))
                unlink("./pub.lcengine/upload/media/thumb_" . $filename);

            // Remove square image file
            if (file_exists("./pub.lcengine/upload/media/square_" . $filename))
                unlink("./pub.lcengine/upload/media/square_" . $filename);
        }


    }

}

?>
