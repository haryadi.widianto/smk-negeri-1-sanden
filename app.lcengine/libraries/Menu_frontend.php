<?php

class Menu_frontend {

    static private $data;

    public function make($params = []) {
        self::$data = $params;
        return self::render(self::buildTree($params));
    }

    private function buildTree(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parents'] == $parentId) {
                $children = self::buildTree($elements, $element['id']);
                if ($children) {
                    $element['childs'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    static private function render($array, $index = 0) {
        if (!is_array($array))
            return '';

        if ($index == 0)
            $output = '<ul class="nav navbar-nav" >';
        if ($index == 1)
            $output = '<ul class="dropdown-menu" >';

        foreach ($array as $item) {
            if (!key_exists('childs', $item)) {
                if ($item['parents'] == 0)
                    $output .= '<li class="nav-item" ><a href="'.site_url('/routing/index/' . $item['data']) .'">' . $item['menu'].'</a>';
                if ($item['parents'] != 0)
                    $output .= '<li><a href="'.site_url('/routing/index/' . $item['data']) .'">' . $item['menu'].'</a>';
            }else {
                if(self::level($item['id'], self::$data) == 1) $output .= '<li class="nav-item dropdown"><a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">' . $item['menu'] . ' <i class="fa fa-angle-down"></i></a>';
                if(self::level($item['id'], self::$data) > 1) $output .= '<li class="dropdown-submenu"><a tabindex="-1" href="#">' . $item['menu'] . ' <i class="fa fa-angle-right"></i></a>';
                $output .= self::render($item['childs'], 1);
            }
            $output .= '</li>';
        }
        $output .= '</ul>';

        return $output;
    }

    static private function level($id_child = 0, $data = array(), $level = 1) {
        foreach ($data as $k => $i) {
            if ($i['id'] == $id_child) {
                if ($i['parents'] == 0) {
                    return $level;
                } else {
                    return self::level($i['parents'], $data, $level + 1);
                }
            }
        }
    }

}

?>