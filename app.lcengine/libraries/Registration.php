<?php

class Registration {

    private $self;

    public function __construct() {
        $this->self = & get_instance();
        $this->self->load->model('registration/student');
        $this->self->load->model('registration/parents');
        $this->self->load->model('registration/prestasi');
        $this->self->load->model('registration/raport');
        $this->self->load->model('registration/school_from');
    }

    public function set_student() {

        /* Validation */
        $config = array(
            array('field' => 'name', 'label' => 'Nama ', 'rules' => 'trim|required'),
            array('field' => 'nisn', 'label' => 'NISN ', 'rules' => 'required'),
            array('field' => 'home_address', 'label' => 'Alamat Asal', 'rules' => 'required'),
            array('field' => 'stay_address', 'label' => 'Alamat Tinggal', 'rules' => 'required'),
            array('field' => 'gender', 'label' => 'Jenis Kelamin', 'rules' => 'required'),
            array('field' => 'religion', 'label' => 'Agama', 'rules' => 'required'),
            array('field' => 'telp', 'label' => 'Telepon', 'rules' => 'required'),
            array('field' => 'birth_place', 'label' => 'Tempat Lahir', 'rules' => 'required'),
            array('field' => 'birth_date', 'label' => 'Tanggal Lahir', 'rules' => 'required'),
            array('field' => 'kota', 'label' => 'Kota', 'rules' => 'required'),
            array('field' => 'email', 'label' => 'Kota', 'rules' => 'valid_email'),
            array('field' => 'option_1', 'label' => 'Pilihan 1', 'rules' => 'required'),
            array('field' => 'option_2', 'label' => 'Pilihan 2', 'rules' => 'required'),
            array('field' => 'option_3', 'label' => 'Pilihan 3', 'rules' => 'required'),
        );

        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');
        $this->self->form_validation->set_message('valid_email', 'Email tidak valid');

        if ($this->self->form_validation->run() == FALSE) {
            set_alert('alert-danger', validation_errors());
            return false;
        } else {

            $data = array(
                'name' => $this->self->input->post('name'),
                'nisn' => $this->self->input->post('nisn'),
                'home_address' => $this->self->input->post('home_address'),
                'stay_address' => $this->self->input->post('stay_address'),
                'gender' => $this->self->input->post('gender'),
                'religion' => $this->self->input->post('religion'),
                'telp' => $this->self->input->post('telp'),
                'birth_place' => $this->self->input->post('birth_place'),
                'birth_date' => $this->self->input->post('birth_date'),
                'email' => $this->self->input->post('email'),
                'option_1' => $this->self->input->post('option_1'),
                'option_2' => $this->self->input->post('option_2'),
                'option_3' => $this->self->input->post('option_3'),
                'kota' => $this->self->input->post('kota'),
                'register_number' => rand(1000000, 9999999),
                'year_session' => date('Y'),
                'created_at' => date('Y-m-d H:i:s'),
                'steps' => 1
            );

            /* Image upload */
            if ($_FILES['userfile']['name'] != '')
                $data['image'] = $this->upload_file(set_route('data-siswa'));

            $this->self->student->insert_data($data);

            $this->self->session->set_userdata('id_student', mysql_insert_id());
            
            /* Insert Raport */
            $temp['id_student'] =   $this->self->session->userdata('id_student');
            $this->self->raport->insert_data($temp);

            redirect('routing/index/post-data-school-from', 'refresh');
        }
    }

    public function set_school() {

        /* Validation */
        $config = array(
            array('field' => 'school_name', 'label' => 'Nama sekolah ', 'rules' => 'trim|required'),
            array('field' => 'status', 'label' => 'Status sekolah ', 'rules' => 'required'),
            array('field' => 'school_address', 'label' => 'Alamat sekolah', 'rules' => 'required'),
        );

        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');

        if ($this->self->form_validation->run() == FALSE) {
            set_alert('alert-danger', validation_errors());
            return false;
        } else {

            $data = array(
                'school_name' => $this->self->input->post('school_name'),
                'status' => $this->self->input->post('status'),
                'school_address' => $this->self->input->post('school_address'),
                'id_student' => $this->self->input->post('id_student')
            );

            $this->self->school_from->insert_data($data);

            /* Update Step */
            $step['id'] = $this->self->session->userdata('id_student');
            $step['steps'] = 2;
            $this->self->student->update($step);

            redirect('routing/index/post-data-orang-tua', 'refresh');
        }
    }

    public function set_parent() {

        /* Validation */
        $config = array(
            array('field' => 'name', 'label' => 'Nama orang tua', 'rules' => 'trim|required'),
            array('field' => 'address', 'label' => 'Alamat orang tua', 'rules' => 'required'),
            array('field' => 'occupation', 'label' => 'Pekerjaan orang tua', 'rules' => 'required'),
        );

        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');

        if ($this->self->form_validation->run() == FALSE) {
            set_alert('alert-danger', validation_errors());
            return false;
        } else {

            $data = array(
                'name' => $this->self->input->post('name'),
                'address' => $this->self->input->post('address'),
                'occupation' => $this->self->input->post('occupation'),
                'alt_name' => $this->self->input->post('alt_name'),
                'alt_address' => $this->self->input->post('alt_address'),
                'alt_occupation' => $this->self->input->post('alt_occupation'),
                'id_student' => $this->self->input->post('id_student')
            );

            $this->self->parents->insert_data($data);

            /* Update Step */
            $step['id'] =  $this->self->session->userdata('id_student');
            $step['steps'] = 3;
            $this->self->student->update($step);

            redirect('routing/index/post-data-prestasi-bakat', 'refresh');
        }
    }

    public function set_prestasi() {
        /* Validation */
        $config = array(
            array('field' => 'bidang', 'label' => 'Bidang', 'rules' => 'required'),
            array('field' => 'prestasi', 'label' => 'Prestasi', 'rules' => 'required'),
        );

        $this->self->form_validation->set_rules($config);
        $this->self->form_validation->set_message('required', 'Kolom %s wajib diisi');

        if ($this->self->form_validation->run() == FALSE) {
            set_alert('alert-danger', validation_errors());
            return false;
        } else {

            $redata = array();
            foreach ($this->self->input->post('bidang') as $key => $item) {
                if ($this->self->input->post('prestasi')[$key] != '') {
                    $redata[$key]['id_student'] = $this->self->input->post('id_student');
                    $redata[$key]['bidang'] = $this->self->input->post('bidang')[$key];
                    $redata[$key]['prestasi'] = $this->self->input->post('prestasi')[$key];
                }
            }

            $this->self->prestasi->insert_data($redata);

            /* Update Step */
            $step['id'] =  $this->self->session->userdata('id_student');
            $step['steps'] = 4;
            $this->self->student->update($step);

            redirect('routing/index/data-success', 'refresh');
        }
    }

    public function upload_file($path = '') {

        $config['upload_path'] = './pub.lcengine/upload/ppdb/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000';
        $config['encrypt_name'] = true;

        $this->self->load->library('upload', $config);

        if (!$this->self->upload->do_upload()) {
            set_alert('alert-danger', $this->self->upload->display_errors());
            redirect($path, 'refresh');
        } else {
            $data = $this->self->upload->data();
            return $data['file_name'];
        }
    }

}

?>
