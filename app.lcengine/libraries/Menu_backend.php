<?php

class Menu_backend {

    static private $data;

    public function make($params = []) {
        self::$data = $params;
        return self::make_ulli(self::buildTree($params));
    }

    private function buildTree(array $elements, $parentId = 0) {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parents'] == $parentId) {
                $children = self::buildTree($elements, $element['id']);
                if ($children) {
                    $element['childs'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    static private function make_ulli($array, $strip = '') {
        if (!is_array($array))
            return '';

        $output = '';
        foreach ($array as $key => $item) {

            $output .= '<tr><td style="display: none;" >' . $item['id'] . '</td>';
            $output .= '<td>' . $strip . '|--- &nbsp' . $item['menu'] . '</td>';
            $output .= '<td>';
            $output .= '<input type="hidden" name="id_menu" value="' . $item['id'] . '" />';
            $output .= '<select class="form-control parent" style="width:150px;">';

            $selected = ($item['id'] == 0) ? "selected='selected'" : '';

            $output .= '<option value="0" ' . $selected . '>-- No Parent --</option>';
            if (!empty(self::$data)) {
                foreach (self::$data as $value) {
                    if (self::isChild($item['id'], $value['id'], self::$data) == 0) {
                        $selected = ($value['id'] == $item['parents']) ? "selected='selected'" : '';
                        $output .= '<option  value="' . $value['id'] . '" ' . $selected . ' >' . $value['menu'] . '</option>';
                    }
                }
            }
            $output .= '</select></td>';
            $output .= '<td>' . $item['type'] . '</td>';
            $output .= '<td>' . $item['template'] . '</td>';
            $output .= '<td align="center">';
            $output .= '<div class="btn-group">';
            $disbled = ($key + 1 == count($array)) ? 'disabled' : '';
            $output .= '<a href="' . base_url("index.php/backend/cmenu/down/" . $item['id'] . "/" . $item['position'] . "/" . $item['parents']) . '" class="btn btn-info btn-xs ' . $disbled . '" ><i class="fa fa-arrow-down"></i></a>';
            $disbled = ($key == 0) ? 'disabled' : '';
            $output .= '<a href="' . base_url("index.php/backend/cmenu/up/" . $item['id'] . "/" . $item['position'] . "/" . $item['parents']) . '" class="btn btn-info btn-xs ' . $disbled . '" ><i class="fa fa-arrow-up"></i></a>';
            $output .= '</div></td>';

            $output .= '<td align="center"><div class="btn-group">';
            $output .= '<a href = "' . base_url('index.php/backend/cmenu/index/' . $item['id']) . '" class = "btn btn-info  btn-xs" ><i class = "fa fa-pencil"></i></a>';
            $output .= '<a href = "' . base_url("index.php/backend/cmenu/destroy/" . $item['id'] . "/false") . '" class = "btn btn-danger  btn-xs"><i class = "fa fa-times"></i></a >';
            $output .= '</div></td>';

            $output .= '</tr>';

            if (key_exists('childs', $item)) {
                $output .= self::make_ulli($item['childs'], $strip . '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp');
            }
        }

        return $output;
    }

    static private function isChild($id_parent = 0, $id_child = 0, $data = array()) {
        foreach ($data as $k => $i) {
            if ($i['id'] == $id_child && $i['parents'] != 0) {
                return self::isChild($id_parent, $i['parents'], $data);
            } else if ($i['parents'] == 0) {
                if ($id_child == $id_parent) {
                    return 1;
                }
            }
        }
        return 0;
    }

}

?>