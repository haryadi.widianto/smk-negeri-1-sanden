<?php

class Auth
{

    private $self;

    public function __construct()
    {

        $this->self = & get_instance();
        $this->self->load->model('mauth');

    }

    public function login($data = array(/* [username] => '', [password] => '' */), $session = 'login')
    {

        $result = $this->self->mauth->get_auth_var($data['username']);

        if ($result && ($data['password'] == $this->self->encrypt->decode($result['password'], get_key()))) {
            $this->self->session->set_userdata($session, $result);
            return true;
        } else {
            return false;
        }

    }

    public function logout($url_to = '', $session = 'login')
    {

        $this->self->session->unset_userdata($session);

        set_alert('alert-success', "Anda telah berhasil logout");

        redirect($url_to, 'refresh');

    }

    public function routing_auth($url_to = '', $level = 'admin', $session = 'login')
    {

        if (empty($this->self->session->userdata($session)) || $this->self->session->userdata($session)['level'] != $level) {
            redirect($url_to, 'refresh');
        }

    }

    public function is_login($url_to = '', $level = 'admin', $session)
    {

        if (!empty($this->self->session->userdata($session)) && $this->self->session->userdata($session)['level'] == $level) {
            redirect($url_to, 'refresh');
        }

    }

    public function get_id($session = '')
    {
        return $this->self->session->userdata($session)['id_user'];

    }

}

?>
